FROM node:alpine

RUN mkdir -p /pdpa

WORKDIR /pdpa

COPY package*.json ./

RUN npm install

COPY . /pdpa/

EXPOSE 8080
CMD ["node","app"]
