# วิธีการ Setup Project

- 1. npm install เพื่อทำการติดตั้ง node_modules package
- 2. สร้าง file ชื่อว่า .env
- 3. ทำการ copy ข้อมูลดังนี้

## Example

````shell
PORT= port ที่ต้องการให้ server run
DB_NAME= database name
DB_HOST= database ip host
DB_PASSWORD= database password
DB_USER= database username
FOLDER_FILESUPLOAD= ชื่อ folder ที่ทำการ upload ข้อมูลลงไป
LISTEN_HOST= ip host of agent listen that installed
LISTEN_PORT= port of agent listen that installed
COOKIE_DOMAIN= ชื่อโดเมน/url สำหรับ link cookie script
COOKIE_API= ชื่อโดเมน/url สำหรับส่งข้อมูลจาก cookie popup มาเก็บ
SHARE_DOMAIN= ชื่อโดเมน/url สำหรับ link ในเนื้อหา email
EMAIL_API= dev กรณีที่อยู่ใน dev  ถ้าอยู่ใน ที่ดิน  http://172.16.42.126/SMTPMailPIPR/api/SMTPMailPIPR/SendMail
EMAIL_NAME= (ALLTRA) ใช้กับตัว dev เเละ (DOL PIPR PDPA) ใช้กับของ ที่ดิน
NAME= (ALLTRA) ใช้กับตัว dev เเละ (DOL PIPR PDPA) ใช้กับของ ที่ดิน
MAIL_NAME= (ALLTRA) ใช้กับตัว dev เเละ (DOL PIPR PDPA) ใช้กับของ ที่ดิน
NAME_HEADER= (All Inclusive Security) ใช้กับตัว dev เเละ (ระบบจัดการข้อมูลส่วนบุคคล (DOL PIPR PDPA)) ใช้กับของ ที่ดิน
PICTURE_LOGO= (/UI/assets/images/favicon.png) ใช้กับตัว dev เเละ (/UI/assets/images/favicon_bak.png) ใช้กับของ ที่ดิน
FOOTER= (ALLTRA) ใช้กับตัว dev เเละ (DOL PIPR PDPA) ใช้กับของ ที่ดิน Version 1.0, All right reserved by Sense InfoTech Co., Ltd.
LOGO= (/UI/assets/images/logo.png) ใช้กับตัว dev เเละ (/UI/assets/images/favicon_bak.png) ใช้กับของ ที่ดิน
SUBFOLDER= กำหนด sub-folder เช่น pdpa/ ถ้าไม่มีปล่อยว่างไว้
````

### เช่น 

````shell
PORT=8080
DB_NAME=DOL_PDPA_UPDATE
DB_HOST=119.81.197.130
DB_PASSWORD=P@ssw0rd
DB_USER=users
FOLDER_FILESUPLOAD=files_upload_dev
LISTEN_HOST=119.81.197.130
LISTEN_PORT=5050
COOKIE_DOMAIN=http://119.81.197.130:8080/
COOKIE_API=http://119.81.197.130:8080/api/send_cookieTypes
SHARE_DOMAIN=http://119.81.197.130:8080/pdpa/
EMAIL_API=dev
EMAIL_NAME=ALLTRA
NAME=ALLTRA
MAIL_NAME=ALLTRA
NAME_HEADER=All Inclusive Security
PICTURE_LOGO=/UI/assets/images/favicon.png
FOOTER=ALLTRA Version 1.1, All right reserved by Sense InfoTech Co., Ltd.
LOGO=/UI/assets/images/logo.png
SUBFOLDER=pdpa/
````
