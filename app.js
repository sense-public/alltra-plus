const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const path = require('path');
const fileUpload = require('express-fileupload');

const app = express();
const cors = require('cors');
require('dotenv').config();

app.use(cors());
app.use(cookie());

app.use('/assets', express.static('assets'));
app.use('/dist', express.static('dist'));
const folderFileUpload = process.env.FOLDER_FILESUPLOAD;
app.use(`/${folderFileUpload}`, express.static(`${folderFileUpload}`));

app.use(express.static(path.join(__dirname, '/public')));
app.use(fileUpload());
// setting
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.use(body.json({ limit: '50mb' }));
app.use(body.urlencoded({ limit: '50mb', extended: true }));
// app.set('views' , 'views');
app.set('views', path.join(__dirname, '/views'));
app.use(express.static(`${__dirname}/public`));

// middlewares
app.use(session({
  secret: 'Passw0rd',
  resave: true,
  saveUninitialized: true,
  cookie: { maxAge: 60000000 },
}));

config = {
  host: `${process.env.DB_HOST}`,
  user: `${process.env.DB_USER}`,
  password: `${process.env.DB_PASSWORD}`,
  port: 3306,
  database: `${process.env.DB_NAME}`,
  timezone: 'utc',
  multipleStatements: true,
  connectionLimit: 20,
  waitForConnections: true,
  queueLimit: 0,
  connectionTimeout: 5000,
  idleTimeoutMillis: 30000 
}

app.use(connection(mysql, config, 'single'));

app.get('/', function (req, res) {
  if (process.env.SUBFOLDER == '') {
    if (!req.url.endsWith(`/${process.env.SUBFOLDER}`)) {
      res.redirect(`/${process.env.SUBFOLDER}`);
    }else{
      req.getConnection((err, conn) => {
        conn.query('SELECT * FROM TB_TR_ACCOUNT', (err, data) => {
          //res.json(data);
          if (data != undefined || data != '') {
            config = {
              NAME: `${process.env.NAME}`,
              HEADER: `${process.env.NAME_HEADER}`,
              LOGO: `${process.env.PICTURE_LOGO}`,
              LOGOLOG: `${process.env.LOGO}`,
              FOOTER: `${process.env.FOOTER}`,
            }
            
            res.render(`./login`, { session: req.session,config});
          } else {
            res.render(`./account/wizard`, { session: req.session });
          }
        });
      });
    }
  }else{
    res.redirect(`/${process.env.SUBFOLDER}`);
  }
  
});

app.get(`/${process.env.SUBFOLDER}`, function (req, res) {
  if (!req.url.endsWith(`/${process.env.SUBFOLDER}`)) {
    res.redirect(`/${process.env.SUBFOLDER}`);
  }else{
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM TB_TR_ACCOUNT', (err, data) => {
        //res.json(data);
        if (data != undefined || data != '') {
          config = {
            NAME: `${process.env.NAME}`,
            HEADER: `${process.env.NAME_HEADER}`,
            LOGO: `${process.env.PICTURE_LOGO}`,
            LOGOLOG: `${process.env.LOGO}`,
            FOOTER: `${process.env.FOOTER}`,
          }
          
          res.render(`./login`, { session: req.session,config});
        } else {
          res.render(`./account/wizard`, { session: req.session });
        }
      });
    });
  }
});
app.get(`/${process.env.SUBFOLDER}logout`, (req, res) => {
  const id = req.session.userid;
  // console.log(`session: ${id}`);
  req.getConnection((err, conn) => {
    conn.query('UPDATE TB_TR_ACCOUNT set session_id = null where acc_id = ?', [id], (err, accountupdate) => {
    });
    req.session.destroy((error) => {
      if (error) console.log(error);
      res.redirect(`/${process.env.SUBFOLDER}`);
    });
  });
});

const logRoutes = require('./routes/log_routes');
app.use(`/${process.env.SUBFOLDER}`, logRoutes);

const groupRoutes = require('./routes/group_routes');
app.use(`/${process.env.SUBFOLDER}`, groupRoutes);

const deviceRoutes = require('./routes/device_routes');
app.use(`/${process.env.SUBFOLDER}`, deviceRoutes);

const settingRoutes = require('./routes/setting_routes');
app.use(`/${process.env.SUBFOLDER}`, settingRoutes);

const accountRoutes = require('./routes/account_routes');
app.use(`/${process.env.SUBFOLDER}`, accountRoutes);

const blockRoutes = require('./routes/block_routes');
app.use(`/${process.env.SUBFOLDER}`, blockRoutes);

const viewsRoutes = require('./routes/views_routes');
app.use(`/${process.env.SUBFOLDER}`, viewsRoutes);

const importdataRoutes = require('./routes/importdata_routes');
app.use(`/${process.env.SUBFOLDER}`, importdataRoutes);

const getdataRoutes = require('./routes/getdata_routes');
app.use(`/${process.env.SUBFOLDER}`, getdataRoutes);

const exportdataRoutes = require('./routes/exportdata_routes');
app.use(`/${process.env.SUBFOLDER}`, exportdataRoutes);

const boardSystem = require('./routes/board_system');
app.use(`/${process.env.SUBFOLDER}`, boardSystem);

const routeConsentManagement = require('./routes/route_consent_management/route_consent_management');
app.use(`/${process.env.SUBFOLDER}`, routeConsentManagement);

const routeCookieManagemet = require('./routes/route_cookie_managemet/route_cookie_managemet');
app.use(`/${process.env.SUBFOLDER}`, routeCookieManagemet);

const routeCookietype = require('./routes/route_cookietype/route_cookietype');
app.use(`/${process.env.SUBFOLDER}`, routeCookietype);

const routeDomain = require('./routes/route_domain/route_domain');
app.use(`/${process.env.SUBFOLDER}`, routeDomain);

const routeReportAppeal = require('./routes/route_report/route_report_appeal');
app.use(`/${process.env.SUBFOLDER}`, routeReportAppeal);

const routeReportPolicy = require('./routes/route_report/route_report_policy');
app.use(`/${process.env.SUBFOLDER}`, routeReportPolicy);

const routeReportPattern = require('./routes/route_report/route_report_pattern');
app.use(`/${process.env.SUBFOLDER}`, routeReportPattern);

const routeReportClassification = require('./routes/route_report/route_report_classification');
app.use(`/${process.env.SUBFOLDER}`, routeReportClassification);

const routeReportCookieconsent = require('./routes/route_report/route_report_cookieconsent');
app.use(`/${process.env.SUBFOLDER}`, routeReportCookieconsent);

const routeReportEmailconsent = require('./routes/route_report/route_report_emailconsent');
app.use(`/${process.env.SUBFOLDER}`, routeReportEmailconsent);

const routeReportPaperconsent = require('./routes/route_report/route_report_paperconsent');
app.use(`/${process.env.SUBFOLDER}`, routeReportPaperconsent);

const routeEmailconsent = require('./routes/route_report/route_report_emailconsent');
app.use(`/${process.env.SUBFOLDER}`, routeEmailconsent);

const routeCookieconsent = require('./routes/route_cookieconsent/route_cookieconsent');
app.use(`/${process.env.SUBFOLDER}`, routeCookieconsent);

const routeEmail = require('./routes/route_email/route_email');
app.use(`/${process.env.SUBFOLDER}`, routeEmail);

const routeAppeal = require('./routes/route_appeal/route_appeal');
app.use(`/${process.env.SUBFOLDER}`, routeAppeal);

const indexRoute = require('./routes/indexRoute');
app.use(`/${process.env.SUBFOLDER}`, indexRoute);

// Agent
const agentRoute = require('./routes/agentsRoute');
app.use(`/${process.env.SUBFOLDER}`, agentRoute);

// Pattern
const patternRoute = require('./routes/patternRoute');

app.use(`/${process.env.SUBFOLDER}`, patternRoute);
// Classification
const classificationRoute = require('./routes/classificationRoute');
app.use(`/${process.env.SUBFOLDER}`, classificationRoute);

// dataOut
const dataoutRoute = require('./routes/dataoutRoute');
app.use(`/${process.env.SUBFOLDER}`, dataoutRoute);

// apiToken
const apicheckverifyRoute = require('./routes/apicheckverifyRoute');
app.use(`/${process.env.SUBFOLDER}`, apicheckverifyRoute);

const rout_security_section = require('./routes/route_security_section/rout_security_section');
app.use(`/${process.env.SUBFOLDER}`, rout_security_section);

const routeActivity = require('./routes/route_policy/route_activity_ropa');
app.use(`/${process.env.SUBFOLDER}`, routeActivity);

const routeemailboard = require('./routes/route_email/route_email_board');
app.use(`/${process.env.SUBFOLDER}`, routeemailboard);

const routeOwnerPersonal = require('./routes/route_email/route_email_owner_personal');
app.use(`/${process.env.SUBFOLDER}`, routeOwnerPersonal);





const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server Listen PORT ${PORT}`));

//test git by tiew
//test git by dew