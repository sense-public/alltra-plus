/* eslint-disable max-len */
/* eslint-disable eqeqeq */
const controller = {};
const { validationResult } = require('express-validator');
const path = require('path');
const uuidv4 = require('uuid').v4;
const md5 = require('md5');
const sha1 = require('sha1');
const sha256 = require('sha256');
const addDate = require('../utils/addDate');
const session = require('express-session');

controller.funchistory = (req, typehistory, msghistory, accID) => {
  const date = addDate();
  const hashmd5 = md5(accID + date + msghistory);
  const hashsha1 = sha1(accID + date + msghistory);
  const hashsha256 = sha256(accID + date + msghistory);
  req.getConnection((_, conn) => {
    conn.query('INSERT INTO `TB_TR_HISTORY` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`,`md5`,`sha1`,`sha256`) VALUES (?, ?, ?, NULL, ?,?,?,?);', [accID, date, msghistory, typehistory, hashmd5, hashsha1, hashsha256], (err) => {
      if (err) {
        // console.log(`Insert history login${err}`);
        console.log(err);
      }
    });
  });
};

controller.profile = (req, res) => {
  const id = req.session.userid;
  if (typeof req.session.userid === 'undefined') {
    res.redirect(`/${process.env.SUBFOLDER}`);
  } else {
    req.getConnection((_, conn) => {
      conn.query(
        'SELECT a.firstname,a.lastname,a.name,a.image,a.acc_id,a.position,a.descrip,a.contact,a.ext,a.phone,a.email,a.line,a.username,a.password,a.admin,date_format(a.bd,"%Y-%m-%d") as bd,m.otp_sms,m.otp_email,m.otp_2fa,m.otp_login,m.otp_system FROM TB_TR_ACCOUNT as a LEFT JOIN TB_TR_MUITIFACTOR as m ON m.acc_id=a.acc_id WHERE a.acc_id = ?',
        [id],
        (error1, account) => {
          if (error1) console.log(error1);
          conn.query(
            'SELECT a.firstname,a.lastname,a.name,a.image,a.acc_id,a.position,a.descrip,a.contact,a.ext,a.phone,a.email,a.line,a.username,a.password,a.admin,date_format(a.bd,"%Y-%m-%d") as bd,m.otp_sms,m.otp_email,m.otp_2fa,m.otp_login,m.otp_system,au.id as auid,au.genkey,au.confirm FROM TB_TR_ACCOUNT as a LEFT JOIN TB_TR_MUITIFACTOR as m ON m.acc_id=a.acc_id left join TB_TR_APIVERIFY_USER as au on au.acc_id=a.acc_id WHERE a.acc_id NOT IN (SELECT acc_id FROM `TB_TR_DEL_ACC`) and a.admin NOT IN (1) and a.acc_id=?;',
            [id],
            (error2, api) => {
              if (error2) console.log(error2);
              conn.query(
                'SELECT * FROM `TB_TR_HISTORY` WHERE acc_id = ?',
                [id],
                (err, history) => {
                  res.render(`./account/profile`, {
                    data: account,
                    data2: history,
                    api,
                    session: req.session,
                  });
                },
              );
            },
          );
        },
      );
    });
  }
};
// controller.login = (req, res) => {
//   req.getConnection((_, conn) => {
//     conn.query('SELECT * FROM TB_TR_ACCOUNT ', (err, data) => {
//       if (err) console.log(err);
//       if (data.length > 0) {
//         res.render('login', { session: req.session });
//       } else {
//         res.render(`./account/wizard`, { session: req.session });
//       }
//     });
//   });
// };

controller.admin = (req, res) => {
  const id = req.session.userid;
  const logperhour = [];
  if (typeof req.session.userid === 'undefined') { res.redirect(`/${process.env.SUBFOLDER}`); } else {

    const currentDate = new Date();
    const sData = currentDate.getFullYear() + '-' + ((currentDate.getMonth() + 1).toString().padStart(2, '0')); //วันเดือนปีปัจจุบัน
    const sData2 = currentDate.toLocaleString('en-US', { month: 'long' }) + ' ' + currentDate.getFullYear(); //วันเดือนปีปัจจุบัน
    //const sData = '2023-08';

    const monthsArray = [];

    for (let i = 0; i < 12; i++) {
      const monthData = currentDate.toLocaleString('en-US', { month: 'long' }) + ' ' + currentDate.getFullYear();
      
      monthsArray.push(monthData);
      currentDate.setMonth(currentDate.getMonth() - 1);
    }

    req.getConnection((err, conn) => {
      // conn.query('SELECT date_format(TB_TR_HISTORY.datetime,"%b %d %T")as date,TB_TR_HISTORY.type,TB_TR_HISTORY.msg,TB_TR_ACCOUNT.name,TB_TR_ACCOUNT.image FROM `TB_TR_HISTORY` JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id=TB_TR_HISTORY.acc_id  ORDER BY TB_TR_HISTORY.datetime DESC limit 20', (err7, history) => {
      // conn.query('SELECT date_format(TB_TR_HISTORY.datetime,"%b %d %T")as date,TB_TR_HISTORY.msg,TB_TR_ACCOUNT.name,TB_TR_HISTORY.type,TB_TR_ACCOUNT.image FROM `TB_TR_HISTORY` JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id=TB_TR_HISTORY.acc_id WHERE TB_TR_HISTORY.acc_id = ? ORDER BY TB_TR_HISTORY.datetime DESC limit 20', [id], (err8, history2) => {
      conn.query('SELECT a.firstname,a.lastname,a.name,a.image,a.acc_id,a.position,a.descrip,a.contact,a.ext,a.phone,a.email,a.line,a.username,a.password,a.admin,date_format(a.bd,"%Y-%m-%d") as bd,m.otp_sms,m.otp_email,m.otp_2fa,m.otp_login,m.otp_system FROM TB_TR_ACCOUNT as a LEFT JOIN TB_TR_MUITIFACTOR as m ON m.acc_id=a.acc_id WHERE a.acc_id= ?', [id], (err1, account) => {
        conn.query('SELECT COUNT(*) as num,admin FROM TB_TR_ACCOUNT WHERE TB_TR_ACCOUNT.acc_id NOT IN (SELECT acc_id FROM TB_TR_DEL_ACC) GROUP BY TB_TR_ACCOUNT.admin ORDER BY admin ASC;', (err2, account2) => {

            conn.query('SELECT DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL 1 DAY),"%Y-%m-%d") as date', (err5, date2) => {
              conn.query('SELECT date_format(pw_keep,"%Y-%m-%d %T")as date,ntp,num_admin,num_user,timezone,day_keep FROM `TB_MM_SET_SYSTEM` ORDER BY `TB_MM_SET_SYSTEM`.`sys_id` DESC LIMIT 1', (err9, setSystem) => {
                conn.query('SELECT * FROM `TB_TR_DEVICE` JOIN TB_TR_LOG  ON TB_TR_LOG.device_id=TB_TR_DEVICE.device_id GROUP BY TB_TR_LOG.device_id', (err12, device) => {
                  conn.query('SELECT date_format(date,"%Y-%m-%d %T")as date FROM `TB_TR_PW_CHANGE` WHERE acc_id = ? ORDER BY `TB_TR_PW_CHANGE`.`change_id`  DESC', [id], (err11, pwChange) => {
                    conn.query('SELECT date_format(CURDATE(),"%Y-%m-%d") as mouth,CURTIME() as time', (err10, date) => {
                      conn.query('SELECT MAX(date_format(TB_TR_LOG.date,"%Y-%m-%d")) as date,status,TB_TR_DEVICE.name as name,TB_TR_DEVICE.image,TB_TR_DEVICE.de_ip as ip,TB_TR_LOG.device_id as device_id,count(*) as number  FROM `TB_TR_USER_MEMBER` as um JOIN `TB_TR_GROUP` as g ON g.group_id=um.group_id JOIN TB_TR_DEVICE_MEMBER as dm ON dm.group_id=g.group_id JOIN TB_TR_ACCOUNT as a ON a.acc_id=um.acc_id JOIN TB_TR_LOG  ON TB_TR_LOG.device_id=dm.de_id JOIN TB_TR_DEVICE ON TB_TR_DEVICE.device_id=dm.de_id WHERE a.acc_id=? and date_format(TB_TR_LOG.date,"%M %Y") = ? GROUP BY TB_TR_DEVICE.device_id order by date DESC', [id, sData2], (err3, deviceList) => { // data device
                        var datavalue = [];
                        var date_values = [];
                        if (deviceList != '') {
                          var sp_date = sData.split("-");
                          var last = new Date(sp_date[0], sp_date[1], 0);
                          for (var i = 1; i <= last.getDate(); i++) {
                            var number_day = sp_date[0] + '-' + sp_date[1] + '-' + i;
                            date_values.push(number_day)
                          }
                          for (let i = 0; i < deviceList.length; i++) {
                            var data_values = [];
                            for (let j = 0; j < date_values.length; j++) {
                              check_c = 0;
                              if (deviceList[i].date == date_values[j]) {
                                data_values.push(deviceList[i].number);
                                check_c = 1;
                              }
                              if (check_c == 0) {
                                data_values.push(0);
                              }
                            }
                            datavalue.push(data_values)
                          }
                        }else{
                          var sp_date = sData.split("-");
                          console.log('sp_date:',sp_date);
                          var last = new Date(sp_date[0], sp_date[1], 0);
                          console.log('last:',last);
                          var data_values = [];
                          for (var i = 1; i <= last.getDate(); i++) {
                            var number_day = sp_date[0] + '-' + sp_date[1] + '-' + i;
                            date_values.push(number_day)
                            data_values.push(0);
                          }
                          datavalue.push(data_values)
                          for (let i = 0; i < deviceList.length; i++) {
                            var data_values = [];
                            for (let j = 0; j < date_values.length; j++) {
                              check_c = 0;
                              if (deviceList[i].date == date_values[j]) {
                                data_values.push(deviceList[i].number);
                                check_c = 1;
                              }
                              if (check_c == 0) {
                                data_values.push(0);
                              }
                            }
                            datavalue.push(data_values)
                          }
                        }

                        if (device) {
                          for (let i = 0; i < device.length; i++) {
                            conn.query('SELECT hour(TB_TR_LOG.date) AS hour,COUNT(*) as num,TB_TR_DEVICE.name FROM `TB_TR_LOG` JOIN TB_TR_DEVICE on TB_TR_DEVICE.device_id=TB_TR_LOG.device_id WHERE TB_TR_LOG.device_id= ? AND TB_TR_LOG.date BETWEEN ' + '2022-01-01' + ' AND ? GROUP BY hour(TB_TR_LOG.date) ORDER BY hour(TB_TR_LOG.date)', [device[i].device_id, date2[0].date], (err13, logdate) => {
                              const lognum = [];
                              const hour = [];
                              if (logdate) {
                                if (logdate.length > 0) {
                                  for (let j = 0; j < logdate.length; j++) {
                                    lognum.push(logdate[j].num);
                                    hour.push(`${logdate[j].hour}:00`);
                                  }
                                  logperhour.push({ name: logdate[0].name, data: lognum, hours: hour });
                                }
                              }
                            });
                          }
                        }
                        if (account[0].name != 'undefined') {
                          conn.query('SELECT * FROM `TB_TR_THEME` WHERE acc_id = ?', [id], (err13, theme) => {
                            if (err13) console.error(err13);
                              const day1 = [`${date[0].mouth} ${date[0].time}`];
                              req.session.acc_name = account[0].name;
                              req.session.acc_email = account[0].email;
                              req.session.acc_id = account[0].acc_id;
                              req.session.admin = account[0].admin;
                              req.session.image = account[0].image;
                              req.session.otp_system = account[0].otp_system;
                              req.session.theme = theme;
                              if (setSystem) {
                                if (setSystem.length > 0) {
                                  req.session.ntp = setSystem[0].ntp;
                                  req.session.num_admin = setSystem[0].num_admin;
                                  req.session.num_user = setSystem[0].num_user;
                                  req.session.timezone = setSystem[0].timezone;
                                  req.session.day_keep = setSystem[0].day_keep;
                                  req.session.set_system = setSystem;
                                }
                              }
                              if (setSystem) {
                                if (setSystem.set_network > 0) {
                                  req.session.ip = setSystem[0].ip;
                                  req.session.netmark = setSystem[0].netmark;
                                  req.session.gateway = setSystem[0].gateway;
                                  req.session.dns = setSystem[0].dns;
                                  req.session.set_network = setSystem;
                                }
                              }
                              req.session.logperhour = logperhour;

                              if (account2.length > 1) {
                                req.session.acc_admin = account2[1].num;
                                req.session.acc_user = account2[0].num;
                              } else if (account2.length == 1) {
                                req.session.acc_admin = account2[0].num;
                                req.session.acc_user = '0';
                              }
                              if (pwChange.length > 0) {
                                if (day1[0] > pwChange[0].date) {
                                  res.render(`./setting/change`, {
                                    data: account,
                                    session: req.session,
                                  });
                                } else {
                                  res.render(`./index2`, {
                                    data: account,
                                    data2: deviceList,
                                    datavalue, date_values,
                                    data4: monthsArray,
                                    data5: undefined,
                                    data6: monthsArray,
                                    session: req.session,
                                  });
                                }
                              } else if (pwChange.length == 0) {
                                res.render(`./setting/change`, {
                                  data: account,
                                  session: req.session,
                                });
                              } else {
                                res.render(`./index2`, {
                                  data: account,
                                  data2: deviceList,
                                  datavalue, date_values,
                                  data4: monthsArray,
                                  data5: undefined,
                                  data6: monthsArray,
                                  session: req.session,
                                });
                              }
                            });
                        } else {
                          res.redirect(`/${process.env.SUBFOLDER}`);
                        }
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
  }
};
controller.indexcheck = (req, res) => {
  const id = req.session.userid;
  var url = req.params;
  const logperhour = [];
  if (typeof req.session.userid === 'undefined') { res.redirect(`/${process.env.SUBFOLDER}`); } else {

    const currentDate = new Date(url.date);
    const sData = currentDate.getFullYear() + '-' + ((currentDate.getMonth() + 1).toString().padStart(2, '0')); //วันเดือนปีปัจจุบัน
    const sData2 = currentDate.toLocaleString('en-US', { month: 'long' }) + ' ' + currentDate.getFullYear(); //วันเดือนปีปัจจุบัน
    //const sData = '2023-08';

    const monthsArray = [];
    const currentDate2 = new Date();
    for (let i = 0; i < 12; i++) {
      const monthData = currentDate2.toLocaleString('en-US', { month: 'long' }) + ' ' + currentDate2.getFullYear();
      
      monthsArray.push(monthData);
      currentDate2.setMonth(currentDate2.getMonth() - 1);
    }

    req.getConnection((err, conn) => {
      // conn.query('SELECT date_format(TB_TR_HISTORY.datetime,"%b %d %T")as date,TB_TR_HISTORY.type,TB_TR_HISTORY.msg,TB_TR_ACCOUNT.name,TB_TR_ACCOUNT.image FROM `TB_TR_HISTORY` JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id=TB_TR_HISTORY.acc_id  ORDER BY TB_TR_HISTORY.datetime DESC limit 20', (err7, history) => {
      // conn.query('SELECT date_format(TB_TR_HISTORY.datetime,"%b %d %T")as date,TB_TR_HISTORY.msg,TB_TR_ACCOUNT.name,TB_TR_HISTORY.type,TB_TR_ACCOUNT.image FROM `TB_TR_HISTORY` JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id=TB_TR_HISTORY.acc_id WHERE TB_TR_HISTORY.acc_id = ? ORDER BY TB_TR_HISTORY.datetime DESC limit 20', [id], (err8, history2) => {
      conn.query('SELECT a.firstname,a.lastname,a.name,a.image,a.acc_id,a.position,a.descrip,a.contact,a.ext,a.phone,a.email,a.line,a.username,a.password,a.admin,date_format(a.bd,"%Y-%m-%d") as bd,m.otp_sms,m.otp_email,m.otp_2fa,m.otp_login,m.otp_system FROM TB_TR_ACCOUNT as a LEFT JOIN TB_TR_MUITIFACTOR as m ON m.acc_id=a.acc_id WHERE a.acc_id= ?', [id], (err1, account) => {
        conn.query('SELECT COUNT(*) as num,admin FROM TB_TR_ACCOUNT WHERE TB_TR_ACCOUNT.acc_id NOT IN (SELECT acc_id FROM TB_TR_DEL_ACC) GROUP BY TB_TR_ACCOUNT.admin ORDER BY admin ASC;', (err2, account2) => {

            conn.query('SELECT DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL 1 DAY),"%Y-%m-%d") as date', (err5, date2) => {
              conn.query('SELECT date_format(pw_keep,"%Y-%m-%d %T")as date,ntp,num_admin,num_user,timezone,day_keep FROM `TB_MM_SET_SYSTEM` ORDER BY `TB_MM_SET_SYSTEM`.`sys_id` DESC LIMIT 1', (err9, setSystem) => {
                conn.query('SELECT * FROM `TB_TR_DEVICE` JOIN TB_TR_LOG  ON TB_TR_LOG.device_id=TB_TR_DEVICE.device_id GROUP BY TB_TR_LOG.device_id', (err12, device) => {
                  conn.query('SELECT date_format(date,"%Y-%m-%d %T")as date FROM `TB_TR_PW_CHANGE` WHERE acc_id = ? ORDER BY `TB_TR_PW_CHANGE`.`change_id`  DESC', [id], (err11, pwChange) => {
                    conn.query('SELECT date_format(CURDATE(),"%Y-%m-%d") as mouth,CURTIME() as time', (err10, date) => {
                      conn.query('SELECT MAX(date_format(TB_TR_LOG.date,"%Y-%m-%d")) as date,status,TB_TR_DEVICE.name as name,TB_TR_DEVICE.image,TB_TR_DEVICE.de_ip as ip,TB_TR_LOG.device_id as device_id,count(*) as number  FROM `TB_TR_USER_MEMBER` as um JOIN `TB_TR_GROUP` as g ON g.group_id=um.group_id JOIN TB_TR_DEVICE_MEMBER as dm ON dm.group_id=g.group_id JOIN TB_TR_ACCOUNT as a ON a.acc_id=um.acc_id JOIN TB_TR_LOG  ON TB_TR_LOG.device_id=dm.de_id JOIN TB_TR_DEVICE ON TB_TR_DEVICE.device_id=dm.de_id WHERE a.acc_id=? and date_format(TB_TR_LOG.date,"%M %Y") = ? GROUP BY TB_TR_DEVICE.device_id order by date DESC', [id, sData2], (err3, deviceList) => { // data device
                        var datavalue = [];
                        var date_values = [];
                        if (deviceList != '') {
                          var sp_date = sData.split("-");
                          console.log('sp_date:',sp_date);
                          var last = new Date(sp_date[0], sp_date[1], 0);
                          console.log('last:',last);
                          for (var i = 1; i <= last.getDate(); i++) {
                            var number_day = sp_date[0] + '-' + sp_date[1] + '-' + i;
                            date_values.push(number_day)
                          }
                          for (let i = 0; i < deviceList.length; i++) {
                            var data_values = [];
                            for (let j = 0; j < date_values.length; j++) {
                              check_c = 0;
                              if (deviceList[i].date == date_values[j]) {
                                data_values.push(deviceList[i].number);
                                check_c = 1;
                              }
                              if (check_c == 0) {
                                data_values.push(0);
                              }
                            }
                            datavalue.push(data_values)
                          }
                        }else{
                          var sp_date = sData.split("-");
                          console.log('sp_date:',sp_date);
                          var last = new Date(sp_date[0], sp_date[1], 0);
                          console.log('last:',last);
                          var data_values = [];
                          for (var i = 1; i <= last.getDate(); i++) {
                            var number_day = sp_date[0] + '-' + sp_date[1] + '-' + i;
                            date_values.push(number_day)
                            data_values.push(0);
                          }
                          datavalue.push(data_values)
                          for (let i = 0; i < deviceList.length; i++) {
                            var data_values = [];
                            for (let j = 0; j < date_values.length; j++) {
                              check_c = 0;
                              if (deviceList[i].date == date_values[j]) {
                                data_values.push(deviceList[i].number);
                                check_c = 1;
                              }
                              if (check_c == 0) {
                                data_values.push(0);
                              }
                            }
                            datavalue.push(data_values)
                          }
                        }

                        if (device) {
                          for (let i = 0; i < device.length; i++) {
                            conn.query('SELECT hour(TB_TR_LOG.date) AS hour,COUNT(*) as num,TB_TR_DEVICE.name FROM `TB_TR_LOG` JOIN TB_TR_DEVICE on TB_TR_DEVICE.device_id=TB_TR_LOG.device_id WHERE TB_TR_LOG.device_id= ? AND TB_TR_LOG.date BETWEEN ' + '2022-01-01' + ' AND ? GROUP BY hour(TB_TR_LOG.date) ORDER BY hour(TB_TR_LOG.date)', [device[i].device_id, date2[0].date], (err13, logdate) => {
                              const lognum = [];
                              const hour = [];
                              if (logdate) {
                                if (logdate.length > 0) {
                                  for (let j = 0; j < logdate.length; j++) {
                                    lognum.push(logdate[j].num);
                                    hour.push(`${logdate[j].hour}:00`);
                                  }
                                  logperhour.push({ name: logdate[0].name, data: lognum, hours: hour });
                                }
                              }
                            });
                          }
                        }
                        if (account[0].name != 'undefined') {
                          conn.query('SELECT * FROM `TB_TR_THEME` WHERE acc_id = ?', [id], (err13, theme) => {
                            if (err13) console.error(err13);
                              const day1 = [`${date[0].mouth} ${date[0].time}`];
                              req.session.acc_name = account[0].name;
                              req.session.acc_email = account[0].email;
                              req.session.acc_id = account[0].acc_id;
                              req.session.admin = account[0].admin;
                              req.session.image = account[0].image;
                              req.session.otp_system = account[0].otp_system;
                              req.session.theme = theme;
                              if (setSystem) {
                                if (setSystem.length > 0) {
                                  req.session.ntp = setSystem[0].ntp;
                                  req.session.num_admin = setSystem[0].num_admin;
                                  req.session.num_user = setSystem[0].num_user;
                                  req.session.timezone = setSystem[0].timezone;
                                  req.session.day_keep = setSystem[0].day_keep;
                                  req.session.set_system = setSystem;
                                }
                              }
                              if (setSystem) {
                                if (setSystem.set_network > 0) {
                                  req.session.ip = setSystem[0].ip;
                                  req.session.netmark = setSystem[0].netmark;
                                  req.session.gateway = setSystem[0].gateway;
                                  req.session.dns = setSystem[0].dns;
                                  req.session.set_network = setSystem;
                                }
                              }
                              req.session.logperhour = logperhour;

                              if (account2.length > 1) {
                                req.session.acc_admin = account2[1].num;
                                req.session.acc_user = account2[0].num;
                              } else if (account2.length == 1) {
                                req.session.acc_admin = account2[0].num;
                                req.session.acc_user = '0';
                              }
                              if (pwChange.length > 0) {
                                if (day1[0] > pwChange[0].date) {
                                  res.render(`./setting/change`, {
                                    data: account,
                                    session: req.session,
                                  });
                                } else {
                                  res.render(`./index2`, {
                                    data: account,
                                    data2: deviceList,
                                    datavalue, date_values,
                                    data4: undefined,
                                    data5: url.date,
                                    data6: monthsArray,
                                    session: req.session,
                                  });
                                }
                              } else if (pwChange.length == 0) {
                                res.render(`./setting/change`, {
                                  data: account,
                                  session: req.session,
                                });
                              } else {
                                res.render(`./index2`, {
                                  data: account,
                                  data2: deviceList,
                                  datavalue, date_values,
                                  data4: undefined,
                                  data5: url.date,
                                  data6: monthsArray,
                                  session: req.session,
                                });
                              }
                            });
                        } else {
                          res.redirect(`/${process.env.SUBFOLDER}`);
                        }
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
  }
  
};
controller.check = (req, res) => {
  const data = req.body;
  const getcookie = req.cookies;
  let cookie = JSON.stringify(getcookie);
  cookie = cookie.replace('"connect.sid"', '"connect"');
  const cookieParse = JSON.parse(cookie);
  const date = addDate();
  const dateNow = new Date(date);
  var date_now = new Date(dateNow.setHours(dateNow.getHours() + 7));
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM TB_TR_ACCOUNT LEFT JOIN `TB_TR_MUITIFACTOR` ON TB_TR_ACCOUNT.acc_id=TB_TR_MUITIFACTOR.acc_id WHERE username = ? AND password = ? AND  TB_TR_ACCOUNT.acc_id NOT IN (SELECT acc_id FROM `TB_TR_DEL_ACC`)', [data.username, data.password], (err, account) => {
      conn.query('SELECT * FROM `TB_MM_TIMEZONE`', (err, data3) => {
        conn.query('SELECT * FROM TB_TR_HISTORY ', (err, data2) => {
          if (account.length > 0 && account[0].acc_id != undefined) { // check Data Account
            conn.query('SELECT *,DATE_FORMAT(datetime, "%Y-%m-%d %H:%i:%s ") as time FROM TB_TR_HISTORY WHERE acc_id = ? ORDER BY datetime DESC limit 1;', [account[0].acc_id], (err, history) => {
              if (history.length == 0) {   // check Data history user
                conn.query('UPDATE TB_TR_ACCOUNT set session_id = ? where acc_id = ?', [cookieParse.connect, account[0].acc_id], (err, accountupdate) => {
                });
                msg = `${account[0].name} เข้าใช้งานเมื่อ ${date}`;
                type = 'login';
                hashmd5 = md5(account[0].acc_id + date + msg);
                hashsha1 = sha1(account[0].acc_id + date + msg);
                hashsha256 = sha256(account[0].acc_id + date + msg);
                conn.query('INSERT INTO `TB_TR_HISTORY` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`,`md5`,`sha1`,`sha256`) VALUES (?, ?, ?, NULL, ?,?,?,?);', [account[0].acc_id, date, msg, type, hashmd5, hashsha1, hashsha256], (err, history) => {
                  if (err) {
                    console.log(err);
                  }

                  if (data2.length > 0) {
                    req.session.userid = account[0].acc_id;
                    req.session.admin = account[0].admin;

                    req.session.success = true;
                    res.redirect(`/${process.env.SUBFOLDER}index2`);
                  } else if (data2.length == 0 && account[0].admin == 1) {  // No have history user and position user is admin
                    req.session.userid = account[0].acc_id;
                    req.session.admin = account[0].admin;
                    req.session.success = true;
                    res.render(`./setting/setting`, {
                      data2: data3,
                      session: req.session,
                    });
                  }
                });
              } else {
                const time = new Date(history[0].time);
                var time_history = new Date(time.setHours(time.getHours() + 7));
                const ms = date_now - time_history;
                const seconds = ms / 1000;
                const minutes = parseInt(seconds / 60, 10);

                if (account[0].session_id == null || account[0].session_id == 'undefined') {   // chek session_id user == ''
                  conn.query('UPDATE TB_TR_ACCOUNT set session_id = ? where acc_id = ?', [cookieParse.connect, account[0].acc_id], (err, accountupdate) => {
                  });
                  msg = `${account[0].name} เข้าใช้งานเมื่อ ${date}`;
                  type = 'login';
                  hashmd5 = md5(account[0].acc_id + date + msg);
                  hashsha1 = sha1(account[0].acc_id + date + msg);
                  hashsha256 = sha256(account[0].acc_id + date + msg);
                  conn.query('INSERT INTO `TB_TR_HISTORY` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`,`md5`,`sha1`,`sha256`) VALUES (?, ?, ?, NULL, ?,?,?,?);', [account[0].acc_id, date, msg, type, hashmd5, hashsha1, hashsha256], (err, history) => {
                    if (err) {
                      console.log(err);
                    }

                    if (data2.length > 0) {
                      req.session.userid = account[0].acc_id;
                      req.session.admin = account[0].admin;

                      req.session.success = true;
                      res.redirect(`/${process.env.SUBFOLDER}index2`);
                    } else if (data2.length == 0 && account[0].admin == 1) {  // No have history user and position user is admin
                      req.session.userid = account[0].acc_id;
                      req.session.admin = account[0].admin;
                      req.session.success = true;
                      res.render(`./setting/setting`, {
                        data2: data3,
                        session: req.session,
                      });
                    }
                  });
                } else {
                  if (minutes < 30) {
                    req.session.loginfalse = true;
                    res.redirect(`/${process.env.SUBFOLDER}`);
                  } else {
                    conn.query('UPDATE TB_TR_ACCOUNT set session_id = ? where acc_id = ?', [cookieParse.connect, account[0].acc_id], (err, accountupdate) => {
                    });
                    msg = `${account[0].name} เข้าใช้งานเมื่อ ${date}`;
                    type = 'login';
                    hashmd5 = md5(account[0].acc_id + date + msg);
                    hashsha1 = sha1(account[0].acc_id + date + msg);
                    hashsha256 = sha256(account[0].acc_id + date + msg);
                    conn.query('INSERT INTO `TB_TR_HISTORY` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`,`md5`,`sha1`,`sha256`) VALUES (?, ?, ?, NULL, ?,?,?,?);', [account[0].acc_id, date, msg, type, hashmd5, hashsha1, hashsha256], (err, history) => {
                      if (err) {
                        console.log(err);
                      }

                      if (data2.length > 0) {
                        req.session.userid = account[0].acc_id;
                        req.session.admin = account[0].admin;

                        req.session.success = true;
                        res.redirect(`/${process.env.SUBFOLDER}index2`);
                      } else if (data2.length == 0 && account[0].admin == 1) {  // No have history user and position user is admin
                        req.session.userid = account[0].acc_id;
                        req.session.admin = account[0].admin;
                        req.session.success = true;
                        res.render(`./setting/setting`, {
                          data2: data3,
                          session: req.session,
                        });
                      }
                    });
                  }
                }
              }
            });
          } else {
            req.session.false = true;
            res.redirect(`/${process.env.SUBFOLDER}`);
          }
        });
      });
    });
  });
};
controller.list = (req, res) => {
  const data = null;
  if (typeof req.session.userid === 'undefined' || req.session.admin == '0') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    req.getConnection((err, conn) => {
      conn.query(' SELECT TB_TR_ACCOUNT.*,date_format(TB_TR_ACCOUNT.bd,"%Y-%m-%d") as bd,m.otp_sms,m.otp_email,m.otp_2fa,m.otp_login,m.otp_system FROM TB_TR_ACCOUNT LEFT JOIN TB_TR_MUITIFACTOR as m ON m.acc_id=TB_TR_ACCOUNT.acc_id WHERE TB_TR_ACCOUNT.acc_id NOT IN (SELECT acc_id FROM TB_TR_DEL_ACC);', (err, account_list) => {
        conn.query('SELECT date_format(TB_TR_HISTORY.datetime,"%Y-%m-%d %T")as date,TB_TR_HISTORY.msg,TB_TR_ACCOUNT.name FROM `TB_TR_HISTORY` JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id=TB_TR_HISTORY.acc_id ORDER BY TB_TR_HISTORY.datetime DESC', (err, history) => {
          controller.funchistory(req, 'account', 'เข้าสู่เมนู รายชื่อผู้ใช้', req.session.userid);
          res.render(`./account/account_list`, {
            data: account_list,
            history,
            session: req.session,
          });
        });
      });
    });
  }
};
controller.quest = (req, res) => {
  const data = null;
  if (typeof req.session.userid === 'undefined' || req.session.admin == '0') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM `TB_MM_QUESTIONNAIRE` ORDER BY `TB_MM_QUESTIONNAIRE`.`quest_id` DESC', (err, account_list) => {
        res.render(`./account/questionnaire`, {
          data: account_list,
          session: req.session,
        });
      });
    });
  }
};
controller.questview = (req, res) => {
  const data = null;
  if (typeof req.session.userid === 'undefined' || req.session.admin == '0') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM `TB_MM_QUESTIONNAIRE` ORDER BY `TB_MM_QUESTIONNAIRE`.`quest_id` DESC', (err, account_list) => {
        res.render(`./account/questionnaire_view`, {
          data: account_list,
          session: req.session,
        });
      });
    });
  }
};
controller.ans = (req, res) => {
  const data = req.body;
  //  res.json(data);
  if (data.md_checkbox_21 == 'on') {
    data.md_checkbox_21 = 1;
  } else {
    data.md_checkbox_21 = 0;
  }
  if (data.md_checkbox_22 == 'on') {
    data.md_checkbox_22 = 1;
  } else {
    data.md_checkbox_22 = 0;
  }
  if (data.md_checkbox_23 == 'on') {
    data.md_checkbox_23 = 1;
  } else {
    data.md_checkbox_23 = 0;
  }
  if (data.md_checkbox_24 == 'on') {
    data.md_checkbox_24 = 1;
  } else {
    data.md_checkbox_24 = 0;
  }

  if (typeof req.session.userid === 'undefined' || req.session.admin == '0') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    req.getConnection((err, conn) => {
      conn.query('INSERT INTO TB_MM_QUESTIONNAIRE set ?', [data], (err, questionnaire) => {
        conn.query('SELECT * FROM `TB_MM_QUESTIONNAIRE` ORDER BY `TB_MM_QUESTIONNAIRE`.`quest_id` DESC', (err, account_list) => {
          res.render(`./account/questionnaire`, {
            data: account_list,
            session: req.session,
          });
        });
      });
    });
  }
};
controller.new = (req, res) => {
  const data = null;
  if (typeof req.session.userid === 'undefined' || req.session.admin == '0') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM TB_TR_ACCOUNT WHERE admin = 1', (err, account_list1) => {
        conn.query('SELECT * FROM TB_TR_ACCOUNT WHERE admin = 0', (err, account_list2) => {
          conn.query('SELECT * FROM TB_TR_ACCOUNT WHERE admin = 2', (err, account_list3) => {
            conn.query('SELECT * FROM `TB_MM_SET_SYSTEM`  ORDER BY `TB_MM_SET_SYSTEM`.`sys_id` DESC LIMIT 1', (err, set_system) => {
              conn.query("SELECT * FROM  TB_TR_PDPA_DOCUMENT WHERE  doc_status=2 AND  type !=3 AND doc_action =0", (err, doc) => {
              res.render(`./account/account_new`, {
                data1: account_list1,
                doc:doc,
                data2: account_list2,
                data3: account_list3,
                data4: set_system,
                session: req.session,
              });
            });
          });
          });
        });
      });
    });
  }
};
controller.adduser = (req, res) => {
  const errors = validationResult(req);
  const data = req.body;

  doc_data = '';
  if (Array.isArray(data.doc_id)) {
      for (let c = 0; c < data.doc_id.length; c++) {
          if (c == 0) {
            doc_data = doc_data + data.doc_id[c];
          }else{
            doc_data = doc_data + ',' + data.doc_id[c];
          }
      }
  }else{
    doc_data = data.doc_id;
  }

  console.log(doc_data);
  date = addDate();
  if (data.otp_email == 'on') {
    data.otp_email = 1;
  } else {
    data.otp_email = 0;
  }
  if (data.otp_sms == 'on') {
    data.otp_sms = 1;
  } else {
    data.otp_sms = 0;
  }
  if (data.otp_2fa == 'on') {
    data.otp_2fa = 1;
  } else {
    data.otp_2fa = 0;
  }
  if (data.otp_login == 'on') {
    data.otp_login = 1;
  } else {
    data.otp_login = 0;
  }
  if (data.otp_system == 'on') {
    data.otp_system = 1;
  } else {
    data.otp_system = 0;
  }
  userid = req.session.userid;
  if (data.admin) {
    if (data.admin == '1') {
      data.type_user = 'ผู้ดูแลระบบ';
    } else if (data.admin == '0') {
      data.type_user = 'ผู้ดูแลข้อมูล';
    } else if (data.admin == '2') {
      data.type_user = 'ผู้ใช้งานทั่วไป (User Access)';
    } else if (data.admin == '3') {
      data.type_user = 'เจ้าหน้าที่คุ้มครองข้อมูลส่วนบุคคล (DPO)';
    } else if (data.admin == '4') {
      data.type_user = 'ผู้ประมวลผลข้อมูลส่วนบุคคล (Data Processor)';
    } else if (data.admin == '5') {
      data.type_user = 'ผู้ควบคุมข้อมูลส่วนบุคคล (Data Controller)';
    }
  }

  if (typeof req.session.userid === 'undefined' || req.session.admin == '0') { res.redirect(`/${process.env.SUBFOLDER}`); } else if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect(`/${process.env.SUBFOLDER}account/new`);
  } else {
    req.session.success = true;
    req.session.topic = 'เพิ่มข้อมูลสำเร็จ';
    req.getConnection((err, conn) => {
      if (req.files) {
        const filename = req.files.img;
        if (!filename.map) {
          var newfilename = `${uuidv4()}.${filename.name.split('.')[1]}`;
          const savePath = path.join(__dirname, '../public/UI/image', newfilename);
          filename.mv(savePath);
        }
        data.image = newfilename;
      }
      conn.query('INSERT INTO TB_TR_ACCOUNT (firstname,lastname,name,position,descrip,contact,ext,phone,email,line,username,password,admin ,bd,image,type_user,doc_acount_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [data.firstname, data.lastname, data.name, data.position, data.descrip, data.contact, data.ext, data.phone, data.email, data.line, data.username, data.password, data.admin, data.bd, data.image, data.type_user, doc_data], (err, admin_add) => {
        conn.query('SELECT * FROM `TB_TR_ACCOUNT` WHERE username= ? AND password=? AND  TB_TR_ACCOUNT.acc_id NOT IN (SELECT acc_id FROM `TB_TR_DEL_ACC`)', [data.username, data.password], (err, account) => {
          conn.query('INSERT INTO `TB_TR_THEME` SET acc_id= ?', [account[0].acc_id], (err, theme) => {
            conn.query('INSERT INTO `TB_TR_MUITIFACTOR` (`acc_id`, `otp_sms`, `otp_email`, `otp_2fa`, `otp_login`, `otp_system`) VALUES (?, ?, ?, ?, ?, ?);', [account[0].acc_id, data.otp_sms, data.otp_email, data.otp_2fa, data.otp_login, data.otp_system], (err, muitifactor) => {
              if (err) {
                res.json(err);
              }
              controller.funchistory(req, 'account', `เพิ่มข้อมูล ผู้ใช้ ${data.name}`, req.session.userid);
              res.redirect(`/${process.env.SUBFOLDER}account/list`);
            });
          });
        });
      });
    });
  }
};
controller.add = (req, res) => {
  const data = req.body;
  const errors = validationResult(req);
  if (data.otp_email == 'on') {
    data.otp_email = 1;
  } else {
    data.otp_email = 0;
  }
  if (data.otp_sms == 'on') {
    data.otp_sms = 1;
  } else {
    data.otp_sms = 0;
  }
  if (data.otp_2fa == 'on') {
    data.otp_2fa = 1;
  } else {
    data.otp_2fa = 0;
  }
  if (data.otp_login == 'on') {
    data.otp_login = 1;
  } else {
    data.otp_login = 0;
  }
  if (data.otp_system == 'on') {
    data.otp_system = 1;
  } else {
    data.otp_system = 0;
  }
  if (data.md_checkbox_21 == 'on') {
    data.md_checkbox_21 = 1;
  } else {
    data.md_checkbox_21 = 0;
  }
  if (data.md_checkbox_22 == 'on') {
    data.md_checkbox_22 = 1;
  } else {
    data.md_checkbox_22 = 0;
  }
  if (data.md_checkbox_23 == 'on') {
    data.md_checkbox_23 = 1;
  } else {
    data.md_checkbox_23 = 0;
  }
  if (data.md_checkbox_24 == 'on') {
    data.md_checkbox_24 = 1;
  } else {
    data.md_checkbox_24 = 0;
  }
  if (typeof req.session.userid === 'undefined' || req.session.admin == '0') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    req.getConnection((err, conn) => {
      conn.query('INSERT INTO TB_TR_ACCOUNT (firstname,lastname,name,position,descrip,contact,ext,phone,email,line,username,password,admin ,bd,image) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,"user_pic.png")', [data.firstname, data.lastname, data.name, data.position, data.descrip, data.contact, data.ext, data.phone, data.email, data.line, data.username, data.password, 1, data.bd], (err, admin_add) => {
        conn.query('SELECT * FROM `TB_TR_ACCOUNT` WHERE username= ? AND password=? AND  TB_TR_ACCOUNT.acc_id NOT IN (SELECT acc_id FROM `TB_TR_DEL_ACC`)', [data.username, data.password], (err, account_list) => {
          conn.query('INSERT INTO `TB_TR_MUITIFACTOR` (`acc_id`, `otp_sms`, `otp_email`, `otp_2fa`, `otp_login`, `otp_system`) VALUES (?, ?, ?, ?, ?, ?);', [account_list[0].acc_id, data.otp_sms, data.otp_email, data.otp_2fa, data.otp_login, data.otp_system], (err, muitifactor) => {
            conn.query('INSERT INTO `TB_MM_QUESTIONNAIRE` (`quest_id`, `Name1`, `firstName1`, `lastName1`, `emailAddress1`, `phone1`, `shortDescription1`, `service`, `md_checkbox_21`, `md_checkbox_22`, `md_checkbox_23`, `md_checkbox_24`, `store`, `shortDescription2`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);', [data.Name1, data.firstName1, data.lastName1, data.emailAddress1, data.phone1, data.shortDescription1, data.service, data.md_checkbox_21, data.md_checkbox_22, data.md_checkbox_23, data.md_checkbox_24, data.store, data.shortDescription2], (err, questionnaire) => {
              res.redirect(`/${process.env.SUBFOLDER}`);
            });
          });
        });
      });
    });
  }
};
controller.delete = (req, res) => {
  const { id } = req.params;
  // res.json(id);
  if (typeof req.session.userid === 'undefined' || req.session.admin == '0') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    req.getConnection((err, conn) => {
      conn.query('SELECT TB_TR_ACCOUNT.*,date_format(bd,"%Y-%m-%d") as bd,TB_TR_MUITIFACTOR.otp_sms,TB_TR_MUITIFACTOR.otp_email,TB_TR_MUITIFACTOR.otp_2fa,TB_TR_MUITIFACTOR.otp_login,TB_TR_MUITIFACTOR.otp_system FROM TB_TR_ACCOUNT LEFT JOIN TB_TR_MUITIFACTOR ON TB_TR_MUITIFACTOR.acc_id=TB_TR_ACCOUNT.acc_id WHERE TB_TR_ACCOUNT.acc_id=?;', [id], (err, admin_delete) => {
        if (err) {
          res.json(err);
        }
        res.render(`./account/del_account`, {
          data: admin_delete,
          session: req.session,
        });
      });
    });
  }
};
controller.del = (req, res) => {
  const { id } = req.params;
  id2 = req.session.userid;
  date = addDate();
  const errors = validationResult(req);
  if (typeof req.session.userid === 'undefined' || req.session.admin == '0') { res.redirect(`/${process.env.SUBFOLDER}`); } else if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect(`/account/list${id}`);
  } else {
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM `TB_TR_ACCOUNT` WHERE acc_id = ?', [id], (err, account) => {
        conn.query('INSERT INTO `TB_TR_DEL_ACC` (`del_id`, `acc_id`) VALUES (NULL, ?);', [id], (err, admin_confirmdelete) => {
          controller.funchistory(req, 'account', `ลบข้อมูล ผู้ใช้ ${account[0].name}`, req.session.userid);
          res.redirect(`/${process.env.SUBFOLDER}account/list`);
        });
      });
    });
  }
};
controller.edit = (req, res) => {
  const { id } = req.params;
  if (typeof req.session.userid == 'undefined') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM TB_TR_ACCOUNT WHERE admin = 1', (err, account_list1) => {
        conn.query('SELECT * FROM TB_TR_ACCOUNT WHERE admin = 0', (err, account_list2) => {
          conn.query('SELECT * FROM TB_TR_ACCOUNT WHERE admin = 2', (err, account_list3) => {
            conn.query('SELECT * FROM `TB_MM_SET_SYSTEM`  ORDER BY `TB_MM_SET_SYSTEM`.`sys_id` DESC LIMIT 1', (err, set_system) => {
              conn.query('SELECT a.firstname,a.lastname,a.name,a.image,a.acc_id,a.position,a.descrip,a.contact,a.ext,a.phone,a.email,a.line,a.username,a.password,a.admin,date_format(a.bd,"%Y-%m-%d") as bd,m.otp_sms,m.otp_email,m.otp_2fa,m.otp_login,m.otp_system,doc_acount_id FROM TB_TR_ACCOUNT as a LEFT JOIN TB_TR_MUITIFACTOR as m ON m.acc_id=a.acc_id WHERE a.acc_id=?', [id], (err, admin_edit) => {
                conn.query("SELECT * FROM  TB_TR_PDPA_DOCUMENT WHERE  doc_status=2 AND  type !=3 AND doc_action =0", (err, doc) => {


                  arr_doc = []
                  if (admin_edit[0].doc_acount_id) {
                    arr_doc.push(...admin_edit[0].doc_acount_id.split(','))
                }

                res.render(`./account/edit_account`, {
                  arr_doc:arr_doc,
                  data: admin_edit,
                  doc:doc,
                  data1: account_list1,
                  data2: account_list2,
                  data3: account_list3,
                  data4: set_system,
                  session: req.session,
                });
              });
              });
            });
          });
        });
      });
    });
  }
};
controller.save = (req, res) => {
  const { id } = req.params;
  const data = req.body;
  doc_data = '';
  if (Array.isArray(data.doc_id)) {
      for (let c = 0; c < data.doc_id.length; c++) {
          if (c == 0) {
            doc_data = doc_data + data.doc_id[c];
          }else{
            doc_data = doc_data + ',' + data.doc_id[c];
          }
      }
  }else{
    doc_data = data.doc_id;
  }
  id2 = req.session.userid;
  date = addDate();
  const errors = validationResult(req);
  if (data.otp_email == 'on') {
    data.otp_email = 1;
  } else {
    data.otp_email = 0;
  }
  if (data.otp_sms == 'on') {
    data.otp_sms = 1;
  } else {
    data.otp_sms = 0;
  }
  if (data.otp_2fa == 'on') {
    data.otp_2fa = 1;
  } else {
    data.otp_2fa = 0;
  }
  if (data.otp_login == 'on') {
    data.otp_login = 1;
  } else {
    data.otp_login = 0;
  }
  if (data.otp_system == 'on') {
    data.otp_system = 1;
  } else {
    data.otp_system = 0;
  }
  if (data.admin) {
    if (data.admin == '1') {
      data.type_user = 'ผู้ดูแลระบบ';
    } else if (data.admin == '0') {
      data.type_user = 'ผู้ดูแลข้อมูล';
    } else if (data.admin == '2') {
      data.type_user = 'ผู้ใช้งานทั่วไป (User Access)';
    } else if (data.admin == '3') {
      data.type_user = 'เจ้าหน้าที่คุ้มครองข้อมูลส่วนบุคคล (DPO)';
    } else if (data.admin == '4') {
      data.type_user = 'ผู้ประมวลผลข้อมูลส่วนบุคคล (Data Processor)';
    } else if (data.admin == '5') {
      data.type_user = 'ผู้ควบคุมข้อมูลส่วนบุคคล (Data Controller)';
    }
  }
  if (typeof req.session.userid === 'undefined') { res.redirect(`/${process.env.SUBFOLDER}`); } else if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect(`/account/edit/${id}`);
  } else {
    req.session.success = true;
    req.session.topic = 'แก้ไขข้อมูลสำเร็จ';
    req.getConnection((err, conn) => {
      if(req.session.admin == 1){
        if (req.files) {
          const filename = req.files.img;
          if (!filename.map) {
            var newfilename = `${uuidv4()}.${filename.name.split('.')[1]}`;
            const savePath = path.join(__dirname, '../public/UI/image', newfilename);
            filename.mv(savePath);
          }
          data.image = newfilename;
        }
        conn.query('SELECT * FROM `TB_TR_ACCOUNT` WHERE acc_id = ?', [id], (err, account) => {
          conn.query('UPDATE TB_TR_ACCOUNT set firstname =?,lastname =?,name =?,position =?,descrip =?,contact =?,ext =?,phone =?,email =?,line =?,username =?,password =?,admin =? ,bd =?,image=?,type_user =?,doc_acount_id =? where acc_id = ?', [data.firstname, data.lastname, data.name, data.position, data.descrip, data.contact, data.ext, data.phone, data.email, data.line, data.username, data.password, data.admin, data.bd, data.image, data.type_user,doc_data, id], (err, admin_save) => {
            controller.funchistory(req, 'account', `แก้ไขข้อมูล ผู้ใช้ ${account[0].name}`, req.session.userid);
            if (err) {
              res.json(err);
            }
            conn.query('UPDATE `TB_TR_MUITIFACTOR` set `otp_sms`=?, `otp_email`=?, `otp_2fa`=?, `otp_login`=?, `otp_system`=? where acc_id = ?', [data.otp_sms, data.otp_email, data.otp_2fa, data.otp_login, data.otp_system, id], (err, muitifactor) => {
              res.redirect(`/${process.env.SUBFOLDER}account/list`);
            });
          });
        });
      }else{
        if (req.files) {
          const filename = req.files.img;
          if (!filename.map) {
            var newfilename = `${uuidv4()}.${filename.name.split('.')[1]}`;
            const savePath = path.join(__dirname, '../public/UI/image', newfilename);
            filename.mv(savePath);
          }
          data.image = newfilename;
        }
        conn.query('SELECT * FROM `TB_TR_ACCOUNT` WHERE acc_id = ?', [id], (err, account) => {
          conn.query('UPDATE TB_TR_ACCOUNT set firstname =?,lastname =?,name =?,position =?,descrip =?,contact =?,ext =?,phone =?,email =?,line =?,username =?,password =? ,bd =?,image=? where acc_id = ?', [data.firstname, data.lastname, data.name, data.position, data.descrip, data.contact, data.ext, data.phone, data.email, data.line, data.username, data.password,  data.bd, data.image,  id], (err, admin_save) => {
            controller.funchistory(req, 'account', `แก้ไขข้อมูล ผู้ใช้ ${account[0].name}`, req.session.userid);
            if (err) {
              res.json(err);
            }
            conn.query('UPDATE `TB_TR_MUITIFACTOR` set `otp_sms`=?, `otp_email`=?, `otp_2fa`=?, `otp_login`=?, `otp_system`=? where acc_id = ?', [data.otp_sms, data.otp_email, data.otp_2fa, data.otp_login, data.otp_system, id], (err, muitifactor) => {
              res.redirect(`/${process.env.SUBFOLDER}profile`);
            });
          });
        });
      }
    });
  }
};

controller.clear_session = (req, res) => {
  var url = req.params;
  if (typeof req.session.userid === 'undefined' || req.session.admin == '0') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    req.getConnection((err, conn) => {
      conn.query('UPDATE TB_TR_ACCOUNT set session_id = ?  where acc_id = ?', [null,url.id], (err, clear_session) => {
        res.redirect(`/${process.env.SUBFOLDER}account/list`);
      });
    });
  }
};

controller.check_limit = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM TB_TR_LIMIT ', (err, limit) => {
      req.session.limit = {
        'cookie': limit[0].limit_cookie,
        'paper': limit[0].limit_paper,
        'email': limit[0].limit_email,
        'user': limit[0].limit_user,
        'policy': limit[0].limit_policy,
        'pattern': limit[0].limit_pattern,
        'classification': limit[0].limit_classification,
        'email_board': limit[0].limit_email_board,
        'email_personal': limit[0].limit_email_personal,
      }
    });
  });
}


module.exports = controller;
