const { log } = require("console");
const session = require("express-session");
const uuidv4 = require('uuid').v4;
const fs = require('fs');
const controller = {};

controller.report_classification = (req, res) => {
    if (typeof req.session.userid == "undefined") {
        res.redirect(`/${process.env.SUBFOLDER}`);
    } else {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM TB_TR_PDPA_DATA ", (err, TB_TR_PDPA_DATA) => {
                res.render("cookie/view_report/report_classification", {
                    session: req.session
                    , TB_TR_PDPA_DATA
                });
            });
        });
    }
};
controller.api_classification = (req, res) => {
    if (typeof req.session.userid == "undefined") {
        res.redirect(`/${process.env.SUBFOLDER}`);
    } else {
        req.getConnection((err, conn) => {
            conn.query("SELECT *,DATE_FORMAT(classify_create,'%d/%m/%Y') as day_classify_create FROM TB_TR_PDPA_CLASSIFICATION join TB_TR_PDPA_PATTERN on TB_TR_PDPA_PATTERN.pattern_id = TB_TR_PDPA_CLASSIFICATION.pattern_id join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_CLASSIFICATION.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_CLASSIFICATION.acc_id", (err, classify) => {
                conn.query("SELECT *,DATE_FORMAT(pattern_create,'%d/%m/%Y') as day_pattern_create,DATE_FORMAT(pattern_start_date,'%d/%m/%Y') as day_pattern_start_date FROM TB_TR_PDPA_PATTERN join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_PATTERN.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_PATTERN.acc_id", (err, pattern) => {
                conn.query("SELECT * FROM TB_TR_PDPA_DOCUMENT ", (err, TB_TR_PDPA_DOCUMENT) => {
                    conn.query("SELECT *,DATE_FORMAT(data_date_start,'%d/%m/%Y') as day_data_date_start,DATE_FORMAT(data_date_end,'%d/%m/%Y') as day_data_date_end FROM TB_TR_PDPA_DATA ", (err, TB_TR_PDPA_DATA) => {
                        var classify_data = []
                        for (let i = 0; i < classify.length; i++) {
                            var pdpa_data = []
                            for (let j = 0; j < TB_TR_PDPA_DATA.length; j++) {
                                if (classify[i].doc_id_person_data_pattern.search(TB_TR_PDPA_DATA[j].data_id) > -1) {
                                    pdpa_data.push(TB_TR_PDPA_DATA[j].data_name + ' ' + TB_TR_PDPA_DATA[j].day_data_date_start + '-' + TB_TR_PDPA_DATA[j].day_data_date_end + '<br>')
                                }
                            }
                            if (pdpa_data.length == 0) {
                                pdpa_data.push('-')
                            }
                            policy = []
                            classify_data.push({ classify: classify[i], policy: policy, pdpa_data: pdpa_data })
                        }

                        if (classify_data.length > 0) {
                            res.send({ classify_data });
                        } else {
                            var data_nul = "ไม่มีข้อมูล"
                            res.send({ data_nul, classify_data });
                        }
                    });
                });
            });
        });
        });
    }
};
controller.new_report_all = (req, res) => {
    if (typeof req.session.userid == "undefined") {
        res.redirect(`/${process.env.SUBFOLDER}`);
    } else {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM TB_TR_PDPA_DATA ", (err, TB_TR_PDPA_DATA) => {
                res.render("cookie/view_report/report_class_pat", {
                    session: req.session
                    , TB_TR_PDPA_DATA
                });
            });
        });
    }
};

controller.api_new_report_all = (req, res) => {
    if (typeof req.session.userid == "undefined") {
        res.redirect(`/${process.env.SUBFOLDER}`);
    } else {
        req.getConnection((err, conn) => {
            conn.query("SELECT *,DATE_FORMAT(pattern_create,'%d/%m/%Y') as day_pattern_create,DATE_FORMAT(pattern_start_date,'%d/%m/%Y') as day_pattern_start_date FROM TB_TR_PDPA_PATTERN join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_PATTERN.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_PATTERN.acc_id", (err, pattern) => {
                conn.query("SELECT *,DATE_FORMAT(classify_create,'%d/%m/%Y') as day_classify_create FROM TB_TR_PDPA_CLASSIFICATION join TB_TR_PDPA_PATTERN on TB_TR_PDPA_PATTERN.pattern_id = TB_TR_PDPA_CLASSIFICATION.pattern_id join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_CLASSIFICATION.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_CLASSIFICATION.acc_id", (err, classify) => {
                    conn.query("SELECT count(TB_TR_PDPA_PATTERN.pattern_id) as c FROM TB_TR_PDPA_CLASSIFICATION join TB_TR_PDPA_PATTERN on TB_TR_PDPA_PATTERN.pattern_id = TB_TR_PDPA_CLASSIFICATION.pattern_id join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_CLASSIFICATION.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_CLASSIFICATION.acc_id group by TB_TR_PDPA_PATTERN.pattern_id", (err, cound_data) => {

                    conn.query("SELECT * FROM TB_TR_PDPA_DOCUMENT ", (err, TB_TR_PDPA_DOCUMENT) => {
                        conn.query("SELECT *,DATE_FORMAT(data_date_start,'%d/%m/%Y') as day_data_date_start,DATE_FORMAT(data_date_end,'%d/%m/%Y') as day_data_date_end,DATE_FORMAT(data_date_start,'%m/%d/%Y') as d_start,DATE_FORMAT(data_date_end,'%m/%d/%Y') as d_end FROM TB_TR_PDPA_DATA", (err, TB_TR_PDPA_DATA) => {
                        var pattern_data = []
                        var count_date = []
                        for (let i = 0; i < pattern.length; i++) {
                            var pdpa_data = []
                            var pdpa_data_date = []
                            var pdpa_between = []
                            for (let j = 0; j < TB_TR_PDPA_DATA.length; j++) {
                                if (pattern[i].doc_id_person_data.search(TB_TR_PDPA_DATA[j].data_id) > -1) {
                                    pdpa_data.push(TB_TR_PDPA_DATA[j].data_name + '<br>')
                                    pdpa_data_date.push(TB_TR_PDPA_DATA[j].day_data_date_start + ' - ' + TB_TR_PDPA_DATA[j].day_data_date_end + '<br>')
                                    
                                    let date_1 = new Date(TB_TR_PDPA_DATA[j].d_start);
                                    let date_2 = new Date(TB_TR_PDPA_DATA[j].d_end);

                                    function days(d_1, d_2) {
                                        let difference = d_1.getTime() - d_2.getTime();
                                        let TotalDays = Math.ceil(difference / (1000 * 3600 * 24));
                                        return TotalDays;
                                    }

                                    date_sum = days(date_2, date_1);
                                    pdpa_between.push(date_sum + '<br>');
                                }
                            }
                            const d = new Date(pattern[i].pattern_create);
                            const e = new Date(pattern[i].pattern_create);

                            const theDay = d.getDate() + pattern[i].pattern_total_date;
                            const theDay_2 = e.getDate() + pattern[i].pattern_total_date + pattern[i].pattern_set_end_date_total;
                            d.setDate(theDay);
                            e.setDate(theDay_2);

                            const result1 = d.toLocaleDateString('en-GB');
                            pattern[i].count_date = result1;

                            const result2 = e.toLocaleDateString('en-GB');
                            pattern[i].count_date_end = result2;

                            

                            if (pdpa_data.length == 0) {
                                pdpa_data.push('-')
                            }
                            policy = []
                            for (let k = 0; k < TB_TR_PDPA_DOCUMENT.length; k++) {
                                if (pattern[i].doc_id.search(TB_TR_PDPA_DOCUMENT[k].doc_id) > -1) {
                                    policy.push(TB_TR_PDPA_DOCUMENT[k].doc_name)
                                }
                            }
                            if (policy.length == 0) {
                                policy.push('-')
                            }
                            if (pattern[i].pattern_set_end_date_total == null) {
                                pattern[i].pattern_set_end_date_total = 'ไม่กำหนด'
                            }
                            
                            pattern_data.push({ pattern: pattern[i], policy: policy, pdpa_data: pdpa_data ,pdpa_data_date:pdpa_data_date,pdpa_between:pdpa_between})
                        }
                  
                        if (pattern_data.length > 0) {
                            res.send({ pattern_data ,classify ,cound_data});
                        } else {
                            var data_nul = "ไม่มีข้อมูล"
                            res.send({ data_nul, pattern_data });
                        }
                    });
                });
            });
        });
    });
});
    }
};

controller.api_new_report_all_search = (req, res) => {
    if (typeof req.session.userid == "undefined") {
        res.redirect(`/${process.env.SUBFOLDER}`);
    } else {
        const data = req.body;
   
        req.getConnection((err, conn) => {
            conn.query("SELECT *,DATE_FORMAT(pattern_create,'%d/%m/%Y') as day_pattern_create,DATE_FORMAT(pattern_start_date,'%d/%m/%Y') as day_pattern_start_date FROM TB_TR_PDPA_PATTERN join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_PATTERN.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_PATTERN.acc_id", (err, pattern) => {
                conn.query("SELECT *,DATE_FORMAT(classify_create,'%d/%m/%Y') as day_classify_create FROM TB_TR_PDPA_CLASSIFICATION join TB_TR_PDPA_PATTERN on TB_TR_PDPA_PATTERN.pattern_id = TB_TR_PDPA_CLASSIFICATION.pattern_id join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_CLASSIFICATION.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_CLASSIFICATION.acc_id", (err, classify) => {
                    conn.query("SELECT count(TB_TR_PDPA_PATTERN.pattern_id) as c FROM TB_TR_PDPA_CLASSIFICATION join TB_TR_PDPA_PATTERN on TB_TR_PDPA_PATTERN.pattern_id = TB_TR_PDPA_CLASSIFICATION.pattern_id join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_CLASSIFICATION.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_CLASSIFICATION.acc_id group by TB_TR_PDPA_PATTERN.pattern_id", (err, cound_data) => {

                    conn.query("SELECT * FROM TB_TR_PDPA_DOCUMENT ", (err, TB_TR_PDPA_DOCUMENT) => {
                        conn.query("SELECT *,DATE_FORMAT(data_date_start,'%d/%m/%Y') as day_data_date_start,DATE_FORMAT(data_date_end,'%d/%m/%Y') as day_data_date_end,DATE_FORMAT(data_date_start,'%m/%d/%Y') as d_start,DATE_FORMAT(data_date_end,'%m/%d/%Y') as d_end,DATE_FORMAT(now(),'%m/%d/%Y') as date_now,DATE_FORMAT(data_date_end,'%m') as month_end,DATE_FORMAT(now(),'%m') as month_now FROM TB_TR_PDPA_DATA", (err, TB_TR_PDPA_DATA) => {
                        var pattern_data = []
                        var count_date = []

                        for (let i = 0; i < pattern.length; i++) {
                            var pdpa_data = []
                            var pdpa_data_date = []
                            var pdpa_between = []
                            for (let j = 0; j < TB_TR_PDPA_DATA.length; j++) {
                                if (pattern[i].doc_id_person_data.search(TB_TR_PDPA_DATA[j].data_id) > -1) {
                                    pdpa_data.push(TB_TR_PDPA_DATA[j].data_name + '<br>')
                                    pdpa_data_date.push(TB_TR_PDPA_DATA[j].day_data_date_start + ' - ' + TB_TR_PDPA_DATA[j].day_data_date_end + '<br>')
                                    
                                    let date_1 = new Date(TB_TR_PDPA_DATA[j].d_start);
                                    let date_2 = new Date(TB_TR_PDPA_DATA[j].d_end);

                                    function days(d_1, d_2) {
                                        let difference = d_1.getTime() - d_2.getTime();
                                        let TotalDays = Math.ceil(difference / (1000 * 3600 * 24));
                                        return TotalDays;
                                    }

                                    date_sum = days(date_2, date_1);
                                    pdpa_between.push(date_sum + '<br>');
                                }
                            }
                            const d = new Date(pattern[i].pattern_create);
                            const e = new Date(pattern[i].pattern_create);

                            const theDay = d.getDate() + pattern[i].pattern_total_date;
                            const theDay_2 = e.getDate() + pattern[i].pattern_total_date + pattern[i].pattern_set_end_date_total;
                            d.setDate(theDay);
                            e.setDate(theDay_2);

                            const result1 = d.toLocaleDateString('en-GB');
                            pattern[i].count_date = result1;

                            const result2 = e.toLocaleDateString('en-GB');
                            pattern[i].count_date_end = result2;

                            
                            if (pdpa_data.length == 0) {
                                pdpa_data.push('-')
                            }
                            policy = []
                            for (let k = 0; k < TB_TR_PDPA_DOCUMENT.length; k++) {
                                if (pattern[i].doc_id.search(TB_TR_PDPA_DOCUMENT[k].doc_id) > -1) {
                                    policy.push(TB_TR_PDPA_DOCUMENT[k].doc_name)
                                }
                            }
                            if (policy.length == 0) {
                                policy.push('-')
                            }
                            if (pattern[i].pattern_set_end_date_total == null) {
                                pattern[i].pattern_set_end_date_total = 'ไม่กำหนด'
                            }
                            
                            pattern_data.push({ pattern: pattern[i], policy: policy, pdpa_data: pdpa_data ,pdpa_data_date:pdpa_data_date,pdpa_between:pdpa_between})
                        }
                        
                        if (pattern.length > 0) {
                            sum_data_p = [];
                            if (data.search_pdpa == 0 || data.search_pdpa == 1) { //ทั้งหมด
                       
                                if (data.search_data == '') {
                                    if (pattern_data.length > 0) {
                                        res.send({ pattern_data ,classify ,cound_data});
                                    }else{
                                        var data_nul = "ไม่มีข้อมูล"
                                        res.send({ data_nul, pattern_data });
                                    }
                                }else{
                                    for (let p = 0; p < pattern_data.length; p++) {
                                        if (pattern_data[p].pattern_name.search(data.search_data) > -1 || 
                                        pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                        pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                        pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                        pattern_data[p].day_pattern_start_date.search(data.search_data) > -1 ||
                                        pattern_data[p].count_date.search(data.search_data) > -1 ||
                                        pattern_data[p].pattern_total_date.toString().search(data.search_data) > -1 ||
                                        pattern_data[p].pattern_processing_base_name.search(data.search_data) > -1 ||
                                        pattern_data[p].pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                        pattern_data[p].count_date_end.toString().search(data.search_data) > -1
                                        ) {     
                                            sum_data_p.push(pattern_data[p]); 
                                        }else{
                                            for (let j = 0; j < classify.length; j++) {
                                                const d_2 = new Date(classify[j].classify_create);
                                                const e_2 = new Date(classify[j].classify_create);

                                                const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                d_2.setDate(theDayx);
                                                e_2.setDate(theDay_2x);

                                                const result1x = d_2.toLocaleDateString('en-GB');
                                                const result2x = e_2.toLocaleDateString('en-GB');
                                                date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                       
                                                classify[j].date_class = date_class;
                                                classify[j].date_class_end = result2x;

                                                
                                                if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                classify[j].date_class.search(data.search_data) > -1 ||
                                                classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                classify[j].date_class_end.toString().search(data.search_data) > -1
                                                ) {
                                                    if (classify[j].pattern_id == pattern_data[p].pattern_id) {
                                                        sum_data_p.push(pattern_data[p]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (sum_data_p.length > 0) {
                                        res.send({ pattern_data:sum_data_p ,classify ,cound_data});
                                    }else{
                                        var data_nul = "ไม่มีข้อมูล"
                                        res.send({ data_nul, pattern_data });
                                    }
                                    
                                }
                                
                            }else if (data.search_pdpa == 2) { //ข้อมูลที่ยังไม่หมดอายุ
                         
                                list_pdpa_for = [];
                                for (let u = 0; u < TB_TR_PDPA_DATA.length; u++) {
                                    date_s_1 = new Date(TB_TR_PDPA_DATA[u].d_end).getTime()
                                    date_s_2 = new Date(TB_TR_PDPA_DATA[u].date_now).getTime()
        
                                    month_end = TB_TR_PDPA_DATA[u].month_end;
                                    month_now = TB_TR_PDPA_DATA[u].month_now;
        
                                    
                                    if (date_s_1 >= date_s_2) { //ข้อมูลที่ยังไม่หมดอายุ
                                        list_pdpa_for.push(TB_TR_PDPA_DATA[u].data_id)
                                    }
                                }
                                check_again = []
                                for (let oo = 0; oo < pattern_data.length; oo++) {
                                    var thank_you = pattern_data[oo].pattern.doc_id_person_data.split(",")
                                    for (let music = 0; music < list_pdpa_for.length; music++) {
                                        
                                        for (let hi = 0; hi < thank_you.length; hi++) {
                                            if (thank_you[hi] == list_pdpa_for[music]) {
                                            if (check_again.length > 0) {
                                                for (let cookie = 0; cookie < check_again.length; cookie++) {
                                                    if (check_again[cookie] != pattern_data[oo].pattern.pattern_id) {
                                                        check_again.push(pattern_data[oo].pattern.pattern_id)
                                                    }
                                                }
                                            }else{
                                                check_again.push(pattern_data[oo].pattern.pattern_id)
                                            }
                                            }
                                        }
                                    }
                                }
                                if (data.search_data == '') {
                                    for (let home = 0; home < check_again.length; home++) {
                                        for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                            if (check_again[home] == pattern_data[cisco].pattern.pattern_id) {
                                                sum_data_p.push(pattern_data[cisco]);
                                            }
                                            
                                        }
                                        
                                    }
                                    if (sum_data_p.length > 0) {
                                        res.send({ pattern_data:sum_data_p ,classify ,cound_data});
                                    }else{
                                        var data_nul = "ไม่มีข้อมูล"
                                        res.send({ data_nul, pattern_data });
                                    }
                                }else{
                                    for (let home = 0; home < check_again.length; home++) {
                                        for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                            if (check_again[home] == pattern_data[cisco].pattern.pattern_id) {

                                                for (let p = 0; p < pattern_data.length; p++) {
                                                    if (pattern_data[p].pattern.pattern_name.search(data.search_data) > -1 || 
                                                    pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                    pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                    pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.day_pattern_start_date.search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.count_date.search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.pattern_total_date.toString().search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.pattern_processing_base_name.search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.count_date_end.toString().search(data.search_data) > -1
                                                    ) { 
                                                        if (pattern_data[p].pattern.pattern_id == check_again[home]) {
                                                            sum_data_p.push(pattern_data[p]);
                                                        }
                                                    }else{
                                                        for (let j = 0; j < classify.length; j++) {
                                                            const d_2 = new Date(classify[j].classify_create);
                                                            const e_2 = new Date(classify[j].classify_create);
            
                                                            const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                            const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                            d_2.setDate(theDayx);
                                                            e_2.setDate(theDay_2x);
            
                                                            const result1x = d_2.toLocaleDateString('en-GB');
                                                            const result2x = e_2.toLocaleDateString('en-GB');
                                                            date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                                   
                                                            classify[j].date_class = date_class;
                                                            classify[j].date_class_end = result2x;
            
                                                            
                                                            if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                            classify[j].date_class.search(data.search_data) > -1 ||
                                                            classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                            classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                            classify[j].date_class_end.toString().search(data.search_data) > -1
                                                            ) {
                                                                if (classify[j].pattern_id == check_again[home]) {
                                                                    sum_data_p.push(pattern_data[p]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                        
                                    }
                                    if (sum_data_p.length > 0) {
                                        res.send({ pattern_data:sum_data_p ,classify ,cound_data});
                                    }else{
                                        var data_nul = "ไม่มีข้อมูล"
                                        res.send({ data_nul, pattern_data });
                                    }
                                }


                            }else if (data.search_pdpa == 3) { //ข้อมูลที่หมดอายุแล้ว
                              
                                list_pdpa_for = [];
                                for (let u = 0; u < TB_TR_PDPA_DATA.length; u++) {
                                    date_s_1 = new Date(TB_TR_PDPA_DATA[u].d_end).getTime()
                                    date_s_2 = new Date(TB_TR_PDPA_DATA[u].date_now).getTime()
        
                                    month_end = TB_TR_PDPA_DATA[u].month_end;
                                    month_now = TB_TR_PDPA_DATA[u].month_now;
        
                                    
                                    if (date_s_1 < date_s_2) { //ข้อมูลที่หมดอายุแล้ว
                                        list_pdpa_for.push(TB_TR_PDPA_DATA[u].data_id)
                                    }
                                }
                                check_again = []
                                for (let oo = 0; oo < pattern_data.length; oo++) {
                                    var thank_you = pattern_data[oo].pattern.doc_id_person_data.split(",")
                                    for (let music = 0; music < list_pdpa_for.length; music++) {
                                        
                                        for (let hi = 0; hi < thank_you.length; hi++) {
                                            if (thank_you[hi] == list_pdpa_for[music]) {
                                            if (check_again.length > 0) {
                                                for (let cookie = 0; cookie < check_again.length; cookie++) {
                                                    if (check_again[cookie] != pattern_data[oo].pattern.pattern_id) {
                                                        check_again.push(pattern_data[oo].pattern.pattern_id)
                                                    }
                                                }
                                            }else{
                                                check_again.push(pattern_data[oo].pattern.pattern_id)
                                            }
                                            }
                                        }
                                    }
                                }
                                if (data.search_data == '') {
                                    for (let home = 0; home < check_again.length; home++) {
                                        for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                            if (check_again[home] == pattern_data[cisco].pattern.pattern_id) {
                                                sum_data_p.push(pattern_data[cisco]);
                                            }
                                            
                                        }
                                        
                                    }
                                    if (sum_data_p.length > 0) {
                                        res.send({ pattern_data:sum_data_p ,classify ,cound_data});
                                    }else{
                                        var data_nul = "ไม่มีข้อมูล"
                                        res.send({ data_nul, pattern_data });
                                    }
                                }else{
                                    for (let home = 0; home < check_again.length; home++) {
                                        for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                            if (check_again[home] == pattern_data[cisco].pattern.pattern_id) {

                                                for (let p = 0; p < pattern_data.length; p++) {
                                                    if (pattern_data[p].pattern.pattern_name.search(data.search_data) > -1 || 
                                                    pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                    pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                    pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.day_pattern_start_date.search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.count_date.search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.pattern_total_date.toString().search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.pattern_processing_base_name.search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.count_date_end.toString().search(data.search_data) > -1
                                                    ) { 
                                                        if (pattern_data[p].pattern.pattern_id == check_again[home]) {
                                                            sum_data_p.push(pattern_data[p]);
                                                        }
                                                    }else{
                                                        for (let j = 0; j < classify.length; j++) {
                                                            const d_2 = new Date(classify[j].classify_create);
                                                            const e_2 = new Date(classify[j].classify_create);
            
                                                            const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                            const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                            d_2.setDate(theDayx);
                                                            e_2.setDate(theDay_2x);
            
                                                            const result1x = d_2.toLocaleDateString('en-GB');
                                                            const result2x = e_2.toLocaleDateString('en-GB');
                                                            date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                                   
                                                            classify[j].date_class = date_class;
                                                            classify[j].date_class_end = result2x;
            
                                                            
                                                            if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                            classify[j].date_class.search(data.search_data) > -1 ||
                                                            classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                            classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                            classify[j].date_class_end.toString().search(data.search_data) > -1
                                                            ) {
                                                                if (classify[j].pattern_id == check_again[home]) {
                                                                    sum_data_p.push(pattern_data[p]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                        
                                    }
                                    if (sum_data_p.length > 0) {
                                        res.send({ pattern_data:sum_data_p ,classify ,cound_data});
                                    }else{
                                        var data_nul = "ไม่มีข้อมูล"
                                        res.send({ data_nul, pattern_data });
                                    }
                                }
                            }else if (data.search_pdpa == 4) { //ข้อมูลที่จะหมดอายุในเดือนนี้
                            
                                list_pdpa_for = [];
                                for (let u = 0; u < TB_TR_PDPA_DATA.length; u++) {
                                    date_s_1 = new Date(TB_TR_PDPA_DATA[u].d_end).getTime()
                                    date_s_2 = new Date(TB_TR_PDPA_DATA[u].date_now).getTime()
        
                                    month_end = TB_TR_PDPA_DATA[u].month_end;
                                    month_now = TB_TR_PDPA_DATA[u].month_now;
        
                                    
                                    if (month_end == month_now) { //ข้อมูลที่จะหมดอายุในเดือนนี้
                                        list_pdpa_for.push(TB_TR_PDPA_DATA[u].data_id)
                                    }
                                }
                                check_again = []
                                for (let oo = 0; oo < pattern_data.length; oo++) {
                                    var thank_you = pattern_data[oo].pattern.doc_id_person_data.split(",")
                                    for (let music = 0; music < list_pdpa_for.length; music++) {
                                        
                                        for (let hi = 0; hi < thank_you.length; hi++) {
                                            if (thank_you[hi] == list_pdpa_for[music]) {
                                            if (check_again.length > 0) {
                                                for (let cookie = 0; cookie < check_again.length; cookie++) {
                                                    if (check_again[cookie] != pattern_data[oo].pattern.pattern_id) {
                                                        check_again.push(pattern_data[oo].pattern.pattern_id)
                                                    }
                                                }
                                            }else{
                                                check_again.push(pattern_data[oo].pattern.pattern_id)
                                            }
                                            }
                                        }
                                    }
                                }
                                if (data.search_data == '') {
                                    for (let home = 0; home < check_again.length; home++) {
                                        for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                            if (check_again[home] == pattern_data[cisco].pattern.pattern_id) {
                                                sum_data_p.push(pattern_data[cisco]);
                                            }
                                            
                                        }
                                        
                                    }
                                    if (sum_data_p.length > 0) {
                                        res.send({ pattern_data:sum_data_p ,classify ,cound_data});
                                    }else{
                                        var data_nul = "ไม่มีข้อมูล"
                                        res.send({ data_nul, pattern_data });
                                    }
                                }else{
                                    for (let home = 0; home < check_again.length; home++) {
                                        for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                            if (check_again[home] == pattern_data[cisco].pattern.pattern_id) {

                                                for (let p = 0; p < pattern_data.length; p++) {
                                                    if (pattern_data[p].pattern.pattern_name.search(data.search_data) > -1 || 
                                                    pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                    pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                    pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.day_pattern_start_date.search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.count_date.search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.pattern_total_date.toString().search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.pattern_processing_base_name.search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                                    pattern_data[p].pattern.count_date_end.toString().search(data.search_data) > -1
                                                    ) { 
                                                        if (pattern_data[p].pattern.pattern_id == check_again[home]) {
                                                            sum_data_p.push(pattern_data[p]);
                                                        }
                                                    }else{
                                                        for (let j = 0; j < classify.length; j++) {
                                                            const d_2 = new Date(classify[j].classify_create);
                                                            const e_2 = new Date(classify[j].classify_create);
            
                                                            const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                            const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                            d_2.setDate(theDayx);
                                                            e_2.setDate(theDay_2x);
            
                                                            const result1x = d_2.toLocaleDateString('en-GB');
                                                            const result2x = e_2.toLocaleDateString('en-GB');
                                                            date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                                   
                                                            classify[j].date_class = date_class;
                                                            classify[j].date_class_end = result2x;
            
                                                            
                                                            if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                            classify[j].date_class.search(data.search_data) > -1 ||
                                                            classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                            classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                            classify[j].date_class_end.toString().search(data.search_data) > -1
                                                            ) {
                                                                if (classify[j].pattern_id == check_again[home]) {
                                                                    sum_data_p.push(pattern_data[p]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                        
                                    }
                                    if (sum_data_p.length > 0) {
                                        res.send({ pattern_data:sum_data_p ,classify ,cound_data});
                                    }else{
                                        var data_nul = "ไม่มีข้อมูล"
                                        res.send({ data_nul, pattern_data });
                                    }
                                }
                            }
                        }else{
                            var data_nul = "ไม่มีข้อมูล"
                            res.send({ data_nul, pattern_data });
                        }
                        });
                    });
                });
            });
        });
    });
    }
};
controller.api_search_classification_text = (req, res) => {
    if (typeof req.session.userid == "undefined") {
        res.redirect(`/${process.env.SUBFOLDER}`);
    } else {
        const data = req.body;
        req.getConnection((err, conn) => {
            conn.query("SELECT *,DATE_FORMAT(classify_create,'%d/%m/%Y') as day_classify_create FROM TB_TR_PDPA_CLASSIFICATION join TB_TR_PDPA_PATTERN on TB_TR_PDPA_PATTERN.pattern_id = TB_TR_PDPA_CLASSIFICATION.pattern_id join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_CLASSIFICATION.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_CLASSIFICATION.acc_id WHERE  (TB_TR_PDPA_CLASSIFICATION.classify_name LIKE ? ) AND  DATE_FORMAT(classify_create,'%Y-%m-%d') BETWEEN  ?  AND  ? "
                , [('%' + data.text + '%'), data.firstDay, data.lastDay], (err, classify) => {
                    conn.query("SELECT * FROM TB_TR_PDPA_DOCUMENT ", (err, TB_TR_PDPA_DOCUMENT) => {
                        conn.query("SELECT *,DATE_FORMAT(data_date_start,'%d/%m/%Y') as day_data_date_start,DATE_FORMAT(data_date_end,'%d/%m/%Y') as day_data_date_end FROM TB_TR_PDPA_DATA ", (err, TB_TR_PDPA_DATA) => {
                            var classify_data = []
                            for (let i = 0; i < classify.length; i++) {
                                var pdpa_data = []
                                for (let j = 0; j < TB_TR_PDPA_DATA.length; j++) {
                                    if (classify[i].doc_id_person_data_pattern.search(TB_TR_PDPA_DATA[j].data_id) > -1) {
                                        pdpa_data.push(TB_TR_PDPA_DATA[j].data_name + ' ' + TB_TR_PDPA_DATA[j].day_data_date_start + '-' + TB_TR_PDPA_DATA[j].day_data_date_end + '<br>')
                                    }
                                }
                                if (pdpa_data.length == 0) {
                                    pdpa_data.push('-')
                                }

                                policy = []
                                for (let k = 0; k < TB_TR_PDPA_DOCUMENT.length; k++) {
                                    if (classify[i].doc_id.search(TB_TR_PDPA_DOCUMENT[k].doc_id) > -1) {
                                        policy.push(TB_TR_PDPA_DOCUMENT[k].doc_name)
                                    }
                                }
                                if (policy.length == 0) {
                                    policy.push('-')
                                }
                                classify_data.push({ classify: classify[i], policy: policy, pdpa_data: pdpa_data })
                            }
                            if (classify_data.length > 0) {
                                res.send({ classify_data });
                            } else {
                                var data_nul = "ไม่มีข้อมูล"
                                res.send({ data_nul, classify_data });
                            }
                        });
                    });
                });
        });
    }
};

controller.api_search_classification_date = (req, res) => {
    if (typeof req.session.userid == "undefined") {
        res.redirect(`/${process.env.SUBFOLDER}`);
    } else {
        const data = req.body;
        req.getConnection((err, conn) => {
            conn.query("SELECT *,DATE_FORMAT(classify_create,'%d/%m/%Y') as day_classify_create FROM TB_TR_PDPA_CLASSIFICATION join TB_TR_PDPA_PATTERN on TB_TR_PDPA_PATTERN.pattern_id = TB_TR_PDPA_CLASSIFICATION.pattern_id join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_CLASSIFICATION.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_CLASSIFICATION.acc_id WHERE DATE_FORMAT(classify_create,'%Y-%m-%d') BETWEEN  ?  AND  ?  "
                , [data.firstDay, data.lastDay], (err, classify) => {
                    conn.query("SELECT * FROM TB_TR_PDPA_DOCUMENT ", (err, TB_TR_PDPA_DOCUMENT) => {
                        conn.query("SELECT *,DATE_FORMAT(data_date_start,'%d/%m/%Y') as day_data_date_start,DATE_FORMAT(data_date_end,'%d/%m/%Y') as day_data_date_end FROM TB_TR_PDPA_DATA ", (err, TB_TR_PDPA_DATA) => {
                            var classify_data = []
                            for (let i = 0; i < classify.length; i++) {
                                var pdpa_data = []
                                for (let j = 0; j < TB_TR_PDPA_DATA.length; j++) {
                                    if (classify[i].doc_id_person_data_pattern.search(TB_TR_PDPA_DATA[j].data_id) > -1) {
                                        pdpa_data.push(TB_TR_PDPA_DATA[j].data_name + ' ' + TB_TR_PDPA_DATA[j].day_data_date_start + '-' + TB_TR_PDPA_DATA[j].day_data_date_end + '<br>')
                                    }
                                }
                                if (pdpa_data.length == 0) {
                                    pdpa_data.push('-')
                                }

                                policy = []
                                for (let k = 0; k < TB_TR_PDPA_DOCUMENT.length; k++) {
                                    if (classify[i].doc_id.search(TB_TR_PDPA_DOCUMENT[k].doc_id) > -1) {
                                        policy.push(TB_TR_PDPA_DOCUMENT[k].doc_name)
                                    }
                                }
                                if (policy.length == 0) {
                                    policy.push('-')
                                }
                                classify_data.push({ classify: classify[i], policy: policy, pdpa_data: pdpa_data })
                            }
                            if (classify_data.length > 0) {
                                res.send({ classify_data });
                            } else {
                                var data_nul = "ไม่มีข้อมูล"
                                res.send({ data_nul, classify_data });
                            }
                        });
                    });
                });
        });
    }
};

controller.new_report_all_excel = (req, res) => {
    const data = req.body;
    var delayInMilliseconds = 1000; //1 second
   
        req.getConnection((err, conn) => {
            conn.query("SELECT *,DATE_FORMAT(pattern_create,'%d/%m/%Y') as day_pattern_create,DATE_FORMAT(pattern_start_date,'%d/%m/%Y') as day_pattern_start_date FROM TB_TR_PDPA_PATTERN join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_PATTERN.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_PATTERN.acc_id", (err, pattern) => {
                conn.query("SELECT *,DATE_FORMAT(classify_create,'%d/%m/%Y') as day_classify_create FROM TB_TR_PDPA_CLASSIFICATION join TB_TR_PDPA_PATTERN on TB_TR_PDPA_PATTERN.pattern_id = TB_TR_PDPA_CLASSIFICATION.pattern_id join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_CLASSIFICATION.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_CLASSIFICATION.acc_id", (err, classify) => {
                    conn.query("SELECT TB_TR_PDPA_PATTERN.pattern_id as id, count(TB_TR_PDPA_PATTERN.pattern_id) as c FROM TB_TR_PDPA_CLASSIFICATION join TB_TR_PDPA_PATTERN on TB_TR_PDPA_PATTERN.pattern_id = TB_TR_PDPA_CLASSIFICATION.pattern_id join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_CLASSIFICATION.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_CLASSIFICATION.acc_id group by TB_TR_PDPA_PATTERN.pattern_id", (err, cound_data) => {
                        conn.query("SELECT *,DATE_FORMAT(data_date_start,'%d/%m/%Y') as day_data_date_start,DATE_FORMAT(data_date_end,'%d/%m/%Y') as day_data_date_end,DATE_FORMAT(data_date_start,'%m/%d/%Y') as d_start,DATE_FORMAT(data_date_end,'%m/%d/%Y') as d_end,DATE_FORMAT(now(),'%m/%d/%Y') as date_now,DATE_FORMAT(data_date_end,'%m') as month_end,DATE_FORMAT(now(),'%m') as month_now FROM TB_TR_PDPA_DATA", (err, TB_TR_PDPA_DATA) => {
                            
                            var pattern_data = []

                            for (let i = 0; i < pattern.length; i++) {
                                var pdpa_data = ''
                                var pdpa_data_date = ''
                                var pdpa_between = ''
                                for (let j = 0; j < TB_TR_PDPA_DATA.length; j++) {
                                    if (pattern[i].doc_id_person_data.search(TB_TR_PDPA_DATA[j].data_id) > -1) {
                                        pdpa_data = pdpa_data + TB_TR_PDPA_DATA[j].data_name + '\n';
                                        pdpa_data_date = pdpa_data_date + TB_TR_PDPA_DATA[j].day_data_date_start + ' - ' + TB_TR_PDPA_DATA[j].day_data_date_end + '\n'
                                        
                                        let date_1 = new Date(TB_TR_PDPA_DATA[j].d_start);
                                        let date_2 = new Date(TB_TR_PDPA_DATA[j].d_end);
        
                                        function days(d_1, d_2) {
                                            let difference = d_1.getTime() - d_2.getTime();
                                            let TotalDays = Math.ceil(difference / (1000 * 3600 * 24));
                                            return TotalDays;
                                        }
        
                                        date_sum = days(date_2, date_1);
                                        pdpa_between = pdpa_between + date_sum + '\n';
                                    }
                                    
                                }
                                pdpa_data = pdpa_data.substring(0, pdpa_data.length-1);
                                pdpa_data_date = pdpa_data_date.substring(0, pdpa_data_date.length-1);
                                pdpa_between = pdpa_between.substring(0, pdpa_between.length-1);
        
                                if (pdpa_data == '') {
                                    pdpa_data = '-'
                                }
                                if (pdpa_data_date == '') {
                                    pdpa_data_date = '-'
                                }
                                if (pdpa_between == '') {
                                    pdpa_between = '-'
                                }
                                
        
                                pattern[i].pdpa_data = pdpa_data;
                                pattern[i].pdpa_data_date = pdpa_data_date;
                                pattern[i].pdpa_between = pdpa_between;
        
                                const d = new Date(pattern[i].pattern_create);
                                const e = new Date(pattern[i].pattern_create);
        
                                const theDay = d.getDate() + pattern[i].pattern_total_date;
                                const theDay_2 = e.getDate() + pattern[i].pattern_total_date + pattern[i].pattern_set_end_date_total;
                                d.setDate(theDay);
                                e.setDate(theDay_2);
        
                                const result1 = d.toLocaleDateString('en-GB');
                                pattern[i].count_date = result1;
        
                                const result2 = e.toLocaleDateString('en-GB');
                                pattern[i].count_date_end = result2;
                                
                                if (pattern[i].pattern_set_end_date_total == null) {
                                    pattern[i].pattern_set_end_date_total = 'ไม่กำหนด'
                                }
                            }
                            pattern_data = pattern;

                            for (let k = 0; k < classify.length; k++) {
                                const f = new Date(classify[k].classify_create);
                                const g = new Date(classify[k].classify_create);
                                
                                const theDay3 = f.getDate() + classify[k].classify_period_process;
                                const theDay_4 = g.getDate() + classify[k].classify_period_process + classify[k].classify_period_end;
                                f.setDate(theDay3);
                                g.setDate(theDay_4);

                                const result3 = f.toLocaleDateString('en-GB');
                                const result4 = g.toLocaleDateString('en-GB');
                                classify[k].date_class = `${classify[k].day_classify_create} - ${result3}`;
                                classify[k].date_class_end = result4;

                                if (classify[k].classify_period_end == null) {
                                    classify[k].end_date_class = `ไม่กำหนด`;
                                }else{
                                    classify[k].end_date_class = classify[k].classify_period_end;
                                }
                            }
                            if (pattern.length > 0) {
                                sum_data_p = [];
                                if (data.search_pdpa == 0 || data.search_pdpa == 1) { //ทั้งหมด
                               
                                    
                                    if (data.search_data == '') {
                                        if (pattern_data.length > 0) {
                                            white_ex = excel_d1(pattern ,classify ,cound_data);
                                            if (white_ex == true) {
                                                setTimeout(function() {
                                                    res.send('True');
                                                  }, delayInMilliseconds);
                                            }
                                            
                                        }else{
                                            
                                        }
                                    }else{
                                        for (let p = 0; p < pattern_data.length; p++) {
                                            if (pattern_data[p].pattern_name.search(data.search_data) > -1 || 
                                            pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                            pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                            pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                            pattern_data[p].day_pattern_start_date.search(data.search_data) > -1 ||
                                            pattern_data[p].count_date.search(data.search_data) > -1 ||
                                            pattern_data[p].pattern_total_date.toString().search(data.search_data) > -1 ||
                                            pattern_data[p].pattern_processing_base_name.search(data.search_data) > -1 ||
                                            pattern_data[p].pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                            pattern_data[p].count_date_end.toString().search(data.search_data) > -1
                                            ) {     
                                                sum_data_p.push(pattern_data[p]); 
                                            }else{
                                                for (let j = 0; j < classify.length; j++) {
                                                    const d_2 = new Date(classify[j].classify_create);
                                                    const e_2 = new Date(classify[j].classify_create);
    
                                                    const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                    const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                    d_2.setDate(theDayx);
                                                    e_2.setDate(theDay_2x);
    
                                                    const result1x = d_2.toLocaleDateString('en-GB');
                                                    const result2x = e_2.toLocaleDateString('en-GB');
                                                    date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                           
                                                    classify[j].date_class = date_class;
                                                    classify[j].date_class_end = result2x;
    
                                                    
                                                    if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                    classify[j].date_class.search(data.search_data) > -1 ||
                                                    classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                    classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                    classify[j].date_class_end.toString().search(data.search_data) > -1
                                                    ) {
                                                        if (classify[j].pattern_id == pattern_data[p].pattern_id) {
                                                            sum_data_p.push(pattern_data[p]);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }
                                    
                                }else if (data.search_pdpa == 2) { //ข้อมูลที่ยังไม่หมดอายุ
                                    list_pdpa_for = [];
                                    for (let u = 0; u < TB_TR_PDPA_DATA.length; u++) {
                                        date_s_1 = new Date(TB_TR_PDPA_DATA[u].d_end).getTime()
                                        date_s_2 = new Date(TB_TR_PDPA_DATA[u].date_now).getTime()
            
                                        month_end = TB_TR_PDPA_DATA[u].month_end;
                                        month_now = TB_TR_PDPA_DATA[u].month_now;
            
                                        
                                        if (date_s_1 >= date_s_2) { //ข้อมูลที่ยังไม่หมดอายุ
                                            list_pdpa_for.push(TB_TR_PDPA_DATA[u].data_id)
                                        }
                                    }
                                    check_again = []
                                    for (let oo = 0; oo < pattern_data.length; oo++) {
                                        var thank_you = pattern_data[oo].doc_id_person_data.split(",")
                                        for (let music = 0; music < list_pdpa_for.length; music++) {
                                            
                                            for (let hi = 0; hi < thank_you.length; hi++) {
                                                if (thank_you[hi] == list_pdpa_for[music]) {
                                                if (check_again.length > 0) {
                                                    for (let cookie = 0; cookie < check_again.length; cookie++) {
                                                        if (check_again[cookie] != pattern_data[oo].pattern_id) {
                                                            check_again.push(pattern_data[oo].pattern_id)
                                                        }
                                                    }
                                                }else{
                                                    check_again.push(pattern_data[oo].pattern_id)
                                                }
                                                }
                                            }
                                        }
                                    }
                                    if (data.search_data == '') {
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
                                                    sum_data_p.push(pattern_data[cisco]);
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {

                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }else{
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
    
                                                    for (let p = 0; p < pattern_data.length; p++) {
                                                        if (pattern_data[p].pattern_name.search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                                        pattern_data[p].day_pattern_start_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_total_date.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date_end.toString().search(data.search_data) > -1
                                                        ) { 
                                                            if (pattern_data[p].pattern_id == check_again[home]) {
                                                                sum_data_p.push(pattern_data[p]);
                                                            }
                                                        }else{
                                                            for (let j = 0; j < classify.length; j++) {
                                                                const d_2 = new Date(classify[j].classify_create);
                                                                const e_2 = new Date(classify[j].classify_create);
                
                                                                const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                                const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                                d_2.setDate(theDayx);
                                                                e_2.setDate(theDay_2x);
                
                                                                const result1x = d_2.toLocaleDateString('en-GB');
                                                                const result2x = e_2.toLocaleDateString('en-GB');
                                                                date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                                       
                                                                classify[j].date_class = date_class;
                                                                classify[j].date_class_end = result2x;
                
                                                                
                                                                if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class.search(data.search_data) > -1 ||
                                                                classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                                classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class_end.toString().search(data.search_data) > -1
                                                                ) {
                                                                    if (classify[j].pattern_id == check_again[home]) {
                                                                        sum_data_p.push(pattern_data[p]);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }
    
    
                                }else if (data.search_pdpa == 3) { //ข้อมูลที่หมดอายุแล้ว
                       
                                    list_pdpa_for = [];
                                    for (let u = 0; u < TB_TR_PDPA_DATA.length; u++) {
                                        date_s_1 = new Date(TB_TR_PDPA_DATA[u].d_end).getTime()
                                        date_s_2 = new Date(TB_TR_PDPA_DATA[u].date_now).getTime()
            
                                        month_end = TB_TR_PDPA_DATA[u].month_end;
                                        month_now = TB_TR_PDPA_DATA[u].month_now;
            
                                        
                                        if (date_s_1 < date_s_2) { //ข้อมูลที่หมดอายุแล้ว
                                            list_pdpa_for.push(TB_TR_PDPA_DATA[u].data_id)
                                        }
                                    }
                                    check_again = []
                                    for (let oo = 0; oo < pattern_data.length; oo++) {
                                        var thank_you = pattern_data[oo].doc_id_person_data.split(",")
                                        for (let music = 0; music < list_pdpa_for.length; music++) {
                                            
                                            for (let hi = 0; hi < thank_you.length; hi++) {
                                                if (thank_you[hi] == list_pdpa_for[music]) {
                                                if (check_again.length > 0) {
                                                    for (let cookie = 0; cookie < check_again.length; cookie++) {
                                                        if (check_again[cookie] != pattern_data[oo].pattern_id) {
                                                            check_again.push(pattern_data[oo].pattern_id)
                                                        }
                                                    }
                                                }else{
                                                    check_again.push(pattern_data[oo].pattern_id)
                                                }
                                                }
                                            }
                                        }
                                    }
                                    if (data.search_data == '') {
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
                                                    sum_data_p.push(pattern_data[cisco]);
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }else{
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
    
                                                    for (let p = 0; p < pattern_data.length; p++) {
                                                        if (pattern_data[p].pattern_name.search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                                        pattern_data[p].day_pattern_start_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_total_date.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date_end.toString().search(data.search_data) > -1
                                                        ) { 
                                                            if (pattern_data[p].pattern_id == check_again[home]) {
                                                                sum_data_p.push(pattern_data[p]);
                                                            }
                                                        }else{
                                                            for (let j = 0; j < classify.length; j++) {
                                                                const d_2 = new Date(classify[j].classify_create);
                                                                const e_2 = new Date(classify[j].classify_create);
                
                                                                const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                                const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                                d_2.setDate(theDayx);
                                                                e_2.setDate(theDay_2x);
                
                                                                const result1x = d_2.toLocaleDateString('en-GB');
                                                                const result2x = e_2.toLocaleDateString('en-GB');
                                                                date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                                       
                                                                classify[j].date_class = date_class;
                                                                classify[j].date_class_end = result2x;
                
                                                                
                                                                if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class.search(data.search_data) > -1 ||
                                                                classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                                classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class_end.toString().search(data.search_data) > -1
                                                                ) {
                                                                    if (classify[j].pattern_id == check_again[home]) {
                                                                        sum_data_p.push(pattern_data[p]);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }
                                }else if (data.search_pdpa == 4) { //ข้อมูลที่จะหมดอายุในเดือนนี้
                           
                                    list_pdpa_for = [];
                                    for (let u = 0; u < TB_TR_PDPA_DATA.length; u++) {
                                        date_s_1 = new Date(TB_TR_PDPA_DATA[u].d_end).getTime()
                                        date_s_2 = new Date(TB_TR_PDPA_DATA[u].date_now).getTime()
            
                                        month_end = TB_TR_PDPA_DATA[u].month_end;
                                        month_now = TB_TR_PDPA_DATA[u].month_now;
            
                                        
                                        if (month_end == month_now) { //ข้อมูลที่จะหมดอายุในเดือนนี้
                                            list_pdpa_for.push(TB_TR_PDPA_DATA[u].data_id)
                                        }
                                    }
                                    check_again = []
                                    for (let oo = 0; oo < pattern_data.length; oo++) {
                                        var thank_you = pattern_data[oo].doc_id_person_data.split(",")
                                        for (let music = 0; music < list_pdpa_for.length; music++) {
                                            
                                            for (let hi = 0; hi < thank_you.length; hi++) {
                                                if (thank_you[hi] == list_pdpa_for[music]) {
                                                if (check_again.length > 0) {
                                                    for (let cookie = 0; cookie < check_again.length; cookie++) {
                                                        if (check_again[cookie] != pattern_data[oo].pattern_id) {
                                                            check_again.push(pattern_data[oo].pattern_id)
                                                        }
                                                    }
                                                }else{
                                                    check_again.push(pattern_data[oo].pattern_id)
                                                }
                                                }
                                            }
                                        }
                                    }
                                    if (data.search_data == '') {
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
                                                    sum_data_p.push(pattern_data[cisco]);
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }else{
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
    
                                                    for (let p = 0; p < pattern_data.length; p++) {
                                                        if (pattern_data[p].pattern_name.search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                                        pattern_data[p].day_pattern_start_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_total_date.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date_end.toString().search(data.search_data) > -1
                                                        ) { 
                                                            if (pattern_data[p].pattern_id == check_again[home]) {
                                                                sum_data_p.push(pattern_data[p]);
                                                            }
                                                        }else{
                                                            for (let j = 0; j < classify.length; j++) {
                                                                const d_2 = new Date(classify[j].classify_create);
                                                                const e_2 = new Date(classify[j].classify_create);
                
                                                                const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                                const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                                d_2.setDate(theDayx);
                                                                e_2.setDate(theDay_2x);
                
                                                                const result1x = d_2.toLocaleDateString('en-GB');
                                                                const result2x = e_2.toLocaleDateString('en-GB');
                                                                date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                                       
                                                                classify[j].date_class = date_class;
                                                                classify[j].date_class_end = result2x;
                
                                                                
                                                                if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class.search(data.search_data) > -1 ||
                                                                classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                                classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class_end.toString().search(data.search_data) > -1
                                                                ) {
                                                                    if (classify[j].pattern_id == check_again[home]) {
                                                                        sum_data_p.push(pattern_data[p]);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }
                                }
                            }else{
                            }
                            
                    });
                });
            });
        });
        });
};

controller.new_report_all_csv = (req, res) => {
    const data = req.body;
    var delayInMilliseconds = 1000; //1 second

        req.getConnection((err, conn) => {
            conn.query("SELECT *,DATE_FORMAT(pattern_create,'%d/%m/%Y') as day_pattern_create,DATE_FORMAT(pattern_start_date,'%d/%m/%Y') as day_pattern_start_date FROM TB_TR_PDPA_PATTERN join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_PATTERN.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_PATTERN.acc_id", (err, pattern) => {
                conn.query("SELECT *,DATE_FORMAT(classify_create,'%d/%m/%Y') as day_classify_create FROM TB_TR_PDPA_CLASSIFICATION join TB_TR_PDPA_PATTERN on TB_TR_PDPA_PATTERN.pattern_id = TB_TR_PDPA_CLASSIFICATION.pattern_id join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_CLASSIFICATION.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_CLASSIFICATION.acc_id", (err, classify) => {
                    conn.query("SELECT TB_TR_PDPA_PATTERN.pattern_id as id, count(TB_TR_PDPA_PATTERN.pattern_id) as c FROM TB_TR_PDPA_CLASSIFICATION join TB_TR_PDPA_PATTERN on TB_TR_PDPA_PATTERN.pattern_id = TB_TR_PDPA_CLASSIFICATION.pattern_id join TB_MM_PDPA_PATTERN_PROCESSING_BASE on TB_MM_PDPA_PATTERN_PROCESSING_BASE.pattern_processing_base_id = TB_TR_PDPA_CLASSIFICATION.pattern_processing_base_id JOIN TB_TR_ACCOUNT ON TB_TR_ACCOUNT.acc_id = TB_TR_PDPA_CLASSIFICATION.acc_id group by TB_TR_PDPA_PATTERN.pattern_id", (err, cound_data) => {
                        conn.query("SELECT *,DATE_FORMAT(data_date_start,'%d/%m/%Y') as day_data_date_start,DATE_FORMAT(data_date_end,'%d/%m/%Y') as day_data_date_end,DATE_FORMAT(data_date_start,'%m/%d/%Y') as d_start,DATE_FORMAT(data_date_end,'%m/%d/%Y') as d_end,DATE_FORMAT(now(),'%m/%d/%Y') as date_now,DATE_FORMAT(data_date_end,'%m') as month_end,DATE_FORMAT(now(),'%m') as month_now FROM TB_TR_PDPA_DATA", (err, TB_TR_PDPA_DATA) => {
                            
                            var pattern_data = []

                            for (let i = 0; i < pattern.length; i++) {
                                var pdpa_data = ''
                                var pdpa_data_date = ''
                                var pdpa_between = ''
                                for (let j = 0; j < TB_TR_PDPA_DATA.length; j++) {
                                    if (pattern[i].doc_id_person_data.search(TB_TR_PDPA_DATA[j].data_id) > -1) {
                                        pdpa_data = pdpa_data + TB_TR_PDPA_DATA[j].data_name + '\n';
                                        pdpa_data_date = pdpa_data_date + TB_TR_PDPA_DATA[j].day_data_date_start + ' - ' + TB_TR_PDPA_DATA[j].day_data_date_end + '\n'
                                        
                                        let date_1 = new Date(TB_TR_PDPA_DATA[j].d_start);
                                        let date_2 = new Date(TB_TR_PDPA_DATA[j].d_end);
        
                                        function days(d_1, d_2) {
                                            let difference = d_1.getTime() - d_2.getTime();
                                            let TotalDays = Math.ceil(difference / (1000 * 3600 * 24));
                                            return TotalDays;
                                        }
        
                                        date_sum = days(date_2, date_1);
                                        pdpa_between = pdpa_between + date_sum + '\n';
                                    }
                                    
                                }
                                pdpa_data = pdpa_data.substring(0, pdpa_data.length-1);
                                pdpa_data_date = pdpa_data_date.substring(0, pdpa_data_date.length-1);
                                pdpa_between = pdpa_between.substring(0, pdpa_between.length-1);
        
                                if (pdpa_data == '') {
                                    pdpa_data = '-'
                                }
                                if (pdpa_data_date == '') {
                                    pdpa_data_date = '-'
                                }
                                if (pdpa_between == '') {
                                    pdpa_between = '-'
                                }
                                
        
                                pattern[i].pdpa_data = pdpa_data;
                                pattern[i].pdpa_data_date = pdpa_data_date;
                                pattern[i].pdpa_between = pdpa_between;
        
                                const d = new Date(pattern[i].pattern_create);
                                const e = new Date(pattern[i].pattern_create);
        
                                const theDay = d.getDate() + pattern[i].pattern_total_date;
                                const theDay_2 = e.getDate() + pattern[i].pattern_total_date + pattern[i].pattern_set_end_date_total;
                                d.setDate(theDay);
                                e.setDate(theDay_2);
        
                                const result1 = d.toLocaleDateString('en-GB');
                                pattern[i].count_date = result1;
        
                                const result2 = e.toLocaleDateString('en-GB');
                                pattern[i].count_date_end = result2;
                                
                                if (pattern[i].pattern_set_end_date_total == null) {
                                    pattern[i].pattern_set_end_date_total = 'ไม่กำหนด'
                                }
                            }
                            pattern_data = pattern;

                            for (let k = 0; k < classify.length; k++) {
                                const f = new Date(classify[k].classify_create);
                                const g = new Date(classify[k].classify_create);
                                
                                const theDay3 = f.getDate() + classify[k].classify_period_process;
                                const theDay_4 = g.getDate() + classify[k].classify_period_process + classify[k].classify_period_end;
                                f.setDate(theDay3);
                                g.setDate(theDay_4);

                                const result3 = f.toLocaleDateString('en-GB');
                                const result4 = g.toLocaleDateString('en-GB');
                                classify[k].date_class = `${classify[k].day_classify_create} - ${result3}`;
                                classify[k].date_class_end = result4;

                                if (classify[k].classify_period_end == null) {
                                    classify[k].end_date_class = `ไม่กำหนด`;
                                }else{
                                    classify[k].end_date_class = classify[k].classify_period_end;
                                }
                            }
                            if (pattern.length > 0) {
                                sum_data_p = [];
                                if (data.search_pdpa == 0 || data.search_pdpa == 1) { //ทั้งหมด
                                
                                    
                                    if (data.search_data == '') {
                                        if (pattern_data.length > 0) {
                                            white_ex = excel_d1(pattern ,classify ,cound_data);
                                            if (white_ex == true) {
                                                setTimeout(function() {
                                                    excel_d1(pattern ,classify ,cound_data)
                                                    XLSX = require('xlsx');
                                                    const workBook = XLSX.readFile('public/UI/assets/form_excel_report_all/Excel_report_alltra.xlsx');
                                                    XLSX.writeFile(workBook, 'public/UI/assets/form_excel_report_all/Excel_report_alltra.csv', { bookType: "csv" });
                                                    res.send('True');
                                                  }, delayInMilliseconds);
                                            }
                                            
                                        }else{
                                            
                                        }
                                    }else{
                                        for (let p = 0; p < pattern_data.length; p++) {
                                            if (pattern_data[p].pattern_name.search(data.search_data) > -1 || 
                                            pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                            pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                            pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                            pattern_data[p].day_pattern_start_date.search(data.search_data) > -1 ||
                                            pattern_data[p].count_date.search(data.search_data) > -1 ||
                                            pattern_data[p].pattern_total_date.toString().search(data.search_data) > -1 ||
                                            pattern_data[p].pattern_processing_base_name.search(data.search_data) > -1 ||
                                            pattern_data[p].pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                            pattern_data[p].count_date_end.toString().search(data.search_data) > -1
                                            ) {     
                                                sum_data_p.push(pattern_data[p]); 
                                            }else{
                                                for (let j = 0; j < classify.length; j++) {
                                                    const d_2 = new Date(classify[j].classify_create);
                                                    const e_2 = new Date(classify[j].classify_create);
    
                                                    const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                    const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                    d_2.setDate(theDayx);
                                                    e_2.setDate(theDay_2x);
    
                                                    const result1x = d_2.toLocaleDateString('en-GB');
                                                    const result2x = e_2.toLocaleDateString('en-GB');
                                                    date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                           
                                                    classify[j].date_class = date_class;
                                                    classify[j].date_class_end = result2x;
    
                                                    
                                                    if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                    classify[j].date_class.search(data.search_data) > -1 ||
                                                    classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                    classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                    classify[j].date_class_end.toString().search(data.search_data) > -1
                                                    ) {
                                                        if (classify[j].pattern_id == pattern_data[p].pattern_id) {
                                                            sum_data_p.push(pattern_data[p]);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        XLSX = require('xlsx');
                                                        const workBook = XLSX.readFile('public/UI/assets/form_excel_report_all/Excel_report_alltra.xlsx');
                                                        XLSX.writeFile(workBook, 'public/UI/assets/form_excel_report_all/Excel_report_alltra.csv', { bookType: "csv" });
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }
                                    
                                }else if (data.search_pdpa == 2) { //ข้อมูลที่ยังไม่หมดอายุ
                           
                                
                                    list_pdpa_for = [];
                                    for (let u = 0; u < TB_TR_PDPA_DATA.length; u++) {
                                        date_s_1 = new Date(TB_TR_PDPA_DATA[u].d_end).getTime()
                                        date_s_2 = new Date(TB_TR_PDPA_DATA[u].date_now).getTime()
            
                                        month_end = TB_TR_PDPA_DATA[u].month_end;
                                        month_now = TB_TR_PDPA_DATA[u].month_now;
            
                                        
                                        if (date_s_1 >= date_s_2) { //ข้อมูลที่ยังไม่หมดอายุ
                                            list_pdpa_for.push(TB_TR_PDPA_DATA[u].data_id)
                                        }
                                    }
                                    check_again = []
                                    for (let oo = 0; oo < pattern_data.length; oo++) {
                                        var thank_you = pattern_data[oo].doc_id_person_data.split(",")
                                        for (let music = 0; music < list_pdpa_for.length; music++) {
                                            
                                            for (let hi = 0; hi < thank_you.length; hi++) {
                                                if (thank_you[hi] == list_pdpa_for[music]) {
                                                if (check_again.length > 0) {
                                                    for (let cookie = 0; cookie < check_again.length; cookie++) {
                                                        if (check_again[cookie] != pattern_data[oo].pattern_id) {
                                                            check_again.push(pattern_data[oo].pattern_id)
                                                        }
                                                    }
                                                }else{
                                                    check_again.push(pattern_data[oo].pattern_id)
                                                }
                                                }
                                            }
                                        }
                                    }
                                    if (data.search_data == '') {
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
                                                    sum_data_p.push(pattern_data[cisco]);
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {

                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        XLSX = require('xlsx');
                                                        const workBook = XLSX.readFile('public/UI/assets/form_excel_report_all/Excel_report_alltra.xlsx');
                                                        XLSX.writeFile(workBook, 'public/UI/assets/form_excel_report_all/Excel_report_alltra.csv', { bookType: "csv" });
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }else{
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
    
                                                    for (let p = 0; p < pattern_data.length; p++) {
                                                        if (pattern_data[p].pattern_name.search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                                        pattern_data[p].day_pattern_start_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_total_date.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date_end.toString().search(data.search_data) > -1
                                                        ) { 
                                                            if (pattern_data[p].pattern_id == check_again[home]) {
                                                                sum_data_p.push(pattern_data[p]);
                                                            }
                                                        }else{
                                                            for (let j = 0; j < classify.length; j++) {
                                                                const d_2 = new Date(classify[j].classify_create);
                                                                const e_2 = new Date(classify[j].classify_create);
                
                                                                const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                                const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                                d_2.setDate(theDayx);
                                                                e_2.setDate(theDay_2x);
                
                                                                const result1x = d_2.toLocaleDateString('en-GB');
                                                                const result2x = e_2.toLocaleDateString('en-GB');
                                                                date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                                       
                                                                classify[j].date_class = date_class;
                                                                classify[j].date_class_end = result2x;
                
                                                                
                                                                if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class.search(data.search_data) > -1 ||
                                                                classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                                classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class_end.toString().search(data.search_data) > -1
                                                                ) {
                                                                    if (classify[j].pattern_id == check_again[home]) {
                                                                        sum_data_p.push(pattern_data[p]);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        XLSX = require('xlsx');
                                                        const workBook = XLSX.readFile('public/UI/assets/form_excel_report_all/Excel_report_alltra.xlsx');
                                                        XLSX.writeFile(workBook, 'public/UI/assets/form_excel_report_all/Excel_report_alltra.csv', { bookType: "csv" });
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }
    
    
                                }else if (data.search_pdpa == 3) { //ข้อมูลที่หมดอายุแล้ว
                                   
                                    list_pdpa_for = [];
                                    for (let u = 0; u < TB_TR_PDPA_DATA.length; u++) {
                                        date_s_1 = new Date(TB_TR_PDPA_DATA[u].d_end).getTime()
                                        date_s_2 = new Date(TB_TR_PDPA_DATA[u].date_now).getTime()
            
                                        month_end = TB_TR_PDPA_DATA[u].month_end;
                                        month_now = TB_TR_PDPA_DATA[u].month_now;
            
                                        
                                        if (date_s_1 < date_s_2) { //ข้อมูลที่หมดอายุแล้ว
                                            list_pdpa_for.push(TB_TR_PDPA_DATA[u].data_id)
                                        }
                                    }
                                    check_again = []
                                    for (let oo = 0; oo < pattern_data.length; oo++) {
                                        var thank_you = pattern_data[oo].doc_id_person_data.split(",")
                                        for (let music = 0; music < list_pdpa_for.length; music++) {
                                            
                                            for (let hi = 0; hi < thank_you.length; hi++) {
                                                if (thank_you[hi] == list_pdpa_for[music]) {
                                                if (check_again.length > 0) {
                                                    for (let cookie = 0; cookie < check_again.length; cookie++) {
                                                        if (check_again[cookie] != pattern_data[oo].pattern_id) {
                                                            check_again.push(pattern_data[oo].pattern_id)
                                                        }
                                                    }
                                                }else{
                                                    check_again.push(pattern_data[oo].pattern_id)
                                                }
                                                }
                                            }
                                        }
                                    }
                                    if (data.search_data == '') {
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
                                                    sum_data_p.push(pattern_data[cisco]);
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        XLSX = require('xlsx');
                                                        const workBook = XLSX.readFile('public/UI/assets/form_excel_report_all/Excel_report_alltra.xlsx');
                                                        XLSX.writeFile(workBook, 'public/UI/assets/form_excel_report_all/Excel_report_alltra.csv', { bookType: "csv" });
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }else{
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
    
                                                    for (let p = 0; p < pattern_data.length; p++) {
                                                        if (pattern_data[p].pattern_name.search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                                        pattern_data[p].day_pattern_start_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_total_date.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date_end.toString().search(data.search_data) > -1
                                                        ) { 
                                                            if (pattern_data[p].pattern_id == check_again[home]) {
                                                                sum_data_p.push(pattern_data[p]);
                                                            }
                                                        }else{
                                                            for (let j = 0; j < classify.length; j++) {
                                                                const d_2 = new Date(classify[j].classify_create);
                                                                const e_2 = new Date(classify[j].classify_create);
                
                                                                const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                                const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                                d_2.setDate(theDayx);
                                                                e_2.setDate(theDay_2x);
                
                                                                const result1x = d_2.toLocaleDateString('en-GB');
                                                                const result2x = e_2.toLocaleDateString('en-GB');
                                                                date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                                       
                                                                classify[j].date_class = date_class;
                                                                classify[j].date_class_end = result2x;
                
                                                                
                                                                if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class.search(data.search_data) > -1 ||
                                                                classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                                classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class_end.toString().search(data.search_data) > -1
                                                                ) {
                                                                    if (classify[j].pattern_id == check_again[home]) {
                                                                        sum_data_p.push(pattern_data[p]);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        XLSX = require('xlsx');
                                                        const workBook = XLSX.readFile('public/UI/assets/form_excel_report_all/Excel_report_alltra.xlsx');
                                                        XLSX.writeFile(workBook, 'public/UI/assets/form_excel_report_all/Excel_report_alltra.csv', { bookType: "csv" });
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }
                                }else if (data.search_pdpa == 4) { //ข้อมูลที่จะหมดอายุในเดือนนี้
                                    
                                    list_pdpa_for = [];
                                    for (let u = 0; u < TB_TR_PDPA_DATA.length; u++) {
                                        date_s_1 = new Date(TB_TR_PDPA_DATA[u].d_end).getTime()
                                        date_s_2 = new Date(TB_TR_PDPA_DATA[u].date_now).getTime()
            
                                        month_end = TB_TR_PDPA_DATA[u].month_end;
                                        month_now = TB_TR_PDPA_DATA[u].month_now;
            
                                        
                                        if (month_end == month_now) { //ข้อมูลที่จะหมดอายุในเดือนนี้
                                            list_pdpa_for.push(TB_TR_PDPA_DATA[u].data_id)
                                        }
                                    }
                                    check_again = []
                                    for (let oo = 0; oo < pattern_data.length; oo++) {
                                        var thank_you = pattern_data[oo].doc_id_person_data.split(",")
                                        for (let music = 0; music < list_pdpa_for.length; music++) {
                                            
                                            for (let hi = 0; hi < thank_you.length; hi++) {
                                                if (thank_you[hi] == list_pdpa_for[music]) {
                                                if (check_again.length > 0) {
                                                    for (let cookie = 0; cookie < check_again.length; cookie++) {
                                                        if (check_again[cookie] != pattern_data[oo].pattern_id) {
                                                            check_again.push(pattern_data[oo].pattern_id)
                                                        }
                                                    }
                                                }else{
                                                    check_again.push(pattern_data[oo].pattern_id)
                                                }
                                                }
                                            }
                                        }
                                    }
                                    if (data.search_data == '') {
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
                                                    sum_data_p.push(pattern_data[cisco]);
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        XLSX = require('xlsx');
                                                        const workBook = XLSX.readFile('public/UI/assets/form_excel_report_all/Excel_report_alltra.xlsx');
                                                        XLSX.writeFile(workBook, 'public/UI/assets/form_excel_report_all/Excel_report_alltra.csv', { bookType: "csv" });
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }else{
                                        for (let home = 0; home < check_again.length; home++) {
                                            for (let cisco = 0; cisco < pattern_data.length; cisco++) {
                                                if (check_again[home] == pattern_data[cisco].pattern_id) {
    
                                                    for (let p = 0; p < pattern_data.length; p++) {
                                                        if (pattern_data[p].pattern_name.search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_data_date.toString().replace(/,/g, "").search(data.search_data) > -1 || 
                                                        pattern_data[p].pdpa_between.toString().replace(/,/g, "").search(data.search_data) > -1 ||
                                                        pattern_data[p].day_pattern_start_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_total_date.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                        pattern_data[p].pattern_set_end_date_total.toString().search(data.search_data) > -1 ||
                                                        pattern_data[p].count_date_end.toString().search(data.search_data) > -1
                                                        ) { 
                                                            if (pattern_data[p].pattern_id == check_again[home]) {
                                                                sum_data_p.push(pattern_data[p]);
                                                            }
                                                        }else{
                                                            
                                                            for (let j = 0; j < classify.length; j++) {
                                                                const d_2 = new Date(classify[j].classify_create);
                                                                const e_2 = new Date(classify[j].classify_create);
                
                                                                const theDayx = d_2.getDate() + classify[j].classify_period_process;
                                                                const theDay_2x = e_2.getDate() + classify[j].classify_period_process + classify[j].classify_period_end;
                                                                d_2.setDate(theDayx);
                                                                e_2.setDate(theDay_2x);
                
                                                                const result1x = d_2.toLocaleDateString('en-GB');
                                                                const result2x = e_2.toLocaleDateString('en-GB');
                                                                date_class = `${classify[j].day_classify_create} - ${result1x}`;
                                                       
                                                                classify[j].date_class = date_class;
                                                                classify[j].date_class_end = result2x;
                
                                                                
                                                                if (classify[j].classify_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class.search(data.search_data) > -1 ||
                                                                classify[j].classify_period_process.toString().search(data.search_data) > -1 ||
                                                                classify[j].pattern_processing_base_name.search(data.search_data) > -1 ||
                                                                classify[j].date_class_end.toString().search(data.search_data) > -1
                                                                ) {
                                                                    if (classify[j].pattern_id == check_again[home]) {
                                                                        sum_data_p.push(pattern_data[p]);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            
                                        }
                                        if (sum_data_p.length > 0) {
                                            if (pattern_data.length > 0) {
                                                white_ex = excel_d1(sum_data_p ,classify ,cound_data);
                                                if (white_ex == true) {
                                                    setTimeout(function() {
                                                        XLSX = require('xlsx');
                                                        const workBook = XLSX.readFile('public/UI/assets/form_excel_report_all/Excel_report_alltra.xlsx');
                                                        XLSX.writeFile(workBook, 'public/UI/assets/form_excel_report_all/Excel_report_alltra.csv', { bookType: "csv" });
                                                        res.send('True');
                                                      }, delayInMilliseconds);
                                                }
                                                
                                            }else{
                                                
                                            }
                                        }else{
                                        }
                                    }
                                }
                            }else{
                            }
                            
                    });
                });
            });
        });
        });
};

function excel_d1(pattern ,classify ,cound_data) {
    var excel = require('excel4node');
    var workbook = new excel.Workbook();
    var ws = workbook.addWorksheet('Sheet 1');
    var worksheet2 = workbook.addWorksheet('Sheet 2');

    var style = workbook.createStyle({
        font: {
            name: 'TH SarabunPSK',
            bold: true,

        },
        alignment:{
            vertical: 'center',
            horizontal: 'center',
    },
    numberFormat: '$#,##0.00; ($#,##0.00); -'
    });
    var style_2 = workbook.createStyle({
        font: {
            name: 'TH SarabunPSK',
        },
        alignment:{
            vertical: 'top',
            horizontal: 'left',
            wrapText: true,
            
        },
        numberFormat: '$#,##0.00; ($#,##0.00); -'
    });
    var style_3 = workbook.createStyle({
        font: {
            color: '#FF0800',
            name: 'TH SarabunPSK',
        },
        alignment:{
            vertical: 'top',
            horizontal: 'left',
            wrapText: true,
            
        },
        numberFormat: '$#,##0.00; ($#,##0.00); -'
    });
    var style_4 = workbook.createStyle({
        font: {
            name: 'TH SarabunPSK',

        },
        alignment:{
            vertical: 'center',
            horizontal: 'center',
    },
    numberFormat: '$#,##0.00; ($#,##0.00); -'
    });
    var style_5 = workbook.createStyle({
        font: {
            name: 'TH SarabunPSK',
        },
        alignment:{
            vertical: 'top',
            horizontal: 'left',
        },
        numberFormat: '$#,##0.00; ($#,##0.00); -'
    });
    

    ws.cell(1, 1, 1, 16, true).string('รายงานระยะเวลาจัดเก็บและประมวลผลข้อมูลส่วนบุคคล').style(style);
    ws.cell(3, 1, 4, 1, true).string('ลำดับ').style(style);
    ws.cell(3, 2, 3, 10, true).string('รูปแบบรวบรวมข้อมูลส่วนบุคคล (Pattern)').style(style);
    ws.cell(3, 11, 3, 16, true).string('รูปแบบการประมวลผลข้อมูลส่วนบุคคล (Classification)').style(style);

    ws.cell(4, 2).string('ชื่อรูปแบบ').style(style);
    ws.cell(4, 3).string('ข้อมูลส่วนบุคคล').style(style);
    ws.cell(4, 4).string('ระยะเวลาการจัดเก็บข้อมูล').style(style);
    ws.cell(4, 5).string('จำนวน (วัน)').style(style);
    ws.cell(4, 6).string('ระยะเวลาจัดเก็บตาม Pattern').style(style);
    ws.cell(4, 7).string('จำนวน (วัน)').style(style);
    ws.cell(4, 8).string('ฐานการประมวลผล').style(style);
    ws.cell(4, 9).string('ระยะเวลาสิ้นสุด (วัน)').style(style);
    ws.cell(4, 10).string('วันที่สิ้นสุด').style(style);

    ws.cell(4, 11).string('กิจกรรมประมวลผล').style(style);
    ws.cell(4, 12).string('ระยะเวลาการประมวลผล').style(style);
    ws.cell(4, 13).string('จำนวน (วัน)').style(style);
    ws.cell(4, 14).string('ฐานการประมวลผล').style(style);
    ws.cell(4, 15).string('ระยะเวลาสิ้นสุด (วัน)').style(style);
    ws.cell(4, 16).string('วันที่สิ้นสุด').style(style);
    col = 5;
    x = 4;
    z = 5;
    aa = 0;
    for (let i = 0; i < pattern.length; i++) {
        for (let numc = 0; numc < cound_data.length; numc++) {
            
            if (pattern[i].pattern_id == cound_data[numc].id) {
                x = x + cound_data[numc].c;
            }
        }
     
        
        
        if (i == 0) {
         
            ws.cell(5, 1, x, 1, true).string(`${1 + i}`).style(style_4);
            ws.cell(5, 2, x, 2, true).string(pattern[i].pattern_name).style(style_2);
            ws.cell(5, 3, x, 3, true).string(pattern[i].pdpa_data).style(style_2);
            ws.cell(5, 4, x, 4, true).string(pattern[i].pdpa_data_date).style(style_2);
            ws.cell(5, 5, x, 5, true).string(pattern[i].pdpa_between).style(style_2);
            ws.cell(5, 6, x, 6, true).string(`${pattern[i].day_pattern_create} - ${pattern[i].count_date}`).style(style_2);
            ws.cell(5, 7, x, 7, true).string(`${pattern[i].pattern_total_date}`).style(style_2);
            ws.cell(5, 8, x, 8, true).string(pattern[i].pattern_processing_base_name).style(style_2);
            if (pattern[i].pattern_set_end_date_total == 'ไม่กำหนด') {
                ws.cell(5, 9, x, 9, true).string(`${pattern[i].pattern_set_end_date_total}`).style(style_3);
            }else{
                ws.cell(5, 9, x, 9, true).string(`${pattern[i].pattern_set_end_date_total}`).style(style_2);
            }
            
            ws.cell(5, 10, x, 10, true).string(pattern[i].count_date_end).style(style_3);
        }else{
            
            
            y = x + cound_data[i].c;
            if (y - x == 1) {
                y = y -1
            }
         
            ws.cell(x, 1, y, 1, true).string(`${1 + i}`).style(style_4);
            ws.cell(x, 2, y, 2, true).string(pattern[i].pattern_name).style(style_2);
            ws.cell(x, 3, y, 3, true).string(pattern[i].pdpa_data).style(style_2);
            ws.cell(x, 4, y, 4, true).string(pattern[i].pdpa_data_date).style(style_2);
            ws.cell(x, 5, y, 5, true).string(pattern[i].pdpa_between).style(style_2);
            ws.cell(x, 6, y, 6, true).string(`${pattern[i].day_pattern_create} - ${pattern[i].count_date}`).style(style_2);
            ws.cell(x, 7, y, 7, true).string(`${pattern[i].pattern_total_date}`).style(style_2);
            ws.cell(x, 8, y, 8, true).string(pattern[i].pattern_processing_base_name).style(style_2);
            if (pattern[i].pattern_set_end_date_total == 'ไม่กำหนด') {
                ws.cell(x, 9, y, 9, true).string(`${pattern[i].pattern_set_end_date_total}`).style(style_3);
            }else{
                ws.cell(x, 9, y, 9, true).string(`${pattern[i].pattern_set_end_date_total}`).style(style_2);
            }
            ws.cell(x, 10, y, 10, true).string(pattern[i].count_date_end).style(style_3);
        }
        
        for (let j = 0; j < classify.length; j++) {
          
            if (classify[j].pattern_id == pattern[i].pattern_id) {
                if (aa == 0) {
                    aa = aa + 1;
                    ws.cell(5, 11).string(classify[j].classify_name).style(style_5);
                    ws.cell(5, 12).string(classify[j].date_class).style(style_5);
                    ws.cell(5, 13).string(`${classify[j].classify_period_process}`).style(style_5);
                    ws.cell(5, 14).string(classify[j].pattern_processing_base_name).style(style_5);
                    if (classify[j].end_date_class == 'ไม่กำหนด') {
                        ws.cell(5, 15).string(`${classify[j].end_date_class}`).style(style_3);
                    }else{
                        ws.cell(5, 15).string(`${classify[j].end_date_class}`).style(style_5);
                    }
                    
                    ws.cell(5, 16).string(`${classify[j].date_class_end}`).style(style_3);
                }else{
                    
                    z = z + 1;
                    ws.cell(z, 11).string(classify[j].classify_name).style(style_5);
                    ws.cell(z, 12).string(classify[j].date_class).style(style_5);
                    ws.cell(z, 13).string(`${classify[j].classify_period_process}`).style(style_5);
                    ws.cell(z, 14).string(classify[j].pattern_processing_base_name).style(style_5);
                    if (classify[j].end_date_class == 'ไม่กำหนด') {
                        ws.cell(z, 15).string(`${classify[j].end_date_class}`).style(style_3);
                    }else{
                        ws.cell(z, 15).string(`${classify[j].end_date_class}`).style(style_5);
                    }
                    
                    ws.cell(z, 16).string(`${classify[j].date_class_end}`).style(style_3);
                }
               
            }
        }
        
    }
    
    workbook.write('public/UI/assets/form_excel_report_all/Excel_report_alltra.xlsx');
    
    return true;
}



module.exports = controller;
