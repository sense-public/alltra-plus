// ================================ Data Format =================================
Sub = document.getElementById('SubDomain').value;


function convert_date(date) {
    var month = date.getMonth() + 1;
    var day = date.getDate();
    if (month.toString().length == 1) {
        month = "0" + month.toString()
    }
    if (day.toString().length == 1) {
        day = "0" + day.toString()
    }
    return day.toString() + "/" + month.toString() + "/" + date.getFullYear()
}
Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate() + days);
    return dat;
}
function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(currentDate)
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}
function date_diff(date, total) {
    var dateArray = getDates(new Date(date), (new Date()).addDays(total));
    return dateArray[dateArray.length - 1]
}
function convert_datetime(date) {
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (month.toString().length == 1) {
        month = "0" + month.toString()
    }
    if (day.toString().length == 1) {
        day = "0" + day.toString()
    }
    if (hours.toString().length == 1) {
        hours = "0" + hours.toString()
    }
    if (minutes.toString().length == 1) {
        minutes = "0" + minutes.toString()
    }
    if (seconds.toString().length == 1) {
        seconds = "0" + seconds.toString()
    }
    return day.toString() + "/" + month.toString() + "/" + date.getFullYear() + " " + hours.toString() + ":" + minutes.toString() + ":" + seconds.toString()
}
function checkUsers(all, name, index) {
    if (all[index] == name) {
        return true
    } else {
        if (all.length - 1 == index) {
            return false
        }
        return checkUsers(all, name, (index + 1))
    }
}
function getColorCode() {
    let color = "#";
    for (let i = 0; i < 3; i++) {
        if (document.getElementById('theme-view').checked == true) {
            color += ("0" + Math.floor(((1 + Math.random()) * Math.pow(16, 2)) / 2).toString(16)).slice(-2);
        } else {
            color += ("0" + Math.floor(Math.random() * Math.pow(16, 2) / 2).toString(16)).slice(-2);
        }
    }
    if (color == '#000000' || color == "#ffffff" || color == "#2C323E" || color == "#363d4a") {
        return getColorCode()
    } else {
        return color;
    }
}
// API in pages
if (document.getElementById('c_index')) {
    // ================================ Data Format =================================
    var date_now = new Date();
    $('#start_time').val(convert_date(date_now));
    // ================================ Statics =====================================
    var convert1 = $('#bar1').text().split('%')
    $('#progress-bar1').css({ 'width': $('#bar1').text(), 'height': '6px' })
    $('#progress-bar1').attr('aria-valuenow', convert1[0])
    var convert2 = $('#bar2').text().split('%')
    $('#progress-bar2').css({ 'width': $('#bar2').text(), 'height': '6px' })
    $('#progress-bar2').attr('aria-valuenow', convert2[0])
    var convert3 = $('#bar3').text().split('%')
    $('#progress-bar3').css({ 'width': $('#bar3').text(), 'height': '6px' })
    $('#progress-bar3').attr('aria-valuenow', convert3[0])
    // ======================== Convert Bytes to GB ==================================
    var convert_gb = parseInt($('#diskSpace').text()) * (10 ** (-9))
    $('#diskSpace').text(convert_gb.toFixed(0))
    // ======================== Build Table Classification ===========================
    $.ajax({
        url: `/${Sub}classification`,
        method: "POST",
        data: { value: "@lltr@Cl@ssific@ti0n" },
        success: function (result) {
            // ============================== Create Prepare ============================
            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า

            var state = {
                'querySet': result.classify,
                'page': 1,
                'rows': 30,
                'window': 10,
            }

            buildTable()

            function pagination(querySet, page, rows) {

                var trimStart = (page - 1) * rows
                var trimEnd = trimStart + rows

                var trimmedData = querySet.slice(trimStart, trimEnd)

                var pages = Math.ceil(querySet.length / rows);
                var start_count = 1
                document.getElementById('start').innerHTML = start_count

                return {
                    'querySet': trimmedData,
                    'pages': pages,
                }
            }
            // ============================== Create Pagination ============================
            function pageButtons(pages) {
                var wrapper = document.getElementById('pagination-wapper')
                wrapper.innerHTML = ``
                var maxLeft = (state.page - Math.floor(state.window / 2))
                var maxRight = (state.page + Math.floor(state.window / 2))

                if (maxLeft < 1) {
                    maxLeft = 1
                    maxRight = state.window
                }

                if (maxRight > pages) {
                    maxLeft = pages - (state.window - 1)
                    if (maxLeft < 1) {
                        maxLeft = 1
                    }
                    maxRight = pages
                }

                if (state.page > 1) {
                    wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                } else {
                    wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                }


                num = 1
                if (maxRight > 5) {
                    if (state.page > (maxRight / 2)) {
                        if ((state.page + 1) > (maxRight / 2)) {
                            wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                            wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        }
                    }
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if ((page >= state.page - 2) && (page <= state.page + 2)) {
                            if (page == state.page) {
                                wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                            else {
                                p = page - 1;
                                wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                        }
                    }
                    if ((state.page) <= (maxRight / 2)) {
                        mp = maxRight - 1;
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                    }
                }
                else {
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if (state.page == page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                        }
                    }
                }

                if (state.page < pages) {
                    wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                } else {
                    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                }

                $('.page').on('click', function () {
                    $('#table-body').empty()
                    state.page = Number($(this).val())
                    buildTable()
                })
            }
            // ============================== Create Table ============================
            function buildTable() {
                var table = $('#table-body')
                var data = pagination(state.querySet, state.page, state.rows)
                var myList = data.querySet

                for (y in myList) {

                    if (myList[y].classify_id != "" && state.page == 1) {
                        //Keep in mind we are using "Template Litterals to create rows"
                        let check = '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลบประเภท</b> <span class="tablesaw-cell-content"><a href="#delete-classify" id=' + result.id_classify[y].classify_id + ' onClick="delClassify(this.id)" data-bs-toggle="modal"><i class="fas fa-trash-alt fa-2x" style="color: red;"></i></a></td>'
                        if (result.check_delete[y] == 1) {
                            check = `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:110px"><b class="tablesaw-cell-label">ลบประเภท</b> 
                            <span class="tablesaw-cell-content"><i class="fas fa-trash-alt fa-2x" style="color:gray"></i></span></td>`
                        } else {
                            check = '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลบประเภท</b> <span class="tablesaw-cell-content"><a href="#delete-classify" id=' + result.id_classify[y].classify_id + ' onClick="delClassify(this.id)" data-bs-toggle="modal"><i class="fas fa-trash-alt fa-2x" style="color: red;"></i></a></td>'
                        }
                        var row = '<tr>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                            '</b> <span class="tablesaw-cell-content">' + myList[y].classify_id + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อประเภท</b> <span class="tablesaw-cell-content">' + myList[y].classify_name + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:250px"><b class="tablesaw-cell-label">ข้อมูลที่แยกประเภท</b> <span class="tablesaw-cell-content">' + result.classify_data_name[y] + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">วันที่สร้าง</b> <span class="tablesaw-cell-content">' + myList[y].classify_create_date + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ผู้สร้าง</b> <span class="tablesaw-cell-content">' + myList[y].firstname + " " + myList[y].lastname + '</span></td>' +
                            `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ดูประเภท</b> <span class="tablesaw-cell-content"><a href="/${Sub}classification/detail` + result.id_classify[y].classify_id + '" class="text-info"><i class="fas fa-file-alt fa-2x"></i></a></td>' +
                            `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">แก้ไขประเภท</b> <span class="tablesaw-cell-content"><a href="/${Sub}classification/edit` + result.id_classify[y].classify_id + '" class="text-warning"><i class="fas fa-pencil-alt fa-2x"></i></a></td>' +
                            check +
                            '</tr>'
                        table.append(row)
                        end_count = myList[y].classify_id
                    } else if (myList[y].classify_id != "" && state.page >= 2) {
                        let check = ``
                        if (result.check_delete[y] == 1) {
                            check = `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:110px"><b class="tablesaw-cell-label">ลบประเภท</b> 
                            <span class="tablesaw-cell-content"><i class="fas fa-trash-alt fa-2x" style="color:gray"></i></span></td>`
                        } else {
                            check = '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลบประเภท</b> <span class="tablesaw-cell-content"><a href="#delete-classify" id=' + result.id_classify[parseInt(state.rows) + parseInt(y)].classify_id + ' onClick="delClassify(this.id)" data-bs-toggle="modal"><i class="fas fa-trash-alt fa-2x" style="color: red;"></i></a></td>'
                        }
                        var row = '<tr>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                            '</b> <span class="tablesaw-cell-content">' + myList[y].classify_id + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อประเภท</b> <span class="tablesaw-cell-content">' + myList[y].classify_name + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:250px"><b class="tablesaw-cell-label">ข้อมูลที่แยกประเภท</b> <span class="tablesaw-cell-content">' + result.classify_data_name[y] + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">วันที่สร้าง</b> <span class="tablesaw-cell-content">' + myList[y].classify_create_date + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ผู้สร้าง</b> <span class="tablesaw-cell-content">' + myList[y].firstname + " " + myList[y].lastname + '</span></td>' +
                            `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ดูประเภท</b> <span class="tablesaw-cell-content"><a href="/${Sub}classification/detail` + result.id_classify[parseInt(state.rows) + parseInt(y)].classify_id + '" class="text-info"><i class="fas fa-file-alt fa-2x"></i></a></td>' +
                            `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">แก้ไขประเภท</b> <span class="tablesaw-cell-content"><a href="/${Sub}classification/edit` + result.id_classify[parseInt(state.rows) + parseInt(y)].classify_id + '" class="text-warning"><i class="fas fa-pencil-alt fa-2x"></i></a></td>' +
                            check +
                            '</tr>'
                        table.append(row)
                        end_count = myList[y].classify_id
                    }
                }
                if (myList.length == 0) {
                    var row = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล</td></tr>'
                    table.append(row)
                    $('#start').text(0)
                    $('#total').text(0)
                } else {
                    if (myList[0].classify_id != "") {
                        $('#start').text(myList[0].classify_id)
                        $('#total').text(result.classify.length)
                    }
                }
                $('#end').text(end_count)
                pageButtons(data.pages)
            }
        }
    })
    function delClassify(id) {
        let url = `/${Sub}classification/delete` + id
        $.post(`/${Sub}classification/getDel`, { value: "@lltr@Cl@ssifyGetDe1", id: id }).done(function (result) {
   
            $('td#c-name').text(result.classify[0].classify_name)
            $('td#c-data').text(result.classify_data_name)
            // $('td#c-create').text( convert_datetime(new Date(result.classify_create_date)))
            $('td#c-create').text(result.classify[0].classify_create_date + '')
            $('td#c-creator').text(result.classify[0].name)
        })
        $("button#sub-del-classify").on('click', function () {
            $('form#del-classify').attr('action', url).submit();
        })
    }
} else if (document.getElementById('c_detail')) {
    // Sidebar
    $('li#li-data-project-action').addClass('selected')
    $('a#li-data-project-action').addClass('active')
    $('ul#ul-data-project-action').addClass('in')
    $('a#a-export').addClass('active')
    $('ul#ul-export').addClass('in')
    $('a#a-classification').addClass('active')
} else if (document.getElementById('c_new')) { // Edit Hide Here
    // Sidebar
    $('li#li-data-project-action').addClass('selected')
    $('a#li-data-project-action').addClass('active')
    $('ul#ul-data-project-action').addClass('in')
    $('a#a-export').addClass('active')
    $('ul#ul-export').addClass('in')
    $('a#a-classification').addClass('active')
    // ==================== ALL FUNCTION ON NEW PAGE ===================
    $('input:checkbox').on('click', function () {
        // Modal Add User (Inside)
        if ($(this).attr('id') == 'customCheck1') {
            if ($(this).is(':checked') == true) {
                $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').removeAttr('hidden')
                $('span#user_access_information_process_inside').html(`
                &nbsp; 
                    <a href="#select-users-inside" class="option-pattern" data-bs-toggle="modal">เลือกสมาชิกในระบบ Alltra</a>&nbsp; &nbsp;
                    <span class="h6" style="font-weight: bold;">รวม</span>
                    <input type="text" name="classify_user_access_info_process_inside_from_new_total" class="form-control" style="display: inline; width: 15%;" placeholder="กรุณาเลือกสมาชิก..." readonly/>&nbsp;
                    <span class="h6" style="font-weight: bold;">คน</span>&nbsp;&nbsp;&nbsp;
                    <span class="span-image" id='user_inside'></span>
                    <input type="text" name="classify_user_access_info_process_inside_from_new_id" class="form-control input-all-number" readonly hidden/>&nbsp;
                `)
                var total_users = []
                var count_click2 = 0
                $('a[href="#select-users-inside"]').on('click', function () {
                    $.post(`/${Sub}users`, { value: "@lltr@Cl@ssific@ti0nUser" }, function (result) {
                        count_click2 += 1
                        // ============================== Create Prepare ============================
                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                        var state = {
                            'querySet': result.users,
                            'page': 1,
                            'rows': 5,
                            'window': 5,
                        }
                        if (count_click2 == 1) {
                            buildTable()
                        }
                        function pagination(querySet, page, rows) {

                            var trimStart = (page - 1) * rows
                            var trimEnd = trimStart + rows

                            var trimmedData = querySet.slice(trimStart, trimEnd)

                            var pages = Math.ceil(querySet.length / rows);

                            var start_count = 1
                            document.getElementById('start-user-inside-add').innerHTML = start_count

                            return {
                                'querySet': trimmedData,
                                'pages': pages,
                            }
                        }
                        // ============================== Create Pagination ============================
                        function pageButtons(pages) {
                            var wrapper = document.getElementById('pagination-wapper-user-inside-add')
                            wrapper.innerHTML = ``
                            var maxLeft = (state.page - Math.floor(state.window / 2))
                            var maxRight = (state.page + Math.floor(state.window / 2))

                            if (maxLeft < 1) {
                                maxLeft = 1
                                maxRight = state.window
                            }

                            if (maxRight > pages) {
                                maxLeft = pages - (state.window - 1)
                                if (maxLeft < 1) {
                                    maxLeft = 1
                                }
                                maxRight = pages
                            }

                            if (state.page > 1) {
                                wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                            } else {
                                wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                            }

                            num = 1
                            if (maxRight > 5) {
                                if (state.page > (maxRight / 2)) {
                                    if ((state.page + 1) > (maxRight / 2)) {
                                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                    }
                                }
                                for (var page = maxLeft; page <= maxRight; page++) {
                                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                                        if (page == state.page) {
                                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                                        }
                                        else {
                                            p = page - 1;
                                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                                        }
                                    }
                                }
                                if ((state.page) <= (maxRight / 2)) {
                                    mp = maxRight - 1;
                                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                                }
                            }
                            else {
                                for (var page = maxLeft; page <= maxRight; page++) {
                                    if (state.page == page) {
                                        wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                                    } else {
                                        wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                                    }
                                }
                            }

                            if (state.page < pages) {
                                wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                            } else {
                                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                            }

                            $('.page').on('click', function () {
                                $('#table-body-user-inside-add').empty()
                                state.page = Number($(this).val())
                                // document.getElementById('page').value = state.page
                                buildTable()
                            })
                        }
                        // ============================== Create Table ============================
                        function buildTable() {
                            var table = $('#table-body-user-inside-add')
                            var data = pagination(state.querySet, state.page, state.rows)
                            var myList = data.querySet
                            for (y in myList) {
                                if (myList[y].acc_id != "") {
                                    let buttonAdd = ""
                                    let buttonDel = ""
                                    let total_users = $('input[name="classify_user_access_info_process_inside_from_new_id"]').val().split(',')
                                    if (state.page == 1) {
                                        if (checkUsers(total_users, result.id_users[y].acc_id, 0) == true) {
                                            buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                            buttonDel = '<a id="_del_users_inside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                        } else {
                                            buttonAdd = '<a id="_add_users_inside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                            buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                        }
                                    } else {
                                        if (checkUsers(total_users, result.id_users[parseInt(state.rows) + parseInt(y)].acc_id, 0) == true) {
                                            buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                            buttonDel = '<a id="_del_users_inside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                        } else {
                                            buttonAdd = '<a id="_add_users_inside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                            buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                        }
                                    }
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<th style="text-align: left; vertical-align: center;">' +
                                        'ชื่อ-นามสกุล: ' + myList[y].firstname + " " + myList[y].lastname + " <br/>" +
                                        '</th>' +
                                        '<td style="vertical-align: middle; width: 50px;">' +
                                        buttonAdd +
                                        '</td>' +
                                        '<td style="vertical-align: middle; width: 50px;">' +
                                        buttonDel +
                                        '</td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].acc_id
                                }
                            }
                            if (myList.length == 0) {
                                var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                                table.append(row)
                                $('#total-user-inside-add').text(0)
                                $('#start-user-inside-add').html(0)
                            } else {
                                if (myList[0].user_id) {
                                    $('#start-user-inside-add').text(myList[0].acc_id)
                                }
                                pageButtons(data.pages)
                                $('#total-user-inside-add').text(result.users.length)
                            }
                            $('#end-user-inside-add').html(end_count)

                            $('a#_add_users_inside').on('click', function () {
                                $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                    total_users.push({ "id": result[0].acc_id, "image": result[0].image })
                                    $('span#user_inside').empty()
                                    var length_users = new Set(total_users)
                                    total_users = [...length_users]
                                    $('input[name="classify_user_access_info_process_inside_from_new_total"]').val(total_users.length)
                                    $('input[name="classify_user_access_info_process_inside_from_new_id"]').val(total_users.map(e => e['id']))
                                    for (var k = 0; k < total_users.length; k++) {
                                        // Image = 600x600
                                        if (k < total_users.length - 1) {
                                            $('span#user_inside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/> &nbsp;')
                                        } else {
                                            $('span#user_inside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/>')
                                        }
                                    }
                                    $('#table-body-user-inside-add').empty()
                                    buildTable()
                                })
                            })
                            $('a#_del_users_inside').on('click', function () {
                                $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                    let new_total_user = total_users.filter(function (item) { return item.id.toString().indexOf(result[0].acc_id) })
                                    total_users = new_total_user
                                    $('input[name="classify_user_access_info_process_inside_from_new_total"]').val(total_users.length)
                                    $('input[name="classify_user_access_info_process_inside_from_new_id"]').val(total_users.map(e => e['id']))
                                    for (var k = 0; k < $('span#user_inside').children().length; k++) {
                                        let old_image = $('span#user_inside').children()[k].getAttribute('data-mark')
                                        if (old_image == result[0].acc_id) {
                                            $('span#user_inside').children()[k].remove()
                                        }
                                    }
                                    $('#table-body-user-inside-add').empty()
                                    buildTable()
                                    if ($('input[name="classify_user_access_info_process_inside_from_new_total"]').val() == "0") {
                                        $('span#user_inside').empty()
                                    }
                                })
                            })
                            //$('input[name="classify_user_access_info_process_inside_from_new_total"]').on('click keyup keydown focus', function () {
                            //    if ($(this).val() == "" || $(this).val() == null) {
                            //        $('span#user_inside').empty()
                            //    } else if ($(this).val() != String(total_image.length)) {
                            //        $('span#user_inside').last().remove()
                            //    }
                            //})
                        }
                    })
                })
            } else {
                $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').attr('hidden', 'hidden')
                $('span#user_access_information_process_inside').empty()
                $('tbody#table-body-user-inside-add').empty()
                $('ul#pagination-wapper-user-inside-add').empty()
                $('span#start-user-inside-add').empty()
                $('span#end-user-inside-add').empty()
                $('span#total-user-inside-add').empty()
            }
        }
        // Modal Add User (Outside)
        if ($(this).attr('id') == 'customCheck2') {
            if ($(this).is(':checked') == true) {
                $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').removeAttr('hidden')
                $('span#user_access_information_process_outside').html(`
                &nbsp;
                    <a href="#select-users-outside" class="option-pattern" data-bs-toggle="modal">เลือกสมาชิกในระบบ Alltra</a>&nbsp; &nbsp;
                    <span class="h6" style="font-weight: bold;">รวม</span>
                    <input type="text" name="classify_user_access_info_process_outside_from_new_total" class="form-control" style="display: inline; width: 15%;" placeholder="กรุณาเลือกสมาชิก..." readonly/>&nbsp;
                    <span class="h6" style="font-weight: bold;">คน</span>&nbsp;&nbsp;&nbsp;
                    <span class="span-image" id="user_outside">

                    </span>
                    <input type="text" name="classify_user_access_info_process_outside_from_new_id" class="form-control input-all-number" readonly hidden/>&nbsp;
                `)
                var total_users = []
                var count_click3 = 0
                $('a[href="#select-users-outside"]').on('click', function () {
                    $.post(`/${Sub}users`, { value: "@lltr@Cl@ssific@ti0nUser" }, function (result) {
                        count_click3 += 1
                        // ============================== Create Prepare ============================
                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                        var state = {
                            'querySet': result.users,
                            'page': 1,
                            'rows': 5,
                            'window': 5,
                        }
                        if (count_click3 == 1) {
                            buildTable()
                        }
                        function pagination(querySet, page, rows) {

                            var trimStart = (page - 1) * rows
                            var trimEnd = trimStart + rows

                            var trimmedData = querySet.slice(trimStart, trimEnd)

                            var pages = Math.ceil(querySet.length / rows);

                            var start_count = 1
                            document.getElementById('start-users-outside-add').innerHTML = start_count

                            return {
                                'querySet': trimmedData,
                                'pages': pages,
                            }
                        }
                        // ============================== Create Pagination ============================
                        function pageButtons(pages) {
                            var wrapper = document.getElementById('pagination-wapper-users-outside-add')
                            wrapper.innerHTML = ``
                            var maxLeft = (state.page - Math.floor(state.window / 2))
                            var maxRight = (state.page + Math.floor(state.window / 2))

                            if (maxLeft < 1) {
                                maxLeft = 1
                                maxRight = state.window
                            }

                            if (maxRight > pages) {
                                maxLeft = pages - (state.window - 1)
                                if (maxLeft < 1) {
                                    maxLeft = 1
                                }
                                maxRight = pages
                            }

                            if (state.page > 1) {
                                wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                            } else {
                                wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                            }

                            num = 1
                            if (maxRight > 5) {
                                if (state.page > (maxRight / 2)) {
                                    if ((state.page + 1) > (maxRight / 2)) {
                                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                    }
                                }
                                for (var page = maxLeft; page <= maxRight; page++) {
                                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                                        if (page == state.page) {
                                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                                        }
                                        else {
                                            p = page - 1;
                                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                                        }
                                    }
                                }
                                if ((state.page) <= (maxRight / 2)) {
                                    mp = maxRight - 1;
                                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                                }
                            }
                            else {
                                for (var page = maxLeft; page <= maxRight; page++) {
                                    if (state.page == page) {
                                        wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                                    } else {
                                        wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                                    }
                                }
                            }

                            if (state.page < pages) {
                                wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                            } else {
                                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                            }

                            $('.page').on('click', function () {
                                $('#table-body-users-outside-add').empty()
                                state.page = Number($(this).val())
                                // document.getElementById('page').value = state.page
                                buildTable()
                            })
                        }
                        // ============================== Create Table ============================
                        function buildTable() {
                            var table = $('#table-body-users-outside-add')
                            var data = pagination(state.querySet, state.page, state.rows)
                            var myList = data.querySet
                            for (y in myList) {
                                if (myList[y].acc_id != "") {
                                    let buttonAdd = ""
                                    let buttonDel = ""
                                    let total_users = $('input[name="classify_user_access_info_process_outside_from_new_id"]').val().split(',')
                                    if (state.page == 1) {
                                        if (checkUsers(total_users, result.id_users[y].acc_id, 0) == true) {
                                            buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                            buttonDel = '<a id="_del_users_outside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                        } else {
                                            buttonAdd = '<a id="_add_users_outside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                            buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                        }
                                    } else {
                                        if (checkUsers(total_users, result.id_users[parseInt(state.rows) + parseInt(y)].acc_id, 0) == true) {
                                            buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                            buttonDel = '<a id="_del_users_outside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                        } else {
                                            buttonAdd = '<a id="_add_users_outside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                            buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                        }
                                    }
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<th style="text-align: left; vertical-align: center;">' +
                                        'ชื่อ-นามสกุล: ' + myList[y].firstname + " " + myList[y].lastname + " <br/>" +
                                        '</th>' +
                                        '<td style="vertical-align: middle; width: 50px;">' +
                                        buttonAdd +
                                        '</td>' +
                                        '<td style="vertical-align: middle; width: 50px;">' +
                                        buttonDel +
                                        '</td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].acc_id
                                }
                            }
                            if (myList.length == 0) {
                                var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                                table.append(row)
                                $('#total-users-outside-add').text(0)
                                $('#start-users-outside-add').html(0)
                            } else {
                                if (myList[0].acc_id) {
                                    $('#start-users-outside-add').text(myList[0].acc_id)
                                }
                                pageButtons(data.pages)
                                $('#total-users-outside-add').text(result.users.length)
                            }

                            $('#end-users-outside-add').html(end_count)

                            $('a#_add_users_outside').on('click', function () {
                                $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                    $('span#user_outside').empty()
                                    total_users.push({ "id": result[0].acc_id, "image": result[0].image })
                                    var length_users = new Set(total_users)
                                    total_users = [...length_users]
                                    $('input[name="classify_user_access_info_process_outside_from_new_total"]').val(total_users.length)
                                    $('input[name="classify_user_access_info_process_outside_from_new_id"]').val(total_users.map(e => e['id']))
                                    for (var k = 0; k < total_users.length; k++) {
                                        // Image = 600x600
                                        if (k < total_users.length - 1) {
                                            $('span#user_outside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/> &nbsp;')
                                        } else {
                                            $('span#user_outside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/>')
                                        }
                                    }
                                    $('#table-body-users-outside-add').empty()
                                    buildTable()
                                })
                            })
                            $('a#_del_users_outside').on('click', function () {
                                $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                    let new_total_users = total_users.filter(function (item) { return item.id.toString().indexOf(result[0].acc_id) })
                                    total_users = new_total_users
                                    $('input[name="classify_user_access_info_process_outside_from_new_total"]').val(total_users.length)
                                    $('input[name="classify_user_access_info_process_outside_from_new_id"]').val(total_users.map(e => e['id']))
                                    for (var k = 0; k < $('span#user_outside').children().length; k++) {
                                        let old_image = $('span#user_outside').children()[k].getAttribute('data-mark')
                                        if (old_image == result[0].acc_id) {
                                            $('span#user_outside').children()[k].remove()
                                        }
                                    }
                                    if ($('input[name="classify_user_access_info_process_outside_from_new_total"]').val() == 0) {
                                        $('span#user_outside').empty()
                                    }
                                    $('#table-body-users-outside-add').empty()
                                    buildTable()
                                })
                            })
                            //$('input[name="classify_user_access_info_process_outside_from_new_total"]').on('click keyup keydown focus', function () {
                            //    if ($(this).val() == "" || $(this).val() == null) {
                            //        $('span#user_outside').empty()
                            //    }
                            //})
                        }
                    })
                })
            } else {
                $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').attr('hidden', 'hidden')
                $('span#user_access_information_process_outside').empty()
                $('tbody#table-body-users-outside-add').empty()
                $('ul#pagination-wapper-users-outside-add').empty()
                $('span#start-users-outside-add').empty()
                $('span#end-users-outside-add').empty()
                $('span#total-users-outside-add').empty()
            }
        }
        if ($(this).attr('id') == 'customCheck3') {
            if ($(this).is(':checked') == true) {
                $('span#period_process_follow_policy').html(`
                <div class="input-group">
                <input type="text" name="classify_period_proccess_follow_policy_total" placeholder="" class="form-control" value="180"  readonly/>
                                        <span class="input-group-text">
                                        วัน
                                        </span>
                                    </div>
                `)
            } else {
                $('span#period_process_follow_policy').empty()
            }
        }
        if ($(this).attr('id') == 'customCheck4') {
            if ($(this).is(':checked') == true) {
                $('input[name="classify_period_end_follow_pattern_total"]').removeAttr('hidden')
                $('span#day_end_pattern').removeAttr('hidden')
            } else {
                $('input[name="classify_period_end_follow_pattern_total"]').attr('hidden', true)
                $('span#day_end_pattern').attr('hidden', true)
            }
        }
    })
    if ($('input:radio[name="classify_risk_assess_only_dpo_data_number_all_used_process_many"]').find('checked')) {
        $('span#risk_assess_only_dpo_data_number_all_used_process_total').html(`
        <div class="row"><div>ระบุ</div><div><input type="text" name="classify_risk_assess_only_dpo_data_number_all_used_process_total" placeholder="กรุณาป้อนจำนวน..." class="form-control"  /> </div></div> 
        `)
    }
    $('input:radio').on('click', function () {
        if ($(this).attr("name") == 'classify_risk_assess_only_dpo_data_number_all_used_process_many') {
            if ($(this).attr('id') == "customRadio5" || $(this).attr('id') == "customRadio4") {
                $('span#classification_risk_assessment_only_dpo_data_number_all_used_process_total').html(`
                <div class="row"><div>ระบุ</div><div><input type="text" name="classify_risk_assess_only_dpo_data_number_all_used_process_total" placeholder="" class="form-control input-radio1" /> </div></div> 
                `)
            }
        }
        //       if ($(this).attr('name') == 'classify_risk_assess_only_dpo_assess_the_impact_of_data') {
        //           if ($(this).attr('id') == "customRadio14" || $(this).attr('id') == "customRadio15" || $(this).attr('id') == "customRadio16") {
        //               $('div#classification_risk_assessment_only_dpo_fix_a_leak_of_data').html(`
        //           <label class="form-check-label h6"
        //                                                                   style="font-weight: bold;">วิธีแก้ไขเมื่อเกิดข้อมูลรั่วไหลหรือผลกระทบต่อเจ้าของข้อมูล</label>
        //                                                               <div>
        //                                                                   <input type="text" name="classify_risk_assess_only_dpo_fix_a_leak_of_data" id="" style="width: 47%;" placeholder="กรุณาป้อนวิธีแก้ไข..." class="form-control">
        //                                                               </div>
        //           `)
        //           }
        //       }
        //       if ($(this).attr('name') == 'classify_risk_assess_only_dpo_assess_the_impact_of_organization') {
        //           if ($(this).attr('id') == "customRadio17" || $(this).attr('id') == "customRadio18" || $(this).attr('id') == "customRadio19") {
        //               $('div#classification_risk_assessment_only_dpo_fix_a_leak_of_organization').html(`
        //           <label class="form-check-label h6"
        //           style="font-weight: bold;">วิธีแก้ไขเมื่อเกิดข้อมูลรั่วไหลหรือผลกระทบต่อองค์กร</label>
        //       <div>
        //           <input type="text" name="classify_risk_assess_only_dpo_fix_a_leak_of_organization" id="" style="width: 47%;" placeholder="กรุณาป้อนวิธีแก้ไข..." class="form-control">
        //       </div>
        //           `)
        //           }
        //       }
        if ($(this).attr('name') == 'classify_type_data_in_event_personal_datamark_check_xxxxxxxxxxxxxxxxxxxxxx') {
            if ($(this).val() == 1) {
                $('span#event_personal_datamark').html(`
                <table class="table table-boardless">
                    <td width="40%">
                        <input type="radio" id="not_use" name="classify_type_data_in_event_personal_datamark1" class="form-check-input" value="3" />
                        <label for="not_use">Anonymized Data (ไม่ระบุข้อมูล) </label>
                    </td>
                    <td width="50%">
                        <input type="radio" id="not_use1" name="classify_type_data_in_event_personal_datamark1" class="form-check-input" checked />
                        <label for="not_used1">Pseudonymized Data (ข้อมูลที่มีการแฝง) </label>
                    </td> 
                </table>
                <span id="pseudonymized"></span>
                <span id="total-pseudonymized"></span>
                `)
                if ($('input:radio[name="classify_type_data_in_event_personal_datamark1"]:checked').length > 0) {
                    if ($('input:radio[name="classify_type_data_in_event_personal_datamark1"]:checked').attr('id') == 'not_use1') {
                        $('span#pseudonymized').html(`
                            <table class="table table-boardless" id="pseudonymized">
                                <td width="32%">
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark1" value="0" class="form-check-input" checked />
                                    <label for="datamark1" > หัวข้อมูลอย่างเดียว </label>
                                </td>
                                <td width="30%">
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark2" value="1" class="form-check-input" />
                                    <label for="datamark2" > หัวและท้ายข้อมูล </label>
                                </td>
                                <td>
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark3" value="2" class="form-check-input" />
                                    <label for="datamark3" > ท้ายข้อมูลอย่างเดียว </label>
                                </td>
                            </table>
                            `)
                        $('span#total-pseudonymized').html(`
                            <table class="table table-boardless" id="total-pseudonymized">
                                <td style="width: 35%; text-align: right;">
                                    <label for="total-digit"> จำนวนตัวอักษรที่แสดง:  </label>
                                </td>
                                <td>
                                    <input type="number" min="1" name="classify_type_data_in_event_personal_datamark_total" placeholder="กรุณาป้อนจำนวน..." class="form-control" />
                                </td>
                            </table>
                            <span id="example-datamark"></span>
                            `)
                        let seleced = $('input:radio[name="classify_type_data_in_event_personal_datamark"]:checked')
                        if (seleced.attr('id') == 'datamark1') {
                            $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["****ย่าง"]
                            </td>
                        </table>
                        `)
                        } else if (seleced.attr('id') == 'datamark2') {
                            $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">3</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["***ย***"]
                            </td>
                        </table>
                        `)
                        } else if (seleced.attr('id') == 'datamark3') {
                            $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["ตัวอ****"]
                            </td>
                        </table>
                        `)
                        }
                    }
                }
                $('input:radio[name="classify_type_data_in_event_personal_datamark1"]').on('click', function () {
                    if ($(this).attr('id') == 'not_use') {
                        $('span#pseudonymized').empty();
                        $('span#total-pseudonymized').empty();
                    } else {
                        $('span#pseudonymized').html(`
                            <table class="table table-boardless" id="pseudonymized">
                                <td width="32%">
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark1" value="0" class="form-check-input" checked />
                                    <label for="datamark1" > หัวข้อมูลอย่างเดียว </label>
                                </td>
                                <td width="30%">
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark2" value="1" class="form-check-input" />
                                    <label for="datamark2" > หัวและท้ายข้อมูล </label>
                                </td>
                                <td>
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark3" value="2" class="form-check-input" />
                                    <label for="datamark3" > ท้ายข้อมูลอย่างเดียว </label>
                                </td>
                            </table>
                            `)
                        $('span#total-pseudonymized').html(`
                            <table class="table table-boardless" id="total-pseudonymized">
                                <td style="width: 35%; text-align: right;">
                                    <label for="total-digit"> จำนวนตัวอักษรที่แสดง:  </label>
                                </td>
                                <td>
                                    <input type="number" min="1" name="classify_type_data_in_event_personal_datamark_total" placeholder="กรุณาป้อนจำนวน..." class="form-control" />
                                </td>
                            </table>
                            <span id="example-datamark"></span>
                            `)
                        let seleced = $('input:radio[name="classify_type_data_in_event_personal_datamark"]:checked')
                        if (seleced.attr('id') == 'datamark1') {
                            $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["****ย่าง"]
                            </td>
                        </table>
                        `)
                        } else if (seleced.attr('id') == 'datamark2') {
                            $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">3</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["***ย***"]
                            </td>
                        </table>
                        `)
                        } else if (seleced.attr('id') == 'datamark3') {
                            $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["ตัวอ****"]
                            </td>
                        </table>
                        `)
                        }
                    }
                    $('input:radio[name="classify_type_data_in_event_personal_datamark"]').on('click', function () {
                        if ($(this).attr('id') == 'datamark1') {
                            $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["****ย่าง"]
                            </td>
                        </table>
                        `)
                        } else if ($(this).attr('id') == 'datamark2') {
                            $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">3</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["***ย***"]
                            </td>
                        </table>
                        `)
                        } else if ($(this).attr('id') == 'datamark3') {
                            $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["ตัวอ****"]
                            </td>
                        </table>
                        `)
                        }
                    })
                })
                $('input:radio[name="classify_type_data_in_event_personal_datamark"]').on('click', function () {
                    if ($(this).attr('id') == 'datamark1') {
                        $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;">ทำเครื่องหมาย</span> ["****ย่าง"]
                            </td>
                        </table>
                        `)
                    } else if ($(this).attr('id') == 'datamark2') {
                        $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">3</span> <br/> ["ตัวอย่าง"] <span style="color: red;">ทำเครื่องหมาย</span> ["***ย***"]
                            </td>
                        </table>
                        `)
                    } else if ($(this).attr('id') == 'datamark3') {
                        $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;">ทำเครื่องหมาย</span> ["ตัวอ****"]
                            </td>
                        </table>
                        `)
                    }
                })
            } else {
                $('span#event_personal_datamark').empty();
            }
        }
    })
    // Modal Add Pattern
    var count_click = 0
    $('a[href="#select-pattern"]').on('click', function () {
        $.post(`/${Sub}pattern`, function (result) {
            count_click += 1
            // ============================== Create Prepare ============================
            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
            var state = {
                'querySet': result.pattern,
                'page': 1,
                'rows': 5,
                'window': 5,
            }
            if (count_click == 1) {
                buildTable()
            }
            function pagination(querySet, page, rows) {

                var trimStart = (page - 1) * rows
                var trimEnd = trimStart + rows

                var trimmedData = querySet.slice(trimStart, trimEnd)

                var pages = Math.ceil(querySet.length / rows);

                var start_count = 1
                document.getElementById('start-pattern-add').innerHTML = start_count

                return {
                    'querySet': trimmedData,
                    'pages': pages,
                }
            }
            // ============================== Create Pagination ============================
            function pageButtons(pages) {
                var wrapper = document.getElementById('pagination-wapper-pattern-add')
                wrapper.innerHTML = ``
                var maxLeft = (state.page - Math.floor(state.window / 2))
                var maxRight = (state.page + Math.floor(state.window / 2))

                if (maxLeft < 1) {
                    maxLeft = 1
                    maxRight = state.window
                }

                if (maxRight > pages) {
                    maxLeft = pages - (state.window - 1)
                    if (maxLeft < 1) {
                        maxLeft = 1
                    }
                    maxRight = pages
                }

                if (state.page > 1) {
                    wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                } else {
                    wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                }


                num = 1
                if (maxRight > 5) {
                    if (state.page > (maxRight / 2)) {
                        if ((state.page + 1) > (maxRight / 2)) {
                            wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                            wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        }
                    }
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if ((page >= state.page - 2) && (page <= state.page + 2)) {
                            if (page == state.page) {
                                wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                            else {
                                p = page - 1;
                                wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                        }
                    }
                    if ((state.page) <= (maxRight / 2)) {
                        mp = maxRight - 1;
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                    }
                }
                else {
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if (state.page == page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                        }
                    }
                }

                if (state.page < pages) {
                    wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                } else {
                    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                }

                $('.page').on('click', function () {
                    $('#table-body-pattern-add').empty()
                    state.page = Number($(this).val())
                    // document.getElementById('page').value = state.page
                    buildTable()
                })
            }
            // ============================== Create Table ============================
            function buildTable() {
                var table = $('#table-body-pattern-add')
                var data = pagination(state.querySet, state.page, state.rows)
                var myList = data.querySet
                for (y in myList) {
                    if (myList[y].pattern_id != "") {
                        let buttonAdd = ""
                        let buttonDel = ""
                        if (state.page == 1) {
                            if (result.id_pattern[y].pattern_id == $('input[name="pattern_id"]').val()) {
                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                buttonDel = '<a id="_del_pattern" href="javascript:void(0);" data-value=' + result.id_pattern[y].pattern_id + '  class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                            } else {
                                buttonAdd = '<a id="_add_pattern" href="javascript:void(0);" data-value=' + result.id_pattern[y].pattern_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                            }
                        } else {
                            if (result.id_pattern[parseInt(state.rows) + parseInt(y)].pattern_id == $('input[name="pattern_id"]').val()) {
                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                buttonDel = '<a id="_del_pattern" href="javascript:void(0);" data-value=' + result.id_pattern[parseInt(state.rows) + parseInt(y)].pattern_id + '  class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                            } else {
                                buttonAdd = '<a id="_add_pattern" href="javascript:void(0);" data-value=' + result.id_pattern[parseInt(state.rows) + parseInt(y)].pattern_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                            }
                        }
                        //Keep in mind we are using "Template Litterals to create rows"
                        var row = '<tr>' +
                            '<th style="vertical-align: center;">' +
                            'ชื่อรูปแบบ: ' + myList[y].pattern_name + " <br/>" +
                            '</th>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonAdd +
                            '</td>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonDel +
                            '</td>' +
                            '</tr>'
                        table.append(row)
                        end_count = myList[y].pattern_id
                    }
                }
                if (myList.length == 0) {
                    var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                    table.append(row)
                    $('#total-pattern-add').text(0)
                    $('#start-pattern-add').html(0)
                } else {
                    if (myList[0].doc_id != "" && myList[0].doc_action != 1 && myList[0].doc_status == 2) {
                        $('#start-pattern-add').text(myList[0].pattern_id)
                    }
                    pageButtons(data.pages)
                    $('#total-pattern-add').text(result.pattern.length)
                }
                $('#end-pattern-add').html(end_count)

                $('a#_add_pattern').on('click', function () {
                    $.post(`/${Sub}pattern/select-pattern`, { value: "@lltr@Se1ectP@ttern", id: $(this).attr('data-value') }).done(function (result) {
                        $('input[name="pattern_id"]').val(result.pattern[0].pattern_id)
                        $('select[name="pattern_processing_base_id"]').find('option:selected').removeAttr('selected')
                        $('input[name="classify_name_part1"]').val(result.pattern[0].pattern_name)
                        $('input[name="classify_data_exception_or_unnecessary_filter_name"]').val(result.pattern[0].pattern_name)
                        $('input[name="classify_data_exception_or_unnecessary_filter_tag"]').val(result.pattern[0].pattern_tag)
                        $('input[name="classify_data_exception_or_unnecessary_filter_label"').val(result.pattern[0].pattern_label)
                        let fullname = result.total_name_inside.map(e => e.join(" "))
                        $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').val([...fullname])
                        $('input[name="classify_user_access_info_process_inside_from_pattern"]').val(result.pattern[0].pattern_processor_inside_id)
                        let fullname_outside = result.total_name_outside.map(e => e.join(" "))
                        $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').val([...fullname_outside])
                        $('input[name="classify_user_access_info_process_outside_from_pattern"]').val(result.pattern[0].pattern_processor_outside_id)
                        var _file_ = ""
                        var _database_ = ""
                        if (result.pattern[0].pattern_type_data_file == 1) {
                            _file_ = "มี"
                        } else {
                            _file_ = "ไม่มี"
                        }
                        if (result.pattern[0].pattern_type_database == 1) {
                            _database_ = result.pattern[0].pattern_type_database_name
                        } else {
                            _database_ = "ไม่มี"
                        }
                        var _inside_ = ""
                        var _outside_0 = ""
                        var _outside_1 = ""
                        var _outside_2 = ""
                        var _outside_3 = ""
                        if (result.pattern[0].pattern_storage_method_inside == 1) {
                            _inside_ = "มี"
                        } else {
                            _inside_ = "ไม่มี"
                        }
                        if (result.pattern[0].pattern_storage_method_outside == 1) {
                            _outside_0 = "มี"
                            if (result.pattern[0].pattern_storage_method_outside_device == 1) {
                                _outside_1 = result.pattern[0].pattern_storage_method_outside_device_name
                            } else {
                                _outside_1 = "ไม่มี"
                            }
                            if (result.pattern[0].pattern_storage_method_outside_agent == 1) {
                                _outside_2 = result.pattern[0].pattern_storage_method_outside_agent_name
                            } else {
                                _outside_2 = "ไม่มี"
                            }
                            if (result.pattern[0].pattern_storage_method_outside_database_outside == 1) {
                                _outside_3 = result.pattern[0].pattern_storage_method_outside_database_outside_name
                            } else {
                                _outside_3 = "ไม่มี"
                            }
                        } else {
                            _outside_0 = "ไม่มี"
                        }
                        var data_name_total = result.data_name_total.map(e => e['data_name'])
                        data_name_total = data_name_total.join(", ")
                        $('textarea[name="example_pattern"]').text(`ชื่อ: ${result.pattern[0].pattern_name}
                       
ข้อมูล: ${data_name_total}
ระยะเวลา: ${convert_date(new Date(result.pattern[0].pattern_start_date))} ทั้งหมด: ${result.pattern[0].pattern_total_date} วัน
ชนิดข้อมูล:
    File Ms Excel/CSV: ${_file_}
    Database: ${_database_}
วิธีการจัดเก็บข้อมูลส่วนบุคคล:
    ภายใน Alltra: ${_inside_}
    ภายนอก: ${_outside_0}, [ (อุปกรณ์: ${_outside_1}),  (Agent: ${_outside_2}),  (Database: ${_outside_3}) ]`)
                        $('#span_example_pattern').html()
                        check_doc_id_pattern();
                        $('input[name="classify_period_end_follow_pattern_total"]').val(result.pattern[0].pattern_total_date)
                        $('select[name="pattern_processing_base_id"]').find('option[value=' + result.pattern[0].pattern_processing_base_id + ']').attr('selected', 'selected')
                        $('div#select-pattern').modal('hide')
                        $('#table-body-pattern-add').empty()
                        buildTable()
                    })
                })
                $('a#_del_pattern').on('click', function () {
                    let selected = $(this).attr('data-value')
                    $('input[name="pattern_id"]').val(null)
                    $('select[name="pattern_processing_base_id"]').find('option:selected').removeAttr('selected')
                    $('input[name="classify_name_part1"]').val(null)
                    $('input[name="classify_data_exception_or_unnecessary_filter_name"]').val(null)
                    $('input[name="classify_data_exception_or_unnecessary_filter_tag"]').val(null)
                    $('input[name="classify_data_exception_or_unnecessary_filter_label"').val(null)
                    $('input[name="classify_user_access_info_process_inside_from_pattern"]').val(null)
                    $('input[name="classify_user_access_info_process_outside_from_pattern"]').val(null)
                    $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').val(null)
                    $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').val(null)
                    $('textarea[name="example_pattern"]').text(null)
                    check_doc_id_pattern();
                    $('select[name="pattern_processing_base_id"]').find('option:disabled').attr('selected', 'selected')
                    $('input[name="classify_period_end_follow_pattern_total"]').val(null)
                    $('#table-body-pattern-add').empty()
                    buildTable()
                })
            }
        })
    })
    // Modal Select Event-process
    var count_click1 = 0
    $('a[href="#select-event-process"]').on('click', function () {
        $.post(`/${Sub}event_process`, { value: "@lltr@Cl@ssific@ti0nEventPr0cess" }, function (result) {
            count_click1 += 1
            // ============================== Create Prepare ============================
            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
            var state = {
                'querySet': result.event,
                'page': 1,
                'rows': 5,
                'window': 5,
            }
            if (count_click1 == 1) {
                buildTable()
            }
            function pagination(querySet, page, rows) {

                var trimStart = (page - 1) * rows
                var trimEnd = trimStart + rows

                var trimmedData = querySet.slice(trimStart, trimEnd)

                var pages = Math.ceil(querySet.length / rows);

                var start_count = 1
                document.getElementById('start-event-add').innerHTML = start_count

                return {
                    'querySet': trimmedData,
                    'pages': pages,
                }
            }
            // ============================== Create Pagination ============================
            function pageButtons(pages) {
                var wrapper = document.getElementById('pagination-wapper-event-add')
                wrapper.innerHTML = ``
                var maxLeft = (state.page - Math.floor(state.window / 2))
                var maxRight = (state.page + Math.floor(state.window / 2))

                if (maxLeft < 1) {
                    maxLeft = 1
                    maxRight = state.window
                }

                if (maxRight > pages) {
                    maxLeft = pages - (state.window - 1)
                    if (maxLeft < 1) {
                        maxLeft = 1
                    }
                    maxRight = pages
                }

                if (state.page > 1) {
                    wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                } else {
                    wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                }


                num = 1
                if (maxRight > 5) {
                    if (state.page > (maxRight / 2)) {
                        if ((state.page + 1) > (maxRight / 2)) {
                            wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                            wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        }
                    }
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if ((page >= state.page - 2) && (page <= state.page + 2)) {
                            if (page == state.page) {
                                wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                            else {
                                p = page - 1;
                                wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                        }
                    }
                    if ((state.page) <= (maxRight / 2)) {
                        mp = maxRight - 1;
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                    }
                }
                else {
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if (state.page == page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                        }
                    }
                }

                if (state.page < pages) {
                    wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                } else {
                    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                }

                $('.page').on('click', function () {
                    $('#table-body-event-add').empty()
                    state.page = Number($(this).val())
                    // document.getElementById('page').value = state.page
                    buildTable()
                })
            }
            // ============================== Create Table ============================
            function buildTable() {
                var table = $('#table-body-event-add')
                var data = pagination(state.querySet, state.page, state.rows)
                var myList = data.querySet
                for (y in myList) {
                    if (myList[y].event_process_id != "") {
                        let buttonAdd = ""
                        let buttonDel = ""
                        if (state.page == 1) {
                            if (myList[y].event_process_name == $('input[name="classify_name_part2"]').val() && result.id_event[y].event_process_id == $('input[name="event_process_id"]').val()) {
                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                buttonDel = '<a id="_del_event" href="javascript:void(0);" data-value=' + result.id_event[y].event_process_id + '  class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                            } else {
                                buttonAdd = '<a id="_add_event" href="javascript:void(0);" data-value=' + result.id_event[y].event_process_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                            }
                        } else {
                            if (myList[y].event_process_name == $('input[name="classify_name_part2"]').val() && result.id_event[parseInt(state.rows) + parseInt(y)].event_process_id == $('input[name="event_process_id"]').val()) {
                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                buttonDel = '<a id="_del_event" href="javascript:void(0);" data-value=' + result.id_event[parseInt(state.rows) + parseInt(y)].event_process_id + '  class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                            } else {
                                buttonAdd = '<a id="_add_event" href="javascript:void(0);" data-value=' + result.id_event[parseInt(state.rows) + parseInt(y)].event_process_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                            }
                        }
                        //Keep in mind we are using "Template Litterals to create rows"
                        var row = '<tr>' +
                            '<th style="vertical-align: center;">' +
                            'ชื่อกิจกรรม: ' + myList[y].event_process_name + " <br/>" +
                            '</th>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonAdd +
                            '</td>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonDel +
                            '</td>' +
                            '</tr>'
                        table.append(row)
                        end_count = myList[y].event_process_id
                    }
                }
                if (myList.length == 0) {
                    var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                    table.append(row)
                    $('#total-event-add').text(0)
                    $('#start-event-add').html(0)
                } else {
                    if (myList[0].event_process_id != "") {
                        $('#start-event-add').text(myList[0].event_process_id)
                    }
                    pageButtons(data.pages)
                    $('#total-event-add').text(result.event.length)
                }
                $('#end-event-add').html(end_count)

                $('a#_add_event').on('click', function () {
                    $.post(`/${Sub}event_process/get`, { value: "@lltr@Cl@ssific@iti0nGetEventPr@cess", id: $(this).attr('data-value') }).done(function (result) {
                        $('input[name="event_process_id"]').val(result[0].event_process_id)
                        $('input[name="classify_name_part2"]').val(result[0].event_process_name)
                        $('div#select-event-process').modal('hide')
                        $('#table-body-event-add').empty()
                        buildTable()
                    })
                })
                $('a#_del_event').on('click', function () {
                    $('input[name="event_process_id"]').val(null)
                    $('input[name="classify_name_part2"]').val(null)
                    $('#table-body-event-add').empty()
                    buildTable()
                })
            }
        })
    })
    // Modal Add Processing-base
    $('a#btn_add_processing_base').on('click', function () {
        $('a#add_processing_base').on('click', function () {
            if ($('input[name="pattern_processing_base_name"]').val() != "") {
                var name = $('input[name="pattern_processing_base_name"]').attr('name')
                var value = $('input[name="pattern_processing_base_name"]').val()
                var obj = {}
                obj[name] = value
                $.ajax({
                    url: `/${Sub}processing_base`,
                    method: "POST",
                    data: obj,
                }).done(function () {
                    $('input[name="pattern_processing_base_name"]').val(null);
                    $('div#add-processing-base').modal('hide');
                    $('#select_processing_base').load(document.URL + ' #select_processing_base ');
                })
            }
        })
    })
    // Modal Add Special-conditions
    $('a#btn_add_special_conditions').on('click', function () {
        $('a#add-special-conditions').on('click', function () {
            if ($('input[name="classification_special_conditions_name"]').val() != "") {
                var name = $('input[name="classification_special_conditions_name"]').attr('name')
                var value = $('input[name="classification_special_conditions_name"]').val()
                var obj = {}
                obj[name] = value
                $.ajax({
                    url: `/${Sub}event_process/add`,
                    method: "POST",
                    data: obj,
                }).done(function () {
                    $('input[name="classification_special_conditions_name"]').val(null)
                    $('div#add_special_conditions').modal('hide')
                    $('#select_special_conditions').load(document.URL + ' #select_special_conditions')
                })
            }
        })
    })
} else if (document.getElementById('c_edit')) {
    // Sidebar
    $('li#li-data-project-action').addClass('selected')
    $('a#li-data-project-action').addClass('active')
    $('ul#ul-data-project-action').addClass('in')
    $('a#a-export').addClass('active')
    $('ul#ul-export').addClass('in')
    $('a#a-classification').addClass('active')
    // ==================== ALL FUNCTION ON EDIT PAGE ===================
    var total_users = []
    var total_users_outside = []
    let now_id = document.getElementById('c_edit').getAttribute('data-mark')
    $.post(`/${Sub}classification/selectUsed`, { value: "@lltr@Se1ectUsedCl@ssify", id: now_id }).done(function (result) {
        if ($('input[name="pattern_id"]').val() != null || $('input[name="pattern_id"]').val() != "") {
            $.post(`/${Sub}pattern/select-pattern`, { value: "@lltr@Se1ectP@ttern", id: $('input[name="pattern_id"]').val() }).done(function (result) {
                let fullname = result.total_name_inside.map(e => e.join(" "))
                $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').val([...fullname])
                $('input[name="classify_user_access_info_process_inside_from_pattern"]').val(result.pattern[0].pattern_processor_inside_id)
                let fullname_outside = result.total_name_outside.map(e => e.join(" "))
                $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').val([...fullname_outside])
                $('input[name="classify_user_access_info_process_outside_from_pattern"]').val(result.pattern[0].pattern_processor_outside_id)
                var _file_ = ""
                var _database_ = ""
                if (result.pattern[0].pattern_type_data_file == 1) {
                    _file_ = "มี"
                } else {
                    _file_ = "ไม่มี"
                }
                if (result.pattern[0].pattern_type_database == 1) {
                    _database_ = result.pattern[0].pattern_type_database_name
                } else {
                    _database_ = "ไม่มี"
                }
                var _inside_ = ""
                var _outside_0 = ""
                var _outside_1 = ""
                var _outside_2 = ""
                var _outside_3 = ""
                if (result.pattern[0].pattern_storage_method_inside == 1) {
                    _inside_ = "มี"
                } else {
                    _inside_ = "ไม่มี"
                }
                if (result.pattern[0].pattern_storage_method_outside == 1) {
                    _outside_0 = "มี"
                    if (result.pattern[0].pattern_storage_method_outside_device == 1) {
                        _outside_1 = result.pattern[0].pattern_storage_method_outside_device_name
                    } else {
                        _outside_1 = "ไม่มี"
                    }
                    if (result.pattern[0].pattern_storage_method_outside_agent == 1) {
                        _outside_2 = result.pattern[0].pattern_storage_method_outside_agent_name
                    } else {
                        _outside_2 = "ไม่มี"
                    }
                    if (result.pattern[0].pattern_storage_method_outside_database_outside == 1) {
                        _outside_3 = result.pattern[0].pattern_storage_method_outside_database_outside_name
                    } else {
                        _outside_3 = "ไม่มี"
                    }
                } else {
                    _outside_0 = "ไม่มี"
                }
                var data_name_total = result.data_name_total.map(e => e['data_name'])
                data_name_total = data_name_total.join(", ")
                $('textarea[name="example_pattern"]').text(`ชื่อ: ${result.pattern[0].pattern_name}
ข้อมูล: ${data_name_total}
ระยะเวลา: ${convert_date(new Date(result.pattern[0].pattern_start_date))} ทั้งหมด: ${result.pattern[0].pattern_total_date} วัน
ชนิดข้อมูล:
    File Ms Excel/CSV: ${_file_}
    Database: ${_database_}
วิธีการจัดเก็บข้อมูลส่วนบุคคล:
    ภายใน Alltra: ${_inside_}
    ภายนอก: ${_outside_0}, [ (อุปกรณ์: ${_outside_1}),  (Agent: ${_outside_2}),  (Database: ${_outside_3}) ]`)
                // check_doc_id_pattern();
                $('div#select-pattern').modal('hide')
                $('#table-body-pattern-add').empty()
            })
        }
        if ($('input#customCheck1').is(':checked')) {
            $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').removeAttr('hidden')
            $('span#user_access_information_process_inside').html(`
                &nbsp; 
                    <a href="#select-users-inside" class="option-pattern" data-bs-toggle="modal">เลือกสมาชิกในระบบ Alltra</a>&nbsp; &nbsp;
                    <span class="h6" style="font-weight: bold;">รวม</span>
                    <input type="text" name="classify_user_access_info_process_inside_from_new_total" class="form-control" style="display: inline; width: 15%;" value="${result[0].classify_user_access_info_process_inside}" placeholder="กรุณาเลือกสมาชิก..." readonly/>&nbsp;
                    <span class="h6" style="font-weight: bold;">คน</span>&nbsp;&nbsp;&nbsp;
                    <span class="span-image" id='user_inside'></span>
                    <input type="text" name="classify_user_access_info_process_inside_from_new_id" value="${result[0].classify_user_access_info_process_inside_from_new_id}" class="form-control input-all-number" readonly hidden/>&nbsp;
                `)
            let total_acc_id = result[0].classify_user_access_info_process_inside_from_new_id.split(",")
            for (i in total_acc_id) {
                $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: total_acc_id[i] }).done(function (result) {
                    total_users.push({ "id": result[0].acc_id, "image": result[0].image })
                    $('span#user_inside').empty()
                    $('input[name="classify_user_access_info_process_inside_from_new_total"]').val(total_users.length)
                    $('input[name="classify_user_access_info_process_inside_from_new_id"]').val(total_users.map(e => e['id']))
                    for (var k = 0; k < total_users.length; k++) {
                        // Image = 600x600
                        if (k < total_users.length - 1) {
                            $('span#user_inside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/> &nbsp;')
                        } else {
                            $('span#user_inside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/>')
                        }
                    }
                })
            }
            var count_click2 = 0
            $('a[href="#select-users-inside"]').on('click', function () {
                $.post(`/${Sub}users`, { value: "@lltr@Cl@ssific@ti0nUser" }, function (result) {
                    count_click2 += 1
                    // ============================== Create Prepare ============================
                    var end_count = 0 // จำนวนทั้งหมด
                    var state = {
                        'querySet': result.users,
                        'page': 1,
                        'rows': 5,
                        'window': 5,
                    }
                    if (count_click2 == 1) {
                        buildTable()
                    }
                    function pagination(querySet, page, rows) {

                        var trimStart = (page - 1) * rows
                        var trimEnd = trimStart + rows

                        var trimmedData = querySet.slice(trimStart, trimEnd)

                        var pages = Math.ceil(querySet.length / rows);

                        var start_count = 1
                        document.getElementById('start-user-inside-add').innerHTML = start_count

                        return {
                            'querySet': trimmedData,
                            'pages': pages,
                        }
                    }
                    // ============================== Create Pagination ============================
                    function pageButtons(pages) {
                        var wrapper = document.getElementById('pagination-wapper-user-inside-add')
                        wrapper.innerHTML = ``
                        var maxLeft = (state.page - Math.floor(state.window / 2))
                        var maxRight = (state.page + Math.floor(state.window / 2))

                        if (maxLeft < 1) {
                            maxLeft = 1
                            maxRight = state.window
                        }

                        if (maxRight > pages) {
                            maxLeft = pages - (state.window - 1)
                            if (maxLeft < 1) {
                                maxLeft = 1
                            }
                            maxRight = pages
                        }

                        if (state.page > 1) {
                            wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ </button></li>`
                        } else {
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ </button></li>`
                        }

                        num = 1
                        if (maxRight > 5) {
                            if (state.page > (maxRight / 2)) {
                                if ((state.page + 1) > (maxRight / 2)) {
                                    wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                }
                            }
                            for (var page = maxLeft; page <= maxRight; page++) {
                                if ((page >= state.page - 2) && (page <= state.page + 2)) {
                                    if (page == state.page) {
                                        wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                                    }
                                    else {
                                        p = page - 1;
                                        wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                                    }
                                }
                            }
                            if ((state.page) <= (maxRight / 2)) {
                                mp = maxRight - 1;
                                wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                            }
                        }
                        else {
                            for (var page = maxLeft; page <= maxRight; page++) {
                                if (state.page == page) {
                                    wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                                } else {
                                    wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                                }
                            }
                        }

                        if (state.page < pages) {
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" > ถัดไป </button>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" > ถัดไป </button>`
                        }

                        $('.page').on('click', function () {
                            $('#table-body-user-inside-add').empty()
                            state.page = Number($(this).val())
                            // document.getElementById('page').value = state.page
                            buildTable()
                        })
                    }
                    // ============================== Create Table ============================
                    function buildTable() {
                        var table = $('#table-body-user-inside-add')
                        var data = pagination(state.querySet, state.page, state.rows)
                        var myList = data.querySet
                        for (y in myList) {
                            if (myList[y].acc_id != "") {
                                let buttonAdd = ""
                                let buttonDel = ""
                                let total_users = $('input[name="classify_user_access_info_process_inside_from_new_id"]').val().split(',')
                                if (state.page == 1) {
                                    if (checkUsers(total_users, result.id_users[y].acc_id, 0) == true) {
                                        buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                        buttonDel = '<a id="_del_users_inside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                    } else {
                                        buttonAdd = '<a id="_add_users_inside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                        buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                    }
                                } else {
                                    if (checkUsers(total_users, result.id_users[parseInt(state.rows) + parseInt(y)].acc_id, 0) == true) {
                                        buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                        buttonDel = '<a id="_del_users_inside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                    } else {
                                        buttonAdd = '<a id="_add_users_inside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                        buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                    }
                                }
                                //Keep in mind we are using "Template Litterals to create rows"
                                var row = '<tr>' +
                                    '<th style="text-align: left; vertical-align: center;">' +
                                    'ชื่อ - นามสกุล: ' + myList[y].firstname + " " + myList[y].lastname + " <br/>" +
                                    '</th>' +
                                    '<td style="vertical-align: middle; width: 50px;">' +
                                    buttonAdd +
                                    '</td>' +
                                    '<td style="vertical-align: middle; width: 50px;">' +
                                    buttonDel +
                                    '</td>' +
                                    '</tr>'
                                table.append(row)
                                end_count = myList[y].acc_id
                            }
                        }
                        if (myList.length == 0) {
                            var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;"> ไม่พบข้อมูล </td></tr>'
                            table.append(row)
                            $('#total-user-inside-add').text(0)
                            $('#start-user-inside-add').html(0)
                        } else {
                            if (myList[0].user_id) {
                                $('#start-user-inside-add').text(myList[0].acc_id)
                            }
                            pageButtons(data.pages)
                            $('#total-user-inside-add').text(result.users.length)
                        }
                        $('#end-user-inside-add').html(end_count)

                        $('a#_add_users_inside').on('click', function () {
                            $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                total_users.push({ "id": result[0].acc_id, "image": result[0].image })
                                $('span#user_inside').empty()
                                $('input[name="classify_user_access_info_process_inside_from_new_total"]').val(total_users.length)
                                $('input[name="classify_user_access_info_process_inside_from_new_id"]').val(total_users.map(e => e['id']))
                                for (var k = 0; k < total_users.length; k++) {
                                    // Image = 600x600
                                    if (k < total_users.length - 1) {
                                        $('span#user_inside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/> &nbsp;')
                                    } else {
                                        $('span#user_inside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/>')
                                    }
                                }
                                $('#table-body-user-inside-add').empty()
                                buildTable()
                            })
                        })
                        $('a#_del_users_inside').on('click', function () {
                            $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                let new_total_user = total_users.filter(function (item) { return item.id.toString().indexOf(result[0].acc_id) })
                                total_users = new_total_user
                                $('input[name="classify_user_access_info_process_inside_from_new_total"]').val(total_users.length)
                                $('input[name="classify_user_access_info_process_inside_from_new_id"]').val(total_users.map(e => e['id']))
                                for (var k = 0; k < $('span#user_inside').children().length; k++) {
                                    let old_image = $('span#user_inside').children()[k].getAttribute('data-mark')
                                    if (old_image == result[0].acc_id) {
                                        $('span#user_inside').children()[k].remove()
                                    }
                                }
                                $('#table-body-user-inside-add').empty()
                                buildTable()
                                if ($('input[name="classify_user_access_info_process_inside_from_new_total"]').val() == "0") {
                                    $('span#user_inside').empty()
                                }
                            })
                        })
                        //$('input[name="classify_user_access_info_process_inside_from_new_total"]').on('click keyup keydown focus', function () {
                        //    if ($(this).val() == "" || $(this).val() == null) {
                        //        $('span#user_inside').empty()
                        //    } else if ($(this).val() != String(total_image.length)) {
                        //        $('span#user_inside').last().remove()
                        //    }
                        //})
                    }
                })
            })
        }
        if ($('input#customCheck2').is(':checked')) {
            $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').removeAttr('hidden')
            $('span#user_access_information_process_outside').html(`
                &nbsp;
                    <a href="#select-users-outside" class="option-pattern" data-bs-toggle="modal">เลือกสมาชิกในระบบ Alltra</a>&nbsp; &nbsp;
                    <span class="h6" style="font-weight: bold;">รวม</span>
                    <input type="text" name="classify_user_access_info_process_outside_from_new_total" class="form-control" style="display: inline; width: 15%;" value="${result[0].classify_user_access_info_process_outside}" placeholder="กรุณาเลือกสมาชิก..." readonly/>&nbsp;
                    <span class="h6" style="font-weight: bold;">คน</span>&nbsp;&nbsp;&nbsp;
                    <span class="span-image" id="user_outside">
                    </span>
                    <input type="text" name="classify_user_access_info_process_outside_from_new_id" value="${result[0].classify_user_access_info_process_outside_from_new_id}" class="form-control input-all-number" readonly hidden/>&nbsp;
                `)
            let total_acc_id_outside = result[0].classify_user_access_info_process_outside_from_new_id.split(',')
            for (i in total_acc_id_outside) {
                $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: total_acc_id_outside[i] }).done(function (result) {
                    $('span#user_outside').empty()
                    total_users_outside.push({ "id": result[0].acc_id, "image": result[0].image })
                    $('input[name="classify_user_access_info_process_outside_from_new_total"]').val(total_users_outside.length)
                    $('input[name="classify_user_access_info_process_outside_from_new_id"]').val(total_users_outside.map(e => e['id']))
                    for (var k = 0; k < total_users_outside.length; k++) {
                        // Image = 600x600
                        if (k < total_users_outside.length - 1) {
                            $('span#user_outside').append('<img class="image-users" src="/UI/image/' + total_users_outside[k].image + '" data-mark="' + total_users_outside[k].id + '"/> &nbsp;')
                        } else {
                            $('span#user_outside').append('<img class="image-users" src="/UI/image/' + total_users_outside[k].image + '" data-mark="' + total_users_outside[k].id + '"/>')
                        }
                    }
                })
            }
            var count_click3 = 0
            $('a[href="#select-users-outside"]').on('click', function () {
                $.post(`/${Sub}users`, { value: "@lltr@Cl@ssific@ti0nUser" }, function (result) {
                    count_click3 += 1
                    // ============================== Create Prepare ============================
                    var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                    var state = {
                        'querySet': result.users,
                        'page': 1,
                        'rows': 5,
                        'window': 5,
                    }
                    if (count_click3 == 1) {
                        buildTable()
                    }
                    function pagination(querySet, page, rows) {

                        var trimStart = (page - 1) * rows
                        var trimEnd = trimStart + rows

                        var trimmedData = querySet.slice(trimStart, trimEnd)

                        var pages = Math.ceil(querySet.length / rows);

                        var start_count = 1
                        document.getElementById('start-users-outside-add').innerHTML = start_count

                        return {
                            'querySet': trimmedData,
                            'pages': pages,
                        }
                    }
                    // ============================== Create Pagination ============================
                    function pageButtons(pages) {
                        var wrapper = document.getElementById('pagination-wapper-users-outside-add')
                        wrapper.innerHTML = ``
                        var maxLeft = (state.page - Math.floor(state.window / 2))
                        var maxRight = (state.page + Math.floor(state.window / 2))

                        if (maxLeft < 1) {
                            maxLeft = 1
                            maxRight = state.window
                        }

                        if (maxRight > pages) {
                            maxLeft = pages - (state.window - 1)
                            if (maxLeft < 1) {
                                maxLeft = 1
                            }
                            maxRight = pages
                        }

                        if (state.page > 1) {
                            wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                        } else {
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                        }

                        num = 1
                        if (maxRight > 5) {
                            if (state.page > (maxRight / 2)) {
                                if ((state.page + 1) > (maxRight / 2)) {
                                    wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                }
                            }
                            for (var page = maxLeft; page <= maxRight; page++) {
                                if ((page >= state.page - 2) && (page <= state.page + 2)) {
                                    if (page == state.page) {
                                        wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                                    }
                                    else {
                                        p = page - 1;
                                        wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                                    }
                                }
                            }
                            if ((state.page) <= (maxRight / 2)) {
                                mp = maxRight - 1;
                                wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                            }
                        }
                        else {
                            for (var page = maxLeft; page <= maxRight; page++) {
                                if (state.page == page) {
                                    wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                                } else {
                                    wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                                }
                            }
                        }

                        if (state.page < pages) {
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                        }

                        $('.page').on('click', function () {
                            $('#table-body-users-outside-add').empty()
                            state.page = Number($(this).val())
                            // document.getElementById('page').value = state.page
                            buildTable()
                        })
                    }
                    // ============================== Create Table ============================
                    function buildTable() {
                        var table = $('#table-body-users-outside-add')
                        var data = pagination(state.querySet, state.page, state.rows)
                        var myList = data.querySet
                        for (y in myList) {
                            if (myList[y].acc_id != "") {
                                let buttonAdd = ""
                                let buttonDel = ""
                                let total_users = $('input[name="classify_user_access_info_process_outside_from_new_id"]').val().split(',')
                                if (state.page == 1) {
                                    if (checkUsers(total_users, result.id_users[y].acc_id, 0) == true) {
                                        buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                        buttonDel = '<a id="_del_users_outside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                    } else {
                                        buttonAdd = '<a id="_add_users_outside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                        buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                    }
                                } else {
                                    if (checkUsers(total_users, result.id_users[parseInt(state.rows) + parseInt(y)].acc_id, 0) == true) {
                                        buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                        buttonDel = '<a id="_del_users_outside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                    } else {
                                        buttonAdd = '<a id="_add_users_outside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                        buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                    }
                                }
                                //Keep in mind we are using "Template Litterals to create rows"
                                var row = '<tr>' +
                                    '<th style="text-align: left; vertical-align: center;">' +
                                    'ชื่อ-นามสกุล: ' + myList[y].firstname + " " + myList[y].lastname + " <br/>" +
                                    '</th>' +
                                    '<td style="vertical-align: middle; width: 50px;">' +
                                    buttonAdd +
                                    '</td>' +
                                    '<td style="vertical-align: middle; width: 50px;">' +
                                    buttonDel +
                                    '</td>' +
                                    '</tr>'
                                table.append(row)
                                end_count = myList[y].acc_id
                            }
                        }
                        if (myList.length == 0) {
                            var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                            table.append(row)
                            $('#total-users-outside-add').text(0)
                            $('#start-users-outside-add').html(0)
                        } else {
                            if (myList[0].acc_id) {
                                $('#start-users-outside-add').text(myList[0].acc_id)
                            }
                            pageButtons(data.pages)
                            $('#total-users-outside-add').text(result.users.length)
                        }

                        $('#end-users-outside-add').html(end_count)

                        $('a#_add_users_outside').on('click', function () {
                            $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                $('span#user_outside').empty()
                                total_users_outside.push({ "id": result[0].acc_id, "image": result[0].image })
                                $('input[name="classify_user_access_info_process_outside_from_new_total"]').val(total_users_outside.length)
                                $('input[name="classify_user_access_info_process_outside_from_new_id"]').val(total_users_outside.map(e => e['id']))
                                for (var k = 0; k < total_users_outside.length; k++) {
                                    // Image = 600x600
                                    if (k < total_users_outside.length - 1) {
                                        $('span#user_outside').append('<img class="image-users" src="/UI/image/' + total_users_outside[k].image + '" data-mark="' + total_users_outside[k].id + '"/> &nbsp;')
                                    } else {
                                        $('span#user_outside').append('<img class="image-users" src="/UI/image/' + total_users_outside[k].image + '" data-mark="' + total_users_outside[k].id + '"/>')
                                    }
                                }
                                $('#table-body-users-outside-add').empty()
                                buildTable()
                            })
                        })
                        $('a#_del_users_outside').on('click', function () {
                            $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                let new_total_users = total_users_outside.filter(function (item) { return item.id.toString().indexOf(result[0].acc_id) })
                                total_users_outside = new_total_users
                                $('input[name="classify_user_access_info_process_outside_from_new_total"]').val(total_users_outside.length)
                                $('input[name="classify_user_access_info_process_outside_from_new_id"]').val(total_users_outside.map(e => e['id']))
                                for (var k = 0; k < $('span#user_outside').children().length; k++) {
                                    let old_image = $('span#user_outside').children()[k].getAttribute('data-mark')
                                    if (old_image == result[0].acc_id) {
                                        $('span#user_outside').children()[k].remove()
                                    }
                                }
                                if ($('input[name="classify_user_access_info_process_outside_from_new_total"]').val() == 0) {
                                    $('span#user_outside').empty()
                                }
                                $('#table-body-users-outside-add').empty()
                                buildTable()
                            })
                        })
                        //$('input[name="classify_user_access_info_process_outside_from_new_total"]').on('click keyup keydown focus', function () {
                        //    if ($(this).val() == "" || $(this).val() == null) {
                        //        $('span#user_outside').empty()
                        //    }
                        //})
                    }
                })
            })
        }
        if ($('input#customCheck3').is(':checked')) {
            $('span#period_process_follow_policy').html(`
                &nbsp;<input type="text" name="classify_period_proccess_follow_policy_total" value="${result[0].classify_period_proccess_follow_policy_total}" placeholder="" class="form-control" value="180" style="width: 52%; display: inline;" readonly/> &nbsp; 
                    <span class="font-bold">วัน</span>
         `)
        }
        if ($('input#customCheck4').is(':checked')) {
            $('input[name="classify_period_end_follow_pattern_total"]').removeAttr('hidden')
            $('span#day_end_pattern').removeAttr('hidden')
        }
        
        if ($('input[name="classify_risk_assess_only_dpo_data_number_all_used_process_many"]').is(":checked")) {
            $('span#risk_assess_only_dpo_data_number_all_used_process_total').html(`
            <div class="row"><div>ระบุ</div><div><input type="text" name="classify_risk_assess_only_dpo_data_number_all_used_process_total" value="${result[0].classify_risk_assess_only_dpo_data_number_all_used_process_total}"placeholder="กรุณาป้อนจำนวน..." class="form-control"  /></div>  </div>
            `)
        }
    })
    $('input:checkbox').on('click', function () {
        // Modal Add User (Inside)
        if ($(this).attr('id') == 'customCheck1') {
            if ($(this).is(':checked') == true) {
                $.post(`/${Sub}classification/selectUsed`, { value: "@lltr@Se1ectUsedCl@ssify", id: now_id }).done(function (result) {
                    $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').removeAttr('hidden')
                    $('span#user_access_information_process_inside').html(`
                &nbsp; 
                    <a href="#select-users-inside" class="option-pattern" data-bs-toggle="modal">เลือกสมาชิกในระบบ Alltra</a>&nbsp; &nbsp;
                    <span class="h6" style="font-weight: bold;">รวม</span>
                    <input type="text" name="classify_user_access_info_process_inside_from_new_total" class="form-control" style="display: inline; width: 15%;" value="${result[0].classify_user_access_info_process_inside}" placeholder="กรุณาเลือกสมาชิก..." readonly/>&nbsp;
                    <span class="h6" style="font-weight: bold;">คน</span>&nbsp;&nbsp;&nbsp;
                    <span class="span-image" id='user_inside'></span>
                    <input type="text" name="classify_user_access_info_process_inside_from_new_id" value="${result[0].classify_user_access_info_process_inside_from_new_id}" class="form-control input-all-number" readonly hidden/>&nbsp;
                `)
                    let total_acc_id = result[0].classify_user_access_info_process_inside_from_new_id.split(",")
                    for (i in total_acc_id) {
                        $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: total_acc_id[i] }).done(function (result) {
                            $('span#user_inside').empty()
                            $('input[name="classify_user_access_info_process_inside_from_new_total"]').val(total_users.length)
                            $('input[name="classify_user_access_info_process_inside_from_new_id"]').val(total_users.map(e => e['id']))
                            for (var k = 0; k < total_users.length; k++) {
                                // Image = 600x600
                                if (k < total_users.length - 1) {
                                    $('span#user_inside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/> &nbsp;')
                                } else {
                                    $('span#user_inside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/>')
                                }
                            }
                        })
                    }
                    var count_click2 = 0
                    $('a[href="#select-users-inside"]').on('click', function () {
                        $.post(`/${Sub}users`, { value: "@lltr@Cl@ssific@ti0nUser" }, function (result) {
                            count_click2 += 1
                            // ============================== Create Prepare ============================
                            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                            var state = {
                                'querySet': result.users,
                                'page': 1,
                                'rows': 5,
                                'window': 5,
                            }
                            if (count_click2 == 1) {
                                buildTable()
                            }
                            function pagination(querySet, page, rows) {

                                var trimStart = (page - 1) * rows
                                var trimEnd = trimStart + rows

                                var trimmedData = querySet.slice(trimStart, trimEnd)

                                var pages = Math.ceil(querySet.length / rows);

                                var start_count = 1
                                document.getElementById('start-user-inside-add').innerHTML = start_count

                                return {
                                    'querySet': trimmedData,
                                    'pages': pages,
                                }
                            }
                            // ============================== Create Pagination ============================
                            function pageButtons(pages) {
                                var wrapper = document.getElementById('pagination-wapper-user-inside-add')
                                wrapper.innerHTML = ``
                                var maxLeft = (state.page - Math.floor(state.window / 2))
                                var maxRight = (state.page + Math.floor(state.window / 2))

                                if (maxLeft < 1) {
                                    maxLeft = 1
                                    maxRight = state.window
                                }

                                if (maxRight > pages) {
                                    maxLeft = pages - (state.window - 1)
                                    if (maxLeft < 1) {
                                        maxLeft = 1
                                    }
                                    maxRight = pages
                                }

                                if (state.page > 1) {
                                    wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                                } else {
                                    wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                                }

                                num = 1
                                if (maxRight > 5) {
                                    if (state.page > (maxRight / 2)) {
                                        if ((state.page + 1) > (maxRight / 2)) {
                                            wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                                            wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                        }
                                    }
                                    for (var page = maxLeft; page <= maxRight; page++) {
                                        if ((page >= state.page - 2) && (page <= state.page + 2)) {
                                            if (page == state.page) {
                                                wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                                            }
                                            else {
                                                p = page - 1;
                                                wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                                            }
                                        }
                                    }
                                    if ((state.page) <= (maxRight / 2)) {
                                        mp = maxRight - 1;
                                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                        wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                                    }
                                }
                                else {
                                    for (var page = maxLeft; page <= maxRight; page++) {
                                        if (state.page == page) {
                                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                                        } else {
                                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                                        }
                                    }
                                }

                                if (state.page < pages) {
                                    wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                                } else {
                                    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                                }

                                $('.page').on('click', function () {
                                    $('#table-body-user-inside-add').empty()
                                    state.page = Number($(this).val())
                                    // document.getElementById('page').value = state.page
                                    buildTable()
                                })
                            }
                            // ============================== Create Table ============================
                            function buildTable() {
                                var table = $('#table-body-user-inside-add')
                                var data = pagination(state.querySet, state.page, state.rows)
                                var myList = data.querySet
                                for (y in myList) {
                                    if (myList[y].acc_id != "") {
                                        let buttonAdd = ""
                                        let buttonDel = ""
                                        let total_users = $('input[name="classify_user_access_info_process_inside_from_new_id"]').val().split(',')
                                        if (state.page == 1) {
                                            if (checkUsers(total_users, result.id_users[y].acc_id, 0) == true) {
                                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                                buttonDel = '<a id="_del_users_inside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                            } else {
                                                buttonAdd = '<a id="_add_users_inside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                            }
                                        } else {
                                            if (checkUsers(total_users, result.id_users[parseInt(state.rows) + parseInt(y)].acc_id, 0) == true) {
                                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                                buttonDel = '<a id="_del_users_inside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                            } else {
                                                buttonAdd = '<a id="_add_users_inside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                            }
                                        }
                                        //Keep in mind we are using "Template Litterals to create rows"
                                        var row = '<tr>' +
                                            '<th style="vertical-align: center;">' +
                                            'ชื่อ-นามสกุล: ' + myList[y].firstname + " " + myList[y].lastname + " <br/>" +
                                            '</th>' +
                                            '<td style="vertical-align: middle; width: 50px;">' +
                                            buttonAdd +
                                            '</td>' +
                                            '<td style="vertical-align: middle; width: 50px;">' +
                                            buttonDel +
                                            '</td>' +
                                            '</tr>'
                                        table.append(row)
                                        end_count = myList[y].acc_id
                                    }
                                }
                                if (myList.length == 0) {
                                    var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                                    table.append(row)
                                    $('#total-user-inside-add').text(0)
                                    $('#start-user-inside-add').html(0)
                                } else {
                                    if (myList[0].user_id) {
                                        $('#start-user-inside-add').text(myList[0].acc_id)
                                    }
                                    pageButtons(data.pages)
                                    $('#total-user-inside-add').text(result.users.length)
                                }
                                $('#end-user-inside-add').html(end_count)

                                $('a#_add_users_inside').on('click', function () {
                                    $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                        total_users.push({ "id": result[0].acc_id, "image": result[0].image })
                                        $('span#user_inside').empty()
                                        var length_users = new Set(total_users)
                                        total_users = [...length_users]
                                        $('input[name="classify_user_access_info_process_inside_from_new_total"]').val(total_users.length)
                                        $('input[name="classify_user_access_info_process_inside_from_new_id"]').val(total_users.map(e => e['id']))
                                        for (var k = 0; k < total_users.length; k++) {
                                            // Image = 600x600
                                            if (k < total_users.length - 1) {
                                                $('span#user_inside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/> &nbsp;')
                                            } else {
                                                $('span#user_inside').append('<img class="image-users" src="/UI/image/' + total_users[k].image + '" data-mark="' + total_users[k].id + '"/>')
                                            }
                                        }
                                        $('#table-body-user-inside-add').empty()
                                        buildTable()
                                    })
                                })
                                $('a#_del_users_inside').on('click', function () {
                                    $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                        let new_total_user = total_users.filter(function (item) { return item.id.toString().indexOf(result[0].acc_id) })
                                        total_users = new_total_user
                                        $('input[name="classify_user_access_info_process_inside_from_new_total"]').val(total_users.length)
                                        $('input[name="classify_user_access_info_process_inside_from_new_id"]').val(total_users.map(e => e['id']))
                                        for (var k = 0; k < $('span#user_inside').children().length; k++) {
                                            let old_image = $('span#user_inside').children()[k].getAttribute('data-mark')
                                            if (old_image == result[0].acc_id) {
                                                $('span#user_inside').children()[k].remove()
                                            }
                                        }
                                        $('#table-body-user-inside-add').empty()
                                        buildTable()
                                        if ($('input[name="classify_user_access_info_process_inside_from_new_total"]').val() == "0") {
                                            $('span#user_inside').empty()
                                        }
                                    })
                                })
                                //$('input[name="classify_user_access_info_process_inside_from_new_total"]').on('click keyup keydown focus', function () {
                                //    if ($(this).val() == "" || $(this).val() == null) {
                                //        $('span#user_inside').empty()
                                //    } else if ($(this).val() != String(total_image.length)) {
                                //        $('span#user_inside').last().remove()
                                //    }
                                //})
                            }
                        })
                    })
                })
            } else {
                $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').attr('hidden', 'hidden')
                $('span#user_access_information_process_inside').empty()
                $('tbody#table-body-user-inside-add').empty()
                $('ul#pagination-wapper-user-inside-add').empty()
                $('span#start-user-inside-add').empty()
                $('span#end-user-inside-add').empty()
                $('span#total-user-inside-add').empty()
            }
        }
        // Modal Add User (Outside)
        if ($(this).attr('id') == 'customCheck2') {
            if ($(this).is(':checked') == true) {
                $.post(`/${Sub}classification/selectUsed`, { value: "@lltr@Se1ectUsedCl@ssify", id: now_id }).done(function (result) {
                    $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').removeAttr('hidden')
                    $('span#user_access_information_process_outside').html(`
                &nbsp;
                    <a href="#select-users-outside" class="option-pattern" data-bs-toggle="modal">เลือกสมาชิกในระบบ Alltra</a>&nbsp; &nbsp;
                    <span class="h6" style="font-weight: bold;">รวม</span>
                    <input type="text" name="classify_user_access_info_process_outside_from_new_total" value="${result[0].classify_user_access_info_process_outside}" class="form-control" style="display: inline; width: 15%;" placeholder="กรุณาเลือกสมาชิก..." readonly/>&nbsp;
                    <span class="h6" style="font-weight: bold;">คน</span>&nbsp;&nbsp;&nbsp;
                    <span class="span-image" id="user_outside">

                    </span>
                    <input type="text" name="classify_user_access_info_process_outside_from_new_id" value="${result[0].classify_user_access_info_process_outside_from_new_id}" class="form-control input-all-number" readonly hidden/>&nbsp;
                `)
                    let total_acc_id_outside = result[0].classify_user_access_info_process_outside_from_new_id.split(',')
                    for (i in total_acc_id_outside) {
                        $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: total_acc_id_outside[i] }).done(function (result) {
                            $('span#user_outside').empty()
                            $('input[name="classify_user_access_info_process_outside_from_new_total"]').val(total_users_outside.length)
                            $('input[name="classify_user_access_info_process_outside_from_new_id"]').val(total_users_outside.map(e => e['id']))
                            for (var k = 0; k < total_users_outside.length; k++) {
                                // Image = 600x600
                                if (k < total_users_outside.length - 1) {
                                    $('span#user_outside').append('<img class="image-users" src="/UI/image/' + total_users_outside[k].image + '" data-mark="' + total_users_outside[k].id + '"/> &nbsp;')
                                } else {
                                    $('span#user_outside').append('<img class="image-users" src="/UI/image/' + total_users_outside[k].image + '" data-mark="' + total_users_outside[k].id + '"/>')
                                }
                            }
                        })
                    }
                    var count_click3 = 0
                    $('a[href="#select-users-outside"]').on('click', function () {
                        $.post(`/${Sub}users`, { value: "@lltr@Cl@ssific@ti0nUser" }, function (result) {
                            count_click3 += 1
                            // ============================== Create Prepare ============================
                            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                            var state = {
                                'querySet': result.users,
                                'page': 1,
                                'rows': 5,
                                'window': 5,
                            }
                            if (count_click3 == 1) {
                                buildTable()
                            }
                            function pagination(querySet, page, rows) {

                                var trimStart = (page - 1) * rows
                                var trimEnd = trimStart + rows

                                var trimmedData = querySet.slice(trimStart, trimEnd)

                                var pages = Math.ceil(querySet.length / rows);

                                var start_count = 1
                                document.getElementById('start-users-outside-add').innerHTML = start_count

                                return {
                                    'querySet': trimmedData,
                                    'pages': pages,
                                }
                            }
                            // ============================== Create Pagination ============================
                            function pageButtons(pages) {
                                var wrapper = document.getElementById('pagination-wapper-users-outside-add')
                                wrapper.innerHTML = ``
                                var maxLeft = (state.page - Math.floor(state.window / 2))
                                var maxRight = (state.page + Math.floor(state.window / 2))

                                if (maxLeft < 1) {
                                    maxLeft = 1
                                    maxRight = state.window
                                }

                                if (maxRight > pages) {
                                    maxLeft = pages - (state.window - 1)
                                    if (maxLeft < 1) {
                                        maxLeft = 1
                                    }
                                    maxRight = pages
                                }

                                if (state.page > 1) {
                                    wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                                } else {
                                    wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                                }

                                num = 1
                                if (maxRight > 5) {
                                    if (state.page > (maxRight / 2)) {
                                        if ((state.page + 1) > (maxRight / 2)) {
                                            wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                                            wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                        }
                                    }
                                    for (var page = maxLeft; page <= maxRight; page++) {
                                        if ((page >= state.page - 2) && (page <= state.page + 2)) {
                                            if (page == state.page) {
                                                wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                                            }
                                            else {
                                                p = page - 1;
                                                wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                                            }
                                        }
                                    }
                                    if ((state.page) <= (maxRight / 2)) {
                                        mp = maxRight - 1;
                                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                        wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                                    }
                                }
                                else {
                                    for (var page = maxLeft; page <= maxRight; page++) {
                                        if (state.page == page) {
                                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                                        } else {
                                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                                        }
                                    }
                                }

                                if (state.page < pages) {
                                    wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                                } else {
                                    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                                }

                                $('.page').on('click', function () {
                                    $('#table-body-users-outside-add').empty()
                                    state.page = Number($(this).val())
                                    // document.getElementById('page').value = state.page
                                    buildTable()
                                })
                            }
                            // ============================== Create Table ============================
                            function buildTable() {
                                var table = $('#table-body-users-outside-add')
                                var data = pagination(state.querySet, state.page, state.rows)
                                var myList = data.querySet
                                for (y in myList) {
                                    if (myList[y].acc_id != "") {
                                        let buttonAdd = ""
                                        let buttonDel = ""
                                        let total_users = $('input[name="classify_user_access_info_process_outside_from_new_id"]').val().split(',')
                                        if (state.page == 1) {
                                            if (checkUsers(total_users, result.id_users[y].acc_id, 0) == true) {
                                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                                buttonDel = '<a id="_del_users_outside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                            } else {
                                                buttonAdd = '<a id="_add_users_outside" href="javascript:void(0);" data-value=' + result.id_users[y].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                            }
                                        } else {
                                            if (checkUsers(total_users, result.id_users[parseInt(state.rows) + parseInt(y)].acc_id, 0) == true) {
                                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                                buttonDel = '<a id="_del_users_outside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                            } else {
                                                buttonAdd = '<a id="_add_users_outside" href="javascript:void(0);" data-value=' + result.id_users[parseInt(state.rows) + parseInt(y)].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                            }
                                        }
                                        //Keep in mind we are using "Template Litterals to create rows"
                                        var row = '<tr>' +
                                            '<th style="text-align: left; vertical-align: center;">' +
                                            'ชื่อ-นามสกุล: ' + myList[y].firstname + " " + myList[y].lastname + " <br/>" +
                                            '</th>' +
                                            '<td style="vertical-align: middle; width: 50px;">' +
                                            buttonAdd +
                                            '</td>' +
                                            '<td style="vertical-align: middle; width: 50px;">' +
                                            buttonDel +
                                            '</td>' +
                                            '</tr>'
                                        table.append(row)
                                        end_count = myList[y].acc_id
                                    }
                                }
                                if (myList.length == 0) {
                                    var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                                    table.append(row)
                                    $('#total-users-outside-add').text(0)
                                    $('#start-users-outside-add').html(0)
                                } else {
                                    if (myList[0].acc_id) {
                                        $('#start-users-outside-add').text(myList[0].acc_id)
                                    }
                                    pageButtons(data.pages)
                                    $('#total-users-outside-add').text(result.users.length)
                                }

                                $('#end-users-outside-add').html(end_count)

                                $('a#_add_users_outside').on('click', function () {
                                    $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                        $('span#user_outside').empty()
                                        total_users_outside.push({ "id": result[0].acc_id, "image": result[0].image })
                                        $('input[name="classify_user_access_info_process_outside_from_new_total"]').val(total_users_outside.length)
                                        $('input[name="classify_user_access_info_process_outside_from_new_id"]').val(total_users_outside.map(e => e['id']))
                                        for (var k = 0; k < total_users_outside.length; k++) {
                                            // Image = 600x600
                                            if (k < total_users_outside.length - 1) {
                                                $('span#user_outside').append('<img class="image-users" src="/UI/image/' + total_users_outside[k].image + '" data-mark="' + total_users_outside[k].id + '"/> &nbsp;')
                                            } else {
                                                $('span#user_outside').append('<img class="image-users" src="/UI/image/' + total_users_outside[k].image + '" data-mark="' + total_users_outside[k].id + '"/>')
                                            }
                                        }
                                        $('#table-body-users-outside-add').empty()
                                        buildTable()
                                    })
                                })
                                $('a#_del_users_outside').on('click', function () {
                                    $.post(`/${Sub}users/get`, { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                        let new_total_users = total_users_outside.filter(function (item) { return item.id.toString().indexOf(result[0].acc_id) })
                                        total_users_outside = new_total_users
                                        $('input[name="classify_user_access_info_process_outside_from_new_total"]').val(total_users_outside.length)
                                        $('input[name="classify_user_access_info_process_outside_from_new_id"]').val(total_users_outside.map(e => e['id']))
                                        for (var k = 0; k < $('span#user_outside').children().length; k++) {
                                            let old_image = $('span#user_outside').children()[k].getAttribute('data-mark')
                                            if (old_image == result[0].acc_id) {
                                                $('span#user_outside').children()[k].remove()
                                            }
                                        }
                                        if ($('input[name="classify_user_access_info_process_outside_from_new_total"]').val() == 0) {
                                            $('span#user_outside').empty()
                                        }
                                        $('#table-body-users-outside-add').empty()
                                        buildTable()
                                    })
                                })
                                //$('input[name="classify_user_access_info_process_outside_from_new_total"]').on('click keyup keydown focus', function () {
                                //    if ($(this).val() == "" || $(this).val() == null) {
                                //        $('span#user_outside').empty()
                                //    }
                                //})
                            }
                        })
                    })
                })
            } else {
                $('input[name="user_access_info_process_outside_from_pattern_not_use"]').attr('hidden', 'hidden')
                $('span#user_access_information_process_outside').empty()
                $('tbody#table-body-users-outside-add').empty()
                $('ul#pagination-wapper-users-outside-add').empty()
                $('span#start-users-outside-add').empty()
                $('span#end-users-outside-add').empty()
                $('span#total-users-outside-add').empty()
            }
        }
        if ($(this).attr('id') == 'customCheck3') {
            if ($(this).is(':checked') == true) {
                $('span#period_process_follow_policy').html(`

                <div class="input-group">
                <input type="text" name="classify_period_proccess_follow_policy_total" placeholder="" class="form-control" value="180"  readonly/>
                                        <span class="input-group-text">
                                        วัน
                                        </span>
                                    </div>
                `)
            } else {
                $('span#period_process_follow_policy').empty()
            }
        }
        if ($(this).attr('id') == 'customCheck4') {
            if ($(this).is(':checked') == true) {
                $('input[name="classify_period_end_follow_pattern_total"]').removeAttr('hidden')
                $('span#day_end_pattern').removeAttr('hidden')
            } else {
                $('input[name="classify_period_end_follow_pattern_total"]').attr('hidden', true)
                $('span#day_end_pattern').attr('hidden', true)
            }
        }
    })
    $('input:radio').on('click', function () {
        if ($(this).attr("name") == 'classify_risk_assess_only_dpo_data_number_all_used_process_many') {
            if ($(this).attr('id') == "customRadio5" || $(this).attr('id') == "customRadio4") {
                $('span#risk_assess_only_dpo_data_number_all_used_process_total').html(`
                <div class="row"><div>ระบุ</div><div><input type="text" name="classify_risk_assess_only_dpo_data_number_all_used_process_total" value="${result[0].classify_risk_assess_only_dpo_data_number_all_used_process_total}"placeholder="กรุณาป้อนจำนวน..." class="form-control"/>  </div></div>
                `)
            } else {
                $('span#risk_assess_only_dpo_data_number_all_used_process_total').empty();
            }
        }
        //if ($(this).attr('name') == 'classify_risk_assess_only_dpo_assess_the_impact_of_data') {
        //    if ($(this).attr('id') == "customRadio14" || $(this).attr('id') == "customRadio15" || $(this).attr('id') == "customRadio16") {
        //        $('div#classification_risk_assessment_only_dpo_fix_a_leak_of_data').html(`
        //    <label class="form-check-label h6" style="font-weight: bold;">วิธีแก้ไขเมื่อเกิดข้อมูลรั่วไหลหรือผลกระทบต่อเจ้าของข้อมูล</label>
        //    <div>
        //       <input type="text" name="classify_risk_assess_only_dpo_fix_a_leak_of_data" id="" style="width: 47%;" placeholder="กรุณาป้อนวิธีแก้ไข..." class="form-control">
        //    </div>
        //    `)
        //    }
        //}
        //if ($(this).attr('name') == 'classify_risk_assess_only_dpo_assess_the_impact_of_organization') {
        //    if ($(this).attr('id') == "customRadio17" || $(this).attr('id') == "customRadio18" || $(this).attr('id') == "customRadio19") {
        //        $('div#classification_risk_assessment_only_dpo_fix_a_leak_of_organization').html(`
        //    <label class="form-check-label h6"
        //    style="font-weight: bold;">วิธีแก้ไขเมื่อเกิดข้อมูลรั่วไหลหรือผลกระทบต่อองค์กร</label>
        //<div>
        //    <input type="text" name="classify_risk_assess_only_dpo_fix_a_leak_of_organization" id="" style="width: 47%;" placeholder="กรุณาป้อนวิธีแก้ไข..." class="form-control">
        //</div>
        //    `)
        //    }
        //}
        if ($(this).attr('name') == 'classify_type_data_in_event_personal_datamark_check_xxxxxxxxxxxxxxxxxxxxxxxxxxxxx') {
            if ($(this).val() == 1) {
                $.post(`/${Sub}classification/selectUsed`, { value: "@lltr@Se1ectUsedCl@ssify", id: now_id }).done(function (result) {
                    let datamark_agin1 = ["", ""]
                    if (result[0].classify_type_data_in_event_personal_datamark == 3) {
                        datamark_agin1[0] = "checked"
                    } else {
                        datamark_agin1[1] = "checked"
                    }
                    let datamark_agin = ["", "", ""]
                    if (result[0].classify_type_data_in_event_personal_datamark == 0) {
                        datamark_agin[0] = "checked"
                    } else if (result[0].classify_type_data_in_event_personal_datamark == 1) {
                        datamark_agin[1] = "checked"
                    } else if (result[0].classify_type_data_in_event_personal_datamark == 2) {
                        datamark_agin[2] = "checked"
                    }
                    $('span#event_personal_datamark').html(`
                <table class="table table-boardless">
                    <td width="40%">
                        <input type="radio" id="not_use" name="classify_type_data_in_event_personal_datamark1" class="form-check-input" value="3" ${datamark_agin1[0]}/>
                        <label for="not_use">Anonymized Data (ไม่ระบุข้อมูล) </label>
                    </td>
                    <td width="50%">
                        <input type="radio" id="not_use1" name="classify_type_data_in_event_personal_datamark1" class="form-check-input" ${datamark_agin1[1]} />
                        <label for="not_used1">Pseudonymized Data (ข้อมูลที่มีการแฝง) </label>
                    </td> 
                </table>
                <span id="pseudonymized"></span>
                <span id="total-pseudonymized"></span>
                `)
                    if ($('input:radio[name="classify_type_data_in_event_personal_datamark1"]:checked').length > 0) {
                        if ($('input:radio[name="classify_type_data_in_event_personal_datamark1"]:checked').attr('id') == 'not_use1') {
                            $('span#pseudonymized').html(`
                            <table class="table table-boardless" id="pseudonymized">
                                <td width="32%">
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark1" value="0" class="form-check-input" ${datamark_agin[0]} />
                                    <label for="datamark1" > หัวข้อมูลอย่างเดียว </label>
                                </td>
                                <td width="30%">
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark2" value="1" class="form-check-input" ${datamark_agin[1]}/>
                                    <label for="datamark2" > หัวและท้ายข้อมูล </label>
                                </td>
                                <td>
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark3" value="2" class="form-check-input" ${datamark_agin[2]}/>
                                    <label for="datamark3" > ท้ายข้อมูลอย่างเดียว </label>
                                </td>
                            </table>
                            `)
                            $('span#total-pseudonymized').html(`
                            <table class="table table-boardless" id="total-pseudonymized">
                                <td style="width: 35%; text-align: right;">
                                    <label for="total-digit"> จำนวนตัวอักษรที่แสดง:  </label>
                                </td>
                                <td>
                                    <input type="number" min="1" name="classify_type_data_in_event_personal_datamark_total" placeholder="กรุณาป้อนจำนวน..." class="form-control" value="${result[0].classify_type_data_in_event_personal_datamark_total}" />
                                </td>
                            </table>
                            <span id="example-datamark"></span>
                            `)
                            let seleced = $('input:radio[name="classify_type_data_in_event_personal_datamark"]:checked')
                            if (seleced.attr('id') == 'datamark1') {
                                $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["****ย่าง"]
                            </td>
                        </table>
                        `)
                            } else if (seleced.attr('id') == 'datamark2') {
                                $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">3</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["***ย***"]
                            </td>
                        </table>
                        `)
                            } else if (seleced.attr('id') == 'datamark3') {
                                $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["ตัวอ****"]
                            </td>
                        </table>
                        `)
                            }
                        }
                    }
                    $('input:radio[name="classify_type_data_in_event_personal_datamark1"]').on('click', function () {
                        if ($(this).attr('id') == 'not_use') {
                            $('span#pseudonymized').empty();
                            $('span#total-pseudonymized').empty();
                        } else {
                            $('span#pseudonymized').html(`
                            <table class="table table-boardless" id="pseudonymized">
                                <td width="32%">
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark1" value="0" class="form-check-input" ${datamark_agin[0]} />
                                    <label for="datamark1" > หัวข้อมูลอย่างเดียว </label>
                                </td>
                                <td width="30%">
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark2" value="1" class="form-check-input" ${datamark_agin[1]}/>
                                    <label for="datamark2" > หัวและท้ายข้อมูล </label>
                                </td>
                                <td>
                                    <input type="radio" name="classify_type_data_in_event_personal_datamark" id="datamark3" value="2" class="form-check-input" ${datamark_agin[2]}/>
                                    <label for="datamark3" > ท้ายข้อมูลอย่างเดียว </label>
                                </td>
                            </table>
                            `)
                            $('span#total-pseudonymized').html(`
                            <table class="table table-boardless" id="total-pseudonymized">
                                <td style="width: 35%; text-align: right;">
                                    <label for="total-digit"> จำนวนตัวอักษรที่แสดง:  </label>
                                </td>
                                <td>
                                    <input type="number" min="1" name="classify_type_data_in_event_personal_datamark_total" placeholder="กรุณาป้อนจำนวน..." class="form-control" value="${result[0].classify_type_data_in_event_personal_datamark_total}" />
                                </td>
                            </table>
                            <span id="example-datamark"></span>
                            `)
                            let seleced = $('input:radio[name="classify_type_data_in_event_personal_datamark"]:checked')
                            if (seleced.attr('id') == 'datamark1') {
                                $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["****ย่าง"]
                            </td>
                        </table>
                        `)
                            } else if (seleced.attr('id') == 'datamark2') {
                                $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">3</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["***ย***"]
                            </td>
                        </table>
                        `)
                            } else if (seleced.attr('id') == 'datamark3') {
                                $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["ตัวอ****"]
                            </td>
                        </table>
                        `)
                            }
                        }
                        $('input:radio[name="classify_type_data_in_event_personal_datamark"]').on('click', function () {
                            if ($(this).attr('id') == 'datamark1') {
                                $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["****ย่าง"]
                            </td>
                        </table>
                        `)
                            } else if ($(this).attr('id') == 'datamark2') {
                                $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">3</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["***ย***"]
                            </td>
                        </table>
                        `)
                            } else if ($(this).attr('id') == 'datamark3') {
                                $('span#example-datamark').html(`
                        <table class="table table-boardless">
                            <td>
                                สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;"> ทำเครื่องหมาย </span> ["ตัวอ****"]
                            </td>
                        </table>
                        `)
                            }
                        })
                    })
                })
            } else {
                $('span#event_personal_datamark').empty();
            }
        }
    })
    // Modal Add Pattern
    var count_click = 0
    $('a[href="#select-pattern"]').on('click', function () {
        $.post(`/${Sub}pattern`, function (result) {
            count_click += 1
            // ============================== Create Prepare ============================
            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
            var state = {
                'querySet': result.pattern,
                'page': 1,
                'rows': 5,
                'window': 5,
            }
            if (count_click == 1) {
                buildTable()
            }
            function pagination(querySet, page, rows) {

                var trimStart = (page - 1) * rows
                var trimEnd = trimStart + rows

                var trimmedData = querySet.slice(trimStart, trimEnd)

                var pages = Math.ceil(querySet.length / rows);

                var start_count = 1
                document.getElementById('start-pattern-add').innerHTML = start_count

                return {
                    'querySet': trimmedData,
                    'pages': pages,
                }
            }
            // ============================== Create Pagination ============================
            function pageButtons(pages) {
                var wrapper = document.getElementById('pagination-wapper-pattern-add')
                wrapper.innerHTML = ``
                var maxLeft = (state.page - Math.floor(state.window / 2))
                var maxRight = (state.page + Math.floor(state.window / 2))

                if (maxLeft < 1) {
                    maxLeft = 1
                    maxRight = state.window
                }

                if (maxRight > pages) {
                    maxLeft = pages - (state.window - 1)
                    if (maxLeft < 1) {
                        maxLeft = 1
                    }
                    maxRight = pages
                }

                if (state.page > 1) {
                    wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                } else {
                    wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                }


                num = 1
                if (maxRight > 5) {
                    if (state.page > (maxRight / 2)) {
                        if ((state.page + 1) > (maxRight / 2)) {
                            wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                            wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        }
                    }
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if ((page >= state.page - 2) && (page <= state.page + 2)) {
                            if (page == state.page) {
                                wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                            else {
                                p = page - 1;
                                wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                        }
                    }
                    if ((state.page) <= (maxRight / 2)) {
                        mp = maxRight - 1;
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                    }
                }
                else {
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if (state.page == page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                        }
                    }
                }

                if (state.page < pages) {
                    wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                } else {
                    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                }

                $('.page').on('click', function () {
                    $('#table-body-pattern-add').empty()
                    state.page = Number($(this).val())
                    // document.getElementById('page').value = state.page
                    buildTable()
                })
            }
            // ============================== Create Table ============================
            function buildTable() {
                var table = $('#table-body-pattern-add')
                var data = pagination(state.querySet, state.page, state.rows)
                var myList = data.querySet
                for (y in myList) {
                    if (myList[y].pattern_id != "") {
                        let buttonAdd = ""
                        let buttonDel = ""
                        if (state.page == 1) {
                            if (result.id_pattern[y].pattern_id == $('input[name="pattern_id"]').val()) {
                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                buttonDel = '<a id="_del_pattern" href="javascript:void(0);" data-value=' + result.id_pattern[y].pattern_id + '  class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                            } else {
                                buttonAdd = '<a id="_add_pattern" href="javascript:void(0);" data-value=' + result.id_pattern[y].pattern_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                            }
                        } else {
                            if (result.id_pattern[parseInt(state.rows) + parseInt(y)].pattern_id == $('input[name="pattern_id"]').val()) {
                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                buttonDel = '<a id="_del_pattern" href="javascript:void(0);" data-value=' + result.id_pattern[parseInt(state.rows) + parseInt(y)].pattern_id + '  class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                            } else {
                                buttonAdd = '<a id="_add_pattern" href="javascript:void(0);" data-value=' + result.id_pattern[parseInt(state.rows) + parseInt(y)].pattern_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                            }
                        }
                        //Keep in mind we are using "Template Litterals to create rows"
                        var row = '<tr>' +
                            '<th style="vertical-align: center;">' +
                            'ชื่อรูปแบบ: ' + myList[y].pattern_name + " <br/>" +
                            '</th>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonAdd +
                            '</td>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonDel +
                            '</td>' +
                            '</tr>'
                        table.append(row)
                        end_count = myList[y].pattern_id
                    }
                }
                if (myList.length == 0) {
                    var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                    table.append(row)
                    $('#total-pattern-add').text(0)
                    $('#start-pattern-add').html(0)
                } else {
                    if (myList[0].doc_id != "" && myList[0].doc_action != 1 && myList[0].doc_status == 2) {
                        $('#start-pattern-add').text(myList[0].pattern_id)
                    }
                    pageButtons(data.pages)
                    $('#total-pattern-add').text(result.pattern.length)
                }
                $('#end-pattern-add').html(end_count)

                $('a#_add_pattern').on('click', function () {
                    $.post(`/${Sub}pattern/select-pattern`, { value: "@lltr@Se1ectP@ttern", id: $(this).attr('data-value') }).done(function (result) {
                        $('input[name="pattern_id"]').val(result.pattern[0].pattern_id)
                        $('select[name="pattern_processing_base_id"]').find('option:selected').removeAttr('selected')
                        $('input[name="classify_name_part1"]').val(result.pattern[0].pattern_name)
                        $('input[name="classify_data_exception_or_unnecessary_filter_name"]').val(result.pattern[0].pattern_name)
                        $('input[name="classify_data_exception_or_unnecessary_filter_tag"]').val(result.pattern[0].pattern_tag)
                        $('input[name="classify_data_exception_or_unnecessary_filter_label"').val(result.pattern[0].pattern_label)
                        let fullname = result.total_name_inside.map(e => e.join(" "))
                        $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').val([...fullname])
                        $('input[name="classify_user_access_info_process_inside_from_pattern"]').val(result.pattern[0].pattern_processor_inside_id)
                        let fullname_outside = result.total_name_outside.map(e => e.join(" "))
                        $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').val([...fullname_outside])
                        $('input[name="classify_user_access_info_process_outside_from_pattern"]').val(result.pattern[0].pattern_processor_outside_id)
                        var _file_ = ""
                        var _database_ = ""
                        if (result.pattern[0].pattern_type_data_file == 1) {
                            _file_ = "มี"
                        } else {
                            _file_ = "ไม่มี"
                        }
                        if (result.pattern[0].pattern_type_database == 1) {
                            _database_ = result.pattern[0].pattern_type_database_name
                        } else {
                            _database_ = "ไม่มี"
                        }
                        var _inside_ = ""
                        var _outside_0 = ""
                        var _outside_1 = ""
                        var _outside_2 = ""
                        var _outside_3 = ""
                        if (result.pattern[0].pattern_storage_method_inside == 1) {
                            _inside_ = "มี"
                        } else {
                            _inside_ = "ไม่มี"
                        }
                        if (result.pattern[0].pattern_storage_method_outside == 1) {
                            _outside_0 = "มี"
                            if (result.pattern[0].pattern_storage_method_outside_device == 1) {
                                _outside_1 = result.pattern[0].pattern_storage_method_outside_device_name
                            } else {
                                _outside_1 = "ไม่มี"
                            }
                            if (result.pattern[0].pattern_storage_method_outside_agent == 1) {
                                _outside_2 = result.pattern[0].pattern_storage_method_outside_agent_name
                            } else {
                                _outside_2 = "ไม่มี"
                            }
                            if (result.pattern[0].pattern_storage_method_outside_database_outside == 1) {
                                _outside_3 = result.pattern[0].pattern_storage_method_outside_database_outside_name
                            } else {
                                _outside_3 = "ไม่มี"
                            }
                        } else {
                            _outside_0 = "ไม่มี"
                        }
                        var data_name_total = result.data_name_total.map(e => e['data_name'])
                        data_name_total = data_name_total.join(", ")
                        $('textarea[name="example_pattern"]').text(`ชื่อ: ${result.pattern[0].pattern_name}
                       
ข้อมูล: ${data_name_total}
ระยะเวลา: ${convert_date(new Date(result.pattern[0].pattern_start_date))} ทั้งหมด: ${result.pattern[0].pattern_total_date} วัน
ชนิดข้อมูล:
    File Ms Excel/CSV: ${_file_}
    Database: ${_database_}
วิธีการจัดเก็บข้อมูลส่วนบุคคล:
    ภายใน Alltra: ${_inside_}
    ภายนอก: ${_outside_0}, [ (อุปกรณ์: ${_outside_1}),  (Agent: ${_outside_2}),  (Database: ${_outside_3}) ]`)
                        check_doc_id_pattern();
                        $('input[name="classify_period_end_follow_pattern_total"]').val(result.pattern[0].pattern_total_date)
                        $('select[name="pattern_processing_base_id"]').find('option[value=' + result.pattern[0].pattern_processing_base_id + ']').attr('selected', 'selected')
                        $('div#select-pattern').modal('hide')
                        $('#table-body-pattern-add').empty()
                        buildTable()
                    })
                })
                $('a#_del_pattern').on('click', function () {
                    let selected = $(this).attr('data-value')
                    $('input[name="pattern_id"]').val(null)
                    $('select[name="pattern_processing_base_id"]').find('option:selected').removeAttr('selected')
                    $('input[name="classify_name_part1"]').val(null)
                    $('input[name="classify_data_exception_or_unnecessary_filter_name"]').val(null)
                    $('input[name="classify_data_exception_or_unnecessary_filter_tag"]').val(null)
                    $('input[name="classify_data_exception_or_unnecessary_filter_label"').val(null)
                    $('input[name="classify_user_access_info_process_inside_from_pattern"]').val(null)
                    $('input[name="classify_user_access_info_process_outside_from_pattern"]').val(null)
                    $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').val(null)
                    $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').val(null)
                    $('textarea[name="example_pattern"]').text(null)
                    check_doc_id_pattern();
                    $('select[name="pattern_processing_base_id"]').find('option:disabled').attr('selected', 'selected')
                    $('input[name="classify_period_end_follow_pattern_total"]').val(null)
                    $('#table-body-pattern-add').empty()
                    buildTable()
                })
            }
        })
    })
    // Modal Select Event-process
    var count_click1 = 0
    $('a[href="#select-event-process"]').on('click', function () {
        $.post(`/${Sub}event_process`, { value: "@lltr@Cl@ssific@ti0nEventPr0cess" }, function (result) {
            count_click1 += 1
            // ============================== Create Prepare ============================
            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
            var state = {
                'querySet': result.event,
                'page': 1,
                'rows': 5,
                'window': 5,
            }
            if (count_click1 == 1) {
                buildTable()
            }
            function pagination(querySet, page, rows) {

                var trimStart = (page - 1) * rows
                var trimEnd = trimStart + rows

                var trimmedData = querySet.slice(trimStart, trimEnd)

                var pages = Math.ceil(querySet.length / rows);

                var start_count = 1
                document.getElementById('start-event-add').innerHTML = start_count

                return {
                    'querySet': trimmedData,
                    'pages': pages,
                }
            }
            // ============================== Create Pagination ============================
            function pageButtons(pages) {
                var wrapper = document.getElementById('pagination-wapper-event-add')
                wrapper.innerHTML = ``
                var maxLeft = (state.page - Math.floor(state.window / 2))
                var maxRight = (state.page + Math.floor(state.window / 2))

                if (maxLeft < 1) {
                    maxLeft = 1
                    maxRight = state.window
                }

                if (maxRight > pages) {
                    maxLeft = pages - (state.window - 1)
                    if (maxLeft < 1) {
                        maxLeft = 1
                    }
                    maxRight = pages
                }

                if (state.page > 1) {
                    wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                } else {
                    wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                }


                num = 1
                if (maxRight > 5) {
                    if (state.page > (maxRight / 2)) {
                        if ((state.page + 1) > (maxRight / 2)) {
                            wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                            wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        }
                    }
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if ((page >= state.page - 2) && (page <= state.page + 2)) {
                            if (page == state.page) {
                                wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                            else {
                                p = page - 1;
                                wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                        }
                    }
                    if ((state.page) <= (maxRight / 2)) {
                        mp = maxRight - 1;
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                    }
                }
                else {
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if (state.page == page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                        }
                    }
                }

                if (state.page < pages) {
                    wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                } else {
                    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                }

                $('.page').on('click', function () {
                    $('#table-body-event-add').empty()
                    state.page = Number($(this).val())
                    // document.getElementById('page').value = state.page
                    buildTable()
                })
            }
            // ============================== Create Table ============================
            function buildTable() {
                var table = $('#table-body-event-add')
                var data = pagination(state.querySet, state.page, state.rows)
                var myList = data.querySet
                for (y in myList) {
                    if (myList[y].event_process_id != "") {
                        let buttonAdd = ""
                        let buttonDel = ""
                        if (state.page == 1) {
                            if (myList[y].event_process_name == $('input[name="classify_name_part2"]').val() && result.id_event[y].event_process_id == $('input[name="event_process_id"]').val()) {
                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                buttonDel = '<a id="_del_event" href="javascript:void(0);" data-value=' + result.id_event[y].event_process_id + '  class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                            } else {
                                buttonAdd = '<a id="_add_event" href="javascript:void(0);" data-value=' + result.id_event[y].event_process_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                            }
                        } else {
                            if (myList[y].event_process_name == $('input[name="classify_name_part2"]').val() && result.id_event[parseInt(state.rows) + parseInt(y)].event_process_id == $('input[name="event_process_id"]').val()) {
                                buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                buttonDel = '<a id="_del_event" href="javascript:void(0);" data-value=' + result.id_event[parseInt(state.rows) + parseInt(y)].event_process_id + '  class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                            } else {
                                buttonAdd = '<a id="_add_event" href="javascript:void(0);" data-value=' + result.id_event[parseInt(state.rows) + parseInt(y)].event_process_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                            }
                        }
                        //Keep in mind we are using "Template Litterals to create rows"
                        var row = '<tr>' +
                            '<th style="vertical-align: center;">' +
                            'ชื่อกิจกรรม: ' + myList[y].event_process_name + " <br/>" +
                            '</th>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonAdd +
                            '</td>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonDel +
                            '</td>' +
                            '</tr>'
                        table.append(row)
                        end_count = myList[y].event_process_id
                    }
                }
                if (myList.length == 0) {
                    var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                    table.append(row)
                    $('#total-event-add').text(0)
                    $('#start-event-add').html(0)
                } else {
                    if (myList[0].event_process_id != "") {
                        $('#start-event-add').text(myList[0].event_process_id)
                    }
                    pageButtons(data.pages)
                    $('#total-event-add').text(result.event.length)
                }
                $('#end-event-add').html(end_count)

                $('a#_add_event').on('click', function () {
                    $.post(`/${Sub}event_process/get`, { value: "@lltr@Cl@ssific@iti0nGetEventPr@cess", id: $(this).attr('data-value') }).done(function (result) {
                        $('input[name="event_process_id"]').val(result[0].event_process_id)
                        $('input[name="classify_name_part2"]').val(result[0].event_process_name)
                        $('div#select-event-process').modal('hide')
                        $('#table-body-event-add').empty()
                        buildTable()
                    })
                })
                $('a#_del_event').on('click', function () {
                    $('input[name="event_process_id"]').val(null)
                    $('input[name="classify_name_part2"]').val(null)
                    $('#table-body-event-add').empty()
                    buildTable()
                })
            }
        })
    })
    // Modal Add Processing-base
    $('a#btn_add_processing_base').on('click', function () {
        $('a#add_processing_base').on('click', function () {
            if ($('input[name="pattern_processing_base_name"]').val() != "") {
                var name = $('input[name="pattern_processing_base_name"]').attr('name')
                var value = $('input[name="pattern_processing_base_name"]').val()
                var obj = {}
                obj[name] = value
                $.ajax({
                    url: `/${Sub}processing_base`,
                    method: "POST",
                    data: obj,
                }).done(function () {
                    $('input[name="pattern_processing_base_name"]').val(null);
                    $('div#add-processing-base').modal('hide');
                    $('#select_processing_base').load(document.URL + ' #select_processing_base ');
                })
            }
        })
    })
    // Modal Add Special-conditions
    $('a#btn_add_special_conditions').on('click', function () {
        $('a#add-special-conditions').on('click', function () {
            if ($('input[name="classification_special_conditions_name"]').val() != "") {
                var name = $('input[name="classification_special_conditions_name"]').attr('name')
                var value = $('input[name="classification_special_conditions_name"]').val()
                var obj = {}
                obj[name] = value
                $.ajax({
                    url: `/${Sub}event_process/add`,
                    method: "POST",
                    data: obj,
                }).done(function () {
                    $('input[name="classification_special_conditions_name"]').val(null)
                    $('div#add_special_conditions').modal('hide')
                    $('#select_special_conditions').load(document.URL + ' #select_special_conditions')
                })
            }
        })
    })
} else if (document.getElementById('dataflow')) {
    // Sidebar
    $('li#li-dataflow').addClass('selected')
    $('a#li-dataflow').addClass('active')
    $('ul#ul-dataflow').addClass('in')
    $('a#a-export-dataflow').addClass('active')
    $('ul#ul-export-dataflow').addClass('in')
    $('a#a-dataflow').addClass('active')
    // =========================== Main Function =======================
    let selected = new Array(5)
    // ================== Check Theme ==================
    let background_color = "#ffffff";
    let border_color = "#000000";
    let color = "#000000";
    let color_name = "";
    let color_event = "";
    if (document.getElementById('theme-view').checked == true) {
        color_name = "#ffffff";
        color_event = "yellow";
    } else {
        color_name = "#000000";
        color_event = "red";
    }
    $('div#data-content').html('<p style="color:red; text-align: center">ไม่พบข้อมูล...</p>')
    $('button:button#search-dataflow').on('click', function () {
        for (var i = 0; i < $('select').length; i++) {
            selected[i] = $('select')[i].value
        }
        $.post(`/${Sub}dataflow/`, { policy: selected[0], pattern: selected[1], classify: selected[2], account: selected[3], data: selected[4] }).done(function (result) {
            let re = result.result
            let raw = result.data
            let user = result.account
            if (re.policy.length > 0 && re.pattern.length > 0 && re.classify.length > 0 && (re.account.length > 0 || re.data.length > 0)) {
                let total_raw_data;
                let total_raw_data_from_pattern;
                let end_date_pattern = new Array(re.pattern.length)
                let total_raw_data_from_classify;
                let end_date_classify = new Array(re.classify.length)
                let set_classify_name = new Array(re.classify.length)
                for (i in re.classify) {
                    set_classify_name[i] = re.classify[i].classify_name.split(/[()]+/).filter(function (item) { return item; })
                }
                // ================== Datail Pattern ===============
                let type_data = []
                let storage_method = []
                let set_end = []
                let dpo = []
                let processor_in = []
                let processor_out = []
                // ================== Datail Classify ===============
                let c_type_data = []
                let c_storage_method = []
                let c_user_info_in = []
                let c_user_info_out = []
                let c_process_time = []
                let c_end_process_time = []
                let c_data_private = []
                let c_base_process = []
                let c_cond_special = []
                let c_time_info = []
                let c_2_factor = []
                let c_identify = []
                let c_total_data_process = []
                let c_control_data_out = []
                let c_protect_data_out = []
                let c_method_of_personal = []
                let c_method_of_organization = []
                // ================== Check runtime ===============
                if (re.policy.length > 0) {
                    let total_data = re.policy.map(e => e['page_content'])
                    let clean_html = total_data.map(e => String(e).replace(/(<([^>]+)>)/gim, ""))
                    let clean_another = clean_html.map(e => String(e).replace(/(\r\n|\n|\r)/gm, ""))
                    let clean_code = clean_another.map(e => String(e).replace(/\&nbsp;/g, ''))
                    clean_code.join("")
                    total_raw_data = raw.filter(function (e) { return clean_code.indexOf(e.data_code) != -1 })
                    total_raw_data = new Set(total_raw_data)
                    total_raw_data = [...total_raw_data]
                    $('div#data-content').html(`
<div id="content-policy" class="col-pop-main" align="center">
    <a id="show-total-data" href="javascript:void(0);" method="${re.policy[0].doc_id}">
    <div style="background-color: ${background_color}; width:140px; height:45px; border-radius: 20px; color: ${color}; vertical-align: middle;border-style: solid; border-color: ${border_color};">
        <p style="padding-top: 2px; text-align: center; ">${total_raw_data.length}&emsp;<img src="/UI/image/${re.policy[0].image}" style="width: 20%; border-radius: 50%; vertical-align: -7px;" /></p>
        <p id="name-policy" style="padding-top: 10px; color: ${color_name}; text-align: center;">${re.policy[0].doc_name}<br>Exp. 12/09/2023</p>
    </div><br/><br/><br/>
    </a>
    <div id="card-total-data" class="card" data-value="${re.policy[0].doc_id}" hidden>
        <div id="total-data" class="card-body" style="background-color: ${background_color}; border-style: solid; border-color: ${border_color}">
        </div>
    </div>
</div>
<div id="arrow_right_pattern" class="col-connect-pattern" hidden>
</div>
<div id="content-pattern" class="col-pop-main" align="center" hidden></div>
<div id="arrow_right_classify" class="col-connect-classify" hidden></div>
<div id="content-classify" class="col-pop-main" align="center" hidden></div>
            `)
                    if (total_raw_data.length > 0) {
                        $('div#total-data').html(`<span style="color: red;">${total_raw_data}</span>`)
                    } else {
                        $('div#total-data').attr('align', 'center');
                        $('div#total-data').html(`<span style="color: red; text-align: center;">ไม่พบข้อมูลส่วนบุคคล</span>`)
                    }
                }
                if (re.pattern.length > 0) {
                    $('div#content-pattern').removeAttr('hidden');
                    let end = re.pattern.map(e => e['pattern_start_date'])
                    let total = re.pattern.map(e => e['pattern_total_date'])
                    for (i in end) {
                        let _end_ = new Date(end[i])
                        _end_.setDate(_end_.getDate() + total[i]);
                        _end_.toLocaleString('th-TH', { timezone: "Asia/Thailand" })
                        let month = _end_.getMonth() + 1
                        let day = _end_.getDate()
                        if (String(month).length == 1) {
                            month = "0" + String(month)
                        }
                        if (String(day).length == 1) {
                            day = "0" + String(day)
                        }
                        end_date_pattern[i] = (day + "/" + month + "/" + (_end_.getFullYear()))
                    }
                    let total_tag = re.pattern.map(e => e['pattern_tag'].split(","))
                    let search_data = total_tag.map(function (e) {
                        return e.map(function (f) {
                            return raw.filter(function (item) { return item.data_tag.indexOf(f) != -1 })
                        })
                    })
                    let convert_search_data = search_data.map(e => e.map(f => f.map(b => b['data_name'])))
                    let convert_search_data1 = convert_search_data.reduce(function (prev, next) {
                        return prev.concat(next)
                    })
                    let convert_search_data2 = convert_search_data1.reduce(function (prev, next) {
                        return prev.concat(next)
                    })
                    let set_data = new Set(convert_search_data2)
                    total_raw_data_from_pattern = [...set_data]
                    for (i in re.pattern) {
                        let set_type_data = { 'file': null, "type_path": null, "path": null, "database": null, "db_name": null }
                        let set_storage_method = { "inside": null, "outside": null, "outside_device": null, "outside_de_name": null, "outside_agent": null, "outside_a_name": null, "outside_db": null, "outside_db_name": null }
                        let _set_end = { "end": null, "total": null }
                        let set_dpo = { 'approve_process': null, "approve_raw_file_out": null }
                        let total_users_in;
                        if (re.pattern[i].pattern_processor_inside_id.includes(',') || re.pattern[i].pattern_processor_inside_id != null || re.pattern[i].pattern_processor_inside_id != "") {
                            total_users_in = re.pattern[i].pattern_processor_inside_id.split(",")
                        } else {
                            total_users_in = 0
                        }
                        let set_processor_in = [];
                        if (total_users_in.length > 0) {
                            total_users_in.forEach(e => set_processor_in.push({ 'inside': null, "full_name": null, "image": null }))
                        } else {
                            set_processor_in = { 'inside': null, "full_name": null, "image": null }
                        }
                        let total_users_out;
                        if (re.pattern[i].pattern_processor_outside_id.includes(',') || re.pattern[i].pattern_processor_outside_id != null || re.pattern[i].pattern_processor_outside_id != "") {
                            total_users_out = re.pattern[i].pattern_processor_outside_id.split(",")
                        } else {
                            total_users_out = 0
                        }
                        let set_processor_out = [];
                        if (total_users_out.length > 0) {
                            total_users_out.forEach(e => set_processor_out.push({ 'outside': null, "full_name": null, "image": null }))
                        } else {
                            total_users_out = { 'outside': null, "full_name": null, "image": null }
                        }
                        if (re.pattern[i].pattern_type_data_file == 1) {
                            set_type_data.file = "มี"
                            if (re.pattern[i].pattern_type_data_file_of_path == 1) {
                                set_type_data.type_path = "Windows"
                                if (re.pattern[i].pattern_type_data_file_path != null || re.pattern[i].pattern_type_data_file_path != "") {
                                    set_type_data.path = re.pattern[i].pattern_type_data_file_path
                                } else {
                                    set_type_data.path = "ไม่ระบุ"
                                }
                            } else if (re.pattern[i].pattern_type_data_file_of_path == 0) {
                                set_type_data.type_path = "Linux"
                                if (re.pattern[i].pattern_type_data_file_path != null || re.pattern[i].pattern_type_data_file_path != "") {
                                    set_type_data.path = re.pattern[i].pattern_type_data_file_path
                                } else {
                                    set_type_data.path = "ไม่ระบุ"
                                }
                            } else {
                                set_type_data.type_path = "ไม่มี"
                            }
                        } else {
                            set_type_data.file = "ไม่มี"
                        }
                        if (re.pattern[i].pattern_type_data_database == 1) {
                            set_type_data.database = "มี"
                            if (re.pattern[i].pattern_type_data_database_name != null || re.pattern[i].pattern_type_data_database_name != "") {
                                set_type_data.db_name = re.pattern[i].pattern_type_data_database_name
                            } else {
                                set_type_data.db_name = "ไม่ระบุ"
                            }
                        } else {
                            set_type_data.database = "ไม่มี"
                        }
                        if (re.pattern[i].pattern_storage_method_inside == 1) {
                            set_storage_method.inside = "มี"
                        } else {
                            set_storage_method.inside = "ไม่มี"
                        }
                        if (re.pattern[i].pattern_storage_method_outside == 1) {
                            set_storage_method.outside = "มี"
                            if (re.pattern[i].pattern_storage_method_outside_device == 1) {
                                set_storage_method.outside_device = "มี"
                                if (re.pattern[i].pattern_storage_method_outside_device_name != null || re.pattern[i].pattern_storage_method_outside_device_name != "") {
                                    set_storage_method.outside_de_name = re.pattern[i].pattern_storage_method_outside_device_name
                                } else {
                                    set_storage_method.outside_de_name = "ไม่ระบุ"
                                }
                            } else {
                                set_storage_method.outside_device = "ไม่มี"
                            }
                            if (re.pattern[i].pattern_storage_method_outside_agent == 1) {
                                set_storage_method.outside_agent = "มี"
                                if (re.pattern[i].pattern_storage_method_outside_agent_name != null || re.pattern[i].pattern_storage_method_outside_agent_name != "") {
                                    set_storage_method.outside_a_name = re.pattern[i].pattern_storage_method_outside_agent_name
                                } else {
                                    set_storage_method.outside_a_name = "ไม่ระบุ"
                                }
                            } else {
                                set_storage_method.outside_agent = "ไม่มี"
                            }
                            if (re.pattern[i].pattern_storage_method_outside_database_outside == 1) {
                                set_storage_method.outside_db = "มี"
                                if (re.pattern[i].pattern_storage_method_outside_database_outside_name != null || re.pattern[i].pattern_storage_method_outside_database_outside_name != "") {
                                    set_storage_method.outside_db_name = re.pattern[i].pattern_storage_method_outside_database_outside_name
                                } else {
                                    set_storage_method.outside_db_name = "ไม่ระบุ"
                                }
                            } else {
                                set_storage_method.outside_db = "ไม่มี"
                            }
                        } else {
                            set_storage_method.outside = "ไม่มี"
                        }
                        if (re.pattern[i].pattern_set_end_date == 1) {
                            _set_end.end = "มี"
                            if (re.pattern[i].pattern_set_end_date_total != null, re.pattern[i].pattern_set_end_date_total != "") {
                                _set_end.total = re.pattern[i].pattern_set_end_date_total
                            } else {
                                _set_end.total = "ไม่ระบุ"
                            }
                        } else {
                            _set_end.end = "ไม่มี"
                        }
                        if (re.pattern[i].pattern_dpo_approve_process == 1) {
                            set_dpo.approve_process = "มี"
                        } else {
                            set_dpo.approve_process = "ไม่มี"
                        }
                        if (re.pattern[i].pattern_dpo_approve_raw_file_out == 1) {
                            set_dpo.approve_raw_file_out = "มี"
                        } else {
                            set_dpo.approve_raw_file_out = "ไม่มี"
                        }
                        if (re.pattern[i].pattern_processor_inside == 1) {
                            if (re.pattern[i].pattern_processor_inside_id != null, re.pattern[i].pattern_processor_inside_id != "") {
                                total_users_in.forEach(function (d, j) {
                                    set_processor_in[j].inside = "มี"
                                    set_processor_in[j].full_name = user.map(function (item) { if (item.acc_id == d) { return item.firstname + " " + item.lastname } }).join("")
                                    set_processor_in[j].image = user.map(function (item) { if (item.acc_id == d) { return item.image } }).join("")
                                })
                            } else {
                                set_processor_in.full_name = "ไม่ระบุ"
                                set_processor_in.image = "ไม่ระบุ"
                            }
                        } else {
                            set_processor_in.inside = "ไม่มี"
                        }
                        if (re.pattern[i].pattern_processor_outside == 1) {
                            if (re.pattern[i].pattern_processor_outside_id != null, re.pattern[i].pattern_processor_outside_id != "") {
                                total_users_out.forEach(function (d, j) {
                                    set_processor_out[j].outside = "มี"
                                    set_processor_out[j].full_name = user.map(function (item) { if (item.acc_id == d) { return item.firstname + " " + item.lastname } }).join("")
                                    set_processor_out[j].image = user.map(function (item) { if (item.acc_id == d) { return item.image } }).join("")
                                })
                            } else {
                                set_processor_out.full_name = "ไม่ระบุ"
                                set_processor_out.image = "ไม่ระบุ"
                            }
                        } else {
                            set_processor_out.outside = "ไม่มี"
                        }
                        type_data.push(set_type_data)
                        storage_method.push(set_storage_method)
                        set_end.push(_set_end)
                        dpo.push(set_dpo)
                        processor_in.push(set_processor_in)
                        processor_out.push(set_processor_out)
                    }
                    $('div#arrow_right_pattern').removeAttr('hidden')
                    for (i in re.pattern) {
                        $('div#content-pattern').append(`
<div id="div-pattern" method="${re.pattern[i].pattern_id}" style="background-color: ${background_color}; width:140px; height:45px; border-radius: 20px; color: ${color}; align-items: center; vertical-align: middle; border: 5px solid ${getColorCode()}">
            <a id="show-total-pattern" href="javascript:void(0);" method="${re.pattern[i].pattern_id}">
            <p style="text-align: center; color: ${color}">${total_raw_data_from_pattern.length}&emsp;<img src="/UI/image/${re.pattern[i].image}" style="width: 20%; border-radius: 50%; vertical-align: -7px;" />
            </p>
            <p id="name-pattern" style="padding-top: 10px; color: ${color_name};">${re.pattern[i].pattern_name}<br>Exp. ${end_date_pattern[i]}</p>
            </a>
</div><br/><br/><br/>
<div id="total-pattern" data-value="${re.pattern[i].pattern_id}" hidden>
<div class="card">
    <div class="card-body" style="background-color: #ffdb99; color: ${color};">
        ${[...total_raw_data_from_pattern]}
    </div>
</div>
<img src="/UI/assets/images/circle/arrow_bottom.png" width="10%" style="margin-bottom: 5%;"/>
<div class="card">
    <div class="card-body" style="background-color: #ffdb99; font-size: 12px; text-align: left; color: ${color};">
ชนิดข้อมูล: File ms excel = <span style="color: green;">${type_data[i].file}</span> ที่จัดเก็บ <span style="color: green;">${type_data[i].type_path}</span><br/>[ <span style="color: green;">${type_data[i].path}</span> ] / Database = <span style="color: green;">${type_data[i].database}</span> คือ <span style="color: green;">${type_data[i].db_name}</span><br/> 
วิธีจัดเก็บ ภายใน และ ภายนอก Alltra: <br/> ภายใน: <span style="color: green;">${storage_method[i].inside}</span><br/> ภายนอก: <span style="color: green;">${storage_method[i].outside}</span> รูปแบบ [ Device: <span style="color:green;">${storage_method[i].outside_device}</span> / <span style="color: green;">${storage_method[i].outside_de_name}</span>, Agent: <span style="color: green;">${storage_method[i].outside_agent}</span> / <span style="color: green;">${storage_method[i].outside_a_name}</span>, Database ภายนอก: <span style="color: green;">${storage_method[i].outside_db}</span> / <span style="color: green;">${storage_method[i].outside_db_name}</span> ]<br/>
ผู้มีสิทธิ์นำข้อมูลไปใช้ (ภายใน): จำนวนทั้งหมด <span style="color: green;">${re.pattern[i].pattern_processor_inside_total}</span> คือ<br/> <span style="color: green;">${processor_in[i].map(e => " " + e['full_name'])}</span><br/>
ผู้มีสิทธิ์นำข้อมูลไปใช้ (ภายนอก): จำนวนทั้งหมด <span style="color: green;">${re.pattern[i].pattern_processor_outside_total}</span> คือ<br/> <span style="color: green;">${processor_out[i].map(e => " " + e['full_name'])}</span><br/>
กำหนดระยะเวลาสิ้นสุด: <span style="color: green;">${set_end[i].end}</span> / <span style="color: green;">${set_end[i].total}</span><br/>
ตรวจสอบโดย DPO: [ ต้องอนุมัติทุกครั้งถึงสามารถประมวลผลได้: <span style="color: green;">${dpo[i].approve_process}</span>, ต้องอนุมัติทุกครั้งที่จะนำข้อมูล Raw file ออกได้: <span style="color: green;">${dpo[i].approve_raw_file_out}</span> ] <br/>
    </div>
</div>
<img src="/UI/assets/images/circle/arrow_bottom.png" width="10%" style="margin-bottom: 5%;"/>
<p id='processor_in'>
</p>
<p style="margin-top: 10%; margin-bottom: 5%">
    <img src="/UI/assets/images/circle/arrow_bottom.png" width="10%"/>
</p>
<p id="processor_out">
</p>
</div>
                    `)
                        processor_in.forEach(function (d, j) {
                            d.map(function (m, k) {
                                $('p#processor_in').append(`<img src="/UI/image/${m.image}" width="10%" style="border-radius: 50%; border: 2px solid #64ff00;"/> &nbsp;`)
                            })
                        })
                        processor_out.forEach(function (d, j) {
                            d.map(function (m, k) {
                                $('p#processor_out').append(`<img src="/UI/image/${m.image}" width="10%" style="border-radius: 50%; border: 2px solid #dd0000;" /> &nbsp;`)
                            })
                        })
                    }
                }
                if (re.classify.length > 0) {
                    $('div#arrow_right_classify').removeAttr('hidden')
                    $('div#content-classify').removeAttr('hidden')
                    let end = re.classify.map(e => e['classify_create'])
                    let total = re.classify.map(e => e['classify_period_end'])
                    for (i in end) {
                        let _end_ = new Date(end[i])
                        _end_.setDate(_end_.getDate() + total[i])
                        _end_.toLocaleString('th-th', { timezone: "asia/thailand" })
                        let month = _end_.getMonth() + 1
                        let day = _end_.getDate()
                        if (String(month).length == 1) {
                            month = "0" + String(month)
                        }
                        if (String(day).length == 1) {
                            day = "0" + String(day)
                        }
                        end_date_classify[i] = (day + "/" + month + "/" + (_end_.getFullYear()))
                    }
                    // ยังไม่สมบูรณ์
                    let total_tag_from_pattern = re.classify.map(e => e['pattern_tag'].split(','))
                    let filter_tag_from_classify = re.classify.map(e => e['classify_data_exception_or_unnecessary_filter_tag'].split(','))
                    let object_clone_data1 = total_tag_from_pattern.map(e => e.map(function (f) {
                        return raw.filter(item => item.data_tag.indexOf(f) != -1)
                    }))

                    let use_data = []
                    object_clone_data1.forEach((e, i) => e.forEach(function (f, j) {
                        f.forEach(function (d, k) {
                            if (typeof filter_tag_from_classify[i][j] != 'undefined') {
                                if (d.data_tag.indexOf(filter_tag_from_classify[i][j]) == -1) {
                                    use_data.push(d.data_name)
                                }
                            }
                        })
                    }))
                    total_raw_data_from_classify = use_data
                    re.classify.forEach(function (e, i) {
                        c_process_time.push(e.classify_period_process)
                        c_end_process_time.push(e.classify_period_end)
                        if (e.classify_type_data_in_event_personal == 1 && e.classify_type_data_in_event_special_personal_sensitive == 1) {
                            c_data_private.push({ 'normal': "มี", "sensitive": "มี" })
                        } else if (e.classify_type_data_in_event_personal == 1 && e.classify_type_data_in_event_special_personal_sensitive == 0) {
                            c_data_private.push({ 'normal': "มี", "sensitive": "ไม่มี" })
                        } else if (e.classify_type_data_in_event_personal == 0 && e.classify_type_data_in_event_special_personal_sensitive == 1) {
                            c_data_private.push({ 'normal': "ไม่มี", "sensitive": "มี" })
                        } else if (e.classify_type_data_in_event_personal == 0 && e.classify_type_data_in_event_special_personal_sensitive == 0) {
                            c_data_private.push({ 'normal': "ไม่มี", "sensitive": "ไม่มี" })
                        }
                        c_base_process.push(e.pattern_processing_base_name)
                        c_cond_special.push(e.classification_special_conditions_name)
                        if (e.classify_protect_data_limit_follow_datetime == 1) {
                            c_time_info.push('จ-ศ 08.00-17.00')
                        } else {
                            c_time_info.push('ไม่มี')
                        }
                        if (e.classify_approach_protect_used_two_factor_from_google_authen == 1 && e.classify_approach_protect_used_two_factor_from_email == 1 && e.classify_approach_protect_used_two_factor_from_sms == 1) {
                            c_2_factor.push(['google-authen ', 'e-mail ', "sms "])
                        } else if (e.classify_approach_protect_used_two_factor_from_google_authen == 1 && e.classify_approach_protect_used_two_factor_from_email == 1 && e.classify_approach_protect_used_two_factor_from_sms == 0) {
                            c_2_factor.push(['google-authen ', 'e-mail '])
                        } else if (e.classify_approach_protect_used_two_factor_from_google_authen == 1 && e.classify_approach_protect_used_two_factor_from_email == 0 && e.classify_approach_protect_used_two_factor_from_sms == 0) {
                            c_2_factor.push('google-authen ')
                        } else if (e.classify_approach_protect_used_two_factor_from_google_authen == 0 && e.classify_approach_protect_used_two_factor_from_email == 1 && e.classify_approach_protect_used_two_factor_from_sms == 1) {
                            c_2_factor.push(['e-mail ', 'sms '])
                        } else if (e.classify_approach_protect_used_two_factor_from_google_authen == 0 && e.classify_approach_protect_used_two_factor_from_email == 1 && e.classify_approach_protect_used_two_factor_from_sms == 0) {
                            c_2_factor.push('e-mail ')
                        } else if (e.classify_approach_protect_used_two_factor_from_google_authen == 1 && e.classify_approach_protect_used_two_factor_from_email == 1 && e.classify_approach_protect_used_two_factor_from_sms == 1) {
                            c_2_factor.push(['google-authen ', 'sms '])
                        } else if (e.classify_approach_protect_used_two_factor_from_google_authen == 0 && e.classify_approach_protect_used_two_factor_from_email == 1 && e.classify_approach_protect_used_two_factor_from_sms == 1) {
                            c_2_factor.push('sms ')
                        } else if (e.classify_approach_protect_used_two_factor_from_google_authen == 0 && e.classify_approach_protect_used_two_factor_from_email == 1 && e.classify_approach_protect_used_two_factor_from_sms == 0) {
                            c_2_factor.push('ไม่มี ')
                        }
                        if (e.classify_risk_assess_only_dpo_data_personal_can_specify == 1) {
                            c_identify.push('ได้โดยตรง')
                        } else if (e.classify_risk_assess_only_dpo_data_personal_can_specify == 2) {
                            c_identify.push('เป็นข้อมูลไม่ระบุได้โดยตรง')
                        } else if (e.classify_risk_assess_only_dpo_data_personal_can_specify == 3) {
                            c_identify.push('มีการทำข้อมูลแฝง')
                        }
                        if (e.classify_risk_assess_only_dpo_data_number_all_used_process_many == 1) {
                            c_total_data_process.push('มาก')
                        } else {
                            c_total_data_process.push('น้อย')
                        }
                        if (e.classify_risk_assess_only_dpo_access_control_outside == 1) {
                            c_control_data_out.push('มี')
                        } else {
                            c_control_data_out.push('ไม่มี')
                        }
                        if (e.classify_risk_assess_only_dpo_protect_data_outside == 1) {
                            c_protect_data_out.push('มี')
                        } else {
                            c_protect_data_out.push('ไม่มี')
                        }
                        c_method_of_personal.push(e.classify_risk_assess_only_dpo_fix_a_leak_of_data)
                        c_method_of_organization.push(e.classify_risk_assess_only_dpo_fix_a_leak_of_organization)
                    })
                    let total_user_in = []
                    re.classify.forEach(e => total_user_in.push(e.classify_user_access_info_process_inside_from_new_id.split(',')))
                    let total_user_out = []
                    re.classify.forEach(e => total_user_out.push(e.classify_user_access_info_process_outside_from_new_id.split(',')))
                    let selected_user = []
                    total_user_in.forEach(function (d, i) {
                        user.map(function (e) {
                            if (String(e['acc_id']).indexOf(d.join(',')) != -1) {
                                selected_user.push({ "name": e['firstname'] + " " + e['lastname'], "image": e['image'] })
                            }
                        })
                    })
                    let selected_user_outside = []
                    total_user_out.forEach(function (d, i) {
                        user.map(function (e) {
                            if (String(e['acc_id']).indexOf(d.join(",")) != -1) {
                                selected_user_outside.push({ "name": e['firstname'] + " " + e['lastname'], "image": e['image'] })
                            }
                        })
                    })
                    c_user_info_in.push(selected_user)
                    c_user_info_out.push(selected_user_outside)
                    for (i in re.classify) {
                        let getColor;
                        for (var j = 0; j < $('div#div-pattern').length; j++) {
                            if ($('div#div-pattern')[j].getAttribute('method') == re.classify[i].pattern_id) {
                                getColor = $('div#div-pattern')[j].style.border;
                            }
                        }
                        $('div#content-classify').append(`
<div style="background-color: ${background_color}; width:140px; height:45px; border-radius: 20px; color: ${color}; text-align: center; align-items: center; vertical-align: middle; border: ${getColor}">
                        <a id="show-total-classify" href="javascript:void(0);" method="${re.classify[i].classify_id}">
                        <p style="text-align: center; color: ${color}">${total_raw_data_from_classify.length}&emsp;<img src="/UI/image/${re.classify[i].image}" style="width: 20%; border-radius: 50%; vertical-align: -7px;" />
                        </p>
                        <p style="padding-top: 10px;"><span id="name-classify" style="color: ${color_name};">${set_classify_name[i][0]}</span><br><span id="name-event" style="color: ${color_event};">${set_classify_name[i][1]}</span><br/><span id="name-classify1" style="color: ${color_name}">exp. ${end_date_classify}</span></p>
                        </a>
</div><br/><br/><br/><br/>
                        <div id="total-classify" data-value="${re.classify[i].classify_id}" hidden>
<div class="card" style="margin-left: 25%; margin-right: 25%;">
    <div class="card-body" style="background-color: #83b9f9; color: ${color};">
        ${total_raw_data_from_classify}
    </div>
</div>
<p style="margin-bottom: 5%;">
    <img src="/UI/assets/images/circle/arrow_bottom.png" width="10%"/>
</p>
<div class="card" style="margin-left: 2%; margin-right: 2%;">
    <div class="card-body" style="background-color: #83b9f9; color: ${color}; font-size: 12px; text-align: left;">
ชนิดข้อมูล: File ms excel = <span style="color: green;">${type_data[i].file}</span> ที่จัดเก็บ <span style="color: green;">${type_data[i].type_path}</span> <br/> [ <span style="color: green;">${type_data[i].path}</span> ] / Database = <span style="color: green;">${type_data[i].database}</span> คือ <span style="color: green;">${type_data[i].db_name}</span><br/>
วิธีจัดเก็บ ภายใน และ ภายนอก alltra: <br/>
ภายใน: <span style="color: green;">${storage_method[i].inside}</span><br/>
ภายนอก: <span style="colo: green;">${storage_method[i].outside}</span> รูปแบบ [ Device: <span style="color: green;">${storage_method[i].outside_device}</span> / <span style="color: green;">${storage_method[i].outside_de_name}</span>, Agent: <span style="color: green;">${storage_method[i].outside_agent}</span> / <span style="color: green;">${storage_method[i].outside_a_name}</span>, Database ภายนอก: <span style="color: green;">${storage_method[i].outside_db}</span> / <span style="color: green;">${storage_method[i].outside_db_name}</span> ]<br/>
ผู้มีสิทธิ์ประมวลผลภายใน <span style="color: green;">${c_user_info_in[i].length}</span> คน คือ <br/>
<span style="color: green;">${c_user_info_in[i].map(e => e['name'])}</span><br/>
ผู้มีสิทธิ์ประมวลผลภายนอก <span style="color: green;">${c_user_info_out[i].length}</span> คน คือ <br/>
<span style="color: green;">${c_user_info_out[i].map(e => e['name'])}</span><br/>
ระยะเวลาประมวลผล <span style="color: green;">${c_process_time}</span> วัน <br/>
ระยะเวลาสิ้นสุด <span style="color: green;">${c_end_process_time}</span> วัน <br/>
&emsp;&emsp; <span style="color: green;">${c_data_private[0].normal}</span> ข้อมูลส่วนุคคล &emsp; <span style="color: green;">${c_data_private[0].sensitive}</span> ข้อมูลอ่อนไหว <br/>
ฐานการประมวลผล <span style="color: green;">${c_base_process}</span> <br/>
เงื่อนไขพิเศษ <span style="color: green;">${c_cond_special}</span> <br/>
กำหนดเวลาเข้าถึงข้อมูล <span style="color: green;">${c_time_info}</span> <br/>
2 factor <span style="color: green;">${c_2_factor}</span> <br/>
ข้อมูลส่วนุคคล สามารถระบุตัวบุคคลได้ <span style="color: green;">${c_identify}</span> <br/>
ปริมาณข้อมูลทั้งหมดที่ใช้ประมวลผล <span style="color: green;">${c_total_data_process}</span> <br/>
การควบคุมการเข้าถึงข้อมูลระบบนอก alltra <span style="color: green;">${c_control_data_out}</span> <br/>
การป้องกันข้อมูลจากระบบนอก alltra <span style="color: green;">${c_protect_data_out}</span> <br/>
วิธีการแก้ไขเมื่อเกิดข้อมูลรั่วไหลหรือผลกระทบต่อเจ้าของข้อมูล <br/> <span style="color: green;">${c_method_of_personal}</span> <br/>
วิธีการแก้ไขเมื่อเกิดข้อมูลรั่วไหลหรือผลกระทบต่อองค์กร <br/> <span style="color: green;">${c_method_of_organization}</span> <br/>
ตรวจสอบโดย DPO <span style="color: green;">${0}</span>
    </div>
</div>
<p style="margin-bottom: 5%;">
    <img src="/UI/assets/images/circle/arrow_bottom.png" width="10%"/>
</p>
<p id='c_processor_in'>
</p>
<p style="margin-bottom: 5%; margin-top: 10%;">
    <img src="/UI/assets/images/circle/arrow_bottom.png" width="10%"/>
</p>
<p id="c_processor_out">
</p>
                        </div>
                    `)
                        c_user_info_in.forEach(e => e.map(f => $('p#c_processor_in').append(`<img src="/UI/image/${f.image}" style="width: 10%; border-radius: 50%; border: 2px solid #64ff00;" />`)))
                        c_user_info_out.forEach(e => e.map(f => $('p#c_processor_out').append(`<img src="/UI/image/${f.image}" style="width: 10%; border-radius: 50%; border: 2px solid #dd0000;" />`)))
                    }
                }
                // ============ Click show detail ====================
                if ($('a#show-total-data').length > 0) {
                    $('a#show-total-data').on('click', function () {
                        let seleted = $(this).attr('method')
                        if ($('div#card-total-data[data-value="' + seleted + '"]')) {
                            if ($('div#card-total-data[data-value="' + seleted + '"]').attr('hidden')) {
                                $('div#card-total-data[data-value="' + seleted + '"]').removeAttr('hidden')
                            } else {
                                $('div#card-total-data[data-value="' + seleted + '"]').attr('hidden', true)
                            }
                        }
                    })
                }
                if ($('a#show-total-pattern').length > 0) {
                    $('a#show-total-pattern').on('click', function () {
                        let selected = $(this).attr('method')
                        if ($('div#total-pattern[data-value="' + selected + '"]')) {
                            if ($('div#total-pattern[data-value="' + selected + '"]').attr('hidden')) {
                                $('div#total-pattern[data-value="' + selected + '"]').removeAttr('hidden')
                            } else {
                                $('div#total-pattern[data-value="' + selected + '"]').attr('hidden', true)
                            }
                        }
                    })
                }
                if ($('a#show-total-classify').length > 0) {
                    $('a#show-total-classify').on('click', function () {
                        let selected = $(this).attr('method')
                        if ($('div#total-classify[data-value="' + selected + '"]')) {
                            if ($('div#total-classify[data-value="' + selected + '"]').attr('hidden')) {
                                $('div#total-classify[data-value="' + selected + '"]').removeAttr('hidden')
                            } else {
                                $('div#total-classify[data-value="' + selected + '"]').attr('hidden', true)
                            }
                        }
                    })
                }
            } else {
                $('div#data-content').html('<p style="color:red; text-align: center">ไม่พบข้อมูล...</p>')
            }
        })
    })
    $('button:button#preview-print').on('click', function () {
        if ($('div#data-content').children().length > 1) {
            $('a#show-total-data').trigger('click')
            $('a#show-total-pattern').trigger('click')
            $('a#show-total-classify').trigger('click')
            document.getElementById('name-policy').style.color = "#000000"
            document.getElementById('name-pattern').style.color = "#000000"
            document.getElementById('name-classify').style.color = "#000000"
            document.getElementById('name-classify1').style.color = "#000000"
            document.getElementById('name-event').style.color = "red";
            html2canvas(document.getElementById('data-content')).then(function (canvas) {
                //document.getElementById('printer').appendChild(canvas);
                var convert = canvas.toDataURL();
                $.ajax({
                    url: `/${Sub}dataflow/print`,
                    type: "POST",
                    data: { value: convert },
                    success: function (result) {
                        var w = window.open('about:blank');
                        w.document.open();
                        w.document.write(result);
                        w.document.close();
                    }
                });
            });
            document.getElementById('name-policy').style.color = color_name;
            document.getElementById('name-pattern').style.color = color_name;
            document.getElementById('name-classify').style.color = color_name;
            document.getElementById('name-classify1').style.color = color_name;
            document.getElementById('name-event').style.color = color_event;
            $('a#show-total-data').trigger('click')
            $('a#show-total-pattern').trigger('click')
            $('a#show-total-classify').trigger('click')
        } else {
            $('div#swal2-dataflow').css('display', "flex", "overflow-y", "auto");
            $('button:button#alert-close').on('click', function () {
                $('div#swal2-dataflow').css('display', "none");
            });
        }
    });
    $('button:button#download-pdf').on('click', function () {
        if ($('div#data-content').children().length > 1) {
            $('a#show-total-data').trigger('click')
            $('a#show-total-pattern').trigger('click')
            $('a#show-total-classify').trigger('click')
            document.getElementById('name-policy').style.color = "#000000"
            document.getElementById('name-pattern').style.color = "#000000"
            document.getElementById('name-classify').style.color = "#000000"
            document.getElementById('name-classify1').style.color = "#000000"
            document.getElementById('name-event').style.color = "red";
            html2canvas(document.getElementById('data-content')).then(canvas => {
                const ondate = new Date();
                let month = ondate.getMonth() + 1;
                let day = ondate.getDate();
                let hours = ondate.getHours();
                let minutes = ondate.getMinutes();
                let seconds = ondate.getSeconds();
                if (String(month).length == 1) {
                    month = "0" + String(month)
                }
                if (String(day).length == 1) {
                    day = "0" + String(day)
                }
                if (String(hours).length == 1) {
                    hours = "0" + String(hours)
                }
                if (String(minutes).length == 1) {
                    minutes = "0" + String(minutes)
                }
                if (String(seconds).length == 1) {
                    seconds = "0" + String(seconds)
                }
                const maxOnDate = ondate.getFullYear() + "-" + month + "-" + day + "Z" + hours + ":" + minutes + ":" + seconds
                const name_file = "Dataflow_" + maxOnDate + ".pdf";
                const img = canvas.toDataURL('image/jpeg', 1);
                window.jsPDF = window.jspdf.jsPDF;
                var doc = new jsPDF("l", "mm", "a4"); // [297, 210] = "w, s" or "a4"
                var width = doc.internal.pageSize.getWidth() * .95;
                var height = doc.internal.pageSize.getHeight() * .95;
                doc.addImage(img, "JPEG", 5, 5, width, height);
                doc.save(name_file);
            });
            document.getElementById('name-policy').style.color = color_name;
            document.getElementById('name-pattern').style.color = color_name;
            document.getElementById('name-classify').style.color = color_name;
            document.getElementById('name-classify1').style.color = color_name;
            document.getElementById('name-event').style.color = color_event;
            $('a#show-total-data').trigger('click')
            $('a#show-total-pattern').trigger('click')
            $('a#show-total-classify').trigger('click')
        } else {
            $('div#swal2-dataflow').css('display', "flex", "overflow-y", "auto");
            $('button:button#alert-close').on('click', function () {
                $('div#swal2-dataflow').css('display', "none");
            });
        }
    });
} else if (document.getElementById('ev_index')) {
    // ============================== Status block =============================
    // let status1 = document.getElementById('bar1').textContent.split("%");
    // document.getElementById('progress-bar1').setAttribute("aria-valuenow", status1[0]);
    // document.getElementById('progress-bar1').style.cssText = `
    //     width: ${document.getElementById('bar1').textContent};
    //     height: 6px;
    // `;
    // let diskspace_value = document.getElementById('diskspace').textContent;
    // document.getElementById('diskspace').innerHTML = (diskspace_value / 1000000000).toFixed(0);
    // let status2 = document.getElementById('bar2').textContent.split("%");
    // document.getElementById('progress-bar2').setAttribute('aria-valuenow', status2[0]);
    // document.getElementById('progress-bar2').style.cssText = `
    //     width: ${document.getElementById('bar2').textContent};
    //     height: 6px;
    // `
    // let status3 = document.getElementById('bar3').textContent.split("%");
    // document.getElementById('progress-bar3').setAttribute('aria-valuenow', status3[0]);
    // document.getElementById('progress-bar3').style.cssText = `
    //     width: ${document.getElementById('bar3').textContent};
    //     height: 6px;
    // `
    // ============================== Table list =============================
    $.post(`/${Sub}classification/listEventProcess`, { value: "@lltr@Cl@ssifyL1stEvent" }).done(function (result) {
        // ============================== Create Prepare ============================
        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า

        var state = {
            'querySet': result.event,
            'page': 1,
            'rows': 30,
            'window': 10,
            'data_s': result.sp,
        }
        buildTable()
        function pagination(querySet, page, rows) {
            var trimStart = (page - 1) * rows
            var trimEnd = trimStart + rows

            var trimmedData = querySet.slice(trimStart, trimEnd)

            var pages = Math.ceil(querySet.length / rows);
            var start_count = 1
            document.getElementById('start-event').innerHTML = start_count

            return {
                'querySet': trimmedData,
                'pages': pages,
            }
        }
        // ============================== Create Pagination ============================
        function pageButtons(pages) {
            var wrapper = document.getElementById('pagination-wrapper-event')
            wrapper.innerHTML = ``
            var maxLeft = (state.page - Math.floor(state.window / 2))
            var maxRight = (state.page + Math.floor(state.window / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = state.window
            }

            if (maxRight > pages) {
                maxLeft = pages - (state.window - 1)
                if (maxLeft < 1) {
                    maxLeft = 1
                }
                maxRight = pages
            }

            if (state.page > 1) {
                wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
            } else {
                wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
            }


            num = 1
            if (maxRight > 5) {
                if (state.page > (maxRight / 2)) {
                    if ((state.page + 1) > (maxRight / 2)) {
                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    }
                }
                for (var page = maxLeft; page <= maxRight; page++) {
                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                        if (page == state.page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                        }
                        else {
                            p = page - 1;
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                        }
                    }
                }
                if ((state.page) <= (maxRight / 2)) {
                    mp = maxRight - 1;
                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                }
            }
            else {
                for (var page = maxLeft; page <= maxRight; page++) {
                    if (state.page == page) {
                        wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                    } else {
                        wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                    }
                }
            }

            if (state.page < pages) {
                wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
            } else {
                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
            }

            $('.page').on('click', function () {
                $('#table-body-event').empty()
                state.page = Number($(this).val())
                buildTable()
            })
        }
        // ============================== Create Table ============================
        function buildTable() {
            var table = $('#table-body-event')
            var data = pagination(state.querySet, state.page, state.rows)
            var myList = data.querySet
            var data_specifi = state.data_s;
            console.log(data_specifi);

            
            for (y in myList) {
                var specifi_data_event = '';
                for (let io = 0; io < data_specifi.length; io++) {
                    if (myList[y].event_process_id == data_specifi[io].event_process_id) {
                        specifi_data_event = specifi_data_event + ` <a class="text-info" target="_blank" href="/${Sub}Specific-Measures/details/${data_specifi[io].specific_id}"><i class=" fas fa-file-alt fa-2x"></i></a> `
                    }
                }
                if (specifi_data_event == '') {
                    specifi_data_event = `<p style="color: red;">ยังไม่มีมาตรการ</p>`
                }
                console.log(specifi_data_event);
                if (myList[y].event_process_id != "" && state.page == 1) {
                    //Keep in mind we are using "Template Litterals to create rows"

                    var row = '<tr>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                        '</b> <span class="tablesaw-cell-content">' + myList[y].event_process_id + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อกิจกรรมประมวลผล</b> <span class="tablesaw-cell-content">' + myList[y].event_process_code + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อกิจกรรมประมวลผล</b> <span class="tablesaw-cell-content">' + myList[y].event_process_name + '</span></td>' +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" > ${specifi_data_event} </td>` +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">แก้ไขประเภท</b> <span class="tablesaw-cell-content"><a href="/${Sub}classification/event/edit` + result.id_event[y].event_process_id + '" class="text-warning"><i class="fas fa-pencil-alt fa-2x"></i></a></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลบประเภท</b> <span class="tablesaw-cell-content"><a href="#delete-event" id=' + result.id_event[y].event_process_id + ' onClick="delEventProcess(this.id)" data-bs-toggle="modal"><i class="fas fa-trash-alt fa-2x" style="color: red;"></i></a></td>' +
                        '</tr>'
                    table.append(row)
                    end_count = myList[y].event_process_id;
                } else if (myList[y].event_process_id != "" && state.page >= 2) {
                    var row = '<tr>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                        '</b> <span class="tablesaw-cell-content">' + myList[y].event_process_id + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อประเภท</b> <span class="tablesaw-cell-content">' + myList[y].event_process_code + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อประเภท</b> <span class="tablesaw-cell-content">' + myList[y].event_process_name + '</span></td>' +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" > ${specifi_data_event} </td>` +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">แก้ไขประเภท</b> <span class="tablesaw-cell-content"><a ${Sub}classification/event/edit` + result.id_event[parseInt(state.rows) + parseInt(y)].event_process_id + '" class="text-warning"><i class="fas fa-pencil-alt fa-2x"></i></a></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลบประเภท</b> <span class="tablesaw-cell-content"><a href="#delete-event" id=' + result.id_event[parseInt(state.rows) + parseInt(y)].event_process_id + ' onClick="delEventProcess(this.id)" data-bs-toggle="modal"><i class="fas fa-trash-alt fa-2x" style="color: red;"></i></a></td>' +
                        '</tr>'
                    table.append(row)
                    end_count = myList[y].event_process_id;
                } else {
                }
            }
            if (myList.length == 0) {
                var row = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล</td></tr>'
                table.append(row)
                $('#start-event').text(0)
                $('#total-event').text(0)
            }
            if (myList[0].event_process_id != "") {
                $('#start-event').text(myList[0].event_process_id)
                $('#total-event').text(result.event.length)
            }
            $('#end-event').text(end_count)
            pageButtons(data.pages)
        }
    })
    function delEventProcess(id) {
        
        let url = `/${Sub}classification/event/delete` + id
        $.post(`/${Sub}classification/selectEventProcess`, { value: "@lltr@Cl@ssifySe1ectEvent1", id: id }).done(function (result) {
            $('td#ev-name').text(result[0].event_process_name)

            $('button#sub-del-event').on('click', function () {
                console.log('trueBotton');
                $('form#del-event').attr('action', url).submit();
            })
        })
    }
} else if (document.getElementById('ev_new')) {
    // ============================== Sidebar =============================
    $('li#li_event_process').addClass('selected')
    $('a#a_event_process').addClass('active')
} else if (document.getElementById('ev_edit')) {
    // ============================== Sidebar =============================
    $('li#li_event_process').addClass('selected')
    $('a#a_event_process').addClass('active')
}

$('.search-key').on('click', function () {
    var keyword_search = document.getElementById("myInput").value;
    document.getElementById('table-body-event').innerHTML = '';
    $.post(`/${Sub}classification/listEventProcessSearch`, { 
        value: "@lltr@Cl@ssifyL1stEvent",
        search: keyword_search,
    }).done(function (result) {
        // ============================== Create Prepare ============================
        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า

        var state = {
            'querySet': result.event,
            'page': 1,
            'rows': 30,
            'window': 10,
            'data_s': result.sp,
            
        }
        buildTable_search()
        function pagination(querySet, page, rows) {
            var trimStart = (page - 1) * rows
            var trimEnd = trimStart + rows

            var trimmedData = querySet.slice(trimStart, trimEnd)

            var pages = Math.ceil(querySet.length / rows);
            var start_count = 1
            document.getElementById('start-event').innerHTML = start_count

            return {
                'querySet': trimmedData,
                'pages': pages,
            }
        }
        // ============================== Create Pagination ============================
        function pageButtons(pages) {
            var wrapper = document.getElementById('pagination-wrapper-event')
            wrapper.innerHTML = ``
            var maxLeft = (state.page - Math.floor(state.window / 2))
            var maxRight = (state.page + Math.floor(state.window / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = state.window
            }

            if (maxRight > pages) {
                maxLeft = pages - (state.window - 1)
                if (maxLeft < 1) {
                    maxLeft = 1
                }
                maxRight = pages
            }

            if (state.page > 1) {
                wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
            } else {
                wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
            }


            num = 1
            if (maxRight > 5) {
                if (state.page > (maxRight / 2)) {
                    if ((state.page + 1) > (maxRight / 2)) {
                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    }
                }
                for (var page = maxLeft; page <= maxRight; page++) {
                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                        if (page == state.page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                        }
                        else {
                            p = page - 1;
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                        }
                    }
                }
                if ((state.page) <= (maxRight / 2)) {
                    mp = maxRight - 1;
                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                }
            }
            else {
                for (var page = maxLeft; page <= maxRight; page++) {
                    if (state.page == page) {
                        wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                    } else {
                        wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                    }
                }
            }

            if (state.page < pages) {
                wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
            } else {
                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
            }

            $('.page').on('click', function () {
                $('#table-body-event').empty()
                state.page = Number($(this).val())
                buildTable()
            })
        }
        // ============================== Create Table ============================
        function buildTable_search() {
            var table = $('#table-body-event')
            var data = pagination(state.querySet, state.page, state.rows)
            var myList = data.querySet
            var data_specifi = state.data_s;
            
            for (y in myList) {
                var specifi_data_event = '';
                for (let io = 0; io < data_specifi.length; io++) {
                    // console.log(myList[y].event_process_id,':',data_specifi[io].event_process_id);
                    if (myList[y].e_id == data_specifi[io].event_process_id) {
                        specifi_data_event = specifi_data_event + ` <a class="text-info" target="_blank" href="/${Sub}Specific-Measures/details/${data_specifi[io].specific_id}"><i class=" fas fa-file-alt fa-2x"></i></a> `
                    }
                }
                if (myList[y].event_process_id != "" && state.page == 1) {
                    //Keep in mind we are using "Template Litterals to create rows"
                    var row = '<tr>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                        '</b> <span class="tablesaw-cell-content">' + myList[y].event_process_id + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อกิจกรรมประมวลผล</b> <span class="tablesaw-cell-content">' + myList[y].event_process_code + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อกิจกรรมประมวลผล</b> <span class="tablesaw-cell-content">' + myList[y].event_process_name + '</span></td>' +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" > ${specifi_data_event} </td>` +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">แก้ไขประเภท</b> <span class="tablesaw-cell-content"><a href="/${Sub}classification/event/edit` + myList[y].e_id + '" class="text-warning"><i class="fas fa-pencil-alt fa-2x"></i></a></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลบประเภท</b> <span class="tablesaw-cell-content"><a href="#delete-event" id=' + myList[y].e_id + ' onClick="delEventProcess(this.id)" data-bs-toggle="modal"><i class="fas fa-trash-alt fa-2x" style="color: red;"></i></a></td>' +
                        '</tr>'
                    table.append(row)
                    end_count = myList[y].event_process_id;
                } else if (myList[y].event_process_id != "" && state.page >= 2) {
                    var row = '<tr>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                        '</b> <span class="tablesaw-cell-content">' + myList[y].event_process_id + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อประเภท</b> <span class="tablesaw-cell-content">' + myList[y].event_process_code + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อประเภท</b> <span class="tablesaw-cell-content">' + myList[y].event_process_name + '</span></td>' +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" > ${specifi_data_event} </td>` +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">แก้ไขประเภท</b> <span class="tablesaw-cell-content"><a href="/${Sub}classification/event/edit` + myList[y].e_id + '" class="text-warning"><i class="fas fa-pencil-alt fa-2x"></i></a></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลบประเภท</b> <span class="tablesaw-cell-content"><a href="#delete-event" id=' + myList[y].e_id + ' onClick="delEventProcess(this.id)" data-bs-toggle="modal"><i class="fas fa-trash-alt fa-2x" style="color: red;"></i></a></td>' +
                        '</tr>'
                    table.append(row)
                    end_count = myList[y].event_process_id;
                } else {
                }
            }
            if (myList.length == 0) {
                var row = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล</td></tr>'
                table.append(row)
                $('#start-event').text(0)
                $('#total-event').text(0)
            }
            if (myList[0].event_process_id != "") {
                $('#start-event').text(myList[0].event_process_id)
                $('#total-event').text(result.event.length)
            }
            $('#end-event').text(end_count)
            pageButtons(data.pages)
        }
    })
});


$('.search-key-2').on('click', function () {
   var keyword_search = document.getElementById("myInput-2").value;
   document.getElementById('table-body').innerHTML = '';

   $.ajax({
    url: `/${Sub}classificationSearch`,
    method: "POST",
    data: { 
        value: "@lltr@Cl@ssific@ti0n",
        search: keyword_search,
    },success: function (result) {
        // ============================== Create Prepare ============================
        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า

        var state = {
            'querySet': result.classify,
            'page': 1,
            'rows': 30,
            'window': 10,
        }

        buildTable()

        function pagination(querySet, page, rows) {

            var trimStart = (page - 1) * rows
            var trimEnd = trimStart + rows

            var trimmedData = querySet.slice(trimStart, trimEnd)

            var pages = Math.ceil(querySet.length / rows);
            var start_count = 1
            document.getElementById('start').innerHTML = start_count

            return {
                'querySet': trimmedData,
                'pages': pages,
            }
        }
        // ============================== Create Pagination ============================
        function pageButtons(pages) {
            var wrapper = document.getElementById('pagination-wapper')
            wrapper.innerHTML = ``
            var maxLeft = (state.page - Math.floor(state.window / 2))
            var maxRight = (state.page + Math.floor(state.window / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = state.window
            }

            if (maxRight > pages) {
                maxLeft = pages - (state.window - 1)
                if (maxLeft < 1) {
                    maxLeft = 1
                }
                maxRight = pages
            }

            if (state.page > 1) {
                wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
            } else {
                wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
            }


            num = 1
            if (maxRight > 5) {
                if (state.page > (maxRight / 2)) {
                    if ((state.page + 1) > (maxRight / 2)) {
                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    }
                }
                for (var page = maxLeft; page <= maxRight; page++) {
                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                        if (page == state.page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                        }
                        else {
                            p = page - 1;
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                        }
                    }
                }
                if ((state.page) <= (maxRight / 2)) {
                    mp = maxRight - 1;
                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                }
            }
            else {
                for (var page = maxLeft; page <= maxRight; page++) {
                    if (state.page == page) {
                        wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                    } else {
                        wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                    }
                }
            }

            if (state.page < pages) {
                wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
            } else {
                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
            }

            $('.page').on('click', function () {
                $('#table-body').empty()
                state.page = Number($(this).val())
                buildTable()
            })
        }
        // ============================== Create Table ============================
        function buildTable() {
            var table = $('#table-body')
            var data = pagination(state.querySet, state.page, state.rows)
            var myList = data.querySet

            for (y in myList) {

                if (myList[y].classify_id != "" && state.page == 1) {
                    //Keep in mind we are using "Template Litterals to create rows"
                    let check = '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลบประเภท</b> <span class="tablesaw-cell-content"><a href="#delete-classify" id=' + myList[y].c_id + ' onClick="delClassify(this.id)" data-bs-toggle="modal"><i class="fas fa-trash-alt fa-2x" style="color: red;"></i></a></td>'
                    if (result.check_delete[y] == 1) {
                        check = `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:110px"><b class="tablesaw-cell-label">ลบประเภท</b> 
                        <span class="tablesaw-cell-content"><i class="fas fa-trash-alt fa-2x" style="color:gray"></i></span></td>`
                    } else {
                        check = '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลบประเภท</b> <span class="tablesaw-cell-content"><a href="#delete-classify" id=' + myList[y].c_id + ' onClick="delClassify(this.id)" data-bs-toggle="modal"><i class="fas fa-trash-alt fa-2x" style="color: red;"></i></a></td>'
                    }
                    var row = '<tr>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                        '</b> <span class="tablesaw-cell-content">' + myList[y].classify_id + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อประเภท</b> <span class="tablesaw-cell-content">' + myList[y].classify_name + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:250px"><b class="tablesaw-cell-label">ข้อมูลที่แยกประเภท</b> <span class="tablesaw-cell-content">' + result.classify_data_name[y] + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">วันที่สร้าง</b> <span class="tablesaw-cell-content">' + myList[y].classify_create_date + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ผู้สร้าง</b> <span class="tablesaw-cell-content">' + myList[y].firstname + " " + myList[y].lastname + '</span></td>' +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ดูประเภท</b> <span class="tablesaw-cell-content"><a href="/${Sub}classification/detail` + myList[y].c_id + '" class="text-info"><i class="fas fa-file-alt fa-2x"></i></a></td>' +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">แก้ไขประเภท</b> <span class="tablesaw-cell-content"><a href="/${Sub}classification/edit` + myList[y].c_id + '" class="text-warning"><i class="fas fa-pencil-alt fa-2x"></i></a></td>' +
                        check +
                        '</tr>'
                    table.append(row)
                    end_count = myList[y].classify_id
                } else if (myList[y].classify_id != "" && state.page >= 2) {
                    let check = ``
                    if (result.check_delete[y] == 1) {
                        check = `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:110px"><b class="tablesaw-cell-label">ลบประเภท</b> 
                        <span class="tablesaw-cell-content"><i class="fas fa-trash-alt fa-2x" style="color:gray"></i></span></td>`
                    } else {
                        check = '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลบประเภท</b> <span class="tablesaw-cell-content"><a href="#delete-classify" id=' + myList[y].c_id + ' onClick="delClassify(this.id)" data-bs-toggle="modal"><i class="fas fa-trash-alt fa-2x" style="color: red;"></i></a></td>'
                    }
                    var row = '<tr>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                        '</b> <span class="tablesaw-cell-content">' + myList[y].classify_id + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อประเภท</b> <span class="tablesaw-cell-content">' + myList[y].classify_name + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:250px"><b class="tablesaw-cell-label">ข้อมูลที่แยกประเภท</b> <span class="tablesaw-cell-content">' + result.classify_data_name[y] + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">วันที่สร้าง</b> <span class="tablesaw-cell-content">' + myList[y].classify_create_date + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ผู้สร้าง</b> <span class="tablesaw-cell-content">' + myList[y].firstname + " " + myList[y].lastname + '</span></td>' +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ดูประเภท</b> <span class="tablesaw-cell-content"><a href="/${Sub}classification/detail` + myList[y].c_id + '" class="text-info"><i class="fas fa-file-alt fa-2x"></i></a></td>' +
                        `<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">แก้ไขประเภท</b> <span class="tablesaw-cell-content"><a href="/${Sub}classification/edit` + myList[y].c_id + '" class="text-warning"><i class="fas fa-pencil-alt fa-2x"></i></a></td>' +
                        check +
                        '</tr>'
                    table.append(row)
                    end_count = myList[y].classify_id
                }
            }
            if (myList.length == 0) {
                var row = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล</td></tr>'
                table.append(row)
                $('#start').text(0)
                $('#total').text(0)
            } else {
                if (myList[0].classify_id != "") {
                    $('#start').text(myList[0].classify_id)
                    $('#total').text(result.classify.length)
                }
            }
            $('#end').text(end_count)
            pageButtons(data.pages)
        }
    }
})
});


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$(document).ready(function () {
    // $('input[name="doc_id"]').change(function(){
    // })


    // $('input[name="doc_id"]').change(function(){
    //   });
});




function form_add_classify_submit() {
    if ($('input[name="pattern_id"]').val() == '') {
        $('#select_pattern').focus();
        $('#select_pattern').addClass('highlighted');
        setTimeout(function () {
            $('#select_pattern').removeClass('highlighted');
        }, 1500);
    } else if ($('input[name="event_process_id"]').val() == '') {
        $('a[href="#select-event-process"]').focus();
        $('a[href="#select-event-process"]').addClass('highlighted');
        setTimeout(function () {
            $('a[href="#select-event-process"]').removeClass('highlighted');
        }, 1500);
    } else if ($('input[name="classify_period_process"]').val() == '') {
        $('input[name="classify_period_process"]').focus();
        $('input[name="classify_period_process"]').addClass('highlighted');
        setTimeout(function () {
            $('input[name="classify_period_process"]').removeClass('highlighted');
        }, 1500);
    } else if ($('input[name="classify_period_end"]').val() == '') {
        $('input[name="classify_period_end"]').focus();
        $('input[name="classify_period_end"]').addClass('highlighted');
        setTimeout(function () {
            $('input[name="classify_period_end"]').removeClass('highlighted');
        }, 1500);
    } else if ($('input[name="classify_protect_data_limit_process"]').val() == '') {
        $('input[name="classify_protect_data_limit_process"]').focus();
        $('input[name="classify_protect_data_limit_process"]').addClass('highlighted');
        setTimeout(function () {
            $('input[name="classify_protect_data_limit_process"]').removeClass('highlighted');
        }, 1500);
    } else if ($('input[name="classify_user_access_info_process_inside"]').val() == '') {
        $('input[name="classify_user_access_info_process_inside"]').focus();
        $('input[name="classify_user_access_info_process_inside"]').addClass('highlighted');
        setTimeout(function () {
            $('input[name="classify_user_access_info_process_inside"]').removeClass('highlighted');
        }, 1500);
    } else if ($('input[name="classify_user_access_info_process_outside"]').val() == '') {
        $('input[name="classify_user_access_info_process_outside"]').focus();
        $('input[name="classify_user_access_info_process_outside"]').addClass('highlighted');
        setTimeout(function () {
            $('input[name="classify_user_access_info_process_outside"]').removeClass('highlighted');
        }, 1500);
    } else {
        $('#form_add_classify').submit();
    }

}