let type = "";
if (document.getElementById('file_log_ag')){
  type = 'file';
}else if(document.getElementById('database_ag')){
  type = 'database'
}else if(document.getElementById('logger_hash_ag')){
  type = 'log0'
}
else if(document.getElementById('a-sniffer')){
  type = 'sniffer'
}
$.post("/agent/chart", {'value': "@lltr@@gentCh@rt", 'type': type}).done(function(result){
  $(function () {
    var _date_ = []
    var _count_ = []
    for (const i in result){
      if(type !== 'sniffer'){
        _date_.push(convert_datetime(new Date(result[i].date), result[i].date))
        _count_.push(result[i].count)
      }else{
        _date_.push(result[i].data)
        _count_.push(result[i].len)
      }
    }
    "use strict";
    // -----------------------------------------------------------------------
    // Revenue Statistics
    // -----------------------------------------------------------------------
    var Revenue_Statistics = {
      series: [
        {
          name: "จำนวนทั้งหมด",
          data: _count_,
        },
        // {
        //     name: "Product B ",
        //     data: [0, 4, 0, 4, 0, 4, 0, 4],
        // },
      ],
      chart: {
        fontFamily: 'Rubik,sans-serif',
        height: 350,
        type: "area",
        toolbar: {
          show: false,
        },
      },
      fill: {
        type: 'solid',
        opacity: 0.2,
        colors: ["#FF4500"],
      },
      grid: {
        show: true,
        borderColor: "rgba(0,0,0,0.1)",
        strokeDashArray: 3,
        xaxis: {
          lines: {
            show: true
          }
        },
      },
      colors: ["#FF4500"],
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: "smooth",
        width: 1,
        colors: ["#FF4500"],
      },
      markers: {
        size: 3,
        colors: ["#FF4500"],
        strokeColors: "transparent",
      },
      xaxis: {
        axisBorder: {
          show: true,
        },
        axisTicks: {
          show: true,
        },
        categories: _date_,
        labels: {
          style: {
            colors: "#a1aab2",
          },
        },
      },
      yaxis: {
        tickAmount: 9,
        labels: {
          style: {
            colors: "#a1aab2",
          },
        },
      },
      tooltip: {
        x: {
          format: "dd/MM/yy HH:mm",
        },
        theme: "dark",
      },
      legend: {
        show: false,
      },
    };
    var chartRevnue = new ApexCharts(document.querySelector("#revenue-statistics"), Revenue_Statistics);
    chartRevnue.render();
  })
})
