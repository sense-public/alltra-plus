function get_data() {
    Sub = document.getElementById('SubDomain').value;
    ftp_id = document.getElementById('ftp_id').value;
    $.ajax({
        type: 'GET',
        url: `/${Sub}api_views_import/${ftp_id}`,
        success: async function (result) {
            await DataTable(result.data,result.button,result.count_ftp)
        },
        error: function (e) {
            console.log(e);
        }
    });
}
get_data()

async function DataTable(data,button,count_ftp) {
    if (data.length <= 0) {
        $('#tbody_data_ftp').empty().append(`
        <tr>
            <td colspan="20" class="text-center" style="border: none;">
                <b class="text-danger">ไม่พบข้อมูล</b>
            </td>
        </tr>
        `)
        document.querySelector("#show").innerHTML = 0;  //  แสดงถึง row เเรกของหน้า 
            document.querySelector("#to_show").innerHTML = 0;  //  แสดงถึง row สุดท้ายของหน้า
            document.querySelector("#show_all").innerHTML = 0;  //  แสดงถึงจำนวนข้อมูลทั้งหมดของหน้า
    } else {
        for (let i = 0; i < data.length; i++) {
            data[i].no = i + 1
        }
        var state = {
            'querySet': data,
            'page': 1,
            'rows': 5, // จำนวน row
            'window': 10000, // จำนวนหน้าที่เเสดง
        }
        buildTable()
        function pagination(querySet, page, rows) {
            var trimStart = (page - 1) * rows
            var trimEnd = trimStart + rows
            var trimmedData = querySet.slice(trimStart, trimEnd)
            var pages = button; // Math.ceil ปัดทศนิยมขึ้น Math.round ปัดทศนิยมลง
            return {
                'querySet': trimmedData,
                'pages': pages,
            }
        }

        function pageButtons(pages) {
            // var wrapper = document.getElementById('pagination-wrapper')
            var wrapper = document.querySelector('.pagination')
            wrapper.innerHTML = ``
            var maxLeft = (state.page - Math.floor(state.window / 2))
            var maxRight = (state.page + Math.floor(state.window / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = state.window
            }
            if (maxRight > pages) {
                maxLeft = pages - (state.window - 1)
                if (maxLeft < 1) {
                    maxLeft = 1
                }
                maxRight = pages
            }

            // เช็คหน้าเเรก (ปุ่มย้อนกลับ)
            // var num = 1
            if (maxRight > 5) {
                if (state.page > (maxRight / 2)) {
                    if ((state.page + 1) > (maxRight / 2)) {
                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    }
                }
                for (var page = maxLeft; page <= maxRight; page++) {
                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                        if (page == state.page) {
                            wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                        }
                        else {
                            wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                        }
                    }
                }
                if ((state.page) <= (maxRight / 2)) {
                    mp = maxRight - 1;
                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                }
            }
            else {
                for (var page = maxLeft; page <= maxRight; page++) {
                    if (state.page == page) {
                        wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                    } else {
                        wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                    }
                }
            }

            if (state.page == 1) {
                wrapper.innerHTML = `<li  class="page-item disabled"><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            } else {
                wrapper.innerHTML = `<li value=${state.page - 1} class="page page-item "><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            }


            // เช็คหน้าสุดท้าย (ปุ่มถัดไป)
            if (state.page == pages) {
                wrapper.innerHTML += `<li  class=" page-item disabled"><button class="page-link">ถัดไป</button></li>`
            } else {
                wrapper.innerHTML += `<li value=${state.page + 1} class="page page-item"><button class="page-link">ถัดไป</button></li>`
            }


            $('.page').on('click', function () {
                state.page = Number($(this).val())
                var s_d = document.getElementById('Srearch').value;
                fid = document.getElementById('ftp_id').value;
                $.ajax({
                    method: "POST",
                    url: `/${Sub}views_import_list_optimize`,
                    data: { page: state.page ,srearch_data2:s_d, fid:fid},
                    dataType: 'json',
                    success: function (result2) {
                        $('#tbody_data_ftp').empty()
                        if (result2.data.length <= 0) {
                            $('#tbody_data_ftp').empty().append(`
                            <tr>
                                <td colspan="20" class="text-center" style="border: none;">
                                    <b class="text-danger">ไม่พบข้อมูล</b>
                                </td>
                            </tr>
                            `)
                            document.querySelector("#show").innerHTML = 0;  //  แสดงถึง row เเรกของหน้า 
                                document.querySelector("#to_show").innerHTML = 0;  //  แสดงถึง row สุดท้ายของหน้า
                                document.querySelector("#show_all").innerHTML = 0;  //  แสดงถึงจำนวนข้อมูลทั้งหมดของหน้า
                        } else {
                            var state = {
                                'querySet': result2.data,
                                'page': 1,
                                'rows': 5, // จำนวน row
                                'window': 10000, // จำนวนหน้าที่เเสดง
                            }
                            function pagination(querySet, page, rows) {
                                var trimStart = (page - 1) * rows
                                var trimEnd = trimStart + rows
                                var trimmedData = querySet.slice(trimStart, trimEnd)
                                var pages = result2.button; // Math.ceil ปัดทศนิยมขึ้น Math.round ปัดทศนิยมลง
                                return {
                                    'querySet': trimmedData,
                                    'pages': pages,
                                }
                            }
                            buildTable()
                            function buildTable() {
                                var table = $('#tbody_data_ftp');
                                var data = pagination(state.querySet, state.page, state.rows)
                                var myList = data.querySet
                                var show = [];
                                var view_data = '';
                                var export_file = '';
                                for (var i in myList) {
                                    let delete_trash = '<i class="fas fa-trash-alt fa-2x text-secondary"></i>'
                                    view_data = `<a href="#" data-bs-toggle="modal" data-bs-target="#samedata10-modal-${myList[i].id}" data-bs-whatever="@mdo" title="ดูข้อมูล"class="text-info">
                                    <i class="fas fa-file-alt fa-2x"></i></a>`
                                    var row = `<tr>
                                                <td id="">${myList[i].no2}</td>
                                                <td>${myList[i].type}</td>           
                                                <td>${myList[i].rname}</td>
                                                <td>${myList[i].date}</td>
                                                <td>${view_data}</td>
                                                </tr>
                        `
                                    table.append(row)
                                    show.push(myList[i].no)
                                    end_count = myList[i].no2;
                                }
                    
                                document.querySelector("#show").innerHTML = myList[0].no2;  //  แสดงถึง row เเรกของหน้า 
                                document.querySelector("#to_show").innerHTML = end_count;  //  แสดงถึง row สุดท้ายของหน้า
                                document.querySelector("#show_all").innerHTML = result2.count_ftp;  //  แสดงถึงจำนวนข้อมูลทั้งหมดของหน้า
                                pageButtons(data.pages)
                            }
                        }
                    }
                });
            })
        };
        function buildTable() {
            var table = $('#tbody_data_ftp');
            var data = pagination(state.querySet, state.page, state.rows)
            var myList = data.querySet
            var show = [];
            var view_data = '';
            var export_file = '';

            

            for (var i in myList) {
                let delete_trash = '<i class="fas fa-trash-alt fa-2x text-secondary"></i>'
                view_data = `<a href="#" data-bs-toggle="modal" data-bs-target="#samedata10-modal-${myList[i].id}" data-bs-whatever="@mdo" title="ดูข้อมูล"class="text-info">
                <i class="fas fa-file-alt fa-2x"></i></a>`
                var row = `<tr>
                        <td id="">${myList[i].no2}</td>
                        <td>${myList[i].type}</td>           
                        <td>${myList[i].rname}</td>
                        <td>${myList[i].date}</td>
                        <td>${view_data}</td>
                        </tr>
                        `
                table.append(row)
                show.push(myList[i].no)
            }

            document.querySelector("#show").innerHTML = show[0];  //  แสดงถึง row เเรกของหน้า 
            document.querySelector("#to_show").innerHTML = show[show.length - 1];  //  แสดงถึง row สุดท้ายของหน้า
            document.querySelector("#show_all").innerHTML = count_ftp;  //  แสดงถึงจำนวนข้อมูลทั้งหมดของหน้า
            pageButtons(data.pages)
        }
    }
}

document.getElementById("button-addon2").addEventListener("click", () => {
    var data = document.getElementById('Srearch').value;
    var ftp_id = document.getElementById('ftp_id').value;
    $.ajax({
        url: `/${Sub}api_views_import` ,
        type: "post",
        data: {
            value: data,
            ftp_id: ftp_id,
            check: 'true'
        },
        success: function (data) {
            if (data.length == 0) {
                alert("ใส่รหัสผ่านผิด")
            } else {
                $('#tbody_data_ftp').empty()
                DataTable(data.data,data.button,data.count_ftp)
            }
        }
    });
});