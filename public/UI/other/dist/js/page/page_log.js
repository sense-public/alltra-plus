

$(function () {
    Sub = document.getElementById('SubDomain').value;
    if (document.getElementById('centralized')) {
    document.getElementById('button-search').addEventListener('click', () => {
        if ($('.devices').val() == "") {
            var device_data = [ 'sum' ];
        }else{
            var device_data = $('.devices').val();
        }
        var data = { "date1": $('#date1').val(), "date2": $('#date2').val(), "devices": device_data, "search": $('#search').val(), page: 1 };
        $.ajax({
            method: "POST",
            contentType: "application/json",
            url: `/${Sub}log`,
            data: JSON.stringify(data),
            dataType: 'json',
            success: async function (result) {
                if (result != 'ไม่มีข้อมูล') {
                    $('#table-body').empty()
                    $('#pagination-wapper').show()
                    data_values = result.log_sum;
                    csvFileData = result.log_sum;
                    // ============================== Create Prepare ============================
                    var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                    var total = 0 // จำนวนทั้งหมดของข้อมูล
                    var state = {
                        'querySet': result.log_list,
                        'page': result.page,
                        'rows': result.rows,
                        'window': 5,
                    }
                    buildTable()
                    function pagination(querySet, page, rows, button) {
                        var pages = button;
                        var start_count = 1
                        document.getElementById('start').innerHTML = start_count
                        return {
                            'querySet': querySet,
                            'pages': pages,
                        }
                    }
                    // ============================== Create Pagination ============================
                    function pageButtons(pages) {
                        var wrapper = document.getElementById('pagination-wapper')
                        wrapper.innerHTML = ``
                        var maxLeft = (state.page - Math.floor(state.window / 2))
                        var maxRight = (state.page + Math.floor(state.window / 2))
                        if (maxLeft < 1) {
                            maxLeft = 1
                            maxRight = state.window
                        }
                        if (maxRight > pages) {
                            maxLeft = pages - (state.window - 1)
                            if (maxLeft < 1) {
                                maxLeft = 1
                            }
                            maxRight = pages
                        }
                        for (var page = maxLeft; page <= maxRight; page++) {
                            wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
                        }
                        if (state.page == 1) {
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" >ย้อนกลับ</button></li>` + wrapper.innerHTML
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link">หน้าแรก </button></li>` + wrapper.innerHTML
                        } else {
                            wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>` + wrapper.innerHTML
                            wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link"> หน้าแรก </button></li>` + wrapper.innerHTML

                        }

                        if (state.page != pages) {
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" > หน้าสุดท้าย</button>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link"> หน้าสุดท้าย </button>`
                        }
                        $('.page').on('click', function () {
                            state.page = Number($(this).val())
                            $.ajax({
                                method: "POST",
                                url: `/${Sub}log`,
                                data: { date1: $('#date1').val(), date2: $('#date2').val(), devices: $('.devices').val(), search: $('#search').val(), page: state.page },
                                dataType: 'json',
                                success: function (result) {
                                    if (result != "ไม่มีข้อมูล") {
                                        $('#pagination-wapper').show()
                                        data_values = result.log_sum;
                                        csvFileData = result.log_sum;
                                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                                        var total = 0 // จำนวนทั้งหมดของข้อมูล
                                        var state = {
                                            'querySet': result.log_list,
                                            'page': result.page,
                                            'rows': result.rows,
                                            'window': 5,
                                        }
                                        $('#table-body').empty()
                                        $('#pagination-wapper').show()
                                        buildTable()
                                        function buildTable() {
                                            if (result.system[0].type == 'sha256') {
                                                var access_type = 'SHA-256'
                                            } else if (result.system[0].type == 'sha1') {
                                                var access_type = 'SHA-1'
                                            } else if (result.system[0].type == 'md5') {
                                                var access_type = 'MD5'
                                            }
                                            var table = $('#table-body')
                                            var data = pagination(state.querySet, state.page, state.rows, result.button)
                                            var myList = data.querySet
                                            for (y in myList) {
                                                if (state.page == 1) {
                                                    if (myList[y].no != "") {
                                                        //Keep in mind we are using "Template Litterals to create rows"
                                                        if(myList[y].hash == null){
                                                            var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                            '</tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                        }else{
                                                            var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                            '</tr><tr><td colspan="6" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td>'+
                                                            '</tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                        }
                                                        
                                                    } else {
                                                        var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                        table.append(row)
                                                    }
                                                } else {
                                                    if (myList[y].no != "") {
                                                        //Keep in mind we are using "Template Litterals to create rows"
                                                        if(myList[y].hash == null){
                                                            var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                            '</tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                        }else{
                                                            var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                            '</tr><tr><td colspan="6" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td>'+
                                                            '</tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                        }
                                                        
                                                    } else {
                                                        var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                        table.append(row)
                                                    }
                                                }

                                            }
                                            data_values ='';
                                            csvFileData ='';
                                            $('#end').html(end_count)
                                            if (myList.length > 0) {
                                                if (myList[0].no != "") {
                                                    $('#start').html(myList[0].no)
                                                } else {
                                                    $('#start').html(0)
                                                }
                                            }
                                            $('#total-db').html(result.log_sum);
                                            // end_count = table.find('tr').length
                                            $('button#button_delete_db').on('click', function () {
                                                document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                                                document.getElementById('form_delete_db').submit();
                                            })
                                        }
                                    } else {
                                        data_values = '';
                                        csvFileData = '';
                                        $('#pagination-wapper').hide()
                                        $('#table-body').empty()
                                        var table = $('#table-body')
                                        var row = '<tr>' +
                                            '<td class="text-center" colspan="7" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                                            '</tr>'
                                        table.append(row)
                                        end_count = 0
                                        $('#end').html(end_count)
                                        $('#start').html(0)
                                        $('#total-db').html(0)
                                    }
                                }
                            });
                        })
                    }
                    // ============================== Create Table ============================
                    function buildTable() {
                        if (result.system[0].type == 'sha256') {
                            var access_type = 'SHA-256'
                        } else if (result.system[0].type == 'sha1') {
                            var access_type = 'SHA-1'
                        } else if (result.system[0].type == 'md5') {
                            var access_type = 'MD5'
                        }
                        var table = $('#table-body')
                        var data = pagination(state.querySet, state.page, state.rows, result.button)
                        var myList = data.querySet
                        for (y in myList) {
                            if (state.page == 1) {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    if(myList[y].hash == null){
                                        var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr>'
                                        table.append(row)
                                    end_count = myList[y].no
                                    }else{
                                        var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr><tr><td colspan="6" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td>'+
                                        '</tr>'
                                        table.append(row)
                                    end_count = myList[y].no
                                    }
                                    
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            } else {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    if(myList[y].hash == null){
                                        var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                    }else{
                                        var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr><tr><td colspan="6" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td>'
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                    }
                                    
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            }
                        }
                        $('#end').html(end_count)
                        if (myList.length > 0) {
                            if (myList[0].no != "") {
                                $('#start').html(myList[0].no)
                            } else {
                                $('#start').html(0)
                            }
                        }
                        $('#total-db').html(result.log_sum);
                        pageButtons(data.pages)
                        // end_count = table.find('tr').length
                        $('button#button_delete_db').on('click', function () {
                            document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                            document.getElementById('form_delete_db').submit();
                        })
                    }
                } else {
                    data_values ='';
                    csvFileData ='';
                    $('#pagination-wapper').hide()
                    $('#table-body').empty()
                    var table = $('#table-body')
                    var row = '<tr>' +
                        '<td class="text-center" colspan="6" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                        '</tr>'
                    table.append(row)
                    end_count = 0
                    $('#end').html(end_count)
                    $('#start').html(0)
                    $('#total-db').html(0)
                }
            }
        })
    });
    }
    if (document.getElementById('centralized')) {
        $.ajax({
            method: "POST",
            url: `/${Sub}api/log`,
            data: { page: 1 },
            success: function (result) {
                if (result != 'ไม่มีข้อมูล') {
                    $('#table-body').empty()
                    $('#pagination-wapper').show()
                    // ============================== Create Prepare ============================
                    var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                    var total = 0 // จำนวนทั้งหมดของข้อมูล

                    var state = {
                        'querySet': result.log_list,
                        'page': result.page,
                        'rows': result.rows,
                        'window': 5,
                    }
                    buildTable()
                    function pagination(querySet, page, rows, button) {
                        var pages = button;
                        var start_count = 1
                        document.getElementById('start').innerHTML = start_count
                        return {
                            'querySet': querySet,
                            'pages': pages,
                        }
                    }
                    // ============================== Create Pagination ============================
                    function pageButtons(pages) {
                        var wrapper = document.getElementById('pagination-wapper')
                        wrapper.innerHTML = ``
                        // console.log('Pages:', pages)

                        var maxLeft = (state.page - Math.floor(state.window / 2))
                        var maxRight = (state.page + Math.floor(state.window / 2))

                        if (maxLeft < 1) {
                            maxLeft = 1
                            maxRight = state.window
                        }

                        if (maxRight > pages) {
                            maxLeft = pages - (state.window - 1)

                            if (maxLeft < 1) {
                                maxLeft = 1
                            }
                            maxRight = pages
                        }
                        for (var page = maxLeft; page <= maxRight; page++) {
                            wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
                        }

                        if (state.page == 1) {
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" >ย้อนกลับ</button></li>` + wrapper.innerHTML
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link">หน้าแรก </button></li>` + wrapper.innerHTML
                        } else {
                            wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>` + wrapper.innerHTML
                            wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link"> หน้าแรก </button></li>` + wrapper.innerHTML

                        }

                        if (state.page != pages) {
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" > หน้าสุดท้าย</button>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link"> หน้าสุดท้าย </button>`
                        }

                        $('.page').on('click', function () {
                            state.page = Number($(this).val())
                            $.ajax({
                                method: "POST",
                                url: `/${Sub}api/log`,
                                data: { date1: $('#date1').val(), date2: $('#date2').val(), devices: $('.devices').val(), search: $('#search').val(), page: state.page },
                                dataType: 'json',
                                success: function (result) {
                                    if (result != "ไม่มีข้อมูล") {
                                        $('#pagination-wapper').show()
                                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                                        var total = 0 // จำนวนทั้งหมดของข้อมูล
                                        var state = {
                                            'querySet': result.log_list,
                                            'page': result.page,
                                            'rows': result.rows,
                                            'window': 5,
                                        }
                                        $('#table-body').empty()
                                        $('#pagination-wapper').show()
                                        buildTable()
                                        function buildTable() {
                                            if (result.system[0].type == 'sha256') {
                                                var access_type = 'SHA-256'
                                            } else if (result.system[0].type == 'sha1') {
                                                var access_type = 'SHA-1'
                                            } else if (result.system[0].type == 'md5') {
                                                var access_type = 'MD5'
                                            }
                                            var table = $('#table-body')
                                            var data = pagination(state.querySet, state.page, state.rows, result.button)
                                            var myList = data.querySet
                                            for (y in myList) {
                                                if (state.page == 1) {
                                                    if (myList[y].no != "") {
                                                        //Keep in mind we are using "Template Litterals to create rows"
                                                        if(myList[y].hash == null){
                                                            var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                            '</tr>'
                                                            table.append(row)
                                                        end_count = myList[y].no
                                                        }else{
                                                            var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                            '</tr><tr><td colspan="6" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td>'+
                                                            '</tr>'
                                                            table.append(row)
                                                        end_count = myList[y].no
                                                        }
                                                        
                                                    } else {
                                                        var row = '<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                        table.append(row)
                                                    }
                                                } else {
                                                    if (myList[y].no != "") {
                                                        //Keep in mind we are using "Template Litterals to create rows"
                                                        if(myList[y].hash == null){
                                                            var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                            '</tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                        }else{
                                                            var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                            '</tr><tr><td colspan="6" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td>'+
                                                            '</tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                        }
                                                       
                                                    } else {
                                                        var row = '<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                        table.append(row)
                                                    }
                                                }

                                            }
                                            $('#end').html(end_count)
                                            if (myList.length > 0) {
                                                if (myList[0].no != "") {
                                                    $('#start').html(myList[0].no)
                                                } else {
                                                    $('#start').html(0)
                                                }
                                            }
                                            $('#total-db').html(result.log_sum);

                                            // end_count = table.find('tr').length
                                            $('button#button_delete_db').on('click', function () {
                                                document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                                                document.getElementById('form_delete_db').submit();
                                            })
                                        }
                                    } else {
                                        data_values = '';
                                        csvFileData = '';
                                        $('#pagination-wapper').hide()
                                        $('#table-body').empty()
                                        var table = $('#table-body')
                                        var row = '<tr>' +
                                            '<td class="text-center" colspan="7" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                                            '</tr>'
                                        table.append(row)
                                        end_count = 0
                                        $('#end').html(end_count)
                                        $('#start').html(0)
                                        $('#total-db').html(0)
                                    }
                                }
                            });
                        })
                    }
                    // ============================== Create Table ============================
                    function buildTable() {
                        if (result.system[0].type == 'sha256') {
                            var access_type = 'SHA-256'
                        } else if (result.system[0].type == 'sha1') {
                            var access_type = 'SHA-1'
                        } else if (result.system[0].type == 'md5') {
                            var access_type = 'MD5'
                        }
                        var table = $('#table-body')
                        var data = pagination(state.querySet, state.page, state.rows, result.button)
                        var myList = data.querySet
                        for (y in myList) {
                            if (state.page == 1) {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    if(myList[y].hash == null){
                                        var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                    }else{
                                        var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr><tr><td colspan="6" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td>'+
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                    }
                                   
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            } else {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    if(myList[y].hash == null){
                                        var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                    }else{
                                        var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr><tr><td colspan="6" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td>'+
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                    }
                                   
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            }

                        }
                        $('#end').html(end_count)
                        if (myList.length > 0) {
                            if (myList[0].no != "") {
                                $('#start').html(myList[0].no)
                            } else {
                                $('#start').html(0)
                            }
                        }
                        $('#total-db').html(result.log_sum);
                        pageButtons(data.pages)
                        // end_count = table.find('tr').length
                        $('button#button_delete_db').on('click', function () {
                            document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                            document.getElementById('form_delete_db').submit();
                        })
                    }
                } else {
                    $('#table-body').empty()
                    $('#pagination-wapper').hide()
                    var table = $('#table-body')
                    var row = '<tr>' +
                        '<td class="text-center" colspan="6" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                        '</tr>'
                    table.append(row)
                    end_count = 0
                    $('#end').html(end_count)
                    $('#start').html(0)
                    $('#total-db').html(0)
                }
            }
        })

    }
    if (document.getElementById('ajaxfilelog')) {
        $.ajax({
            method: "POST",
            url: `/${Sub}filelog`,
            data: { page : 1 },
            dataType: 'json',
            success: function (result) {
                if (result.data != "ไม่มีข้อมูล") {
                    // ============================== Create Prepare ============================
                    var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                    var total = 0 // จำนวนทั้งหมดของข้อมูล
                    var state = {
                        'querySet': result.data,
                        'page': result.page,
                        'rows': result.rows,
                        'window': 5,
                    }
                    data_values = result.sum;
                    csvFileData = result.sum;
                    buildTable()
                    function pagination(querySet, page, rows, button) {
                        var pages = button;
                        var start_count = 1
                        document.getElementById('start').innerHTML = start_count
                        return {
                            'querySet': querySet,
                            'pages': pages,
                        }
                    }
                    // ============================== Create Pagination ============================
                    function pageButtons(pages) {
                        var wrapper = document.getElementById('pagination-wapper')
                        wrapper.innerHTML = ``
                        var maxLeft = (state.page - Math.floor(state.window / 2))
                        var maxRight = (state.page + Math.floor(state.window / 2))
                        if (maxLeft < 1) {
                            maxLeft = 1
                            maxRight = state.window
                        }
                        if (maxRight > pages) {
                            maxLeft = pages - (state.window - 1)
                            if (maxLeft < 1) {
                                maxLeft = 1
                            }
                            maxRight = pages
                        }
                        for (var page = maxLeft; page <= maxRight; page++) {
                            wrapper.innerHTML += `<li class="page-item"><button class="page page-link" id='page_values' value='${page}'>${page}</button></li>`
                        }
                        if (state.page == 1) {
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" >ย้อนกลับ</button></li>` + wrapper.innerHTML
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link">หน้าแรก </button></li>` + wrapper.innerHTML
                        } else {
                            wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>` + wrapper.innerHTML
                            wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link"> หน้าแรก </button></li>` + wrapper.innerHTML

                        }

                        if (state.page != pages) {
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" > หน้าสุดท้าย</button>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link"> หน้าสุดท้าย </button>`
                        }
                        $('.page').on('click', function () {
                            $('#table-body').empty()
                            state.page = Number($(this).val())
                            $.ajax({
                                method: "POST",
                                url: `/${Sub}filelog`,
                                data: { page: state.page },
                                success: function (result) {
                                    if (result.data != "ไม่มีข้อมูล") {
                                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                                        var total = 0 // จำนวนทั้งหมดของข้อมูล
                                        var state = {
                                            'querySet': result.data,
                                            'page': result.page,
                                            'rows': result.rows,
                                            'window': 5,
                                        }
                                        data_values = result.sum;
                                        csvFileData = result.sum;
                                        buildTable()
                                        function buildTable() {
                                            var table = $('#table-body')
                                            var data = pagination(state.querySet, state.page, state.rows, result.button)
                                            var myList = data.querySet
                                            for (y in myList) {
                                                if (state.page == 1) {
                                                    if (myList[y].no != "") {
                                                        //Keep in mind we are using "Template Litterals to create rows"
                                                        var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                            '<td class="text-wrap text-break align-top">' + myList[y].hash + '</span></td>' +
                                                            `<td class="align-top"><a href=/${Sub}log/download/` + myList[y].file_id + '>ดาวน์โหลด</a></span></td>' +
                                                            '</tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                    } else {
                                                        var row = '<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                        table.append(row)
                                                    }
                                                } else {
                                                    if (myList[y].no != "") {
                                                        //Keep in mind we are using "Template Litterals to create rows"
                                                        var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                            '<td class="text-wrap text-break align-top">' + myList[y].hash + '</span></td>' +
                                                            `<td class="align-top"><a href=/${Sub}log/download/` + myList[y].file_id + '>ดาวน์โหลด</a></span></td>' +
                                                            '</tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                    } else {
                                                        var row = '<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                        table.append(row)
                                                    }
                                                }
                                            }
                                            $('#end').html(end_count)
                                            if (myList.length > 0) {
                                                if (myList[0].no != "") {
                                                    $('#start').html(myList[0].no)
                                                } else {
                                                    ß
                                                    $('#start').html(0)
                                                }
                                            }
                                            $('#total-db').html(result.sum);
                                            pageButtons(data.pages)
                                            // end_count = table.find('tr').length
                                            $('button#button_delete_db').on('click', function () {
                                                document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                                                document.getElementById('form_delete_db').submit();
                                            })
                                        }
                                    } else {
                                        $('#table-body').empty()
                                        var table = $('#table-body')
                                        var row = '<tr>' +
                                            '<td class="text-center" colspan="7" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                                            '</tr>'
                                        table.append(row)
                                        end_count = 0
                                        $('#end').html(end_count)
                                        $('#start').html(0)
                                        pageButtons(data.pages)
                                    }
                                }
                            });
                        })
                    }
                    // ============================== Create Table ============================
                    function buildTable() {
                        var table = $('#table-body')
                        var data = pagination(state.querySet, state.page, state.rows, result.button)
                        var myList = data.querySet
                        for (y in myList) {
                            if (state.page == 1) {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].hash + '</span></td>' +
                                        `<td class="align-top"><a href=/${Sub}log/download/` + myList[y].file_id + '>ดาวน์โหลด</a></span></td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            } else {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].hash + '</span></td>' +
                                        `<td class="align-top"><a href=/${Sub}log/download/` + myList[y].file_id + '>ดาวน์โหลด</a></span></td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            }
                        }
                        $('#end').html(end_count)
                        if (myList.length > 0) {
                            if (myList[0].no != "") {
                                $('#start').html(myList[0].no)
                            } else {
                                ß
                                $('#start').html(0)
                            }
                        }
                        $('#total-db').html(result.sum);
                        pageButtons(data.pages)
                        // end_count = table.find('tr').length
                        $('button#button_delete_db').on('click', function () {
                            document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                            document.getElementById('form_delete_db').submit();
                        })
                    }
                } else {
                    $('#table-body').empty()
                    var table = $('#table-body')
                    var row = '<tr>' +
                        '<td class="text-center" colspan="7" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                        '</tr>'
                    table.append(row)
                    end_count = 0
                    $('#end').html(end_count)
                    $('#start').html(0)
                }
            },
            error: function (e) {
                console.log(e);
                end_count = 0
                $('#end').html(end_count)
                $('#start').html(0)
            }
        });
        document.getElementById('button-search').addEventListener('click', () => {
            if ($('.devices').val() == "") {
                var device_data = [ 'sum' ];
            }else{
                var device_data = $('.devices').val();
            }
            console.log(device_data);
            $.ajax({
                method: "POST",
                url: `/${Sub}file/ajaxfilesearch`,
                data: { date1: $('#date1').val(), date2: $('#date2').val(), devices: device_data, search: $('#search').val(), page: 1 },
                dataType: 'json',
                success: function (result) {
                    if (result.data != "ไม่มีข้อมูล") {
                        $('#pagination-wapper').show()
                        // ============================== Create Prepare ============================
                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                        var total = 0 // จำนวนทั้งหมดของข้อมูล
                        var state = {
                            'querySet': result.data,
                            'page': result.page,
                            'rows': result.rows,
                            'window': 5,
                        }
                        $('#table-body').empty()
                        buildTable()
                        function pagination(querySet, page, rows, button) {
                           
                            var pages = button;
                            var start_count = 1
                            document.getElementById('start').innerHTML = start_count
                            return {
                                'querySet': querySet,
                                'pages': pages,
                            }
                        }
                        // ============================== Create Pagination ============================
                        function pageButtons(pages) {
                            var wrapper = document.getElementById('pagination-wapper')
                            wrapper.innerHTML = ``
                            var maxLeft = (state.page - Math.floor(state.window / 2))
                            var maxRight = (state.page + Math.floor(state.window / 2))
                            if (maxLeft < 1) {
                                maxLeft = 1
                                maxRight = state.window
                            }
                            if (maxRight > pages) {
                                maxLeft = pages - (state.window - 1)
                                if (maxLeft < 1) {
                                    maxLeft = 1
                                }
                                maxRight = pages
                            }
                            for (var page = maxLeft; page <= maxRight; page++) {
                                wrapper.innerHTML += `<li class="page-item"><button class="page page-link" id='page_values' value='${page}'>${page}</button></li>`
                            }
                            if (state.page == 1) {
                                wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" >ย้อนกลับ</button></li>` + wrapper.innerHTML
                                wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link">หน้าแรก </button></li>` + wrapper.innerHTML
                            } else {
                                wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>` + wrapper.innerHTML
                                wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link"> หน้าแรก </button></li>` + wrapper.innerHTML

                            }

                            if (state.page != pages) {
                                wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                                wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" > หน้าสุดท้าย</button>`
                            } else {
                                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link"> หน้าสุดท้าย </button>`
                            }
                            $('.page').on('click', function () {
                                $('#table-body').empty()
                                state.page = Number($(this).val())
                                $.ajax({
                                    method: "POST",
                                    url:`/${Sub}file/ajaxfilesearch`,
                                    data: { page: state.page, date1: $('#date1').val(), date2: $('#date2').val(), devices: $('.devices').val() ,search: $('#search').val()},
                                    success: function (result) {
                                        if (result.data != "ไม่มีข้อมูล") {
                                            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                                            var total = 0 // จำนวนทั้งหมดของข้อมูล
                                            var state = {
                                                'querySet': result.data,
                                                'page': result.page,
                                                'rows': result.rows,
                                                'window': 5,
                                            }
                                            $('#table-body').empty()
                                            buildTable()
                                            function buildTable() {
                                                var table = $('#table-body')
                                                var data = pagination(state.querySet, state.page, state.rows, result.button)
                                                var myList = data.querySet
                                                for (y in myList) {
                                                    if (state.page == 1) {
                                                        if (myList[y].no != "") {
                                                            //Keep in mind we are using "Template Litterals to create rows"
                                                            var row = '<tr>' +
                                                                '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                                '<td class="text-wrap text-break align-top">' + myList[y].hash + '</span></td>' +
                                                                `<td class="align-top"><a href=/${Sub}log/download/` + myList[y].file_id + '>ดาวน์โหลด</a></span></td>' +
                                                                '</tr>'
                                                            table.append(row)
                                                            end_count = myList[y].no
                                                        } else {
                                                            var row = '<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                            table.append(row)
                                                        }
                                                    } else {
                                                        if (myList[y].no != "") {
                                                            //Keep in mind we are using "Template Litterals to create rows"
                                                            var row = '<tr>' +
                                                                '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                                                '<td class="text-wrap text-break align-top">' + myList[y].hash + '</span></td>' +
                                                                `<td class="align-top"><a href=/${Sub}log/download/` + myList[y].file_id + '>ดาวน์โหลด</a></span></td>' +
                                                                '</tr>'
                                                            table.append(row)
                                                            end_count = myList[y].no
                                                        } else {
                                                            var row = '<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                            table.append(row)
                                                        }
                                                    }
                                                }
                                                $('#end').html(end_count)
                                                if (myList.length > 0) {
                                                    if (myList[0].no != "") {
                                                        $('#start').html(myList[0].no)
                                                    } else {
                                                        ß
                                                        $('#start').html(0)
                                                    }
                                                }
                                                $('#total-db').html(result.sum);
                                                pageButtons(data.pages)
                                                // end_count = table.find('tr').length
                                                $('button#button_delete_db').on('click', function () {
                                                    document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                                                    document.getElementById('form_delete_db').submit();
                                                })
                                            }
                                        } else {
                                            data_values = '';
                                            csvFileData = '';
                                            $('#table-body').empty()
                                            var table = $('#table-body')
                                            var row = '<tr>' +
                                                '<td class="text-center" colspan="7" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                                                '</tr>'
                                            table.append(row)
                                            end_count = 0
                                            $('#end').html(end_count)
                                            $('#start').html(0)
                                        }
                                    }
                                });
                            })
                        }
                        // ============================== Create Table ============================
                        function buildTable() {
                            var table = $('#table-body')
                            var data = pagination(state.querySet, state.page, state.rows, result.button)
                            var myList = data.querySet
                            for (y in myList) {
                                if (state.page == 1) {
                                    if (myList[y].no != "") {
                                        //Keep in mind we are using "Template Litterals to create rows"
                                        var row = '<tr>' +
                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                            '<td class="text-wrap text-break align-top">' + myList[y].hash + '</span></td>' +
                                            `<td class="align-top"><a href=/${Sub}log/download/` + myList[y].file_id + '>ดาวน์โหลด</a></span></td>' +
                                            '</tr>'
                                        table.append(row)
                                        end_count = myList[y].no
                                    } else {
                                        var row = '<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                        table.append(row)
                                    }
                                } else {
                                    if (myList[y].no != "") {
                                        //Keep in mind we are using "Template Litterals to create rows"
                                        var row = '<tr>' +
                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                            '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                            '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                            '<td class="text-wrap text-break align-top">' + myList[y].hash + '</span></td>' +
                                            `<td class="align-top"><a href=/${Sub}log/download/` + myList[y].file_id + '>ดาวน์โหลด</a></span></td>' +
                                            '</tr>'
                                        table.append(row)
                                        end_count = myList[y].no
                                    } else {
                                        var row = '<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                        table.append(row)
                                    }
                                }
                            }
                            $('#end').html(end_count);
                            if (myList.length > 0) {
                                if (myList[0].no != "") {
                                    $('#start').html(myList[0].no)
                                } else {
                                    ß
                                    $('#start').html(0)
                                }
                            }
                            $('#total-db').html(result.sum);
                            pageButtons(data.pages)
                            // end_count = table.find('tr').length
                            $('button#button_delete_db').on('click', function () {
                                document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                                document.getElementById('form_delete_db').submit();
                            })
                        }
                    } else {
                        data_values = '';
                        csvFileData = '';
                        $('#table-body').empty()
                        var table = $('#table-body')
                        var row = '<tr>' +
                            '<td class="text-center" colspan="7" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                            '</tr>'
                        table.append(row)
                        end_count = 0
                        $('#end').html(end_count)
                        $('#start').html(0)
                        $('#total-db').html(0);
                        $('#pagination-wapper').hide()
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });
    };
    //

    if (document.getElementById('ajex_access_history')) {
        $.ajax({
            method: "POST",
            url: `/${Sub}access_history`,
            data: { page: 1 },
            success: function (result) {
                if (result != "ไม่มีข้อมูล") {
                    $('#table-body').empty()
                    data_values = result.history;
                    accessFileData = result.history;
                    // ============================== Create Prepare ============================
                    var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                    var total = 0 // จำนวนทั้งหมดของข้อมูล
                    var state = {
                        'querySet': result.history,
                        'page': result.page,
                        'rows': result.rows,
                        'window': 5,
                    }
                    buildTable()
                    function pagination(querySet, page, rows, button) {
                        var pages = button;
                        var start_count = 1
                        document.getElementById('start').innerHTML = start_count
                        return {
                            'querySet': querySet,
                            'pages': pages,
                        }
                    }
                    // ============================== Create Pagination ============================
                    function pageButtons(pages) {
                        var wrapper = document.getElementById('pagination-wapper')
                        wrapper.innerHTML = ``
                        // console.log('Pages:', pages)

                        var maxLeft = (state.page - Math.floor(state.window / 2))
                        var maxRight = (state.page + Math.floor(state.window / 2))

                        if (maxLeft < 1) {
                            maxLeft = 1
                            maxRight = state.window
                        }

                        if (maxRight > pages) {
                            maxLeft = pages - (state.window - 1)

                            if (maxLeft < 1) {
                                maxLeft = 1
                            }
                            maxRight = pages
                        }
                        for (var page = maxLeft; page <= maxRight; page++) {
                            wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
                        }

                        if (state.page == 1) {
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" >ย้อนกลับ</button></li>` + wrapper.innerHTML
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link">หน้าแรก </button></li>` + wrapper.innerHTML
                        } else {
                            wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>` + wrapper.innerHTML
                            wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link"> หน้าแรก </button></li>` + wrapper.innerHTML

                        }

                        if (state.page != pages) {
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" > หน้าสุดท้าย</button>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link"> หน้าสุดท้าย</button>`
                        }

                        $('.page').on('click', function () {
                            $('#table-body').empty()
                            state.page = Number($(this).val())
                            $.ajax({
                                method: "POST",
                                url: `/${Sub}access_history`,
                                data: { page: state.page },
                                success: function (result) {
                                    if (result.data != "ไม่มีข้อมูล") {
                                        data_values = result.history;
                                        accessFileData = result.history;
                                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                                        var total = 0 // จำนวนทั้งหมดของข้อมูล
                                        var state = {
                                            'querySet': result.history,
                                            'page': result.page,
                                            'rows': result.rows,
                                            'window': 5,
                                        }
                                        $('#table-body').empty()
                                        buildTable()
                                        function buildTable() {
                                            if (result.system[0].view_hash_access_history == 'sha256') {
                                                var access_type = 'SHA-256'
                                            } else if (result.system[0].view_hash_access_history == 'sha1') {
                                                var access_type = 'SHA-1'
                                            } else if (result.system[0].view_hash_access_history == 'md5') {
                                                var access_type = 'MD5'
                                            }
                                            var table = $('#table-body')
                                            var data = pagination(state.querySet, state.page, state.rows, result.button)
                                            var myList = data.querySet
                                            for (y in myList) {
                                                if (state.page == 1) {
                                                    if (myList[y].no != "") {
                                                        //Keep in mind we are using "Template Litterals to create rows"
                                                        var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].type + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                                            '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                    } else {
                                                        var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                        table.append(row)
                                                    }
                                                } else {
                                                    if (myList[y].no != "") {
                                                        //Keep in mind we are using "Template Litterals to create rows"
                                                        var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].type + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                                            '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                    } else {
                                                        var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                        table.append(row)
                                                    }
                                                }

                                            }
                                            $('#end').html(end_count)
                                            if (myList.length > 0) {
                                                if (myList[0].no != "") {
                                                    $('#start').html(myList[0].no)
                                                } else {
                                                    $('#start').html(0)

                                                }
                                            }
                                            $('#total-db').html(result.history_sum.length);
                                            pageButtons(data.pages)
                                            // end_count = table.find('tr').length
                                            $('button#button_delete_db').on('click', function () {
                                                document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                                                document.getElementById('form_delete_db').submit();
                                            })
                                        }
                                    } else {
                                        $('#table-body').empty()
                                        var table = $('#table-body')
                                        var row = '<tr>' +
                                            '<td class="text-center" colspan="7" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                                            '</tr>'
                                        table.append(row)
                                        end_count = 0
                                        $('#end').html(end_count)
                                        $('#start').html(0)
                                        $('#total-db').html(0)
                                    }
                                }
                            });
                        })

                    }
                    // ============================== Create Table ============================
                    function buildTable() {
                        if (result.system[0].view_hash_access_history == 'sha256') {
                            var access_type = 'SHA-256'
                        } else if (result.system[0].view_hash_access_history == 'sha1') {
                            var access_type = 'SHA-1'
                        } else if (result.system[0].view_hash_access_history == 'md5') {
                            var access_type = 'MD5'
                        }
                        var table = $('#table-body')
                        var data = pagination(state.querySet, state.page, state.rows, result.button)
                        var myList = data.querySet
                        for (y in myList) {
                            if (state.page == 1) {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].type + '</span></td>' +
                                        '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                        '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            } else {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].type + '</span></td>' +
                                        '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                        '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            }

                        }
                        $('#end').html(end_count)
                        if (myList.length > 0) {
                            if (myList[0].no != "") {
                                $('#start').html(myList[0].no)
                            } else {
                                $('#start').html(0)
                            }
                        }
                        $('#total-db').html(result.history_sum.length);
                        pageButtons(data.pages)
                        // end_count = table.find('tr').length
                        $('button#button_delete_db').on('click', function () {
                            document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                            document.getElementById('form_delete_db').submit();
                        })
                    }
                } else {
                    $('#table-body').empty()
                    var table = $('#table-body')
                    var row = '<tr>' +
                        '<td class="text-center" colspan="5" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                        '</tr>'
                    table.append(row)
                    end_count = 0
                    $('#end').html(end_count)
                    $('#start').html(0)
                    $('#total-db').html(0)
                }
            }
        })


        document.getElementById('button-search').addEventListener('click', () => {
            console.log('data2');
            var data = { "date1": $('#date1').val(), "date2": $('#date2').val(), "users": $('.users').val(), "type": $('#type').val(), "search": $('#search').val(), page: 1 };
            $.ajax({ // Srearch get api date
                type: "post",
                contentType: "application/json",
                url: `/${Sub}api/history/search`,
                data: JSON.stringify(data),
                dataType: 'json',
                success: async function (result) {
                    if (result != "ไม่มีข้อมูล") {
                        $('#table-body').empty()
                        $('#pagination-wapper').show()
                        // ============================== Create Prepare ============================
                        data_values = result.history_sum;
                        accessFileData = result.history_sum;
                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                        var total = 0 // จำนวนทั้งหมดของข้อมูล
                        var state = {
                            'querySet': result.history,
                            'page': result.page,
                            'rows': result.rows,
                            'window': 5,
                        }
                        buildTable()
                        function pagination(querySet, page, rows, button) {
                            var pages = button;
                            var start_count = 1
                            document.getElementById('start').innerHTML = start_count
                            return {
                                'querySet': querySet,
                                'pages': pages,
                            }
                        }
                        // ============================== Create Pagination ============================
                        function pageButtons(pages) {
                            var wrapper = document.getElementById('pagination-wapper')
                            wrapper.innerHTML = ``

                            var maxLeft = (state.page - Math.floor(state.window / 2))
                            var maxRight = (state.page + Math.floor(state.window / 2))

                            if (maxLeft < 1) {
                                maxLeft = 1
                                maxRight = state.window
                            }

                            if (maxRight > pages) {
                                maxLeft = pages - (state.window - 1)

                                if (maxLeft < 1) {
                                    maxLeft = 1
                                }
                                maxRight = pages
                            }
                            for (var page = maxLeft; page <= maxRight; page++) {
                                wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
                            }

                            if (state.page == 1) {
                                wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" >ย้อนกลับ</button></li>` + wrapper.innerHTML
                                wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link">หน้าแรก </button></li>` + wrapper.innerHTML
                            } else {
                                wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>` + wrapper.innerHTML
                                wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link"> หน้าแรก </button></li>` + wrapper.innerHTML

                            }

                            if (state.page != pages) {
                                wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                                wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" > หน้าสุดท้าย</button>`
                            } else {
                                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link"> หน้าสุดท้าย ></button>`
                            }

                            $('.page').on('click', function () {
                                $('#table-body').empty()
                                state.page = Number($(this).val())
                                $.ajax({
                                    method: "POST",
                                    url: `/${Sub}api/history/search`,
                                    data: { date1: $('#date1').val(), date2: $('#date2').val(), users: $('.users').val(), type: $('#type').val(), search: $('#search').val(), page: state.page },
                                    dataType: 'json',
                                    success: function (result) {
                                        if (result != "ไม่มีข้อมูล") {
                                            $('#pagination-wapper').show()
                                            data_values = result.history;
                                            accessFileData = result.history;
                                            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                                            var total = 0 // จำนวนทั้งหมดของข้อมูล
                                            var state = {
                                                'querySet': result.history,
                                                'page': result.page,
                                                'rows': result.rows,
                                                'window': 5,
                                            }
                                            $('#table-body').empty()
                                            buildTable()
                                            function buildTable() {
                                                if (system[0].view_hash_access_history == 'sha256') {
                                                    var access_type = 'SHA-256'
                                                } else if (system[0].view_hash_access_history == 'sha1') {
                                                    var access_type = 'SHA-1'
                                                } else if (system[0].view_hash_access_history == 'md5') {
                                                    var access_type = 'MD5'
                                                }
                                                var table = $('#table-body')
                                                var data = pagination(state.querySet, state.page, state.rows, result.button)
                                                var myList = data.querySet
                                                for (y in myList) {
                                                    if (state.page == 1) {
                                                        if (myList[y].no != "") {
                                                            //Keep in mind we are using "Template Litterals to create rows"
                                                            var row = '<tr>' +
                                                                '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].type + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                                                '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                                            table.append(row)
                                                            end_count = myList[y].no
                                                        } else {
                                                            var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                            table.append(row)
                                                        }
                                                    } else {
                                                        if (myList[y].no != "") {
                                                            //Keep in mind we are using "Template Litterals to create rows"
                                                            var row = '<tr>' +
                                                                '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].type + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                                                '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                                            table.append(row)
                                                            end_count = myList[y].no
                                                        } else {
                                                            var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                            table.append(row)
                                                        }
                                                    }

                                                }
                                                $('#end').html(end_count)
                                                if (myList.length > 0) {
                                                    if (myList[0].no != "") {
                                                        $('#start').html(myList[0].no)
                                                    } else {
                                                        $('#start').html(0)

                                                    }
                                                }
                                                $('#total-db').html(result.history_sum.length);
                                                pageButtons(data.pages)
                                                // end_count = table.find('tr').length
                                                $('button#button_delete_db').on('click', function () {
                                                    document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                                                    document.getElementById('form_delete_db').submit();
                                                })
                                            }
                                        } else {
                                            data_values = '';
                                            accessFileData = '';
                                            $('#table-body').empty()
                                            var table = $('#table-body')
                                            var row = '<tr>' +
                                                '<td class="text-center" colspan="7" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                                                '</tr>'
                                            table.append(row)
                                            end_count = 0
                                            $('#end').html(end_count)
                                            $('#start').html(0)
                                            $('#total-db').html(0)
                                        }
                                    }
                                });
                            });
                        }
                        // ============================== Create Table ============================
                        function buildTable() {
                            if (result.system[0].view_hash_access_history == 'sha256') {
                                var access_type = 'SHA-256'
                            } else if (result.system[0].view_hash_access_history == 'sha1') {
                                var access_type = 'SHA-1'
                            } else if (result.system[0].view_hash_access_history == 'md5') {
                                var access_type = 'MD5'
                            }
                            var table = $('#table-body')
                            var data = pagination(state.querySet, state.page, state.rows, result.button)
                            var myList = data.querySet
                            for (y in myList) {
                                if (state.page == 1) {
                                    if (myList[y].no != "") {
                                        //Keep in mind we are using "Template Litterals to create rows"
                                        var row = '<tr>' +
                                            '<td class="title align-top">' + myList[y].no + '</td>' +
                                            '<td class="align-top">' + myList[y].date + '</td>' +
                                            '<td class="align-top">' + myList[y].name + '</td>' +
                                            '<td class="align-top">' + myList[y].type + '</td>' +
                                            '<td class="align-top">' + myList[y].msg + '</td>' +
                                            '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                        table.append(row)
                                        end_count = myList[y].no
                                    } else {
                                        var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                        table.append(row)
                                    }
                                } else {
                                    if (myList[y].no != "") {
                                        //Keep in mind we are using "Template Litterals to create rows"
                                        var row = '<tr>' +
                                            '<td class="title align-top">' + myList[y].no + '</td>' +
                                            '<td class="align-top">' + myList[y].date + '</td>' +
                                            '<td class="align-top">' + myList[y].name + '</td>' +
                                            '<td class="align-top">' + myList[y].type + '</td>' +
                                            '<td class="align-top">' + myList[y].msg + '</td>' +
                                            '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                        table.append(row)
                                        end_count = myList[y].no
                                    } else {
                                        var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                        table.append(row)
                                    }
                                }

                            }
                            $('#end').html(end_count)
                            if (myList.length > 0) {
                                if (myList[0].no != "") {
                                    $('#start').html(myList[0].no)
                                } else {
                                    $('#start').html(0)
                                }
                            }
                            $('#total-db').html(result.history_sum.length);
                            pageButtons(data.pages)
                            // end_count = table.find('tr').length
                            $('button#button_delete_db').on('click', function () {
                                document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                                document.getElementById('form_delete_db').submit();
                            })
                        }
                    } else {
                        data_values = '';
                        accessFileData = '';
                        $('#pagination-wapper').hide()
                        $('#table-body').empty()
                        var table = $('#table-body')
                        var row = '<tr>' +
                            '<td class="text-center" colspan="5" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                            '</tr>'
                        table.append(row)
                        $('#end').html(0)
                        $('#start').html(0)
                        $('#total-db').html(0)
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        })
    } //end ajax_history

    if (document.getElementById('ajex_access_compare')) {
        $.ajax({
            method: "POST",
            url: `/${Sub}access_compare`,
            data: { page : 1 },
            success: function (result) {
                if (result != "ไม่มีข้อมูล") {
                    $('#table-body').empty()
                    data_values = result.history_sum;
                    accessFileData = result.history_sum;
                    account = result.account
                    
                    // ============================== Create Prepare ============================
                    var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                    var total = 0 // จำนวนทั้งหมดของข้อมูล
                    var state = {
                        'querySet': result.history,
                        'page': result.page,
                        'rows': result.rows,
                        'window': 5,
                    }
                    buildTable()
                    data_graph(result)
                    function pagination(querySet, page, rows, button) {
                        var pages = button;
                        var start_count = 1
                        document.getElementById('start').innerHTML = start_count
                        return {
                            'querySet': querySet,
                            'pages': pages,
                        }
                    }
                    // ============================== Create Pagination ============================
                    function pageButtons(pages) {
                        var wrapper = document.getElementById('pagination-wapper')
                        wrapper.innerHTML = ``
                        // console.log('Pages:', pages)

                        var maxLeft = (state.page - Math.floor(state.window / 2))
                        var maxRight = (state.page + Math.floor(state.window / 2))

                        if (maxLeft < 1) {
                            maxLeft = 1
                            maxRight = state.window
                        }

                        if (maxRight > pages) {
                            maxLeft = pages - (state.window - 1)

                            if (maxLeft < 1) {
                                maxLeft = 1
                            }
                            maxRight = pages
                        }
                        for (var page = maxLeft; page <= maxRight; page++) {
                            wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
                        }

                        if (state.page == 1) {
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" >ย้อนกลับ</button></li>` + wrapper.innerHTML
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link">หน้าแรก </button></li>` + wrapper.innerHTML
                        } else {
                            wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>` + wrapper.innerHTML
                            wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link"> หน้าแรก </button></li>` + wrapper.innerHTML

                        }

                        if (state.page != pages) {
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" > หน้าสุดท้าย</button>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link"> หน้าสุดท้าย</button>`
                        }

                        $('.page').on('click', function () {
                            $('#table-body').empty()
                            state.page = Number($(this).val())
                            $.ajax({
                                method: "POST",
                                url: `/${Sub}access_compare`,
                                data: { page: state.page },
                                success: function (result) {
                                    if (result.data != "ไม่มีข้อมูล") {
                                        data_values = result.history;
                                        accessFileData = result.history;
                                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                                        var total = 0 // จำนวนทั้งหมดของข้อมูล
                                        var state = {
                                            'querySet': result.history,
                                            'page': result.page,
                                            'rows': result.rows,
                                            'window': 5,
                                        }
                                        $('#table-body').empty()
                                        buildTable()
                                        function buildTable() {
                                            $('#pagination-wapper').show()
                                            if (result.system[0].view_hash_access_history == 'sha256') {
                                                var access_type = 'SHA-256'
                                            } else if (result.system[0].view_hash_access_history == 'sha1') {
                                                var access_type = 'SHA-1'
                                            } else if (result.system[0].view_hash_access_history == 'md5') {
                                                var access_type = 'MD5'
                                            }
                                            var table = $('#table-body')
                                            var data = pagination(state.querySet, state.page, state.rows, result.button)
                                            var myList = data.querySet
                                            for (y in myList) {
                                                if (state.page == 1) {
                                                    if (myList[y].no != "") {
                                                        //Keep in mind we are using "Template Litterals to create rows"
                                                        var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].type + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                                            '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                    } else {
                                                        var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                        table.append(row)
                                                    }
                                                } else {
                                                    if (myList[y].no != "") {
                                                        //Keep in mind we are using "Template Litterals to create rows"
                                                        var row = '<tr>' +
                                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].type + '</span></td>' +
                                                            '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                                            '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                                        table.append(row)
                                                        end_count = myList[y].no
                                                    } else {
                                                        var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                        table.append(row)
                                                    }
                                                }

                                            }
                                            $('#end').html(end_count)
                                            if (myList.length > 0) {
                                                if (myList[0].no != "") {
                                                    $('#start').html(myList[0].no)
                                                } else {
                                                    $('#start').html(0)

                                                }
                                            }
                                            $('#total-db').html(result.history_sum.length);
                                            pageButtons(data.pages)
                                            // end_count = table.find('tr').length
                                            $('button#button_delete_db').on('click', function () {
                                                document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                                                document.getElementById('form_delete_db').submit();
                                            })
                                        }
                                    } else {
                                        $('#table-body').empty()
                                        var table = $('#table-body')
                                        var row = '<tr>' +
                                            '<td class="text-center" colspan="7" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                                            '</tr>'
                                        table.append(row)
                                        end_count = 0
                                        $('#end').html(end_count)
                                        $('#start').html(0)
                                        $('#total-db').html(0)
                                    }
                                }
                            });
                        })

                    }
                    // ============================== Create Table ============================
                    function buildTable() {
                        if (result.system[0].view_hash_access_history == 'sha256') {
                            var access_type = 'SHA-256'
                        } else if (result.system[0].view_hash_access_history == 'sha1') {
                            var access_type = 'SHA-1'
                        } else if (result.system[0].view_hash_access_history == 'md5') {
                            var access_type = 'MD5'
                        }
                        var table = $('#table-body')
                        var data = pagination(state.querySet, state.page, state.rows, result.button)
                        var myList = data.querySet
                        for (y in myList) {
                            if (state.page == 1) {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].type + '</span></td>' +
                                        '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                        '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            } else {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].type + '</span></td>' +
                                        '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                        '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            }

                        }
                        $('#end').html(end_count)
                        if (myList.length > 0) {
                            if (myList[0].no != "") {
                                $('#start').html(myList[0].no)
                            } else {
                                $('#start').html(0)
                            }
                        }
                        $('#total-db').html(result.history_sum.length);
                        pageButtons(data.pages)
                        // end_count = table.find('tr').length
                        $('button#button_delete_db').on('click', function () {
                            document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                            document.getElementById('form_delete_db').submit();
                        })
                    }
                } else {
                    $('#table-body').empty()
                    var table = $('#table-body')
                    var row = '<tr>' +
                        '<td class="text-center" colspan="5" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                        '</tr>'
                    table.append(row)
                    end_count = 0
                    $('#end').html(end_count)
                    $('#start').html(0)
                    $('#total-db').html(0)
                }
            }
        })
        document.getElementById('button-search').addEventListener('click', () => {
            console.log('data3');
            var data = { "date1": $('#date1').val(), "date2": $('#date2').val(), "users": $('.users').val(), "type": $('#type').val(), "search": $('#search').val(), page: 1 };
            $.ajax({
                type: "post",
                contentType: "application/json",
                url: `/${Sub}api/compare/search` ,
                data: JSON.stringify(data),
                dataType: 'json',
                success: async function (result) {
                    if (result != "ไม่มีข้อมูล") {
                        $('#table-body').empty()
                        // ============================== Create Prepare ============================
                        data_values = result.history_sum;
                        accessFileData = result.history_sum;
                        account = result.account
                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                        var total = 0 // จำนวนทั้งหมดของข้อมูล
                        var state = {
                            'querySet': result.history,
                            'page': result.page,
                            'rows': result.rows,
                            'window': 5,
                        }

                        buildTable()
                        data_graph(result)
                        function pagination(querySet, page, rows, button) {
                            var pages = button;
                            var start_count = 1
                            document.getElementById('start').innerHTML = start_count
                            return {
                                'querySet': querySet,
                                'pages': pages,
                            }
                        }
                        // ============================== Create Pagination ============================
                        function pageButtons(pages) {
                            var wrapper = document.getElementById('pagination-wapper')
                            wrapper.innerHTML = ``

                            var maxLeft = (state.page - Math.floor(state.window / 2))
                            var maxRight = (state.page + Math.floor(state.window / 2))

                            if (maxLeft < 1) {
                                maxLeft = 1
                                maxRight = state.window
                            }

                            if (maxRight > pages) {
                                maxLeft = pages - (state.window - 1)

                                if (maxLeft < 1) {
                                    maxLeft = 1
                                }
                                maxRight = pages
                            }
                            for (var page = maxLeft; page <= maxRight; page++) {
                                wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
                            }

                            if (state.page == 1) {
                                wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" >ย้อนกลับ</button></li>` + wrapper.innerHTML
                                wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link">หน้าแรก </button></li>` + wrapper.innerHTML
                            } else {
                                wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>` + wrapper.innerHTML
                                wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link"> หน้าแรก </button></li>` + wrapper.innerHTML

                            }

                            if (state.page != pages) {
                                wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                                wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" > หน้าสุดท้าย</button>`
                            } else {
                                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป</button>`
                                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link"> หน้าสุดท้าย ></button>`
                            }

                            $('.page').on('click', function () {
                                $('#table-body').empty()
                                state.page = Number($(this).val())
                                buildTable()
                                $.ajax({
                                    method: "POST",
                                    url: `/${Sub}api/history/search`,
                                    data: { date1: $('#date1').val(), date2: $('#date2').val(), users: $('.users').val(), type: $('#type').val(), search: $('#search').val(), page: state.page },
                                    dataType: 'json',
                                    success: function (result) {
                                        if (result != "ไม่มีข้อมูล") {
                                            $('#pagination-wapper').show()
                                            data_values = result.history;
                                            accessFileData = result.history;
                                            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                                            var total = 0 // จำนวนทั้งหมดของข้อมูล
                                            var state = {
                                                'querySet': result.history,
                                                'page': result.page,
                                                'rows': result.rows,
                                                'window': 5,
                                            }
                                            $('#table-body').empty()
                                            buildTable()
                                            function buildTable() {
                                                if (result.system[0].view_hash_access_history == 'sha256') {
                                                    var access_type = 'SHA-256'
                                                } else if (result.system[0].view_hash_access_history == 'sha1') {
                                                    var access_type = 'SHA-1'
                                                } else if (result.system[0].view_hash_access_history == 'md5') {
                                                    var access_type = 'MD5'
                                                }
                                                var table = $('#table-body')
                                                var data = pagination(state.querySet, state.page, state.rows, result.button)
                                                var myList = data.querySet
                                                for (y in myList) {
                                                    if (state.page == 1) {
                                                        if (myList[y].no != "") {
                                                            var row = '<tr>' +
                                                                '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].type + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                                                '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                                            table.append(row)
                                                            end_count = myList[y].no
                                                        } else {
                                                            var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                            table.append(row)
                                                        }
                                                    } else {
                                                        if (myList[y].no != "") {
                                                            var row = '<tr>' +
                                                                '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].date + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].name + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].type + '</span></td>' +
                                                                '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                                                '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                                            table.append(row)
                                                            end_count = myList[y].no
                                                        } else {
                                                            var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                                            table.append(row)
                                                        }
                                                    }

                                                }
                                                $('#end').html(end_count)
                                                if (myList.length > 0) {
                                                    if (myList[0].no != "") {
                                                        $('#start').html(myList[0].no)
                                                    } else {
                                                        $('#start').html(0)

                                                    }
                                                }
                                                $('#total-db').html(result.history_sum.length);
                                                pageButtons(data.pages)
                                                $('button#button_delete_db').on('click', function () {
                                                    document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                                                    document.getElementById('form_delete_db').submit();
                                                })
                                            }
                                        } else {
                                            data_values = '';
                                            accessFileData = '';
                                            $('#table-body').empty()
                                            var table = $('#table-body')
                                            var row = '<tr>' +
                                                '<td class="text-center" colspan="7" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                                                '</tr>'
                                            table.append(row)
                                            end_count = 0
                                            $('#end').html(end_count)
                                            $('#start').html(0)
                                            $('#total-db').html(0)
                                        }
                                    }
                                });
                            })

                        }
                        // ============================== Create Table ============================
                        function buildTable() {
                            $('.datagrap').show()
                            $('.tabledata').show()
                            $('#pagination-wapper').show()
                            if (result.system[0].view_hash_access_history == 'sha256') {
                                var access_type = 'SHA-256'
                            } else if (result.system[0].view_hash_access_history == 'sha1') {
                                var access_type = 'SHA-1'
                            } else if (result.system[0].view_hash_access_history == 'md5') {
                                var access_type = 'MD5'
                            }
                            var table = $('#table-body')
                            var data = pagination(state.querySet, state.page, state.rows, result.button)
                            var myList = data.querySet
                            for (y in myList) {
                                if (state.page == 1) {
                                    if (myList[y].no != "") {
                                        var row = '<tr>' +
                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                            '<td class="align-top">' + myList[y].type + '</span></td>' +
                                            '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                            '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                        table.append(row)
                                        end_count = myList[y].no
                                    } else {
                                        var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                        table.append(row)
                                    }
                                } else {
                                    if (myList[y].no != "") {
                                        var row = '<tr>' +
                                            '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                            '<td class="align-top">' + myList[y].date + '</span></td>' +
                                            '<td class="align-top">' + myList[y].name + '</span></td>' +
                                            '<td class="align-top">' + myList[y].type + '</span></td>' +
                                            '<td class="align-top">' + myList[y].msg + '</span></td>' +
                                            '</tr><tr><td colspan="5" style="text-align: center;" >' + access_type + ': ' + myList[y].hash + '</td></tr>'
                                        table.append(row)
                                        end_count = myList[y].no
                                    } else {
                                        var row = '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                        table.append(row)
                                    }
                                }

                            }
                            $('#end').html(end_count)
                            if (myList.length > 0) {
                                if (myList[0].no != "") {
                                    $('#start').html(myList[0].no)
                                } else {
                                    $('#start').html(0)
                                }
                            }
                            $('#total-db').html(result.history_sum.length);
                            pageButtons(data.pages)
                            $('button#button_delete_db').on('click', function () {
                                document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                                document.getElementById('form_delete_db').submit();
                            })
                        }
                    } else {
                        data_values = '';
                        accessFileData = '';
                        $('#pagination-wapper').hide()
                        $('.datagrap').hide()
                        $('.tabledata').show()
                        $('#table-body').empty()
                        var table = $('#table-body')
                        var row = '<tr>' +
                            '<td class="text-center" colspan="5" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                            '</tr>'
                        table.append(row)
                        $('#end').html(0)
                        $('#start').html(0)
                        $('#total-db').html(0)

                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        })
    } //end ajax_compare




    if (document.getElementById('centralizedchack')) {

        $.ajax({
            method: "POST",
            url: `/${Sub}log/check/` + id,
            data: { value: 1 },
            success: function (result) {
                if (result.data.length > 1) {
                    // ============================== Create Prepare ============================
                    var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                    var total = 0 // จำนวนทั้งหมดของข้อมูล

                    var state = {
                        'querySet': result.data,
                        'page': 1,
                        'rows': 10,
                        'window': 5,
                    }





                    buildTable()

                    function pagination(querySet, page, rows) {

                        var trimStart = (page - 1) * rows
                        var trimEnd = trimStart + rows

                        var trimmedData = querySet.slice(trimStart, trimEnd)

                        var pages = Math.ceil(querySet.length / rows);

                        var start_count = 1
                        document.getElementById('start').innerHTML = start_count

                        return {
                            'querySet': trimmedData,
                            'pages': pages,

                        }
                    }
                    // ============================== Create Pagination ============================
                    function pageButtons(pages) {
                        var wrapper = document.getElementById('pagination-wapper')
                        wrapper.innerHTML = ``
                        // console.log('Pages:', pages)

                        var maxLeft = (state.page - Math.floor(state.window / 2))
                        var maxRight = (state.page + Math.floor(state.window / 2))

                        if (maxLeft < 1) {
                            maxLeft = 1
                            maxRight = state.window
                        }

                        if (maxRight > pages) {
                            maxLeft = pages - (state.window - 1)

                            if (maxLeft < 1) {
                                maxLeft = 1
                            }
                            maxRight = pages
                        }
                        for (var page = maxLeft; page <= maxRight; page++) {
                            wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
                        }

                        if (state.page == 1) {
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link" >&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
                        } else {
                            wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link">&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
                        }

                        if (state.page != pages) {
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
                        }

                        $('.page').on('click', function () {
                            $('#table-body').empty()

                            state.page = Number($(this).val())

                            buildTable()
                        })

                    }
                    // ============================== Create Table ============================
                    function buildTable() {
                        var table = $('#table-body')
                        var data = pagination(state.querySet, state.page, state.rows)
                        var myList = data.querySet
                        for (y in myList) {
                            if (state.page == 1) {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            } else {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            }

                        }
                        $('#end').html(end_count)
                        if (myList.length > 0) {
                            if (myList[0].no != "") {
                                $('#start').html(myList[0].no)
                            } else {
                                $('#start').html(0)
                            }
                        }
                        $('#total-db').html(result.data.length);
                        pageButtons(data.pages)
                        // end_count = table.find('tr').length
                        $('button#button_delete_db').on('click', function () {
                            document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                            document.getElementById('form_delete_db').submit();
                        })
                    }
                } else {
                    var table = $('#table-body')
                    var row = '<tr>' +
                        '<td class="text-center" colspan="6" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                        '</tr>'
                    table.append(row)
                    end_count = 0
                    $('#end').html(end_count)
                    $('#start').html(0)
                }
            }
        })

    }


    if (document.getElementById('centralizedsearch')) {

        $.ajax({
            method: "GET",
            url: `/${Sub}log/search/` + id,
            data: { value: 1 },
            success: function (result) {
                if (result.data.length > 1) {
                    // ============================== Create Prepare ============================
                    var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                    var total = 0 // จำนวนทั้งหมดของข้อมูล

                    var state = {
                        'querySet': result.data,
                        'page': 1,
                        'rows': 10,
                        'window': 5,
                    }


                    buildTable()

                    function pagination(querySet, page, rows) {

                        var trimStart = (page - 1) * rows
                        var trimEnd = trimStart + rows

                        var trimmedData = querySet.slice(trimStart, trimEnd)

                        var pages = Math.ceil(querySet.length / rows);

                        var start_count = 1
                        document.getElementById('start').innerHTML = start_count

                        return {
                            'querySet': trimmedData,
                            'pages': pages,

                        }
                    }
                    // ============================== Create Pagination ============================
                    function pageButtons(pages) {
                        var wrapper = document.getElementById('pagination-wapper')
                        wrapper.innerHTML = ``
                        // console.log('Pages:', pages)

                        var maxLeft = (state.page - Math.floor(state.window / 2))
                        var maxRight = (state.page + Math.floor(state.window / 2))

                        if (maxLeft < 1) {
                            maxLeft = 1
                            maxRight = state.window
                        }

                        if (maxRight > pages) {
                            maxLeft = pages - (state.window - 1)

                            if (maxLeft < 1) {
                                maxLeft = 1
                            }
                            maxRight = pages
                        }
                        for (var page = maxLeft; page <= maxRight; page++) {
                            wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
                        }

                        if (state.page == 1) {
                            wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link" >&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
                        } else {
                            wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link">&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
                        }

                        if (state.page != pages) {
                            wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
                        }

                        $('.page').on('click', function () {
                            $('#table-body').empty()

                            state.page = Number($(this).val())

                            buildTable()
                        })

                    }
                    // ============================== Create Table ============================
                    function buildTable() {
                        var table = $('#table-body')
                        var data = pagination(state.querySet, state.page, state.rows)
                        var myList = data.querySet
                        for (y in myList) {
                            if (state.page == 1) {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            } else {
                                if (myList[y].no != "") {
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<td class="title align-top">' + myList[y].no + '</span></td>' +
                                        '<td class="align-top">' + myList[y].ip + '</span></td>' +
                                        '<td class="align-top">' + myList[y].name + '</span></td>' +
                                        '<td class="align-top">' + myList[y].date + '</span></td>' +
                                        '<td class="text-wrap text-break align-top">' + myList[y].msg + '</span></td>' +
                                        '<td class="align-top">' + myList[y].file_name + '</span></td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].no
                                } else {
                                    var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                                    table.append(row)
                                }
                            }

                        }
                        $('#end').html(end_count)
                        if (myList.length > 0) {
                            if (myList[0].no != "") {
                                $('#start').html(myList[0].no)
                            } else {
                                $('#start').html(0)
                            }
                        }
                        $('#total-db').html(result.data.length);
                        pageButtons(data.pages)
                        // end_count = table.find('tr').length
                        $('button#button_delete_db').on('click', function () {
                            document.getElementById('form_delete_db').action = `/${Sub}database_ag/delete` + $(this).attr('data-value')
                            document.getElementById('form_delete_db').submit();
                        })
                    }
                } else {
                    var table = $('#table-body')
                    var row = '<tr>' +
                        '<td class="text-center" colspan="6" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                        '</tr>'
                    table.append(row)
                    end_count = 0
                    $('#end').html(end_count)
                    $('#start').html(0)
                    pageButtons(data.pages)
                }
            }
        })

    }


});