$(function () {
    Sub = document.getElementById('SubDomain').value;
    let date_default = () => {
        const now = new Date();
        const firstDay = new Date(now.getFullYear(), now.getMonth(), 1);
        const lastDay = new Date(now.getFullYear(), now.getMonth() + 1, 0);
        
    }
    function get_data(data) {
        date_default()
        $.ajax({ // ready get api date
            type: 'GET',
            url: `/${Sub}api/report/new_report_all` ,
            success: async function (result) {
                if (result.data_nul == "ไม่มีข้อมูล") {
                    await ready_datanull()
                    icon_close("null")
                } else {
                    icon_close()
                    console.log(result);
                    await Tabeldata_ajax(result)
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    };
    get_data()

    $("#reface").on("click", function (e) {
        location.reload();
    });

    async function ready_datanull() {
        $('#revenue-statistics').empty()
        $('#basic-pie-paper').empty()

        var content = '<table class="tablesaw no-wrap table-bordered table-hover table" id="table_sortable" data-tablesaw-sortable>'
        content += '<thead id="table-thead"> </thead>' + '<tbody id="table-body"></tbody>';
        content += '</table>'
        $('#table_Data-cookie').append(content);
        $('#table_sortable').attr('data-tablesaw-sortable');
        var table = $('#table-body');
        var table_thead = $('#table-thead');

        var thead = ``;
        table_thead.append(thead)
        $('#table-body').empty().append(`
        <tr>
            <td colspan="20" class="text-center" style="border: none;">
                 <b class="text-danger">ไม่พบข้อมูล</b>
             </td>
        </tr>`);

    };


    function icon_close(data) {
        if (data == "null") {
            document.querySelector('.not_allowed').style.cssText = "cursor: not-allowed;"
            document.getElementById('csv').style.cssText = "pointer-events: none;"
            document.getElementById('excel').style.cssText = "pointer-events: none;"
            document.getElementById('pdf').style.cssText = "pointer-events: none;"
        } else {
            document.querySelector('.not_allowed').style.cssText = ""
            document.getElementById('csv').style.cssText = ""
            document.getElementById('excel').style.cssText = ""
            document.getElementById('pdf').style.cssText = ""
        }
    }

    document.getElementById("button-addon2").addEventListener("click", () => {
        var data = ({
            "search_pdpa": document.getElementById('search_list').value,
            "search_data": document.getElementById('Srearch').value
        });
        console.log(data);
        $.ajax({
            type: "post",
            contentType: "application/json",
            url: `/${Sub}api/report/api_new_report_all_search` ,
            data: JSON.stringify(data),
            dataType: 'json',
            success: async function (result) {
                if (result.data_nul == "ไม่มีข้อมูล") {
                    icon_close("null")
                    ready_datanull()
                } else {
                    icon_close()
                    $('#table_sortable').remove()
                    $('#revenue-statistics').empty()
                    await Tabeldata_ajax(result)
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    });


    var csvFileData = []
    var cookiepolicy;
    document.getElementById("csv").addEventListener("click", () => {
        console.log('csv');
        let search_one = document.getElementById("search_list").value 
        let search_two = document.getElementById("Srearch").value 
        $.ajax({
            url: `/${Sub}new_report_all/csv` ,
            method: 'POST',
            data: {
                search_pdpa: search_one,
                search_data: search_two,
            },
        }).done(function (result) {
            $.ajax({
                url: `/UI/assets/form_excel_report_all/Excel_report_alltra.csv`,
                method: 'GET',
                xhrFields: {
                    responseType: 'blob'
                },
                success: function (data) {
                    var a = document.createElement('a');
                    var url = window.URL.createObjectURL(data);
                    a.href = url;
                    a.download = `Excel_report_alltra.csv`;
                    document.body.append(a);
                    a.click();
                    a.remove();
                    window.URL.revokeObjectURL(url);
                }
            });
        })
    });

    document.getElementById("excel").addEventListener("click", () => {
        console.log('excel');
        let search_one = document.getElementById("search_list").value 
        let search_two = document.getElementById("Srearch").value 
        console.log(search_one,search_two);
        $.ajax({
            url: `/${Sub}new_report_all/excel` ,
            method: 'POST',
            data: {
                search_pdpa: search_one,
                search_data: search_two,
            },
        }).done(function (result) {
            $.ajax({
                url: `/UI/assets/form_excel_report_all/Excel_report_alltra.xlsx`,
                method: 'GET',
                xhrFields: {
                    responseType: 'blob'
                },
                success: function (data) {
                    var a = document.createElement('a');
                    var url = window.URL.createObjectURL(data);
                    a.href = url;
                    a.download = `Excel_report_alltra.xlsx`;
                    document.body.append(a);
                    a.click();
                    a.remove();
                    window.URL.revokeObjectURL(url);
                }
            });
        })
    });

    

    async function Tabeldata_ajax(data) {
        // console.log(data.classify);
        var pattern = data.pattern_data
        var data_pattern = [];
        var data_classify = data.classify;
        var cound_data = data.cound_data;
        csvFileData = [];
        for (var i = 0; i < pattern.length; i++) {
            // console.log(pattern[i].classify);
            data_pattern.push({
                "no": (i + 1),
                "pattern_id": pattern[i].pattern.pattern_id,
                "pattern_name": pattern[i].pattern.pattern_name,
                "user": pattern[i].pattern.name,
                "day_pattern_create": pattern[i].pattern.day_pattern_create,
                "list_policy": pattern[i].policy,
                "count_date": pattern[i].pattern.count_date,
                "count_date_end": pattern[i].pattern.count_date_end,
                "end_date": pattern[i].pattern.pattern_set_end_date_total,
                "list_data": pattern[i].pdpa_data.toString().replace(/,/g, ""),
                "list_data_date": pattern[i].pdpa_data_date.toString().replace(/,/g, ""),
                "date_between": pattern[i].pdpa_between.toString().replace(/,/g, ""),
                "day_pattern_start_date": pattern[i].pattern.day_pattern_start_date,
                "pattern_total_date": pattern[i].pattern.pattern_total_date,
                "pattern_processing_base_name": pattern[i].pattern.pattern_processing_base_name
            });

            csvFileData.push({
                "no": (i + 1),
                "pattern_name": pattern[i].pattern.pattern_name,
                "user": pattern[i].pattern.name,
                "day_pattern_create": pattern[i].pattern.day_pattern_create,
                "list_policy": pattern[i].policy,
                "list_data": pattern[i].pdpa_data,
                "list_data_date": pattern[i].pdpa_data_date,
                "day_pattern_start_date": pattern[i].pattern.day_pattern_start_date,
                "pattern_total_date": pattern[i].pattern.pattern_total_date,
                "pattern_processing_base_name": pattern[i].pattern.pattern_processing_base_name
            });
        }

        var state = {
            'querySet': data_pattern,
            'page': 1,
            'rows': 30, // จำนวน row
            'window': 10000, // จำนวนหน้าที่เเสดง
        }

        buildTable()

        function pagination(querySet, page, rows) {
            var trimStart = (page - 1) * rows
            var trimEnd = trimStart + rows
            var trimmedData = querySet.slice(trimStart, trimEnd)
            var pages = Math.ceil(querySet.length / rows); // Math.ceil ปัดทศนิยมขึ้น Math.round ปัดทศนิยมลง
            return {
                'querySet': trimmedData,
                'pages': pages,
            }
        };

        function pageButtons(pages) {
            var wrapper = document.querySelector('.pagination')
            wrapper.innerHTML = ``
            var maxLeft = (state.page - Math.floor(state.window / 2))
            var maxRight = (state.page + Math.floor(state.window / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = state.window
            }
            if (maxRight > pages) {
                maxLeft = pages - (state.window - 1)
                if (maxLeft < 1) {
                    maxLeft = 1
                }
                maxRight = pages
            }

            // เช็คหน้าเเรก (ปุ่มย้อนกลับ)
            var num = 1
            if (maxRight > 5) {
                if (state.page > (maxRight / 2)) {
                    if ((state.page + 1) > (maxRight / 2)) {
                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    }
                }
                for (var page = maxLeft; page <= maxRight; page++) {
                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                        if (page == state.page) {
                            wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                        }
                        else {
                            wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                        }
                    }
                }
                if ((state.page) <= (maxRight / 2)) {
                    mp = maxRight - 1;
                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                }
            }
            else {
                for (var page = maxLeft; page <= maxRight; page++) {
                    if (state.page == page) {
                        wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                    } else {
                        wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                    }
                }
            }

            if (state.page == 1) {
                wrapper.innerHTML = `<li  class="page-item disabled"><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            } else {
                wrapper.innerHTML = `<li value=${state.page - 1} class="page page-item "><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            }


            // เช็คหน้าสุดท้าย (ปุ่มถัดไป)
            if (state.page == pages) {
                wrapper.innerHTML += `<li  class=" page-item disabled"><button class="page-link">ถัดไป</button></li>`
            } else {
                wrapper.innerHTML += `<li value=${state.page + 1} class="page page-item"><button class="page-link">ถัดไป</button></li>`
            }


            $('.page').on('click', function () {
                $('#table-body').empty()
                $('#table-thead').empty()
                $('#table_sortable').remove() // ลบ เพื่อไม่ให้มันสรา้ง row ใน table เปล่าขึ้นมา
                state.page = Number($(this).val())
                buildTable()
            })
        };


        function buildTable() {
            var content = '<table class="tablesaw no-wrap table-bordered table" id="table_sortable" data-tablesaw-sortable>'
            content += '<thead id="table-thead"> </thead>' + '<tbody id="table-body"></tbody>';
            content += '</table>'
            $('#table_Data-cookie').append(content);
            $('#table_sortable').attr('data-tablesaw-sortable');
            var table = $('#table-body');
            var table_thead = $('#table-thead');

            var thead =`
            <tr>
                <td rowspan="2" style="text-align: center;">No</td>
                <td colspan="9" style="text-align: center;">รูปแบบรวบรวมข้อมูลส่วนบุคคล (Pattern)</td>
                <td colspan="6" style="text-align: center;">รูปแบบการประมวลผลข้อมูลส่วนบุคคล (Classification)</td>
                </tr>
                <tr>
                <td>ชื่อรูปแบบ</td>
                <td>ข้อมูลส่วนบุคคล</td>
                <td>ระยะเวลาการจัดเก็บข้อมูล</td>
                <td>จำนวน (วัน)</td>
                <td>ระยะเวลาจัดเก็บตาม Pattern</td>
                <td>จำนวน (วัน)</td>
                <td>ฐานการประมวลผล</td>
                <td>ระยะเวลาสิ้นสุด (วัน)</td>
                <td>วันที่สิ้นสุด</td>
    
                <td>กิจกรรมประมวลผล</td>
                <td>ระยะเวลาการประมวลผล</td>
                <td>จำนวน (วัน)</td>
                <td>ฐานการประมวลผล</td>
                <td>ระยะเวลาสิ้นสุด (วัน)</td>
                <td>วันที่สิ้นสุด</td>
            </tr>
            `;
            table_thead.append(thead)


            var data = pagination(state.querySet, state.page, state.rows)
            var myList = data.querySet
            var myList_class = data_classify;
            var cound_data_sum = cound_data;
            var show = [];
            for (var i in myList) {
                
                gg = '';
                for (let j = 0; j < myList_class.length; j++) {
                    const d = new Date(myList_class[j].classify_create);
                    const e = new Date(myList_class[j].classify_create);

                    const theDay = d.getDate() + myList_class[j].classify_period_process;
                    const theDay_2 = e.getDate() + myList_class[j].classify_period_process + myList_class[j].classify_period_end;
                    d.setDate(theDay);
                    e.setDate(theDay_2);

                    const result1 = d.toLocaleDateString('en-GB');
                    const result2 = e.toLocaleDateString('en-GB');
                    date_class = `${myList_class[j].day_classify_create} - ${result1}`;

                    if (myList_class[j].pattern_id == myList[i].pattern_id) {

                        if (myList_class[j].classify_period_end == null) {
                            var end_date_class = `ไม่กำหนด`;
                        }else{
                            var end_date_class = myList_class[j].classify_period_end;
                        }

                        var text_sum = `
                        <td>${myList_class[j].classify_name}</td>
                        <td>${date_class}</td>
                        <td>${myList_class[j].classify_period_process}</td>
                        <td>${myList_class[j].pattern_processing_base_name}</td>
                        <td>${end_date_class}</td>
                        <td style="color: red;">${result2}</td>
                        </tr>
                        <tr>`
                        
                    gg = gg + text_sum;
                    }
                    
                }
                if (myList[i].end_date == 'ไม่กำหนด') {
                    var end_date = `<td rowspan="${cound_data_sum[i].c}" class="top_text_c" style="color: red;">ไม่กำหนด</td>`
                }else{
                    var end_date = `<td rowspan="${cound_data_sum[i].c}" class="top_text_c"> ${myList[i].end_date} </td>`
                }
                var row = `
                    <tr>
                    <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].no}</td>
                    <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].pattern_name}</td>
                    <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].list_data}</td>
                    <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].list_data_date}</td>
                    <td rowspan="${cound_data_sum[i].c}" class="top_text_c" style="text-align: center;">${myList[i].date_between}</td>
                    <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].day_pattern_start_date} - ${myList[i].count_date}</td>
                    <td rowspan="${cound_data_sum[i].c}" class="top_text_c" style="text-align: center;">${myList[i].pattern_total_date}</td>
                    <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].pattern_processing_base_name}</td>
                    ${end_date}
                    <td rowspan="${cound_data_sum[i].c}" class="top_text_c" style="color: red;">${myList[i].count_date_end}</td>

                    ${gg}

                    `;
                table.append(row)
                show.push(myList[i].no)
            }
            document.querySelector("#show").innerHTML = show[0];  //  แสดงถึง row เเรกของหน้า 
            document.querySelector("#to_show").innerHTML = show[show.length - 1];  //  แสดงถึง row สุดท้ายของหน้า
            document.querySelector("#show_all").innerHTML = state.querySet.length;  //  แสดงถึงจำนวนข้อมูลทั้งหมดของหน้า
            pageButtons(data.pages)
        };

    };

    document.getElementById('pdf').addEventListener('click', () => {
        var data = ({
            "search_pdpa": document.getElementById('search_list').value,
            "search_data": document.getElementById('Srearch').value
        });
        $.ajax({
            type: "post",
            contentType: "application/json",
            url: `/${Sub}api/report/api_new_report_all_search` ,
            data: JSON.stringify(data),
            dataType: 'json',
            success: async function (result) {
                if (result.data_nul == "ไม่มีข้อมูล") {
                    icon_close("null")
                    ready_datanull()
                } else {
                    gen_pdf(result)
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
        
        function gen_pdf(data) {
            console.log(data);
            var content = ""
        var pattern = data.pattern_data;
        var myList_class = data.classify;
        var cound_data_sum = data.cound_data;
        var show = [];
        var data_pattern = [];
        for (var i = 0; i < pattern.length; i++) {
            // console.log(pattern[i].classify);
            data_pattern.push({
                "no": (i + 1),
                "pattern_id": pattern[i].pattern.pattern_id,
                "pattern_name": pattern[i].pattern.pattern_name,
                "user": pattern[i].pattern.name,
                "day_pattern_create": pattern[i].pattern.day_pattern_create,
                "list_policy": pattern[i].policy,
                "count_date": pattern[i].pattern.count_date,
                "count_date_end": pattern[i].pattern.count_date_end,
                "end_date": pattern[i].pattern.pattern_set_end_date_total,
                "list_data": pattern[i].pdpa_data.toString().replace(/,/g, ""),
                "list_data_date": pattern[i].pdpa_data_date.toString().replace(/,/g, ""),
                "date_between": pattern[i].pdpa_between.toString().replace(/,/g, ""),
                "day_pattern_start_date": pattern[i].pattern.day_pattern_start_date,
                "pattern_total_date": pattern[i].pattern.pattern_total_date,
                "pattern_processing_base_name": pattern[i].pattern.pattern_processing_base_name
            });
        }

        
        var myList = data_pattern;
        console.log(myList);
        for (var i in myList) {
            gg = '';
            for (let j = 0; j < myList_class.length; j++) {
                const d = new Date(myList_class[j].classify_create);
                const e = new Date(myList_class[j].classify_create);

                const theDay = d.getDate() + myList_class[j].classify_period_process;
                const theDay_2 = e.getDate() + myList_class[j].classify_period_process + myList_class[j].classify_period_end;
                d.setDate(theDay);
                e.setDate(theDay_2);

                const result1 = d.toLocaleDateString('en-GB');
                const result2 = e.toLocaleDateString('en-GB');
                date_class = `${myList_class[j].day_classify_create} - ${result1}`;

                if (myList_class[j].pattern_id == myList[i].pattern_id) {

                    if (myList_class[j].classify_period_end == null) {
                        var end_date_class = `ไม่กำหนด`;
                    }else{
                        var end_date_class = myList_class[j].classify_period_end;
                    }

                    var text_sum = `
                    <td>${myList_class[j].classify_name}</td>
                    <td>${date_class}</td>
                    <td>${myList_class[j].classify_period_process}</td>
                    <td>${myList_class[j].pattern_processing_base_name}</td>
                    <td>${end_date_class}</td>
                    <td style="color: red;">${result2}</td>
                    </tr>
                    <tr>`
                    
                gg = gg + text_sum;
                }
                
            }
            if (myList[i].end_date == null) {
                var end_date = `<td rowspan="${cound_data_sum[i].c}" class="top_text_c" style="color: red;">ไม่กำหนด</td>`
            }else{
                var end_date = `<td rowspan="${cound_data_sum[i].c}" class="top_text_c"> ${myList[i].end_date} </td>`
            }
            var row = `
                <tr>
                <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].no}</td>
                <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].pattern_name}</td>
                <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].list_data}</td>
                <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].list_data_date}</td>
                <td rowspan="${cound_data_sum[i].c}" class="top_text_c" style="text-align: center;">${myList[i].date_between}</td>
                <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].day_pattern_start_date} - ${myList[i].count_date}</td>
                <td rowspan="${cound_data_sum[i].c}" class="top_text_c" style="text-align: center;">${myList[i].pattern_total_date}</td>
                <td rowspan="${cound_data_sum[i].c}" class="top_text_c">${myList[i].pattern_processing_base_name}</td>
                ${end_date}
                <td rowspan="${cound_data_sum[i].c}" class="top_text_c" style="color: red;">${myList[i].count_date_end}</td>

                ${gg}

                `;
            content += row;
        }
        console.log(content);
        var myWindow = window.open('xxx', '_blank');
        var header_content = `<table style="margin-top: 15px;">
        <thead>
        <tr>
                <td rowspan="2" style="text-align: center;">No</td>
                <td colspan="9" style="text-align: center;">รูปแบบรวบรวมข้อมูลส่วนบุคคล (Pattern)</td>
                <td colspan="6" style="text-align: center;">รูปแบบการประมวลผลข้อมูลส่วนบุคคล (Classification)</td>
                </tr>
                <tr>
                <td>ชื่อรูปแบบ</td>
                <td>ข้อมูลส่วนบุคคล</td>
                <td>ระยะเวลาการจัดเก็บข้อมูล</td>
                <td>จำนวน (วัน)</td>
                <td>ระยะเวลาจัดเก็บตาม Pattern</td>
                <td>จำนวน (วัน)</td>
                <td>ฐานการประมวลผล</td>
                <td>ระยะเวลาสิ้นสุด (วัน)</td>
                <td>วันที่สิ้นสุด</td>
    
                <td>กิจกรรมประมวลผล</td>
                <td>ระยะเวลาการประมวลผล</td>
                <td>จำนวน (วัน)</td>
                <td>ฐานการประมวลผล</td>
                <td>ระยะเวลาสิ้นสุด (วัน)</td>
                <td>วันที่สิ้นสุด</td>
            </tr>
        </thead>
        <tbody>`
        var footer_content = `</tbody></table>`
        myWindow.document.write("รายงานตามระยะเวลา", ` <br>${header_content}${content}${footer_content}`)
        myWindow.document.write(`
            <style>
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }
            td, th {
              border: 1px solid #dddddd;
              text-align: left;
              padding: 8px;
            }
            td {
                font-size:12px;
            }
            </style>
             <script>
              window.print();
             </script>
             `
        );
        }
        
        
        })

});