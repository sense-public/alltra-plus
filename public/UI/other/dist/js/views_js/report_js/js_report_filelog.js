
$(function () {
   // "use strict";
    
    document.getElementById("csv").addEventListener("click", () => {
        if ($('.devices').val() == "") {
            var device_data = [ 'sum' ];
        }else{
            var device_data = $('.devices').val();
        }
        var csvData = []
        var status = []
        $.ajax({
            method: "POST",
            url: `/${Sub}file/filelog_report`,
            data: { date1: $('#date1').val(), date2: $('#date2').val(), devices: device_data, search: $('#search').val(), page: 1 },
            dataType: 'json',
            success: function (result) {
                if (result == "ไม่มีข้อมูล") {
                    alert("ไม่มีข้อมูล");
                }else{
                    csvFileData = result.sum;
                    for (let i = 0; i < csvFileData.length; i++) {
                        csvData.push(
                            [csvFileData[i].no,
                            csvFileData[i].ip,
                            csvFileData[i].name,
                            csvFileData[i].date,
                            csvFileData[i].msg,
                            csvFileData[i].file_name.replaceAll(',', '-'),
                            csvFileData[i].hash,
                            ]
                        )
                    }
            
                    var csv = `ลำดับ,อุปกรณ์,โฮสต์เนม,วันเวลา,ข้อความ,ชื่อไฟล์,ค่าแฮชของไฟล์(SHA-256)\n`;
                    csvData.forEach(function (row) {
                        csv += row.join(',');
                        csv += "\n";
                    });
                    var hiddenElement = document.createElement('a');
                    hiddenElement.href = 'data:text/csv;charset=utf-8,%EF%BB%BF' + encodeURI(csv);
                    hiddenElement.target = '_blank';
                    hiddenElement.download = 'รายงานไฟล์ล็อก.csv';
                    hiddenElement.click();
                }
                
            },
            error: function (e) {
                console.log(e);
            }
        });
        
    });
    document.getElementById("excel").addEventListener("click", () => {
        if ($('.devices').val() == "") {
            var device_data = [ 'sum' ];
        }else{
            var device_data = $('.devices').val();
        }
        var excel = [];
        var type = []
        var status = []
        $.ajax({
            method: "POST",
            url: `/${Sub}file/filelog_report`,
            data: { date1: $('#date1').val(), date2: $('#date2').val(), devices: device_data, search: $('#search').val(), page: 1 },
            dataType: 'json',
            success: function (result) {
                if (result == "ไม่มีข้อมูล") {
                    alert("ไม่มีข้อมูล");
                }else{
                    csvFileData = result.sum;
                    for (let i = 0; i < csvFileData.length; i++) {
                        var object_exce = {
            
                            ลำดับ: csvFileData[i].no,
                            อุปกรณ์: csvFileData[i].ip,
                            โฮสต์เนม: csvFileData[i].name,
                            วันเวลา: csvFileData[i].date,
                            ข้อความ: csvFileData[i].msg,
                            ชื่อไฟล์: csvFileData[i].file_name.replaceAll(',', '-'),
                            ค่าแฮชของไฟล์: csvFileData[i].hash,
                        }
            
                        excel.push(object_exce)
                    }
                    let binaryWS = XLSX.utils.json_to_sheet(excel);
                    var wb = XLSX.utils.book_new()
                    XLSX.utils.book_append_sheet(wb, binaryWS)
                    XLSX.writeFile(wb, 'รายงานไฟล์ล็อก.xlsx');
    
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
        
    });
    document.getElementById('pdf').addEventListener('click', function (event) {
        if ($('.devices').val() == "") {
            var device_data = [ 'sum' ];
        }else{
            var device_data = $('.devices').val();
        }
        $.ajax({
            method: "POST",
            url: `/${Sub}file/filelog_report`,
            data: { date1: $('#date1').val(), date2: $('#date2').val(), devices: device_data, search: $('#search').val(), page: 1 },
            dataType: 'json',
            success: function (result) {
                if (result == "ไม่มีข้อมูล") {
                    alert("ไม่มีข้อมูล");
                }else{
                    csvFileData = result.sum;
                    event.preventDefault();
                    var myWindow = window.open('xxx', '_blank');
                    var header_content =
                        `<table style="margin-top: 15px;">
                        <thead> 
                        <tr>
                            <th>ลำดับ</th>
                            <th>อุปกรณ์</th>
                            <th>โฮสต์เนม</th>
                            <th>วันเวลา</th>
                            <th>ข้อความ</th>
                            <th>ชื่อไฟล์</th>
                            <th>ค่าแฮชของไฟล์(SHA-256)</th>
                        </tr>
                        </thead> 
                        <tbody>`
                    var content = ""
                    for (let i = 0; i < csvFileData.length; i++) {
                        content +=
                            '<tr><td>' + csvFileData[i].no +
                            '</td><td>' + csvFileData[i].ip +
                            '</td><td>' + csvFileData[i].name +
                            '</td><td>' + csvFileData[i].date +
                            '</td><td>' + csvFileData[i].msg +
                            '</td><td>' + csvFileData[i].file_name.replaceAll(',', '-') +
                            '</td><td>' + csvFileData[i].hash
                        '</td></tr>'
                    }
                    var footer_content = `</tbody></table>`
                    myWindow.document.write("รายงานไฟล์ล็อก", header_content + content + footer_content)
        
                    myWindow.document.write(`<style>table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}td, th { border: 1px solid #dddddd;text-align: left;padding: 8px; }td { font-size:12px; } </style> <script>
                    window.print();
                </script> `);
        
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
        
    });



});

