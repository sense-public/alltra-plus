const express = require('express');
const router = express.Router();
const importdata_controllers = require('../controllers/importdata_controllers');
const validator = require('../controllers/validator');


router.get('/import', importdata_controllers.list);
router.get('/import_data_api', importdata_controllers.list2);
router.post('/import_data_api', importdata_controllers.list_optimize);
router.post('/import_data_api_datasearch', importdata_controllers.datasearch);

router.get('/views_import/:id', importdata_controllers.views_import);
router.get('/api_views_import/:id', importdata_controllers.api_views_import);
router.post('/api_views_import', importdata_controllers.api_views_import_search);
router.post('/views_import_list_optimize', importdata_controllers.views_import_list_optimize);

router.post('/import_file', importdata_controllers.import_file);
router.post('/createftp', importdata_controllers.create);
router.post('/deleteftp/:id', importdata_controllers.deleteftp);
router.get('/alert', importdata_controllers.alert);
router.post('/createalert', importdata_controllers.createalert);
router.post('/updatealert/:id', importdata_controllers.updatealert);
router.post('/dellert', importdata_controllers.dellert);

router.get('/form_EX/:id', importdata_controllers.createform_ex);
router.get('/form_CSV/:id', importdata_controllers.createform_csv);
module.exports = router;