const express = require('express');
const router = express.Router();

const report_classification = require('../../controllers/controller_report/controller_report_classification');
router.get('/report/classification', report_classification.report_classification);
router.get('/api/report/classification', report_classification.api_classification);
//new report dev dew
router.get('/report/new_report_all', report_classification.new_report_all);
router.get('/api/report/new_report_all', report_classification.api_new_report_all);
router.post('/new_report_all/excel', report_classification.new_report_all_excel)
router.post('/new_report_all/csv', report_classification.new_report_all_csv)

router.post('/api/report/api_new_report_all_search', report_classification.api_new_report_all_search);


// Search
router.post('/api/search/report/classification', report_classification.api_search_classification_text)
router.post('/api/report/classification/search_date', report_classification.api_search_classification_date);
module.exports = router;