const express = require('express');
const router = express.Router();
const views_controllers = require('../controllers/views_controllers');
const validator = require('../controllers/validator');


router.get('/userviews', views_controllers.list);


module.exports = router;