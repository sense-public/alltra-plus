function addDate() {
  const convert_date = new Date().toLocaleString('th-th', { timeZone: 'Asia/Bangkok' });
  var date_now = convert_date.split("/",);
  var time_now = convert_date.split(" ");
  var year = date_now[2]
  const date = (parseInt(year)-543)+'-'+date_now[1]+'-'+date_now[0]+' '+time_now[1]; 
  return date;
}
module.exports = addDate;
